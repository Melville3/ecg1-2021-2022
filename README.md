# Cours de mathématiques approfondies

Ce dépôt contient le cours, avec les exercices, de mathématiques approfondies enseigné par Antoine Crouzet au lycée Carnot (Paris). 

* Répertoire Doc : des documents (programmes, ...)
* Répertoire scripts et Makefile : outils permettant de créer les fichiers avec mon fichier style
* Répertoire tex : les sources, par chapitre, et répertoires particuliers (pic, script)
* Répertoire PDF : les fichiers compilés (version elève, sans démonstration et correction; version publi, avec démonstration et sans correction, version prof)

Voici la progression théorique utilisée :

# Progression ECG - 2021-2022

## Premier semestre

### Phase1

* Logique et raisonnements : [**Cours**] [**Exo**]
* Propriétés des réels : [**Cours**] [**Exo**]
* Sommes et produits : [**Cours**] [**Exo**]
* Fonctions usuelles : [**Cours**] [**Exo**]
* Suites : [**Cours**] [**Exo**]
* Limites de suites : [**Cours**] [**Exo**]

### Phase 2

* Ensembles et applications : [**Cours**] [**Exo**] 
* Éléments de combinatoire: [**Cours**] [**Exo**]
* Probabilités finies: [**Cours**] [**Exo**] 
* Variables aléatoires finies :  [**Cours**] [**Exo**] 

### Phase 3

* Limites de fonctions : [**Cours**] [**Exo**] 
* Continuité : [**Cours**] [**Exo**]
* Dérivabilité : [**Cours**] [**Exo**]
* Intégration sur un segment : [**Cours**] [**Exo**] 

### Phase 4

* Polynômes : [**Cours**] [**Exo**]  
* Systèmes linéaires : [**Cours**] [**Exo**] 
* Matrices : [**Cours**] [**Exo**] 
* Intro aux espaces vectoriels : [**Cours**] [**Exo**] 

## Second semestre

### Phase 5

* Analyse asymptotique et DL : [**Cours**] [**Exo**]
* Formule de Taylor : [**Cours**] [**Exo**] 
* Développements limités [**Cours**] [**Exo**]
* Séries : [**Cours**] [**Exo**]
* Intégrales généralisées : [**Cours**] [**Exo**]
* Convexité : [Cours] [Exo] _en cours_

### Phase 6

* Applications linéaires : [**Cours**] [Exo] _en cours_

### Phase 7

* Probabilités sur un univers quelconque : [**Cours**] [**Exo**]
* Variables aléatoires discrètes : [**Cours**] [**Exo**]
* Couples de VA : [Cours] [Exo]

### Phase 8

* Sommes d'EV : [Cours] [Exo]
* Dimension finie : [Cours] [Exo]
* Codage matriciel : [Cours] [Exo]

* Convergence et approximation : [Cours] [Exo]
