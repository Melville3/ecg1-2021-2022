\chapter{Limites de fonctions}
%!TeX root=../../encours.nouveau.tex
%%% Début exercice %%%

\objectifintro{On étend dans ce chapitre la notion de limite de suite au cas plus général des limites de fonctions. Cela permettra de commencer les études de fonctions, en étudiant le comportement asymptotique de celles-ci.}

\begin{extrait}{Johann Wolfgang von Goethe (1749 -- 1832). \emph{Sentences en prose}}
Celui qui reconnaît consciemment ses limites est le plus proche de la perfection.
\end{extrait}


\begin{objectifs}
\begin{numerote}
        \item Connaître la définition des limites :
                        \begin{itemize}[label=\textbullet]
                                \item Connaître les limites usuelles et les croissances comparées\dotfill $\Box$
                                \item Savoir utiliser les théorèmes d'addition, multiplication, quotient de limites\dotfill $\Box$
                                \item Savoir calculer la limite d'une composée de fonction\dotfill $\Box$
                                \item Reconnaître les limites liées au taux d'accroissement\dotfill $\Box$
                        \end{itemize}
        \item Savoir lever les indéterminations classiques :
                        \begin{itemize}[label=\textbullet]
                                \item polynômes et fractions rationnelles\dotfill $\Box$
                                \item fonctions avec des radicaux\dotfill $\Box$
                                \item les cas ``$\infty/\infty$'' ou ``$0/0$''\dotfill $\Box$
                        \end{itemize}
        \item Savoir appliquer les théorèmes d'existence de limites (théorème d'encadrement, de comparaison)\dotfill $\Box$
        \item Savoir déterminer le comportement asymptotique d'une fonction (limites, asymptotes)\dotfill $\Box$
\end{numerote}
\end{objectifs}

\section{Quelques définitions}

Avant de commencer ce chapitre, nous allons introduire deux objets que l'on utilisera naturellement : la notion de voisinage, et de restriction d'une fonction.

  \subsection{Voisinage}

\begin{definition}[Voisinage]
Soit $a$ un élément de $\R\cup\{-\infty; +\infty\}$. On dit que $I$ est un \textbf{voisinage} de $a$ dans $\R$ si :
\begin{itemize}
  \item Cas $a\in \R$ : il existe $\eps>0$ tel que $\interoo{{a-\eps} {a+\eps}} \subset I$.
  \item Cas $a=+\infty$ : il existe $A\in \R$ tel que $\interoo{A +\infty} \subset I$.
  \item Cas $a=-\infty$ : il existe $A\in \R$ tel que $\interoo{-\infty{} A} \subset I$.
\end{itemize}
\end{definition}

Ainsi, un voisinage de $a$ est un domaine qui contient un intervalle ouvert autour de $a$.

\begin{exemple}
Notons $I=\interof{1 2}$. Alors $I$ est un voisinage de $1,5$ et  $1,25$. En revanche, il n'est pas un voisinage de $3$, ni de $2$.
\end{exemple}

\solution[5]{En effet, ${\setkeys[TdSM]{tdsfrmath.sty}{nupletsep=pointvirgule}\interoo{1,25 1,75} \subset I}$ et ${\setkeys[TdSM]{tdsfrmath.sty}{nupletsep=pointvirgule}\interoo{1,1  1,35} \subset I}$. En revanche, quel que soit $\eps>0$, $\interoo{3-\eps{} 3+\eps} \cap I =\vide$ donc il ne peut pas être un voisinage de $3$. De même, $\interoo{2-\eps{} 2+\eps} \cap I =\interof{2-\eps{} 2}$ et donc $\interoo{2-\eps{} 2+\eps} \not \subset I$.
}

\begin{remarque}
Par abus de langage, dans la suite du cours, lorsqu'on dit qu'une propriété est vraie \og{}au voisinage de $a$\fg, cela signifiera qu'elle est vraie sur un intervalle qui est un voisinage de $a$.
\end{remarque}

  \subsection{Restriction}

\begin{definition}[Restiction]
Soit $f:E\dans F$ une fonction et $A\subset E$. On appelle \textbf{restriction} de $f$ à $A$ la fonction, notée $\restric{f}{A}$, définie par :
\begin{align*}
  \restric{f}{A} : A &\dans F\\
  x &\donne f(x)
\end{align*}
\end{definition}


\begin{exemple}
Soit $f:\R \to \R$ définie par $f(x)=x^2-1$. La restriction de $f$ à $\R[+]$ est la fonction :
\begin{align*}
  \restric{f}{\R[+]}: \R[+] &\dans \R\\ x&\donne x^2-1
\end{align*}
\end{exemple}

\begin{remarque}
Restreindre une fonction permet de ne l'étudier que sur un sous-ensemble de son domaine de définition (par exemple, parce qu'elle est paire ou impaire).
\end{remarque}

\section{Limites à l'infini}

	\subsection{Limites nulles}

\begin{definition}
Si, pour tout $\eps > 0$ (aussi petit qu'on veut), la fonction $f$ est comprise entre $-\eps$ et $+\eps$ (c'est-à-dire $|f(x)| \leq \eps$) lorsque $x$ est suffisamment grand , on dit que $f$ \textbf{a pour limite $0$} quand $x$ tend vers $+\infty$. On note
\[\lim_{x \rightarrow +\infty} f(x)=0\quad\textrm{ou}\quad\lim_{+\infty} f =0 \quad\textrm{ou encore} \quad f(x)\tendversen{x\to +\infty} 0 \]
\end{definition}


\begin{center}
    \includegraphics[width=8cm]{limitenulle}
\end{center}

\begin{remarque}
Mathématiquement, on écrit donc
\[\forall~\eps>0,~\exists~M,~\forall~x, x>M\Rightarrow |f(x)|<\eps\]
\end{remarque}

\begin{remarque}
On définit de même $\ds{\lim_{x \rightarrow -\infty}f(x) = 0}$	 :
\[ \forall~\eps>0,~\exists~M,~\forall~x, x<M\Rightarrow |f(x)|<\eps.\]
\end{remarque}

\begin{exemple}
$\ds{\lim_{x\rightarrow +\infty} \frac{1}{x}=0}$
\end{exemple}


    \subsection{Limites finies : $\ell \in \R$}

\begin{definition}
Soit $\ell \in \R$. On dit qu'une fonction $f$ \textbf{a pour limite $\ell$} quand $x$ tend vers $+\infty$ (ou $-\infty$) si $f(x)-\ell$ tend vers $0$ quand $x$ tend vers $+\infty$ (ou $-\infty$).
\end{definition}

\begin{center}
	\includegraphics[width=18cm]{limitel}
\end{center}

\begin{remarque}
Mathématiquement, on écrit donc
\[\forall~\eps>0,~\exists~M,~\forall~x, x>M\Rightarrow |f(x)-\ell|<\eps\]
\end{remarque}


\begin{experiencehistorique}Cette définition rigoureuse est due à \textbf{Karl Weierstrass}, considéré comme le \og{}père de l'analyse moderne\fg, même si \textbf{Bernard Bolzano} avait déjà défini la notion de limite, certes de manière moins rigoureuse..
\end{experiencehistorique}

\begin{exemple}
$\ds{\lim_{x\rightarrow +\infty} 2+\frac{1}{x}=2}$
\end{exemple}

\begin{definition}[Asymptote horizontale]
Soit $f$ une fonction telle que $\ds{\lim_{x \rightarrow +\infty} f(x) = \ell}$ (où $\ell \in \R$). Alors, la droite d'équation $y=\ell$ est appelée \textbf{asymptote horizontale} à la courbe de $f$ au voisinage de $+\infty$ (même chose en $-\infty$).
\end{definition}

\begin{center}
\includegraphics[width=7cm]{ashoriz}\\
On dispose ici d'une asymptote horizontale d'équation $y=\ell$.
\end{center}

\begin{remarque}
Pour étudier la position de la courbe de $f$ par rapport à l'asymptote $y=\ell$, on étudie le signe de $f(x)-\ell$.
\begin{itemize}
	\item[$\bullet$] Si $f(x)-\ell \geq 0$, la courbe est au-dessus de son asymptote.
	\item[$\bullet$] Si $f(x)-\ell \leq 0$, la courbe est en dessous de son asymptote.
\end{itemize}
\end{remarque}

\subsection{Limites infinies}

\begin{definition}
 Si, pour tout nombre $A$ (aussi grand qu'on veut),  $f(x)$ est toujours supérieur ou égal à $A$ dès que $x$ est suffisamment grand, on dit que \textbf{$f$ a pour limite $+\infty$} quand $x$ tend vers $+\infty$.

 On note
\[\lim_{x \rightarrow +\infty} f(x)=+\infty\quad\textrm{ou}\quad\lim_{+\infty} f =+\infty\quad\textrm{ou encore}\quad f(x)\tendversen{x\to +\infty} +\infty\]
\end{definition}

\begin{center}
    \includegraphics[width=6cm]{limiteinfinie}
\end{center}

\begin{remarque}
Mathématiquement, on écrit donc
\[\forall~A>0,~\exists~M,~\forall~x, x>M\Rightarrow f(x)>A\]
\end{remarque}

\begin{remarque}
On définit de même $\ds{\lim_{x \rightarrow -\infty} f(x)=+\infty}$, $\ds{\lim_{x \rightarrow +\infty} f(x)=-\infty}$ et $\ds{\lim_{x \rightarrow -\infty} f(x)=-\infty}$
\end{remarque}

\section{Limites en $a \in \R$}

Ici, on s'intéresse au comportement d'une fonction $f$ quand $x$ tend vers $a \in \R$.

    \subsection{Limite réelle en un point}

\begin{definition}
Soient $x_0$ et $\ell$ deux réels.\\
Si $f(x)$ est aussi proche que l'on veut de $\ell$ dès lors que $x$ est proche de $x_0$, on dit que $f$ \textbf{a pour limite $\ell$} quand $x$ tend vers $x_0$. On note
 \[\lim_{x \rightarrow x_0} f(x)=\ell\quad\textrm{ou}\quad\lim_{x_0} f =\ell\quad\text{ou encore} \quad f(x)\tendversen{x\to x_0} \ell\]
\end{definition}

\begin{center}
    \includegraphics[width=8cm]{limiteenreel}
\end{center}

\begin{remarque}
Mathématiquement, on écrit donc
\[\forall~\eps>0,~\exists~\alpha>0,~\forall~x, |x-x_0|<\alpha \Rightarrow |f(x)-\ell|<\eps\]
\end{remarque}

\begin{proposition}
Il y a unicité de la limite. On peut donc bien parler de la limite en $x_0$.
\end{proposition}

\preuve[7]{Supposons que $f$ admette deux limites $\ell$ et $\ell'$ en $x_0$ et on suppose, par l'absurde, que $\ell\neq \ell'$. On pose alors $\eps=\frac{|\ell-\ell'|}{4}>0$. Il existe $\alpha>0$ et $\alpha'>0$ tels que, pour tout $x\in I$, \[ |x-x_0|\leq \alpha \implies |f(x)-\ell|<\eps \qeq |x-x_0|\leq \alpha'\implies |f(x)-\ell'|<\eps. \]
On prend $y\in I$ tel que $|y-y_0l\leq \min(\alpha, \alpha')$. Nous avons alors
\begin{align*}
  |\ell-\ell'| &= |(\ell-f(y)) + (f(y)-\ell')| \\
  &\leq |\ell-f(y)| + |\ell'-f(y)| \leq 2\eps.
\end{align*}
Ainsi, $|\ell-\ell'| \leq \frac{|\ell-\ell'}{2}$ : c'est absurde.
}

\begin{exemple}
Soit $f$ la fonction définie sur $\R$ par $f(x)=2x+1$. Soit $x_0=2$. Montrer que, quand $x$ tend vers $x_0$, $f(x)$ va tendre vers $5=f(2)$.
\end{exemple}

\solution[5]{
 On peut le démontrer rigoureusement :
\[|f(x)-5|=|2x+1-5|=|2x-4|=2|x-2|\]
Soit $\eps>0$ fixé. Alors $|f(x)-5|<\eps \Leftrightarrow 2|x-2|<\eps \Leftrightarrow |x-2|<\frac{\eps}{2}$. On pose alors $\alpha=\frac{\eps}{2}$.}

    \subsection{Limite infinie en un point}

\begin{definition}
Soit $x_0$ un réel.\\
Si $f(x)$ est aussi grand que l'on veut dès lors que $x$ est proche de $x_0$, on dit que $f$ \textbf{a pour limite $+\infty$} quand $x$ tend vers $x_0$. On note
 \[\lim_{x \rightarrow x_0} f(x)=+\infty\quad\textrm{ou}\quad\lim_{x_0} f =+\infty\quad\textrm{ou encore}\quad f(x)\tendversen{x\to x_0} +\infty\]
\end{definition}

\begin{center}
    \includegraphics[width=8cm]{limitereelinfinie}
\end{center}

\begin{remarque}
Mathématiquement, on écrit donc
\[\forall~A>0,~\exists~\alpha>0,~\forall~x, |x-x_0|<\alpha \Rightarrow f(x)>A\]
\end{remarque}

\begin{remarque}
Il y a unicité de la limite. On peut donc bien parler de la limite en $x_0$. \\On définit également $\ds{\lim_{x\rightarrow x_0} f(x)=-\infty}$ de la même manière.
\end{remarque}


\begin{remarque}
  Les définitions données sont correctes, que $x_0$ soit dans le domaine de définition de $f$ ou non. Dans ce dernier cas, on excluera la valeur $x_0$; par exemple, $f:I\to \R$ tend vers $\ell$ en $x_0\notin I$ si
  \[ \forall~\eps>0,\,\exists \alpha>0,\,\forall x \in I,\, x\in I\cap\left( \interoo{{x_0-\alpha} x_0} \cup\interoo{x_0 {x_0+\alpha}} \right)\implies |f(x)-\ell|<\eps.\]
\end{remarque}

    \subsection{Limite à gauche et à droite}

\begin{definition}
Soit $x_0$ un réel.
\begin{itemize}
    \item Si on s'intéresse à la limite en $x_0$ de $f$, en imposant $x<x_0$, on parle de la \textbf{limite à gauche} en $x_0$, et on note $\ds{\lim_{x\rightarrow x_0^-} f(x)}$ ou $\ds{\lim_{\substack{x\rightarrow x_0\\ x<x_0}} f(x)}$.
    \item Si on s'intéresse à la limite en $x_0$ de $f$, en imposant $x>x_0$, on parle de la \textbf{limite à droite} en $x_0$, et on note $\ds{\lim_{x\rightarrow x_0^+} f(x)}$ ou $\ds{\lim_{\substack{x\rightarrow x_0\\x>x_0}}f(x)}$.
\end{itemize}
Mathématiquement, $f$ admet $\ell$ pour limite à gauche en $x_0$ si \[ \forall~\eps>0,\, \exists \alpha>0,\, \forall~x\in I,\, x\in \interoo{{x_0-\delta} x_0} \implies |f(x)-\ell| \leq \eps. \]
\end{definition}

\begin{remarque}
Une fonction peut avoir des limites à gauche et à droite en $x_0$ différentes ! Par exemple
\[\lim_{x\rightarrow 0^-} \frac{1}{x}=-\infty \textrm{  et  } \lim_{x\rightarrow 0^+} \frac{1}{x}=+\infty\]
\end{remarque}

\begin{proposition}
Soit $f$ une fonction et $x_0$ un réel. Alors $f$ admet une limite en $x_0$ si et seulement si $f$ admet des limites à gauche et à droite en $x_0$ et que ces limites sont les mêmes.\\Dans ce cas, \[\lim_{x\rightarrow x_0} f(x)=\lim_{x\rightarrow x_0^-} f(x) = \lim_{x\rightarrow x_0^+} f(x)\]
\end{proposition}

\preuve[6]{Si $f$ admet une limite en $x_0$, par définition elle admet également une limite à droite et à gauche qui sont égales à la limite.

Réciproquement, on suppose que $f$ admet des limites à droite et à gauche en $x_0$ égales à $\ell$. Soit $\eps>0$. Il existe donc $\alpha_d>0$ et $\alpha_g>0$ tels que, pour tout $x\in I$ :
\[ x\in \interoo{{x_0-\alpha_g} x_0} \implies |f(x)-\ell| \leq \eps \qeq x\in \interoo{x_0 {x_0+\alpha_d}} \implies |f(x)-\ell|\leq \eps. \]
En prenant $\alpha=\min(\alpha_d, \alpha_g)$, on a, pour tout $x\in I$ :
\[ x\in \interoo{{x_0-\alpha} x_0}\cup \interoo{x_0 {x_0+\alpha}} \implies |f(x)-\ell| \leq \eps \]
et donc $f(x)\tendversen{x\to x_0} \ell$.
}

\begin{exemple}
Ainsi, la fonction inverse n'admet pas de limite en $0$, puisque ses limites à droite et à gauche en $0$ sont différentes.
\end{exemple}

\begin{methode}
On est souvent amené à étudier des limites à droite et à gauche lorsque la fonction est définie de deux manières différentes selon les intervalles.
\end{methode}

\begin{exemple}
Etudier la limite en $0$ de la fonction $f$ définie sur $\R$ par
\[f(x)=\left\{ \begin{array}{l} 1 \textrm{ si $x\geq 0$}\\
             \E^x \textrm{ si $x<0$}
 \end{array}\right.\]
\end{exemple}

\solution[5]{On remarque que :
\begin{itemize}
    \item[$\bullet$] $\displaystyle{\lim_{x\rightarrow 0^+} f(x)=\lim_{x\rightarrow 0^+} 1= 1=f(0)}$
    \item[$\bullet$] $\displaystyle{\lim_{x\rightarrow 0^-} f(x)=\lim_{x\rightarrow 0^-} \E^x=\E^0=1}$
\end{itemize}
Puisque les limites à droite et à gauche sont égales, on en déduit que $f$ admet une limite en $0$, et $\displaystyle{\lim_{x\rightarrow 0} f(x)=1}$.
}

\begin{definition}[Asymptote verticale]
 Si $\ds{\lim_{x \rightarrow a^+ \textrm{(ou $a^-$)}} f(x) = +\infty \textrm{ ( ou $-\infty$)} }$, on dit que la droite d'équation $x=a$ est une \textbf{asymptote verticale} à la courbe de $f$.
\end{definition}


\begin{center}
\includegraphics[width=7cm]{asvert}
\end{center}


\section{Théorèmes d'existence et de comparaison}

        \subsection{Fonctions monotones}

Les fonctions monotones possèdent des propriétés intéressantes concernant les limites :

\begin{theoreme}
Soit $f$ une fonction monotone sur un segment $\interff{a b}$. Alors $f$ admet des limites à gauche et à droite en tout point.
\end{theoreme}

\begin{theoreme}
Soit $f$ une fonction monotone sur $I=\interoo{a b}$.
\begin{itemize}
    \item Si $f$ est croissante et majorée sur $I$, alors $f$ admet une limite finie en $b^-$.
    \item Si $f$ est croissante et minorée sur $I$, alors $f$ admet une limite finie en $a^+$.
    \item Si $f$ est décroissante et minorée sur $I$, alors $f$ admet une limite finie en $b^-$.
    \item Si $f$ est décroissante et majorée sur $I$, alors $f$ admet une limite finie en $a^+$.
\end{itemize}
\end{theoreme}

\begin{remarque}
Si $f$ est croissante sur $I=\interoo{a b}$ mais non majorée sur $I$, alors $f$ admet une limite en $b^-$ qui vaut $+\infty$.\\De manière générale, une fonction monotone sur $\interoo{a b}$ admet toujours des limites en $a^+$ et en $b^-$, finie ou infinie.
\end{remarque}

        \subsection{Limites et inégalités}

\begin{theoreme}
Soit $I$ un intervalle et $x_0\in I$. Soient $f$ et $g$ deux fonctions définies sur $I$, sauf éventuellement en $x_0$, mais possédant une limite en $x_0$. Alors, si pour tout $x$ de $I\backslash\{x_0\}$, $f(x)\geq g(x)$, on a \[\lim_{x\rightarrow x_0} f(x) \geq \lim_{x\rightarrow x_0} g(x)\]

En particulier, si $f(x)\geq 0$ pour tout $x\neq x_0$ alors $\ds{\lim_{x\rightarrow x_0} f(x)\geq 0}$.
\end{theoreme}

\begin{remarque}\logoattention
Attention : le passage à la limite ne conserve pas les inégalités strictes. Si $f(x)> g(x)$ pour tout $x \neq x_0$ alors $\displaystyle{\lim_{x\rightarrow x_0} f(x) \geq \lim_{x\rightarrow x_0} g(x)}$. Par exemple, pour tout $x$, $1+\frac{1}{x}>1$ mais $\displaystyle{\lim_{x\rightarrow +\infty} 1+\frac{1}{x} \geq 1}$.
\end{remarque}


	\subsection{Théorème d'encadrement}

\begin{theoreme}[Théorème d'encadrement ou théorème \og{}des gendarmes\fg]
Soit $I$ un intervalle et $x_0\in I$. Soient $f,g,h$ trois fonctions définies sur $I$ sauf éventuellement en $x_0$. Si, pour tout $x$ de $I \backslash \{x_0\}$, on a $f(x) \leq g(x) \leq h(x)$ et si $f$ et $h$ ont la même limite $\ell$ en $x_0$, alors la limite de $g$ en $x_0$ existe, et \[\lim_{x\rightarrow x_0} g(x)=\ell\]
\end{theoreme}


\begin{demonstration}
Admis. Idée de démonstration dans le chapitre sur les suites.
\end{demonstration}

\begin{exemple}
Soit $g$ la fonction définie sur $\interoo{0 +\infty}$ par $g(x)=\dfrac{[x]}{x}$. Montrer que $\displaystyle{\lim_{x\rightarrow+\infty} g(x)=1}$.
\end{exemple}


\solution[4]{
 On a, pour tout $x$ de $\interoo{0 +\infty}$, $x-1\leq [x] \leq x$, donc, en divisant par $x>0$,
 \[\frac{x-1}{x} \leq g(x) \leq \frac{x}{x}=1\]
 Puisque $\ds{\lim_{x\rightarrow +\infty} \frac{x-1}{x} =\lim_{x\rightarrow +\infty} 1=1}$, on en déduit donc, par encadrement, que la limite de $g$ en $+\infty$ existe, et
\[\lim_{x\rightarrow +\infty} \frac{[x]}{x}=1\]
}

\begin{theoreme}[Conséquence du théorème d'encadrement]
Soit $I$ un intervalle, $x_0 \in I$ et $\ell$ un réel. Soient $f$ et $g$ deux fonctions définies sur $I$ sauf éventuellement en $x_0$. Si, pour tout réel $x \in I\backslash \{x_0\}$ on a $\ds{|f(x)-\ell|\leq g(x)}$ et si $\ds{\lim_{x\rightarrow x_0} g(x)=0}$ alors $\ds{\lim_{x\rightarrow x_0} f(x)=\ell}$.
\end{theoreme}


\begin{demonstration}
  C'est une conséquence directe du théorème d'encadrement, puisque $|f(x)-\ell|\leq g(x)$ est équivalent à $\ell-g(x)\leq f(x)\leq \ell+g(x)$. Il suffit alors d'appliquer le théorème d'encadrement.
\end{demonstration}

\begin{remarque}
  On en déduit que si $f$ tend vers $0$ en $x_0$, $|f|$ également. En effet, pour tout $x \in I$ :
  \[ -f(x)\leq |f(x)| \leq f(x) \]
  et on procède par encadrement.
\end{remarque}

\begin{proposition}
  Soient $I$ un intervalle de $\R$, $x_0$ un réel (ou l'infini), et $f$ et $g$ deux fonctions définies sur $I$. Si $f$ est bornée sur $I$ (ou sur un voisinage de $x_0$) et si $g$ a pour limite $0$ en $x_0$, alors $f(x)g(x)\tendversen{x\to x_0} 0$.
\end{proposition}

\preuve[4]{$f$ étant bornée, il existe $M$ tel que, pour tout $x\in I$ (ou sur un voisinage de $x_0$, ce qui sera suffisant), on a \[ |f(x)|\leq M \]
et donc \[ |f(x)g(x)| \leq M|g(x)|. \]
Cette dernière limite, par hypothèse, tend vers $0$, et donc $fg$ également.
}

	\subsection{Comparaison à l'infini}

\begin{theoreme}[Théorème de comparaison]
Soient $f$ et $g$ deux fonctions définies sur $I=\interoo{a +\infty}$ (avec $a\in \R$). Si pour tout $x$ de $I$ :
\begin{itemize}
	\item $f(x)\geq g(x)$ \textbf{et si} $\ds{\lim_{x\rightarrow +\infty} g(x)=+\infty}$ alors $\ds{\lim_{x\rightarrow +\infty} f(x)=+\infty}$.
	\item $f(x)\leq g(x)$ \textbf{et si} $\ds{\lim_{x\rightarrow +\infty} g(x)=-\infty}$ alors $\ds{\lim_{x\rightarrow +\infty} f(x)=-\infty}$.
\end{itemize}
	\end{theoreme}

\ifprof
\begin{demonstration}
Soit $M$ un réel. Par définition, à partir d'un certain réel $b$, on a $g(x)>M$. Or $f(x)\geq g(x)$, donc $f(x) > M$ pour tout $x \geq b$ : par définition, $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=+\infty}$.
\end{demonstration}
\else
\lignes{3}
\fi

\begin{exemple}
Déterminer $\ds{\lim_{x\rightarrow +\infty} \frac{[x]+1}{\sqrt{x}}}$.
\end{exemple}

\solution[5]{Pour tout réel $x$, on a $[x]\leq x\leq [x]+1$. Ainsi, pour tout réel $x>0$ (puisque $\sqrt{x}>0$) on a
\[\frac{x}{\sqrt{x}}\leq\frac{[x]+1}{\sqrt{x}}\]
soit
\[\sqrt{x}\leq \frac{[x]+1}{\sqrt{x}}\]
Puisque $\ds{\lim_{x\rightarrow +\infty} \sqrt{x}=+\infty}$, par comparaison, \[\lim_{x\rightarrow +\infty} \frac{[x]+1}{\sqrt{x}}=+\infty\]}

       \subsection{Asymptote oblique}

\begin{definition}[Asymptote oblique]
Soit $\mathcal{C}_f$ la courbe représentative d'une fonction $f$ dans un repère donné. Soit $(d)$ une droite d'équation $y=ax+b ~(a\neq 0)$. On dit que la droite $(d)$ est une \textbf{asymptote oblique} à $\mathcal{C}_f$ au voisinage de $+\infty$ si
\[\lim_{x\rightarrow+\infty} [f(x)-(ax+b)] =0\]
\end{definition}

\begin{exemple}
Soit $f$ la fonction définie sur $\R*$ par $f(x)=\dfrac{x^2+1}{x}$. Montrer que la droite d'équation $y=x$ est asymptote oblique à la courbe de $f$ au voisinage de $+\infty$ et de $-\infty$.
\end{exemple}

\solution[3]{En effet, pour tout réel $x\in \R*$,
\[f(x)-x=\frac{x^2+1}{x}-x=\frac{1}{x}\]
Or $\ds{\lim_{x\rightarrow -\infty} \frac{1}{x}=\lim_{x\rightarrow +\infty} \frac{1}{x}=0}$.
Ainsi la droite d'équation $y=x$ est bien asymptote oblique à la courbe de $f$ au voisinage de $+\infty$ et $-\infty$}


\section{Opérations sur les limites et limites usuelles}

Avant de donner les résultats sur les opérations, on donne une définition de tendre vers $0^+$ et $0^-$ :

\begin{definition}
  Soient $f:I\to \R$ et $x_0\in \R$.
  \begin{itemize}
    \item On dit que $f$ admet $0^+$ pour limite en $x_0$, et on note $f(x)\tendversen{x\to x_0} 0^+$, si $f$ admet $0$ pour limite en $x_0$, et est strictement positive sur un voisinage de $x_0$.
    \item On dit que $f$ admet $0^-$ pour limite en $x_0$, et on note $f(x)\tendversen{x\to x_0} 0^-$, si $f$ admet $0$ pour limite en $x_0$, et est strictement négative sur un voisinage de $x_0$.
  \end{itemize}
\end{definition}

        \subsection{Opérations sur les limites}

On suppose connues les limites de deux fonctions $f$ et $g$.

            \subsubsection{Limite de $f+g$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $\ell$ & $+\infty$ & $-\infty$ \\
 \hline
  $\ell'$ & $\ell+\ell'$ & $+\infty$ & $-\infty$ \\
  \hline
  $+\infty$ & $+\infty$ & $+\infty$ & IND \\
  \hline
  $-\infty$ & $-\infty$ & IND & $-\infty$\\
  \hline
\end{tabular}
\end{center}

\begin{exemple}
\[\lim_{x \rightarrow +\infty} (x+\sqrt{x}) = +\infty\]
\end{exemple}

\solution[3]{En effet, $\ds{\lim_{x\rightarrow +\infty} x = \lim_{x\rightarrow +\infty} \sqrt{x} = +\infty}$. Par somme, $\ds{\lim_{x\rightarrow +\infty} (x+\sqrt{x})=+\infty}$.}

            \subsubsection{Limite de $f\times g$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $\ell\neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $\ell'\neq 0$ & $\ell.\ell'$ & signe($\ell'$).$\infty$ & -signe($\ell'$).$\infty$ \\
  \hline
  $+\infty$ & signe($\ell$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $-\infty$ & -signe($\ell$).$\infty$ & $-\infty$ & $+\infty$\\
  \hline
\end{tabular}
\end{center}

\begin{remarque}
Si $\ell=0$ (et/ou $\ell'=0$), seul le résultat $\lim(fg)=\ell.\ell'=0$ est déterminé. Toutes les autres limites (du type ``$0\times \infty$'') sont \textbf{indéterminées}.
\end{remarque}

\begin{exemple}
\begin{multicols}{2}
\begin{itemize}[label=]
\item $\ds{\lim_{x\rightarrow +\infty} (x\sqrt{x})=+\infty}$
\item $\ds{\lim_{x\rightarrow 0}  3x\E^x =0}$
\end{itemize}
\end{multicols}
\end{exemple}

\solution[4]{En effet, $\ds{\lim_{x\rightarrow +\infty} x = \lim_{x\rightarrow +\infty} \sqrt{x} = +\infty}$. Par produit, $\ds{\lim_{x\rightarrow +\infty} (x\sqrt{x})=+\infty}$.\\
De même, $\ds{\lim_{x\rightarrow 0} 3x=0}$ et $\ds{\lim_{x\rightarrow 0} \E^x = \E^0=1}$. Par produit, $\ds{\lim_{x\rightarrow 0} 3x\E^x = 0}$.
}

            \subsubsection{Limite de $\frac{f}{g}$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $\ell$ & $+\infty$ & $-\infty$ \\
 \hline
  $\ell'\neq 0$ & $\frac{\ell}{\ell'}$ & signe($\ell'$).$\infty$ & -signe($\ell'$).$\infty$ \\
  \hline
  $+\infty$ & $0$ & IND & IND \\
  \hline
  $-\infty$ & 0 & IND & IND\\
  \hline
\end{tabular}
\end{center}

Si $\lim g = 0$, il faut tout d'abord préciser si $\lim g = 0^+$ ($g$ tend vers $0$ en restant positif) ou si $\lim g=0^-$, et on applique :

\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $0$ & $\ell \neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $0^+$ & IND & signe($\ell$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $0^-$ & IND & -signe($\ell$).$\infty$ & $-\infty$ & $+\infty$ \\
  \hline
\end{tabular}
\end{center}

\begin{exemple}
\begin{multicols}{2}
\begin{itemize}[label=]
\item $\ds{\lim_{x \rightarrow +\infty} \frac{1+\frac{1}{x}}{\sqrt{x}}=0}$
\item $\ds{\lim_{x \rightarrow 0^+} \frac{x^2+1}{x}=+\infty}$
\end{itemize}\end{multicols}
\end{exemple}

\solution[4]{En effet, $\ds{\lim_{x\rightarrow +\infty} 1+\frac{1}{x} = 1}$ par somme, et $\ds{\lim_{x\rightarrow +\infty} \sqrt{x}=+\infty}$. Par quotient, $\ds{\lim_{x\rightarrow +\infty} \frac{1+\frac{1}{x}}{\sqrt{x}}=0}$.\\
De même, $\ds{\lim_{x\rightarrow 0^+} x^2+1=1}$ par somme et $\ds{\lim_{x\rightarrow 0^+} x = 0^+}$. Par quotient, $\ds{\lim_{x \rightarrow 0^+} \frac{x^2+1}{x}=+\infty}$.
}

\afaire{Exercice \lienexo{01}.}

	\subsection{Limite d'une fonction composée}

\begin{theoreme}
Soient $f,g,h$ trois fonctions telles que $f=g\circ h$ sur un intervalle $I$. Soient $a,b,c$ des éléments de $\R\cup \{+\infty;-\infty\}$.\\
Si $\ds{\lim_{x\rightarrow a} h(x)=b}$ et $\ds{\lim_{x\rightarrow b} g(x)=c}$, alors
\[\lim_{x\rightarrow a} f(x)=c\]
\end{theoreme}

\begin{demonstration}
Admis.
\end{demonstration}

\begin{methode}
Pour déterminer la limite d'une fonction composée $f(x)=g(h(x))$ en $x_0$ :
\begin{itemize}
    \item On pose $X=h(x)$.
    \item On détermine la limite $b$ de $X$ en $x_0$.
    \item On détermine la limite $c$ de $g$ en $b$, et on conclut : la limite de $f$ en $x_0$ vaut $c$.
\end{itemize}
\end{methode}

\begin{exemple}
Déterminer $\ds{\lim_{x\rightarrow \textcolor{blue}{+\infty}} \sqrt{x^2-x+1}}$.
\end{exemple}


\solution[5]{
\begin{itemize}
    \item On pose $X=x^2-x+1$. On a
    \[\lim_{x\rightarrow \textcolor{blue}{+\infty}} X = \textcolor{magenta}{+\infty}\]
    \item On a \[\lim_{X\rightarrow \textcolor{magenta}{+\infty}} \sqrt{X} = \textcolor{red}{+\infty}\]
\end{itemize}
Par composée, on a donc
\[\lim_{x\rightarrow \textcolor{blue}{+\infty}} \sqrt{x^2-x+1} = \textcolor{red}{+\infty}\]
}

\begin{exo}
Montrer que $\ds{\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} \frac{1}{\sqrt{3x-1}} =+\infty}$.
\end{exo}


\solution[5]{Posons $X=3x-1$. Alors :
\begin{itemize}
    \item On a $\ds{\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} 3x-1=0^+}$.
    \item  De plus, $\ds{\lim_{X\rightarrow 0^+} \frac{1}{\sqrt{X}} = +\infty}$ par quotient.
\end{itemize}
Par composée,
\[\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} \frac{1}{\sqrt{3x-1}} =+\infty\]
}

\afaire{Exercice \lienexo{07}.}

        \subsection{Limites usuelles}

            \subsubsection{Limites classiques}

On dispose d'un ensemble de limites usuelles.

\begin{proposition}[Fonctions usuelles]
\begin{align*}
\textrm{Pour tout entier $n\in \N^*$, }& \lim_{x\rightarrow +\infty} x^n = +\infty\qeq\lim_{x\rightarrow +\infty} \frac{1}{x^n} = \lim_{x\rightarrow -\infty} \frac{1}{x^n} =0\\
\textrm{Pour tout entier $n\in \N^*$, }& \lim_{x\rightarrow -\infty} x^n = +\infty \textrm{ si $n$ est pair, } -\infty \textrm{ sinon.}	\\
&\lim_{x\rightarrow +\infty} \sqrt{x} = \lim_{x\rightarrow +\infty} \ln(x)=\lim_{x\rightarrow +\infty} \E^x = +\infty\\
&\lim_{x\rightarrow 0} \ln(x)=-\infty,\quad\lim_{x\rightarrow -\infty} \E^x=0\\
&\lim_{x\rightarrow -\infty} \E^{-x} = +\infty,\quad \lim_{x\rightarrow +\infty} \E^{-x} = 0\\
&\lim_{x\rightarrow +\infty} [x]=+\infty \quad\lim_{x\rightarrow -\infty} [x] = -\infty\\
&\lim_{x\rightarrow +\infty} |x|=\lim_{x\rightarrow -\infty} |x|=+\infty
\end{align*}
\end{proposition}

\begin{proposition}[Fonctions puissances]
\begin{align*}
	\textrm{Pour tout }~\alpha>0,&\quad\lim_{x\rightarrow +\infty} x^\alpha=+\infty \qeq \lim_{x\rightarrow 0} x^\alpha=0\\
	\textrm{Pour tout }~\alpha<0,&\quad\lim_{x\rightarrow +\infty} x^\alpha=0 \qeq \lim_{x\rightarrow 0} x^\alpha=+\infty\\
	\textrm{Pour tout }~a>1,&\quad\lim_{x\rightarrow +\infty} a^x=+\infty \qeq \lim_{x\rightarrow -\infty} a^x=0\\
	\textrm{Pour tout $a$ tel que }~0<a<1&\quad\lim_{x\rightarrow +\infty} a^x=0 \qeq \lim_{x\rightarrow -\infty} a^x=-\infty
\end{align*}
\end{proposition}

            \subsubsection{Croissances comparées}

\begin{theoreme}[Croissances comparées]
Pour tout $\alpha>0$, $p>0$ et $q>1$ :
\[\lim_{x\rightarrow +\infty} \frac{q^x}{x^\alpha}=+\infty,\quad \lim_{x\rightarrow +\infty} \frac{\E^x}{x^\alpha}=+\infty \qeq \lim_{x\rightarrow +\infty} \frac{x^\alpha}{\ln^p(x)} = +\infty\]
\end{theoreme}

\begin{consequence}
Par passage à l'inverse,
\[\lim_{x\rightarrow +\infty} \frac{x^\alpha}{\E^x}=0 \qeq \lim_{x\rightarrow +\infty} \frac{\ln^p(x)}{x^\alpha} = 0\]
\end{consequence}

\begin{theoreme}[Conséquence des croissances comparées]
Pour tout $\alpha>0$, \[\lim_{x\rightarrow 0^+} x^\alpha \ln(x) =0\]
\end{theoreme}

\begin{demonstration}
Posons $X=\frac{1}{x}$. Alors $\displaystyle{\lim_{x\rightarrow 0^+} X =+\infty}$ et $\displaystyle{\lim_{x\rightarrow 0^+} x^\alpha \ln(x)=\lim_{X\rightarrow +\infty} -\frac{\ln(X)}{X^\alpha} = 0}$ d'après ce qui précède.
\end{demonstration}

\begin{theoreme}
Pour tout $n\in \N$, on a \[\lim_{x\rightarrow -\infty} x^n\E^x=0\]
\end{theoreme}


\begin{demonstration}
Se démontre de la même manière que précédemment (en posant $X=-x$).
\end{demonstration}

\begin{methode}
Pour utiliser les croissances comparées, il faut souvent faire un changement de variable pour s'y ramener.
\end{methode}


\begin{exemple}
Déterminer $\ds{\lim_{x\rightarrow +\infty} \frac{\E^{2x}}{x^3}}$.
\end{exemple}


\solution[3]{On pose $X=2x$. Alors \[\lim_{x\rightarrow +\infty} X = +\infty \textrm{ et }
\lim_{x\rightarrow +\infty} \frac{\eu{2x}}{x^3}=\lim_{X\rightarrow +\infty} \frac{\eu{X}}{(X/2)^3}=\lim_{X\rightarrow +\infty} 2^3\frac{\eu{X}}{X^3}=+\infty \textrm{ par croissance comparée.}\]
}

\afaire{Exercice \lienexo{03}.}

    \subsection{Autres limites}

\begin{theoreme}[Taux d'accroissement]
On dispose des limites suivantes :
\[\lim_{x \rightarrow 0} \frac{\eu{x}-1}{x}=1,\quad\lim_{x\rightarrow 0}\frac{\ln(1+x)}{x}=1,\quad\lim_{x\rightarrow 1} \frac{\ln(x)}{x-1}=1
\quad \lim_{x\to 0} \frac{\sin(x)}{x}=1\qeq \lim_{x\to 0}\frac{\cos(x)-1}{x}=0. \]

\end{theoreme}

\begin{demonstration}
Nous verrons la démonstration de ces limites dans le chapitre dédié à la Dérivation.
\end{demonstration}

\begin{exemple}
Déterminer $\ds{\lim_{x\rightarrow 0^+} \frac{\ln(1+x)}{x^3}}$.
\end{exemple}

\solution[5]{On remarque que, pour tout réel $x>0$ :
\[\frac{\ln(1+x)}{x^3} = \frac{\ln(1+x)}{x}\times \frac{1}{x^2}\]
Or \[\lim_{x\rightarrow 0^+} \frac{\ln(1+x)}{x} = 1 \qeq \lim_{x\rightarrow 0^+} \frac{1}{x^2} = +\infty\]
Par produit,
\[\lim_{x\rightarrow 0^+} \frac{\ln(1+x)}{x^3}=+\infty\]
}

       \subsection{Quelques indéterminations classiques}

        \subsubsection{Polynômes et fractions rationnelles}

\begin{methode}[Règle du plus haut degré]
En $+\infty$ ou en $-\infty$, il y a une méthode classique dite du terme du plus haut degré.
 \begin{itemize}
    \item Si $\ds{f :x \mapsto a_nx^n+a_{n-1}x^{n-1}+\cdots +a_0}$ est un polynôme, alors $\ds{\lim_{x\rightarrow +\infty} f(x)=\lim_{x\rightarrow +\infty} a_nx^n}$.
    \item Si $\ds{g: x \mapsto \frac{a_nx^n+a_{n-1}x^{n-1}+\cdots +a_0}{b_kx^k+b_{k-1}x^{k-1}\cdots + b_0}}$ est une fraction rationnelle, alors \[\lim_{x\rightarrow +\infty} g(x)=\lim_{x\rightarrow +\infty} \frac{a_nx^n}{b_kx^k}\]
\end{itemize}
\end{methode}

\begin{exemple}
Soient $f:x\mapsto 2x^3-3x^2+6x-1$ et $g:x\mapsto\frac{x^2+1}{2x^2-3x+1}$. Déterminer $\ds{\lim_{x\rightarrow +\infty} f(x)}$ et $\ds{\lim_{x\rightarrow -\infty} g(x)}$
\end{exemple}

\solution[4]{D'après la règle du terme du plus haut degré,
\[\lim_{x\rightarrow +\infty} f(x)=\lim_{x\rightarrow +\infty} 2x^3=+\infty\]
De même,
\[\lim_{x\rightarrow -\infty} g(x)=\lim_{x\rightarrow -\infty} \frac{x^2}{2x^2}=\lim_{x\rightarrow -\infty} \frac{1}{2}= \frac{1}{2}\]
}

\begin{remarque}\logoattention
Cette méthode ne s'applique qu'aux limites en $+\infty$ et $-\infty$.
\end{remarque}

        \subsubsection{Racines}

\begin{methode}[Quantité conjuguée]
Lorsqu'une fonction contient des radicaux, on utilise la quantité conjuguée.
\end{methode}

\begin{exemple}
Soit $f:x\mapsto \sqrt{x^2+1}-x$. Déterminer $\ds{\lim_{x\rightarrow +\infty} f(x)}$.
\end{exemple}

\solution[4]{On constate que la limite en $+\infty$ de $f$ est indéterminée. Alors, pour tout réel $x$, on a
 \[f(x)=(\sqrt{x^2+1}-x)\frac{\sqrt{x^2+1}+x}{\sqrt{x^2+1}+x}=\frac{1}{\sqrt{x^2+1}+x}\] et donc, par composée et quotient,
 \[\lim_{x\rightarrow +\infty} f(x)=0\]
 }

        \subsubsection{\og{}$\frac{\infty}{\infty}$\fg ou \og{}$\frac{0}{0}$\fg}

\begin{methode}
Dans les cas \og{}$\frac{\infty}{\infty}$\fg ou \og{}$\frac{0}{0}$\fg, on commence par mettre au numérateur et au dénominateur le terme prépondérant (en utilisant les croissances comparées).
\end{methode}


\begin{exemple}
Soit $\ds{f:x\mapsto \frac{x+1}{\E^x-1}}$. Déterminer $\ds{\lim_{x\rightarrow +\infty} f(x)}$.
\end{exemple}

\solution[5]{Alors \[\forall~x>0,\quad \frac{x+1}{\E^{x}-1}=\frac{x}{\E^x} \frac{1+\frac{1}{x}}{1-\eu{-x}}\]
Or \[\lim_{x\rightarrow +\infty} \frac{x}{\E^x}=0 \,(\text{croissance comparée})\qeq \lim_{x\rightarrow +\infty} \frac{1+\frac{1}{x}}{1-\E^{-x}}=1\textrm{  par quotient}\]
Par produit $\ds{\lim_{x\rightarrow +\infty} f(x)=0}$.}

\afaire{Exercice \lienexo{02}.}

\section{Caractérisation séquentielle}

Connaître la limite d'une fonction peut permettre, dans certains cas, de déterminer la limite d'une suite.

\begin{theoreme}[Caractérisation séquentielle de la limite]
Soit $I$ un intervalle, $f:I\rightarrow \R$ une fonction, et $(u_n)$ une suite. On suppose que $(u_n)$ converge vers $\ell\in I$ ou à une extrémité de $I$, et que $\ds{\lim_{x\rightarrow \ell} f(x)=a}$. Alors \[ \lim_{n\rightarrow +\infty} f(u_n) = a \]
\end{theoreme}


\begin{exemple}
Soit $f:x\mapsto \sqrt{x}$ et $u$ la suite définie pour tout $n$ par $u_n=1+\frac{1}{n+1}$. Puisque $u_n \tendversen{n\rightarrow +\infty} 1$ et que $\ds{\lim_{x\to 1} f(x)=1}$, alors \[\lim_{n\to+\infty} \sqrt{1+\frac{1}{n+1}} = 1\]
\end{exemple}

\begin{exo}
Déterminer, de la même manière, \[ \lim_{n\rightarrow +\infty} \ln \left(\frac{n}{n^2+1}\right) \]
\end{exo}


\solution[5]{On pose $f:x\mapsto \ln(x)$ et pour tout $n$, $u_n=\frac{n}{n^2+1}$. On a rapidement
\[ u_n \sim \frac{n}{n^2}\tendversen{n\to +\infty} 0 \]
et $\ds{\lim_{x\to 0} \ln(x)=-\infty}$. On en déduit donc que \[ \lim_{n\to +\infty} \ln\left( \frac{n}{n^2+1}\right)=-\infty \]
}

\section{Etude des limites d'une fonction}

    \subsection{1ère étape : limites}

Lorsqu'on se donne une fonction, on commencera toujours par déterminer ses limites au borne de l'intervalle de définition~:
\begin{itemize}
    \item Si $f$ est définie sur $\mathbb{R}$, on déterminera $\ds{\lim_{x \rightarrow +\infty} f}$ et $\ds{\lim_{x\rightarrow-\infty} f}$.
    \item Si $f$ est définie sur $]-\infty;a[\cup]a;+\infty[$, il faut déterminer $4$ limites : en $+\infty$, $-\infty$, $a^+$ et $a^-$.
\end{itemize}

On notera directement les asymptotes horizontales (limite finie en $+\infty$ ou $-\infty$) et les asymptotes verticales (limite infinie en $a^+$ ou $a^-$).

    \subsection{2ème étape : branches infinies}

Si les limites en $+\infty$ et/ou $-\infty$ sont infinies, on cherche une éventuelle asymptote oblique. Pour cela on détermine
\[\lim_{x\rightarrow +\infty} \frac{f(x)}{x}\]
\begin{itemize}
    \item Si cette limite est nulle, on dit que la courbe de $f$ possède une \textbf{branche parabolique de direction l'axe des abscisses} en $+\infty$.
    \begin{center}
    \includegraphics[width=5cm]{paraabs}
    \end{center}
    L'exemple classique est la fonction logarithme népérien.
    \item Si cette limite est infinie, on dit que la courbe de $f$ possède une \textbf{branche parabolique de direction l'axe des ordonnées} en $+\infty$.
    \begin{center}
    \includegraphics[width=5cm]{paraord}
    \end{center}
    L'exemple classique est la fonction exponentielle.
    \item Si cette limite est un nombre réel $a$, on dit que la courbe de $f$ admet une direction asymptotique d'équation $y=ax$. Il reste alors à calculer $\ds{\lim_{x\rightarrow +\infty} (f(x)-ax)}$ .
        \begin{itemize}
                \item[$\circ$] Si cette limite est un réel $b$, la droite d'équation $y=ax+b$ est \textbf{asymptote oblique} à la courbe de $f$ en $+\infty$.
    \begin{center}
    \includegraphics[width=5cm]{asymptote}
    \end{center}
                \item[$\circ$] Si cette limite est infinie, on dit que la courbe de $f$ possède une \textbf{branche parabolique de direction la droite d'équation} $y=ax$.
    \begin{center}
    \includegraphics[width=5cm]{paradroite}
    \end{center}
        \end{itemize}
\end{itemize}
(bien sûr, tout ceci est valable en $-\infty$.)

\afaire{Exercice \lienexo{20}.}
