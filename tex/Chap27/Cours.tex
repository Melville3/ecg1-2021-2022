\chapter{Variables aléatoires discrètes}
%!TeX root=../../encours.nouveau.tex

\objectifintro{
	Dans ce chapitre, on fait des rappels sur les paramètres concernant les variables aléatoires discrètes. On s'intéresse ensuite aux lois de référence : loi géométrique et loi de Poisson.}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Nicolas Grimaldi (1933 -- ). \emph{Le Travail. Communion et excommunication}}
	Tout l'ordre social, pour aléatoire et injuste qu'il soit, si absurde et même si scandaleux qu'il puisse être, n'est fondé que sur une ordinaire, diffuse et commune persuasion.
	\end{extrait}

\begin{objectifs}
	\begin{numerote}
		\item Concernant les paramètres d'une variable aléatoire :
		\begin{itemize}
		  \item \lienobj{1}{connaître la définition de l'espérance}
		  \item \lienobj{2}{savoir utiliser la formule de transfert}
		  \item \lienobj{3}{connaître la définition de la variance}
		  \item \lienobj{4}{savoir utiliser la formule de Koenig-Huygens}
		\end{itemize}
		  \item Concernant les lois usuelles, il faut connaître
		\begin{itemize}
		  \item \lienobj{5}{la définition et les paramètres de la loi certaine}
		  \item \lienobj{6}{la définition et les paramètres de la loi uniforme}
		  \item \lienobj{7}{la définition et les paramètres de la loi de Bernoulli}
		  \item \lienobj{8}{la définition et les paramètres de la loi binomiale}
		  \item \lienobj{9}{la définition et les paramètres de la loi géométrique}
		  \item \lienobj{10}{la définition et les paramètres de la loi de Poisson}
		\end{itemize}
		  \item \lienobj{11}{Savoir générer avec \textsc{Python} les différentes lois}
			\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%
%%
% Pour les aligns
\allowdisplaybreaks
%%
Dans l'ensemble de ce chapitre, on se donne un espace probabilisé $(\Omega, \AA, \PP)$.

\section{Compléments sur les variables aléatoires discrètes}

	\subsection{Généralités sur les variables aléatoires discrètes}

Dans l'ensemble de cette section, les variables aléatoires seront \textbf{discrètes} :

\begin{definition}
	Une variable aléatoire $X$, définie sur $(\Omega, \AA, \PP)$ est dite \textbf{discrète} si $X(\Omega)$ est fini ou dénombrable.
\end{definition}

\begin{remarque}
	Dans ce cas, on peut numéroter $X(\Omega) = \{ x_i,\, i\in I\}$ avec $I$ fini ou dénombrable. En général, on écrira $X(\Omega)=\{x_0,x_1,\hdots \}$.
\end{remarque}

On dispose d'une propriété simplifiant la méthode pour montrer qu'une application est une variable aléatoire discrète :

\begin{proposition}
Soit $X:\Omega \to \R$. $X$ est une variable aléatoire discrète si et seulement si $X(\Omega)$ est fini ou dénombrable, et si
\[ \forall x\in X(\Omega),\quad [X=x] =\{ \omega \in \Omega,\, X(\omega)=x\} \in \AA. \]
\end{proposition}

\preuve[5]{
Si $X$ est une variable aléatoire réelle discrète sur $(\Omega, \AA)$ alors $X(\Omega)$ est au plus dénombrable et, pour tout $x \in X(\Omega)$, $[X=x] \in \AA$ (nous l'avons vu dans le chapitre précédent).

Réciproquement, supposons que $X(\Omega)$ est au plus dénombrable et que $[X=x] \in \AA$ pour tout $x \in X(\Omega)$. On a alors
\[
\forall x \in \R, \quad [X \leq x]=\bigcup\limits_{\substack{y \in X(\Omega) \\ y \leq x}}[X=y] \in \AA,
\]
puisqu'il s'agit d'une union dénombrable et que $\AA$ est une tribu.
}

	\subsection{Tribu engendrée par une variable aléatoire discrète}

Rappelons ce que nous avons déjà vu dans le chapitre précédent :

\begin{proposition}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA)$. La famille $([X=x])_{x\in X(\Omega)}$ est un système complet d'évnénements, appelé \textbf{système complet d'événements associé à $X$}.
\end{proposition}

\preuve[5]{
Pour tout $x \in X(\Omega)$, $[X=x] \in \AA$ par définition d'une variable aléatoire discrète.

Soient $x$ et $y$ dans $X(\Omega)$. Si $x \neq y$, alors $[X=x] \cap[X=y]=\vide$ et donc  les événements $[X=x]$ et $[X=y]$ sont incompatibles.

Enfin, si $\omega \in \Omega$, on a alors  \[ \omega \in [X=X(\omega)] \subset \bigcup\limits_{x \in X(\Omega)} [X=x] \subset \Omega.\]
 Ainsi $\bigcup\limits_{x \in X(\Omega)}[X=x]=\Omega$.
}

On dispose alors d'une tribu très pratique pour l'étude d'une variable aléatoire :

\begin{definition}[Tribu engendrée par une variable aléatoire]
Soient $(\Omega, \AA)$ un espace probabilisable, et $X$ une variable aléatoire réelle discrète sur $(\Omega, \AA)$.

On appelle \textbf{tribu engendrée par X} la tribu engendrée par le système complet d'événements associé à $X$, c'est-à-dire la plus petite (au sens de l'inclusion) tribu contenant tous les $[X = x]$ pour $x \in X(\Omega)$. On la note $\AA_X$ ou $\sigma(X)$.
\end{definition}

\begin{remarque}
	Dans la pratique, on ne déterminera jamais la tribu engendrée par $X$, on sait simplement qu'elle existe.
\end{remarque}

	\subsection{Loi d'une variable aléatoire discrète}

		\subsubsection{Définition}
Rappelons que la loi d'une variable aléatoire réelle est la fonction $\PP_X$ qui, à tout intervalle $I$ de $\R$, associe $\PP_X(I)=\PP(X\in I)$.

On a déjà vu dans le chapitre précédent le résultat suivant, important :

\begin{proposition}
 Soit $X$  une variable aléatoire réelle discrète sur $(\Omega,\AA,\PP)$. Connaître la loi de $X$ est équivalent à connaître $X(\Omega)$ et l'ensemble des $\PP(X=x)$ pour $x\in X(\Omega)$.
\end{proposition}

\preuve[10]{Si on connait la loi $\PP_X$ de $X$ alors, pour tout $x \in X(\Omega)$, on connaît $\PP(X=x)=\PP(X \in[x, x])$ puisque $[x, x]$ est un intervalle.

Réciproquement si on connaît $X(\Omega)$ et $\PP(X=x)$ pour tout $x \in X(\Omega)$ alors, pour tout intervalle $I$ de $\R$,
\[
\PP(X \in I)=\PP\left(\bigcup_{x \in I \cap X(\Omega)}[X=x]\right)
\]
Or, puisque $X(\Omega)$ est au plus dénombrable, par incompatibilité et $\sigma$-additivité :
\[ \PP(X\in I) = \sum_{x \in I \cap X(\Omega)} \PP(X=x). \]
Ainsi on connaît $\PP\left(X \in I\right)$. On connaît donc $\PP_{X}$.
}

		\subsubsection{\'Egalité en loi}

Cela nous permet de définir l'égalité en loi :

\begin{definition}
Soient $X$ et $Y$ deux variables aléatoires réelles discrètes définies sur des espaces probabilisés (potentiellement différents).

On dit que $X$ et $Y$ sont \textbf{égales en loi}, et on note $X \overset{\mathcal{L}}{=} Y$, si elles ont la même loi, c'est-à-dire si $\PP_X=\PP_Y$ (ce qu'on peut noter $\mathcal{L}_X=\mathcal{L}_Y$).
\end{definition}

	\subsubsection{Existence d'une variable aléatoire discrète}

Commençons par un premier résultat, lié au système complet d'événements associé à une variable aléatoire discrète :

\begin{proposition}
Soit $X$ une variable aléatoire discrète sur $(\Omega,\AA,\PP)$. Alors $\sum\limits_{x\in X(\Omega)} \PP(X=x)$ converge et \[ \sum_{x\in X(\Omega)} \PP(X=x)= 1. \]
\end{proposition}

\preuve[3]{
En effet, $\left( [X=x]\right)_{x\in X(\Omega)}$ est un système complet d'événements. Par $\sigma$-additivité et incompatibilité :
\[ 1 = \PP(\Omega) = \PP\left(\bigcup_{x\in X(\Omega)} [X=x]\right) =\sum_{x\in X(\Omega)} \PP(X=x). \]
}

On dispose d'une réciproque :

\begin{theoreme}[Existence d'une variable aléatoire de loi fixée]
Soit $I$ une partie de $\N$. Soient $(p_i)_{i\in I}$ une famille de réels positifs telle que $\sum \limits_{i\in I} p_i=1$ et $(x_i)_{i\in I}$ une famille de réels deux à deux distincts.

Alors il existe un espace probabilisé $(\Omega, \AA, \PP)$ et une variable aléatoire sur $(\Omega, \AA)$ tels que
\begin{itemize}
	\item $X(\Omega) = \{ x_i,\, i\in I\}$,
	\item Pour tout $i\in I$, $\PP(X=x_i)=p_i$.
\end{itemize}
\end{theoreme}

\preuve[4]{On considère $\Omega=\left\{x_{i},\, i \in I\right\}$,  $\AA=\partie(\Omega)$,  $\PP:\AA\to \interff{0 1}$ définie par $\PP(A) = \sum\limits_{i \in A} p_{i}$, et $X$ l'application identité de $\Omega$.

Les conditions sur $\left(p_{i}\right)_{i \in I}$ entraînent que $\PP$ est bien une probabilité sur $(\Omega, \AA)$. On a bien $X(\Omega)=\Omega=\left\{x_{i} \mid i \in I\right\}$ et, pour tout $i \in I, \PP\left(X=x_{i}\right)=\PP\left(\left\{x_{i}\right\}\right)=p_{i}$.
}

Cela nous permet de définir la loi :

\begin{definition} Soit $\mathcal{L}$ une application définie sur un sous-ensemble dénombrable $\{x_i,\, i\in I\}\subset \R$ et à valeurs dans $\R+$ vérifiant $\sum\limits_{i\in I} \mathcal{L}(x_i) = 1$.

On dit que $\mathcal{L}$ est une \textbf{loi de probabilité discrète} sur $\R$.

L'ensemble $\{x_i,\, i \in I\}$ est appelé le \textbf{support} de la loi $\mathcal{L}$.

Si $X$ est une variable aléatoire réelle discrète dont la loi est $\mathcal{L}$, on dit que $X$ suit la loi $\mathcal{L}$ et on note $X \suit \mathcal{L}$.
\end{definition}

Terminons enfin par une propriété fondamentale :

\begin{theoreme}[La fonction de répartition caractérise la loi]
Si $X$ et $Y$ sont deux variables aléatoires réelles discrètes ayant la même fonction de répartition, alors elles ont la même loi.
\end{theoreme}

\preuve[4]{ Si $F_{X}=F_{Y}$ alors, pour tout $t_{0} \in \mathbb{R}$,
\[
\PP\left(X=t_{0}\right)=F_{X}\left(t_{0}\right)-\lim _{\substack{t \rightarrow t_{0} \\ t<t_{0}}} F_{X}(t)=F_{Y}\left(t_{0}\right)-\lim _{\substack{t \rightarrow t_{0} \\ t<t_{0}}} F_{Y}(t)=\PP\left(Y=t_{0}\right)
\]
Par conséquent $X(\Omega)=Y(\Omega)$ et donc $X$ et $Y$ ont même loi.
}

	\subsection{Transfert d'une variable aléatoire}

Connaissant une variable aléatoire, on peut la modifier en appliquant une fonction à celle-ci : c'est le transfert.

\begin{definition}
Soient $(\Omega, \AA)$ un espace probabilisable et $X$ est une variable aléatoire réelle discrète sur $(\Omega, \AA)$. Soit $g$ une application de $X(\Omega)$ à valeurs réelles.

Alors, $Y=g(X)$ est une variable aléatoire réelle discrète sur $(\Omega, \AA)$, appelée \textbf{transfert de $X$ par $g$}.
\end{definition}

\preuve[8]{Puisque $X$ est discrète, $Y$ est une application définie sur $\Omega$ et ne prend, au plus, qu'un nombre dénombrable de valeurs réelle.

On a $Y(\Omega)=\{g(x),\, x \in X(\Omega)\}$. Pour tout $y \in Y(\Omega)$, l'ensemble $A_{y}$ des antécédents de $y$ par $X$ est dénombrable (car $A_{y} \subset X(\Omega)$ ) et donc
	\[
	[Y=y]=\left[X \in A_{y}\right]=\bigcup_{x \in A_{y}}[X=x] \in \AA .
	\]
	Ainsi $Y$ est bien une variable aléatoire réelle discrète $\operatorname{sur}(\Omega, \AA)$.
}

	\subsection{Extension à $\N\cup \{+\infty\}$ et $\Z \cup \{-\infty, +\infty\}$}

Il arrive qu'on étudie une variable aléatoire qui peut, théoriquement, valoir $+\infty$ ou $-\infty$; par exemple, si $X$ compte le nombre de tirage nécessaire pour obtenir un premier pile lors d'une infinité de lancers indépendants, on peut théoriquement avoir $X=+\infty$.

On n'étudiera des cas où $\PP(X\in \R)=1$ (c'est-à-dire des cas où obtenir $+\infty$ ou $-\infty$ est de probabilité nulle), ce qui est le cas dans l'exemple précédent. Dans ce cas, par abus de notation, on continuera d'écrire $X(\Omega) \subset \R$.

Si $\PP(X=+\infty)>0$, en général, on change la valeur de $X$ (par exemple, on donnera $X=-1$ plutôt que $X=+\infty$ si cela est possible).

\section{Moment d'une variable aléatoire}

\subsection{Espérance mathématique}

\begin{definition}
\labelobj{1}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA, \PP)$. On écrit $X(\omega)=\{x_i, i\in I\}$. On dit que $X$ admet une \textbf{espérance} lorsque la série
$\displaystyle{\sum_{i\in I} x_i\PP(X=x_i)}$ est \textbf{absolument} convergente. Dans ce cas, on appelle \textbf{espérance} (ou \textbf{moyenne}) de $X$ le nombre
$$\esperance(X)=\sum_{i\in I} x_i \PP(X=x_i) = \sum_{x \in X(\Omega)} x\PP(X=x)$$
Si $X(\Omega)=\N$, on a ainsi
$$\esperance(X)=\sum_{k=0}^{+\infty} k\PP(X=k)$$
\end{definition}

\begin{remarque}
Si $X(\Omega)$ est fini, $X$ admet une espérance, puisque la somme sera finie.
\end{remarque}

\begin{exemple}
On dispose d'un dé non truqué qu'on lance une fois. On note $X$ la variable aléatoire qui vaut $5$ si on tombe sur $6$, $1$ si on tombe sur $5$ ou $1$, et $-3$ sinon. Démontrer que $X$ admet une espérance, et déterminer $\esperance(X)$.
\end{exemple}

\ifprof
\begin{soluce}
Alors, $X(\Omega)=\{-3,1,5\}$ est fini, donc possède une espérance, et on a
$$\esperance(X)=-3.\frac{3}{6}+1.\frac{2}{6}+5.\frac{1}{6}=-\frac{1}{3}$$
\end{soluce}
\else
\lignes{3}
\fi

\begin{propriete}
Si $X(\Omega) \subset [p,q]$ et si $X$ admet une espérance, alors $p\leq \esperance(X)\leq q$.\\ En particulier, si $X$ est positive, $\esperance(X)\geq 0$, et $\esperance(X)=0$ si et seulement si $X$ est nulle presque sûrement.
\end{propriete}

\preuve[10]{
Si $X(\Omega)\subset [p,q]$ et si $X(\Omega)=\{x_i, i\in I\}$, alors pour tout $i\in I$, $$p\leq x_i \leq q \Rightarrow p\PP(X=x_i)\leq x_i\PP(X=x_i)\leq q\PP(X=x_i)$$
les probabilités étant positives. En additionnant,
$$\sum_{i\in I} p\PP(X=x_i)\leq \esperance(X) \leq \sum_{i \in I} q\PP(X=x_i)$$
et on peut conclure car $\sum\limits_{i\in I} \PP(X=x_i)=1$.

Si $\esperance(X)=0$, puisque $X$ est positive, cela implique que tous les termes de la série sont nuls : pour tout $x\in X(\Omega)$, $\PP(X=x)0$. Ainsi, si $x\neq 0$, $\PP(X=x)=0$ et si $X=0$, $\PP(X=0)=1-\sum\limits_{x\in X(\Omega)\setminus\{0\}} \PP(X=x) = 1$.
}

\begin{proposition}
Soient $X$ et $Y$ deux variables aléatoires définies sur $(\Omega, \AA)$, telles que $Y(\Omega)\in \R+$ et $|X|\leq Y$.

Si $Y$ admet une espérance, alors $X$ admet une espérance, et $\left|\esperance(X)\right|\leq \esperance(Y)$.
\end{proposition}

\begin{demonstration}
	Résultat admis.
\end{demonstration}

\begin{propriete}
[Linéarité de l'espérance] Soient $X$ et $Y$ deux variables aléatoires discrètes sur $(\Omega, \AA, \PP)$ admettant une espérance. Soient $a$ et $b$ deux réels.
\begin{itemize}
  \item la variable aléatoire $aX+b$ admet une espérance, et $\esperance(aX+b)=a\esperance(X)+b$.
  \item la variable aléatoire $X+Y$ admet une espérance, et $\esperance(X+Y)=\esperance(X)+\esperance(Y)$.
\end{itemize}
\end{propriete}

\preuve[10]{Supposons $a \neq 0$ (sinon c'est immédiat car $\esperance(b)=b)$. Notons $Y=a X+b$. On a $Y(\Omega)=\{a x+b,\, x \in X(\Omega)\}$ et
pour tout $x \in X(\Omega)$,
\[ (a x+b) \PP(Y=a x+b)=a x \PP(Y=a x+b)+b \PP(Y=a x+b)=a x \PP(X=x)+b \PP(X=x).\]

Par hypothèse la série de terme général $a x \PP(X=x)$ converge absolument (car $X$ admet une espérance). De plus la série de terme général $b \PP(X=x)$ converge absolument (car $\sum \PP(X=x)$ converge et vaut 1$)$. Or, par inégalité triangulaire,
\[
\forall x \in X(\Omega), \quad|a x+b| \PP(Y=a x+b) \leqslant|a||x| \PP(X=x)+|b| \PP(X=x)
\]
Puisque $\sum(|a||x| \PP(X=x)+|b| \PP(X=x))$ converge, par théorème de comparaison des séries positives, $\sum|a x+b| \PP(Y=a x+b)$ converge. Autrement dit $a X+b$ admet une espérance et
\[
\esperance(Y)=\sum_{x \in X(\Omega)}(a x+b) \PP(Y=a x+b)=a \underbrace{\sum_{x \in X(\Omega)} x \PP(X=x)}_{=\esperance(X)}+b \underbrace{\sum_{x \in X(\Omega)} \PP(X=x)}_{=1}=a \esperance(X)+b .
\]

Le deuxième point est admis.
}

\begin{remarque}
	En combinant les deux résultats, on obtient le résultat général : si $X$ et $Y$ admettent des espérances, alors $\esperance(aX+bY) = a\esperance(X)+b\esperance(Y)$ pour tous réels $a$ et $b$.
\end{remarque}

\begin{definition}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA, \PP)$.
\begin{itemize}
    \item[$\bullet$] On dit que $X$ est \textbf{centrée} si $\esperance(X)=0$.
    \item[$\bullet$] Si elle n'est pas centrée, la variable aléatoire $Y=X-\esperance(X)$ est centrée, et est appelée \textbf{variable aléatoire centrée associée} à $X$.
\end{itemize}
\end{definition}

\ifprof
\begin{demonstration}
En effet, si $Y=X-\esperance(X)$, alors $\esperance(Y)=\esperance(X)-\esperance(X)=0$ en utilisant la linéarité de l'espérance ($a=1$ et $b=-\esperance(X)\in \R$).
\end{demonstration}
\else
\lignes{3}
\fi

\subsection{Formule de transfert}

\begin{theoreme}[Formule de transfert]
\labelobj{2}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA, \PP)$. On écrit $X(\Omega)=\{x_i, i\in I\}$. Soit $g$ une fonction définie sur $\R$. Si $\displaystyle{\sum_{i\in I} g(x_i)\PP(X=x_i)}$ est absolument convergente, alors la variable aléatoire $g(X)$ admet une espérance, et
$$\esperance(g(X))=\sum_{i\in I} g(x_i)\PP(X=x_i)$$
Si $X(\Omega)=\N$, on a ainsi
$$\esperance(g(X)) = \sum_{n=0}^{+\infty} g(n)\PP(X=n)$$
\end{theoreme}

\begin{exemple}
[Exemple fondamental] Si on prend la fonction $g:x\mapsto x^2$, si la série $\displaystyle{\sum_{i\in I} x_i^2\PP(X=x_i)}$ est absolument convergente, alors $X^2$ admet une espérance, et
$$\esperance(X^2)=\sum_{i\in I} x_i^2 \PP(X=x_i)$$
Si $X(\Omega)=\N$, on a ainsi
$$\esperance(X^2)=\sum_{n=0}^{+\infty} n^2\PP(X=n)$$
\end{exemple}

\subsection{Moment d'ordre $r$}

\begin{definition}
Soit $X$ une variable aléatoire discèret sur $(\Omega, \AA, \PP)$. Soit $r\in \N^*$. Si la série $\sum\limits_{x\in X(\Omega)} x^3\PP(X=x)$, on appelle \textbf{moment d'ordre $r$} de $X$ le nombre
\[ m_r(X)=\sum_{x\in X(\Omega)} x^r \PP(X=x)\]
\end{definition}

\begin{remarque}
\begin{itemize}
  \item Les variables aléatoires finies possèdent des moments à tout ordre.
	\item D'après le théorème de transfert, $m_R(X)=\esperance(X^r)$.
\end{itemize}
\end{remarque}

\begin{proposition}
Si $X$ admet un moment d'ordre $r\in \N*$, alors elle admet des moments d'ordre $p$ avec $p\leq r$.
\end{proposition}

\preuve[7]{Supposons que $X$ admette un moment d'ordre $r \in \N*$. Soit $k \in \interent{1 r}$. Si $x \in X(\Omega)$, alors
\begin{itemize}
\item ou bien $|x| \leq 1$ et alors $|x|^{k} \leq 1$,
\item ou bien $|x|>1$ et alors $|x|^{k}<|x|^{r}$.
\end{itemize}
Dans tous les cas, $|x|^{k} \leq 1+|x|^{r}$. Ainsi $|x|^{k} \mathbb{P}(X=x) \leqslant\left(1+|x|^{r}\right) \mathbb{P}(X=x)$ pour tout $x \in X(\Omega)$. Puisque la série de terme général $\left(1+|x|^{r}\right) \mathbb{P}(X=x)$ converge absolument, nous en déduisons que la série de terme général $|x|^{k} \mathbb{P}(X=x)$ aussi, par comparaison de séries à termes positifs. Ainsi $X$ admet un moment d'ordre $k$.
}

\subsection{Variance}

	\subsubsection{Définition}

\begin{definition}
\labelobj{3}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA, \PP)$. Si $X$ admet une espérance, et si $(X-\esperance(X))^2$ admet également une espérance, on appelle
\textbf{variance} de $X$ le nombre mesurant l'écart entre $X$ et son espérance :
    $$\Var(X)=\esperance[ (X-\esperance(X))^2 ].$$
\end{definition}

\begin{remarque}
	D'après la formule du transfert, si $X$ admet une variance, alors \[ \Var(X) = \sum_{x\in X(\Omega)} \left(x-\esperance(X)\right)^2 \PP(X=x). \]
\end{remarque}

\begin{remarque}
Une variable aléatoire discrète peut admettre une espérance sans admettre un moment d'ordre $2$.
\end{remarque}


\begin{theoreme}[Formule de Koenig-Huygens]
\labelobj{4}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA, \PP)$.  $X$ admet un moment d'ordre $2$ si et seulement si $X$ admet une variance, et dans ce cas
$$\Var(X)=\esperance(X^2)-(\esperance(X))^2$$
\end{theoreme}

\ifprof
\begin{demonstration}
On suppose que $X$ admet une espérance. Constatons que $$(X-\esperance(X))^2 = X^2-2X\esperance(X)+(\esperance(X))^2$$ Par linéarité de l'espérance, on remarque, sous réserve d'existence, que $$\esperance[ X^2-2X\esperance(X)+(\esperance(X))^2]=\esperance(X^2)-2\esperance(X)\esperance(X)+(\esperance(X))^2=\esperance(X^2)-\esperance(X)^2$$
Ainsi
$\esperance[(X-\esperance(X))^2]$ existe si et seulement si $\esperance(X^2)$ existe, et dans ce cas, toujours par linéarité de l'espérance,
$$\Var(X)=\esperance[(X-\esperance(X))^2]=\esperance(X^2)-(\esperance(X))^2$$
\end{demonstration}
\else
\lignes{12}
\fi

\begin{experiencehistorique}
\begin{tabular}{p{1cm}p{12cm}p{1cm}}
\lien{https://bit.ly/3kEIVue}
& Cette formule est due à \textbf{Johann Samuel König} (1712--1757), mathématicien allemand, et \textbf{Christian Huygens} (1629--1695), mathématicien néerlandais. &
\lien{https://fr.wikipedia.org/wiki/Christian_Huygens}
\end{tabular}
\end{experiencehistorique}

	\subsubsection{Propriétés}

\begin{proposition}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA,\PP)$  admettant une variance. Alors $\Var(X)\geq 0$ et $\Var(X)=0$ si et seulement si $X$ suit une loi certaine.
\end{proposition}

\preuve[5]{
Notons $m = \esperance(X)$. Puisque $(X-m)^2$ est une variable aléatoire positive admettant une espérance, par positivité de l'espérance, $\Var(X) = \esperance((X-m)^2) \geq 0$.

Si $\Var(X) = 0$, alors $(X-m)^2$ est d'espérance nulle, et donc $(X-m)^2$ est une variable aléatoire nulle presque sûrement, c'est-à-dire $P(X =m)=1$ : la variable aléatoire suit une loi certaine égale à $m$.
}

Cela nous permet de définir l'écart-type :

\begin{definition}
	Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA)$ possédant une variance. On appelle \textbf{écart-type} de $X$, et on note $\sigma(X)$, le nombre $\sigma=\sqrt{\Var(X)}$.
\end{definition}

Enfin, on dispose d'une propriété de transfert :

\begin{propriete}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA, \PP)$ admettant une variance. Alors, pour tous réels $a$ et $b$, $aX+b$ admet une variance, et
$$\Var(aX+b)=a^2\Var(X)$$
\end{propriete}

\ifprof
\begin{demonstration}
On utilise la formule de Koenig-Huygens. Constatons que $(aX+b)^2=a^2X^2+2abX+b^2$ et $\esperance(aX+b)=a\esperance(X)+b$. Donc $aX+b$ admet une variance si $X$ en admet une, et
$$\Var(aX+b)=\esperance[(aX+b)^2]-(\esperance(aX+b))^2=a^2\esperance[X^2]+2ab\esperance[X]+b^2-a^2\esperance(X)^2-2ab\esperance(X)-b^2$$
Donc
$$\Var(aX+b)=a^2(\esperance(X^2)-\esperance(X)^2)=a^2\Var(X)$$
\end{demonstration}
\else
\lignes{7}
\fi

\begin{attention}
	Contrairement à l'epérance, la variance {n'est pas linéaire.}
\end{attention}

\begin{definition}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \AA, \PP)$.
 \begin{itemize}
     \item[$\bullet$] On dit que $X$ est \textbf{réduite} si $\sigma(X)=\Var(X)=1$.
     \item[$\bullet$] Si $X$ admet une variance, et que $\sigma(X)\neq 0$, la variable aléatoire
     $$Y=\frac{X-\esperance(X)}{\sigma(X)}$$ est centrée et réduite, et est appelée \textbf{variable centrée réduite associée} à $X$. On la note en général $X^*$.
 \end{itemize}
\end{definition}


 \afaire{Exercices \lienexo{01}, \lienexo{02}, \lienexo{03} et \lienexo{04}.}

	\subsection{Indépendance de variables aléatoires}

Une propriété intéressante losqu'on s'intéresse aux variables aléatoires, est de savoir si elles sont indépendantes :

\begin{definition}[Indépendance mutuelle]
Soient $(X_1,\hdots, X_n)$ des variables aléatoires définies sur $(\Omega, \AA, \PP)$.

On dit que $(X_1,\hdots, X_n)$ sont \textbf{mutuellement indépendantes} si, pour tout $(x_1,\hdots, x_n) \in X_1(\Omega)\times \hdots \times X_n(\Omega)$,
\[ \PP\left( \bigcap_{i=1}^n\,[X_i=x_i] \right)= \prod_{i=1}^n \PP(X_i=x_i). \]
\end{definition}

\section{Lois usuelles finies}

On rappelle les lois finies vues dans le chapitre $10$, qui sont valables sur un espace probabilisé quelconque :

\subsection{Loi certaine}
\labelobj{5}

\begin{definition}
On dit que la variable aléatoire $X$ suit la \textbf{loi certaine} si et seulement s'il existe un réel $a$ tel que l'événement $(X=a)$ soit presque sûr. Ainsi
$$X(\Omega)=\{a\} \textrm{  et  } \PP(X=A)=1$$
On dit également que $X$ est une variable aléatoire certaine.
\end{definition}

\begin{exemple}
Une urne contient $n$ boules, indiscernables au toucher, de couleurs différentes et portant toutes le numéro $2$ et on tire une boule. On note $X$ le numéro tiré. Alors $X$ est une variable aléatoire suivant la loi certaine, et $(X=2)$ est un événement certain.
\end{exemple}

\begin{theoreme}
Soit $a \in \R$ et $X$ une variable aléatoire suivant la loi certaine égale à $a$. Alors
$$\esperance(X)=a \textrm{ et } \Var(X) = 0$$
\end{theoreme}


\begin{remarque}
Nous avons une réciproque au théorème précédent : si $X$ est une variable aléatoire discrète telle que $\Var(X)=0$ alors $X$ suit une loi certaine.
\end{remarque}

\subsection{Loi uniforme}
\labelobj{6}

\begin{definition}
Soit $n \in \N^*$. On dit que la variable aléatoire $X$ suit la \textbf{loi uniforme} sur $\llbracket 1,n \rrbracket$ si
$$X(\Omega) = \llbracket 1,n \rrbracket \qeq \forall~k \in \llbracket 1,n \rrbracket,~\PP(X=k)=\frac{1}{n}$$
On note alors $X \suit \mathcal{U}(\llbracket 1,n\rrbracket)$.
\end{definition}

\begin{exemple}
On dispose d'un dé à six faces bien équilibré qu'on lance une fois. On note $X$ le chiffre obtenu après le lancer. Alors $X$ suit la loi uniforme sur $\llbracket 1,6 \rrbracket$ : $X \suit \mathcal{U}(\llbracket 1,6 \rrbracket)$.
\end{exemple}

\begin{theoreme}
Soit $n \in \N^*$ et $X$ une variable aléatoire suivant la loi uniforme sur $\llbracket 1,n \rrbracket$. Alors
$$\esperance(X) = \frac{n+1}{2} \qeq \Var(X) = \frac{n^2-1}{12}$$
\end{theoreme}



\begin{remarque}
On peut également définir la loi uniforme sur $\llbracket a,b \rrbracket$ : pour tout $k \in \llbracket a,b \rrbracket$, $$\PP(X=k)= \frac{1}{b-a+1}$$
On note $X \suit \mathcal{U}(\llbracket a,b \rrbracket)$. Dans ce cas, on peut montrer que
$$\esperance(X)=\frac{a+b}{2} \textrm{  et  } \Var(X)=\frac{(b-a)(b-a+2)}{12}$$
Ces formules étant à démontrer systématiquement, en remarquant que si $X\suit \mathcal{U}(\llbracket a;b\rrbracket])$ alors $$X-a+1 \suit \mathcal{U}(\llbracket 1; b-a+1 \rrbracket)$$
\end{remarque}


\subsection{Loi de Bernoulli}
\labelobj{7}

\begin{definition}
Soit $p\in ]0,1[$. On dit que la variable aléatoire $X$ suit la \textbf{loi de Bernoulli} de paramètre $p$ si
$$X(\Omega)=\{0,1\} \textrm{  et  } \PP(X=1)=p ~~~~\PP(X=0)=q=1-p$$
On note alors $X \suit \mathcal{B}(p)$.
\end{definition}

\begin{exemple}
On dispose d'une pièce truquée, dont la probabilité d'obtenir Pile est de $\frac{1}{3}$. Soit $X$ la variable aléatoire valant $1$ si on fait Pile, et $0$ sinon. Alors $X\suit \mathcal{B}\left(\frac{1}{3}\right)$.
\end{exemple}

\begin{theoreme}
Soit $p\in]0,1[$ et $X$ une variable aléatoire suivant la loi de Bernoulli de paramètre $p$. Alors
$$\esperance(X)=p \textrm{  et  } \Var(X)=pq=p(1-p)$$
\end{theoreme}


\subsection{Loi binomiale}
\labelobj{8}

\begin{definition}
Soient $n\in \N^*$ et $p\in ]0,1[$. On dit que la variable aléatoire $X$ suit la \textbf{loi binomiale} de paramètres $(n,p)$ si
$$X(\Omega)=\llbracket 0,n \rrbracket \textrm{  et  } \forall~k\in \llbracket 0,n \rrbracket,~\PP(X=k)=\binom{n}{k}p^k(1-p)^{n-k}$$
On note alors $X\suit \mathcal{B}(n,p)$.
\end{definition}

\begin{exemple}
[Exemple fondamental] On répète $n$ fois de manière indépendante une expérience ayant deux issues possibles : le succès de probabilité $p$, et l'échec de probabilité $1-p$. On dit alors qu'on dispose d'un schéma de Bernoulli. La variable aléatoire $X$ comptant le nombre de succès obtenus suit une loi binomiale de paramètre $(n,p)$.
\end{exemple}

\begin{remarque}
\begin{itemize}
  \item Il s'agit bien d'une loi de probabilité. En effet, en utilisant la formule du binôme de Newton,
$$\sum_{k=0}^n \PP(X=k) = \sum_{k=0}^n \binom{n}{k} p^k (1-p)^{n-k} = (p+(1-p))^n = 1$$
  \item Dans le cas $n=1$, on retrouve la loi de Bernoulli de paramètre $p$. Ainsi, $\mathcal{B}(1,p)$ représente également la loi de Bernoulli de paramètre $p$.
\end{itemize}
\end{remarque}


\begin{theoreme}
Soient $n\in \N^*$ et $p\in]0,1[$. Soit $X$ une variable aléatoire suivant la loi binomiale de paramètre $(n,p)$. Alors
$$\esperance(X)=np \textrm{  et  } \Var(X)=np(1-p)$$
\end{theoreme}

\section{Lois usuelles infinies}

\subsection{Loi géométrique}
\labelobj{9}

\begin{definition}
Soit $p\in ]0,1[$. On dit que la variable aléatoire $X$ suit la \textbf{loi géométrique} de paramètre $p$ si
$$X(\Omega) = \N^* \textrm{  et  } \forall~n\in \N^*,~\PP(X=n)=p(1-p)^{n-1}$$
On note alors $X\suit \mathcal{G}(p)$. $p$ est appelé \textbf{probabilité du succès}.
\end{definition}

\begin{remarque}
Il s'agit bien d'une loi de probabilité. En effet, pour tout $n\geq 1$, $\PP(X=n)\geq 0$ et on a
$$\sum_{k=1}^n p(1-p)^{k-1}=p\sum_{k=0}^{n-1} (1-p)^k$$
Ainsi, en reconnaissant une série géométrique de raison $1-p$ avec $-1<1-p<1$ :
$$\lim_{n\rightarrow +\infty} p\sum_{k=0}^{n-1} (1-p)^k = p \frac{1}{1-(1-p)} = 1$$
\end{remarque}

\begin{exemple}
On dispose d'une pièce truquée, dont la probabilité d'obtenir Face est $p$. On effectue des lancers indépendants, jusqu'à l'obtention d'un Face. On note $X$ le nombre de lancers effectués avant d'obtenir Face : on dit que $X$ est le \textbf{temps d'attente} du premier Face. Alors $X \suit \mathcal{G}(p)$.
\end{exemple}

\begin{proposition}
La variable aléatoire comptant le nombre d'épreuves de Bernoulli de paramètre $p$ répétées indépendamment nécessaires pour obtenir un premier succès suit une loi géométrique de paramètre $p$.
\end{proposition}

\preuve[10]{
On note $X$ la variable aléatoire comptant le nombre d'épreuves nécessaires pour obtenir le premier succès (si on n'obtient que des échecs, on pose $X=+\infty$ ).

Pour tout $j \in \mathbb{N}$, notons $S_{j}$ l'événement \og{}on obtient un succès lors de la $j$-ième épreuve\fg{}. Alors
\[
\forall k \in \mathbb{N} \backslash\{0,1\}, \quad \mathbb{P}(X=k)=\mathbb{P}\left(\left(\bigcap_{j=1}^{k-1} \bar{S}_{j}\right) \cap S_{k}\right)=\left(\prod_{j=1}^{k-1} \mathbb{P}\left(\bar{S}_{j}\right)\right) \times \mathbb{P}\left(S_{k}\right)=(1-p)^{k-1} p
\]
puisque les épreuves sont indépendantes et identiques. Enfin
\[
\mathbb{P}(X=+\infty)=1-\sum_{k=1}^{+\infty} \mathbb{P}(X=k)=1-\sum_{k=1}^{+\infty}(1-p)^{k-1} p=1-p \sum_{j=0}^{+\infty}(1-p)^{j}=1-p \frac{1}{1-(1-p)}=0
\]
}

\begin{proposition}
	Soit $p \in \interoo{0 1}$. Soit $X \suit \mathcal{G}(p)$. Alors, pour tout $t \in \R$,
\[
F_{X}(t)=\PP(X \leq t)=\left\{\begin{array}{lll}
0 & \text { si } & t<1 \\
1-(1-p)^{k} & \text { si } & t \in \interfo{k k+1}\quad \text { avec } k \in \N*
\end{array}\right.
\]
\end{proposition}

\preuve[5]{Si $k \in \N*$, $\PP(X \leq k)=1-\PP(X>k)$ et
\[
\PP(X>k)=\sum_{j=k+1}^{+\infty}(1-p)^{j-1} p=p \sum_{\ell=k}^{+\infty}(1-p)^{\ell}=p \frac{(1-p)^{k}}{1-(1-p)}=(1-p)^{k}
\]
}

\begin{theoreme}
Soit $p\in ]0,1[$ et $X$ une variable aléatoire suivant la loi géométrique de paramètre $p$. Alors
$$\esperance(X)=\frac{1}{p} \textrm{  et  } \Var(X)=\frac{1-p}{p^2}$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition, et en utilisant les séries géométriques et géométriques dérivées ($|1-p|<1$) :
\begin{align*}
\esperance(X)&=\sum_{k=1}^{+\infty} k\PP(X=k)\\
&=\sum_{k=1}^{+\infty} kp(1-p)^{k-1} \\
&=p\sum_{k=1}^{+\infty} k(1-p)^{k-1} \\
&= p \frac{1}{(1-(1-p))^2} = \frac{1}{p}  \text{ en reconnaissant la série géométrique dérivée}
\end{align*}
et
$$\Var(X)=\esperance(X^2)-\esperance(X)^2= \sum_{k=1}^{+\infty} k^2p(1-p)^{k-1}-\frac{1}{p^2}$$
Or, en utilisant les méthodes classiques sur les séries géométriques :
\begin{align*}
\sum_{k=1}^{+\infty} k^2p(1-p)^{k-1}&=p\sum_{k=1}^{+\infty} (k(k-1)+k)(1-p)^{k-1}\\
&=\sum_{k=1}^{+\infty} k(k-1)p(1-p)^{k-1}+\sum_{k=1}^{+\infty} kp(1-p)^{k-1}\\&=p\frac{2(1-p)}{(1-(1-p))^3}+p\frac{1}{(1-(1-p))^2}=\frac{2-p}{p^2}
\end{align*}
et donc
$$\Var(X)=\frac{2-p}{p^2}-\frac{1}{p^2}=\frac{1-p}{p^2}$$
\end{demonstration}
\else
\lignes{22}
\fi

\begin{remarque}
On peut simuler une loi géométrique de paramètre $p$ avec \textsc{np.random}.
\end{remarque}

\begin{pythonfile}[.95]{geometrique.py}
\end{pythonfile}

Ce qui donne, après exécution sur \textsc{Python}, le graphique suivant.

\vspace*{-.5cm}\begin{center}
\includegraphics{loi_geometrique.eps}
\end{center}

On peut également le simuler à la main, on utilisant un nombre aléatoire entre $0$ et $1$ :

\begin{pythonfile}{geometrique_main.py}
\end{pythonfile}

et on obtient

\vspace*{-.5cm}\begin{center}
	\includegraphics{loi_geometrique_main.eps}
	\end{center}

\subsection{Loi de Poisson}
\labelobj{10}



\begin{definition}
Soit $\lambda>0$. On dit que la variable aléatoire $X$ suit la \textbf{loi de Poisson} de paramètre $\lambda$, si
$$X(\Omega)=\N \textrm{  et  } \forall~n\in \N,~\PP(X=n)=\frac{\lambda^n}{n!} \eu{-\lambda}$$
On note alors $X \suit \mathcal{P}(\lambda)$.
\end{definition}

\begin{remarque}
Il s'agit bien d'une loi de probabilité. En effet, pour tout $n$, $P(X=n)\geq 0$ et on a
$$\sum_{n=0}^{+\infty} \frac{\lambda^n}{n!}\eu{-\lambda} = \eu{-\lambda}\sum_{n=0}^{+\infty} \frac{\lambda^n}{n!}=\eu{-\lambda}\eu{\lambda}=1$$
\end{remarque}

\begin{exemple}
Une usine produit des bouteilles d'eau. Parmi celles-ci, $3\%$ sont défectueuses. On tire au hasard $100$ bouteilles, et on note $X$ le nombre de bouteilles défectueuses. On admet que $X$ suit une loi de Poisson. Alors $X\suit \mathcal{P}(0,03*100=3)$.
\end{exemple}

\begin{remarque}
[Lien avec la loi binomiale] Dans l'exemple précédent, on aurait tout aussi bien pu utiliser une loi binomiale de paramètres $(100, 0,03)$.
\end{remarque}

\begin{theoreme}
Par approximation classique, si $X$ suit une loi binomiale de paramètre $(n,p)$, et si on a $$n\geq 50, p\leq 0,1,~\lambda=np\leq 10$$ alors $X$ peut être approchée par la loi de Poisson de paramètre $\lambda=np$.
\end{theoreme}

\begin{remarque}
On verra à la fin de l'année que si, pour tout $n\geq 1$, $X_n\suit \mathcal{B}(n, \frac{\lambda}{n})$ , alors quand $n$ tend vers l'infini, la suite $(X_n)$ \textbf{converge en loi} vers une loi de Poisson de paramètre $\lambda$.
\end{remarque}

\begin{theoreme}
Soit $\lambda>0$ et $X$ une variable aléatoire suivant la loi de Poisson de paramètre $\lambda$. Alors
$$\esperance(X)=\lambda \textrm{  et  } \Var(X)=\lambda$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition, et en reconnaissant la série de l'exponentielle,
$$\esperance(X)=\sum_{n=0}^{+\infty} n\frac{\lambda^n}{n!}\eu{-\lambda}=\eu{-\lambda} \sum_{n=1}^{+\infty} \frac{\lambda^n}{(n-1)!}=\eu{-\lambda}\lambda \sum_{n=1}^{+\infty} \frac{\lambda^{n-1}}{(n-1)!}=\eu{-\lambda} (\lambda \eu{\lambda})=\lambda$$
La variance se fait selon la même idée, en utilisant la série dérivée de l'exponentielle.
\end{demonstration}
\else
\lignes{25}
\fi

\begin{pythonfile}[.95]{poisson.py}
\end{pythonfile}

Ce qui donne, après exécution sur \textsc{Python}, le graphique suivant.

\begin{center}
\includegraphics{loi_poisson.eps}
\end{center}

 \afaire{Exercices \lienexo{11}, \lienexo{12}, \lienexo{13} et \lienexo{17}}


%%
% Retour à l'origine
\allowdisplaybreaks[0]
%%

%%% Fin du cours %%%
