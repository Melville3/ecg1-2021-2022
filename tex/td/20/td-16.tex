%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{2}[Dérivation d'une intégrale]
Soit $f$ la fonction définie sur $\R$ par
\[ f (x) = \int_0^1 \sin(xt^2)\dd t. \]
\begin{enumerate}
		\item Montrer que $f$ est bien définie sur $\R$.
		\item \begin{enumerate}
			\item Montrer que, pour tous $t\in \interff{0 1}$, et tous réels $x$ et $y$, \[ |\sin(yt^2)-\sin(xt^2)| \leq |y-x|. \]
			\item En déduire que $f$ est continue sur $\R$.
		\end{enumerate}
		\item \begin{enumerate}
			\item Montrer que, pour tout $t\in \interff{0 1}$, et tous réels $x$ et $y$, on a \[ \left|\,\sin(yt^2)-\sin(xt^2)-(y-x)t^2\cos(xt^2)\,\right|\leq \frac{1}{2}|y-x|^2.\]
			\item En déduire que $f$ est dérivable sur $\R$ et que, pour tout $x\in \R$, \[ f'(x) = \int_0^1 t^2\cos(xt^2)\dd t.\]
		\end{enumerate}
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Soit $x\in \R$ fixé. La fonction $t\donne \sin(xt^2)$ est une fonction continue sur $\interff{0 1}$, comme composée de deux fonctions continues. L'intégrale $f(x)$ a donc un sens. \\Ainsi, $f$ est définie sur $\R$.
	\item \begin{enumerate}
	\item On fixe $t\in \interff{0 1}$. La fonction $g_t:x\mapsto \sin(yt^2)$ est de classe $\CC^1$ sur $\R$. L'inégalité des accroissements finis appliquée entre $x$ et $y$ donne
	\[ \left |g_t(y)-g_t(x)\right| \leq |y-x|\times \max_{\interff{x y}} |g_t'| . \]
	Or $g'_t : x\mapsto t^2 \cos(xt^2)$ et pour tout $t\in \interff{0 1}$ et $x\in \R$,
	\[ | g'_t(x)| \leq t^2|\cos(xt^2)| \leq 1. \]
	Finalement, pour tous réels $x$ et $y$ :
	\[ \left | \sin(yt^2)-\sin(xt^2)\right| \leq |y-x|. \]
	\item Soit $x\in \R$ fixé. Pour tout réel $y$, on a :
	{\allowdisplaybreaks
	\begin{align*}
			\left |f(y)-f(x)\right| &= \left | \int_0^1 (\sin(yt^2)-\sin(xt^2))\D t\right| \text{ par linéarité}\\
			&\leq \int_0^1 \left | \sin(yt^2)-\sin(xt^2)\right|\D t \text{ par inégalité triangulaire}\\
			&\leq \int_0^1 \left | y-x\right| \D t \text{ d'après le résultat précédent}\\
			&\leq |y-x|
	\end{align*}}
	Lorsque $y$ tend vers $x$, $|y-x|\tendversen{y\to x}0$. Par encadrement, on en déduit que \[ \lim_{y\to x} |f(y)-f(x)|=0 \implies \lim_{y\to x} f(y)=f(x). \]
	Ainsi, $f$ est continue en $x$. Ceci étant vrai pour tout réel $x$, $f$ est continue sur $\R$.
\end{enumerate}
\item \begin{enumerate}
	\item Soit $t$ fixé dans $\interff{0 1}$. L'écriture nous donne envie d'appliquer l'inégalité de Taylor-Lagrange entre $x$ et $y$ à la fonction $g_t$ à l'ordre $1$.
	{\allowdisplaybreaks
	\begin{align*}
		\left | g_t(y) - \left( g_t(x) + g'_t(x)(y-x)\right)\right| &\leq \frac{|y-x|^2}{2!} \max_{\interff{x y}} |g_t^{(2)}|
	\end{align*}
	soit
	\begin{align*}
		\left | \sin(yt^2) - \left( \sin(xt^2) + t^2\cos(xt^2)(y-x)\right)\right| &\leq \frac{|y-x|^2}{2!} \max_{\interff{x y}} |g_t^{(2)}|
	\end{align*}
	}
	Or, pour tout $t\in \interff{0 1}$ et pour tout réel $x$,  $g_t^{(2)}(x) = -t^4\sin(xt^2)$ et donc
	\[ \left | g_t^{(2)}(x)\right | \leq t^4|\sin(xt^2)| \leq t^4\leq 1. \]
	Finalement,
	\begin{align*}
		\left | \sin(yt^2) - \sin(xt^2) - t^2\cos(xt^2)(y-x) \right| &\leq \frac{|y-x|^2}{2}
	\end{align*}
	\item On se fixe $x\in \R$ et on calcule le taux d'accroissement. Pour tout $y\neq x$ :
	\begin{align*}
		\frac{f(y)-f(x)}{y-x} &=  \int_0^1 \frac{\sin(yt^2)-\sin(xt^2)}{y-x}\D t\\
		       &= \int_0^1 \frac{\sin(yt^2)-\sin(xt^2)-(y-x)t^2\cos(xt^2)+(y-x)t^2\cos(xt^2)}{y-x}\D t\\
					 &= \int_0^1 \frac{\sin(yt^2)-\sin(xt^2)-(y-x)t^2\cos(xt^2)}{y-x}\D t + \int_0^1 t^2\cos(xt^2)\D t
	\end{align*}
	D'après le point précédent :
	\begin{align*}
		\left | \int_0^1 \frac{\sin(yt^2)-\sin(xt^2)-(y-x)t^2\cos(xt^2)}{y-x}\D t\right | &\leq \int_0^1 \left|\frac{\sin(yt^2)-\sin(xt^2)-(y-x)t^2\cos(xt^2)}{y-x}\right|\D t\\
		&\leq \int_0^1 \frac{|y-x|^2}{|y-x|}\D t = |y-x|\tendversen{y\to x} 0
	\end{align*}
	Par encadrement,
	\[ \int_0^1 \frac{\sin(yt^2)-\sin(xt^2)-(y-x)t^2\cos(xt^2)}{y-x}\D t \tendversen{y\to x} 0 \]
	et donc
	\[ \frac{f(y)-f(x)}{y-x} \tendversen{y\to x} \int_0^1 t^2\cos(xt^2)\D t. \]
	Ainsi, $f$ est dérivable en $x$, et puisque cela est vrai pour tout réel $x$, on peut conclure que $f$ est dérivable sur $\R$ et \[ \boxed{\forall x\in \R,\quad f'(x)=\int_0^1 t^2\cos(xt^2)\D t.}\]
\end{enumerate}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
