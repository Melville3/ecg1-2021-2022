\begin{exoApp}{20}{2}[Inégalité des accroissements finis et suite - II]
On considère la fonction $h$ définie sur $\R$ par $h(x) = \eu{-x} + 1$. On définit la suite $(u_n)_{n \in \N}$ par :
\[u_0 = 1 \quad \text{ et }  \forall n \in \N,\quad u_{n+1} = h(u_n).\]
\begin{enumerate}
\item Étudier les variations de $h$ et montrer que $h(\interff{1 2}) \subset \interff{1 2}$ (on pourra utiliser $h(1) \approx 1,4$ et $h(2) \approx 1,1$).
\item Montrer que l'équation $h(x) = x$ admet une unique solution sur l'intervalle $\interff{1 2}$ notée $\alpha$.
\item Montrer que $\forall n \in \N$, \quad $1 \leq u_n \leq 2$.
\item Montrer que, pour tout réel $x \in \interff{1 2}$, $\ds{|h'(x)| \leq  \frac{1}{\E}}$.
\item Montrer que pour tout $n \in \N$, \quad $\ds{|u_{n+1}-\alpha| \leq \frac{1}{\E}|u_n-\alpha|}$.
\item En déduire que pour tout $n \in \N$, \[|u_n - \alpha| \leq \frac{1}{\E^n}.\]
\item Montrer que la suite $(u_n)_{n \in \N}$ converge et préciser sa limite.
\end{enumerate}

\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item $h$ est dérivable sur $\R$ comme somme d'une exponentielle et d'une fonction constante. On a, pour tout réel $x$ \[ h'(x)=-\eu{-x} < 0 \]
	De plus, par composée, on a les limites suivantes :
	\[ \lim_{x\to -\infty} h(x)=+\infty \qeq \lim_{x\to +\infty} h(x)=1 \]
	On obtient alors le tableau suivant :
	\begin{center}\begin{tikzpicture}[yscale=0.7]
         \tkzTabInit{$x$ / 1 ,  $h'(x)$ / 1, $h$ / 2}{$-\infty$, $+\infty$}
        \tkzTabLine{,-,}
        \tkzTabVar{+/$+\infty$, - / $1$}
\end{tikzpicture}	\end{center}
	Puisque $h(1)\approx 1,4 \in [1,2]$ et $h(2)\approx 1,1\in[1,2]$, par décroissance de $h$, on en déduit que $h([1,2])\subset [1,2]$.
	\item ~\begin{remarque}Pour montrer qu'une équation du type $f(x)=g(x)$ admet une unique solution, on introduit la fonction différence $h=f-g$, et on applique le théorème de la bijection à celle-ci.\end{remarque}
	Notons $f$ la fonction définie sur $[1,2]$ par $f(x)=h(x)-x$. $f$ est dérivable comme somme de deux fonctions dérivables, et sa dérivée est $h:x\mapsto h'(x)-1=-\eu{-x}-1<0$. \\La fonction $f$ est donc continue (car dérivable) et strictement décroissante sur $[1,2]$. D'après le théorème de la bijection, $f$ établit une bijection de $[1,2]$ sur $f([1,2])$. \\Or $f(1)=h(1)-1\approx 0,4$ et $f(2)=h(2)-2\approx -0,9$. Ainsi, $0\in f([1,2])$ et l'équation $f(x)=0$ admet donc une unique solution sur $[1,2]$, que l'on note $\alpha$.\\L'équation $h(x)=x$ admet  une unique solution sur $[1,2]$.
	\item On utilise le fait que $[1,2]$ soit stable par $h$. Soit $P$ la proposition définie pour tout entier $n$ par $P_n$ : ``$u_n\in [1,2]$''.\\
	$u_0=1\in [1,2]$ donc $P_0$ est vraie.\\
	Supposons la proposition $P_n$ vraie pour un certain entier $n$, et montrons que $P_{n+1}$ est vraie. \\Ainsi, $u_n\in [1,2]$. Puisque $[1,2]$ est stable par $h$, $h(u_n)\in [1,2]$, c'est-à-dire $u_{n+1}\in [1,2]$ : $P_{n+1}$ est vraie.\\D'après le principe de récurrence, la proposition $P_n$ est vraie pour tout entier $n$ :
	\[ \forall~n,\quad u_n\in[1,2] \]
	\item  On rappelle que $h:x\mapsto -\eu{-x}$. Mais alors :
		\begin{align*}
		1\leq x \leq 2 &\text{ donc } -1\geq -x \geq -2 \\
		&\text{ soit } \eu{-1}\geq \eu{-x} \geq \eu{-2} \text{ car $\exp$ est croissante sur $\R$}\\
		&\text{ puis } -\eu{-1}\leq h'(x) \leq -\eu{-2}
		\end{align*}
	Or $-\eu{-2}<0<\frac{1}{\E}$ et donc
	\[ \forall~x\in [1,2],\quad -\frac{1}{\E}\leq h'(x)\leq \frac{1}{\E}\]
	c'est-à-dire $|h'(x)|\leq \frac{1}{\E}$.
	\item $h$ est continue sur $[1,2]$, dérivable sur $]1,2[$ et pour tout $x\in ]1,2[$, $|h'(x)|\leq \frac{1}{\E}$. D'après l'inégalité des accroissements finis :
	\[ \forall~(x,y)\in [1,2]^2,\quad |f(y)-f(x)| \leq \frac{1}{\E} |y-x| \]
	Posons alors $y=u_n\in [1,2]$ (d'après la question $3$) et $x=\alpha\in [1,2]$ (d'après la question $2$). On a alors \[ |f(u_n)-f(\alpha)|\leq \frac{1}{\E}|u_n-\alpha| \]
	Or $f(u_n)=u_{n+1}$ et $f(\alpha)=\alpha$ (d'après question $2$), d'où
	\[ |u_{n+1}-\alpha| \leq \frac{1}{\E}|u_n-\alpha| \]
	\item Soit $Q$ la proposition définie pour tout entier $n$ par $Q_n$ : ``$|u_n-\alpha| \leq \frac{1}{\eu{n}}$''.\\
	Pour $n=0$, $u_0=1$ donc $|u_0-\alpha|=\alpha-1 \in [0,1]$ (car $\alpha \in [1,2]$). Ainsi $|u_0-\alpha \leq 1=\frac{1}{\eu{0}}$ : $Q_0$ est vraie.\\
	Supposons la proposition $Q_n$ vraie pour un certain entier $n$, et montrons que $Q_{n+1}$ est vraie. \\
	Par hypothèse de récurrence, $|u_n-\alpha| \leq \frac{1}{\eu{n}}$. Mais alors
\[
		\frac{1}{\E}|u_n-\alpha| \leq \frac{1}{\E}\frac{1}{\eu{n}}=\frac{1}{\eu{n+1}}
\]	Mais d'après la question précédente, $|u_{n+1}-\alpha|\leq \frac{1}{\E} |u_n-\alpha|$, donc
\[ |u_{n+1}-\alpha|\leq \frac{1}{\E}|u_n-\alpha| \leq \frac{1}{\eu{n+1}} \]
Ainsi, $Q_{n+1}$ est vraie.\\D'après le principe de récurrence, $Q_n$ est vraie pour tout entier $n$ et donc
\[ \forall~n,\quad |u_{n+1}-\alpha|\leq \frac{1}{\eu{n+1}} \]
	\item Remarquons que $\ds{\frac{1}{\eu{n+1}} \tendversen{n\to +\infty} 0}$. D'après le théorème d'encadrement : \[ u_n\tendversen{n\to +\infty} \alpha \]
\end{enumerate}

\end{correction}
