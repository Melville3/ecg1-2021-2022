%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{3}[Formule du crible]
Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. La formule du crible est une généralisation de la formule $\PP(A\cap B)=\PP(A)+\PP(B)-\PP(A\cap B)$ : si $A_1,\hdots, A_n$ sont des événements, alors
\[ \PP\left( \bigcup_{i=1}^n A_i \right) = \sum_{k=1}^n (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\right) \]
\begin{enumerate}
	\item \'Ecrire cette formule pour $n=4$.
	\item En déduire une formule analogue pour $\card(F_1\cup \hdots \cup F_n)$ où $F_1, \hdots, F_n$ sont des parties d'un ensemble fini $E$.
	\item Soit $n$ un entier tel que $n\geq 2$. À l'approche des fêtes de fin d'années, les élèves de la classe, composée de $n$ élèves, ont décidé de s'offrir des cadeaux selon le protocole  suivant: un sac contient les noms de tous les élèves (un morceau de papier par nom). Chacun leur tour, les élèves tirent un nom au hasard parmi les noms restants au moment du tirage. Chaque élève devra alors offrir un cadeau à l'élève dont il a tiré le nom. On cherche à calculer la probabilité qu'un élève ne tire son propre nom.
	\begin{enumerate}
			\item Déterminer un espace probabilisé fini $(\Omega, \partie(\Omega),\PP)$ associé à cette expérience aléatoire. Que vaut $\card(\Omega)$ ?
			\item Pour tout $j\in \interent{1 n}$, on note $A_j$ l'événement \og{}le $j$-ième élève tire son nom\fg{}. Soient $k\in \interent{1 n}$ et $J$ une partie de $\interent{1 n}$ de cardinal $k$. Montrer que \[ \PP\left(\bigcap_{j\in J} A_j \right) = \frac{(n-k)!}{n!}. \]
			\item En déduire que la probabilité qu'aucun élève de la classe ne tire son nom est $\sum\limits_{k=0}^n \frac{(-1)^k}{k!}$. On verra au second semestre que cette probabilité tend vers $\frac{1}{\E}$ lorsque $n$ tend vers $+\infty$.
	\end{enumerate}
	\item Montrer la formule du crible par récurrence.
	\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Pour $n=4$, la formule donne :
	\begin{align*}
		\PP(A_1\cup A_2\cup A_3\cup A_4) &= \PP(A_1)+\PP(A_2)+\PP(A_3)+\PP(A_4)\\&\quad - \PP(A_1\cap A_2)-\PP(A_1\cap A_3)-\PP(A_1\cap A_4)-\PP(A_2\cap A_3)-\PP(A_2\cap A_4)-\PP(A_3\cap A_4)\\
		&\quad +\PP(A_1\cap A_2\cap A_3)+\PP(A_1\cap A_2\cap A_4)+\PP(A_1\cap A_3\cap A_4)+\PP(A_2\cap A_3\cap A_4) \\
		&\quad - \PP(A_1\cap A_2\cap A_3\cap A_4)
	\end{align*}
	\item La formule est strictement identique, en remplaçant $\PP$ par $\card$ :
	\[ \card\left( \bigcup_{i=1}^n A_i \right) = \sum_{k=1}^n (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \card \left( \bigcap_{j\in J}A_j\right) \]
	\item \begin{enumerate}
	\item On peut supposer que chaque élève a un numéro, de $1$ à $n$. Chaque tirage correspond à associer à un élève un autre élève. On peut donc modéliser cette expérience par \[ \Omega = \interent{1 n}^2 \]
	un tirage étant un couple $(i, j)$ où l'élève $i$ a tiré le nom de l'élève $j$. On a alors \[ \card(\Omega) = n^2. \]
	\item Soit $k\in \interent{1 n}$ et $J$ une partie de $\interent{1 n}$ de cardinal $k$.  L'événement $\ds{\bigcap_{j\in J} A_j}$ correspond à l'événement \og{}pour tout $j\in J$, l'élève $j$ a tiré son nom\fg{}.

	On se donne $J\subset\interent{1 n}$ de cardinal $k$. On écrit $J=\{j_1,\hdots, j_k\}$. La formule des probabilités composées nous donne :

	\begin{align*}
	\PP\left(\bigcap_{j\in J} A_j\right) &= \PP\left(\bigcap_{i=1}^j A_{j_k}\right) \\
	&= \PP(A_{j_1})\PP_{A_{j_1}}(A_{j_2})\hdots \PP_{A_{j_1}\cap \hdots \cap A_{j_{k-1}}}(A_{j_k}) \\
	&= \frac{1}{n} \times \frac{1}{n-1}\times \hdots \times \frac{1}{n-(k-1)}
	\end{align*}
 en remarquant que la probabilité de tirer son propre nom, si celui-ci n'est pas encore sorti, s'il y a $m$ élèves restants est de $\frac1m$.

 On peut alors écrire
 \begin{align*}
\PP\left(\bigcap_{j\in J} A_j\right) &= \frac{(n-k)\times \hdots \times 1 }{n\times \hdots \times (n-k+1) \times (n-k) \times \hdots \times 1} = \frac{(n-k)!}{n!}
 \end{align*}
 et on remarquera que ce résultat ne dépend pas de $J$ mais uniquement de son cardinal $k$.
 \item On va s'intéresser à son événement contraire. Notons $A$ l'événement \og{}aucun élève de la classe ne tire son nom\fg{}. $\overline{A}$ est donc l'événement \og{}au moins un élève de la classe a tiré son nom\fg{}, c'est-à-dire
 \[ \overline{A}=\bigcup_{i=1}^n A_i. \]
 On applique la formule du crible et on utilise le point 3b. On remarquera qu'il y a $\binom{n}{k}$ parties de cardinal $k$ de $\interent{1 n}$. Ainsi :
 \begin{align*}
	 \PP(\overline{A}) &= \PP\left(\bigcup_{i=1}^n A_i\right) \\
	 &= \sum_{k=1}^n (-1)^{k+1} \sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \PP\left( \bigcap_{j\in J}A_j\right)\\
	 &= \sum_{k=1}^n (-1)^{k+1} \sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \frac{(n-k)!}{n!} \\
	 &= \sum_{k=1}^n (-1)^{k+1} \frac{(n-k)!}{n!}\times \binom{n}{k}\\
	 &= \sum_{k=1}^n (-1)^{k+1} \frac{\cancel{(n-k)!}}{\bcancel{n!}}\times \frac{\bcancel{n!}}{k!\cancel{(n-k)!}} \\
	 &= \sum_{k=1}^n (-1)^{k+1} \frac{1}{k!}
 \end{align*}
 Mais alors
 \begin{align*}
	 \boxed{\PP(A)} &= 1-\PP(\overline{A})\\
	 &= 1-\sum_{k=1}^n (-1)^{k+1}\frac{1}{k!}\\
	 &= 1 + \sum_{k=1}^n (-1)^{k+2}\frac{1}{k!}\\
	 &= (-1)^0 \frac{1}{0!} + \sum_{k=1}^n (-1)^k \frac{1}{k!} \text{ car }(-1)^{k+2}=(-1)^k\text{ et } 0!=1\\
	 &=\boxed{\sum_{k=0}^n \frac{(-1)^k}{k!}}
 \end{align*}
\end{enumerate}
\item On note $P$ la proposition définie pour tout $n$ par $P_n$: \og{}pour tous événements $A_1,\hdots A_n$, on a $\ds{\PP\left( \bigcup_{i=1}^n A_i \right) = \sum_{k=1}^n (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\right)}$\fg{}.
 Le point principal est l'hérédité, qui repose sur la formule au rang $2$ :
\[ \PP(A\cup B)=\PP(A)+\PP(B)-\PP(A\cap B) \]
Ainsi
\begin{align*}
	\PP\left(\bigcup_{i=1}^{n+1} A_i\right) &= \PP\left( \bigcup_{i=1}^n A_i \cup A_{n+1}\right)\\
	&= \PP\left(\bigcup_{i=1}^n A_i\right) + \PP(A_{n+1}) - \PP\left(\left( \bigcup_{i=1}^n A_i \right)\cap A_{n+1}\right)\\
	&= \PP \left( \bigcup_{i=1}^n A_i\right) + \PP(A_{n+1})-\PP\left( \bigcup_{i=1}^n \left(A_i\cap A_{n+1}\right)\right)
\end{align*}
On applique l'hypothèse de récurrence deux fois :
\begin{align*}
\PP\left(\bigcup_{i=1}^{n+1} A_i\right)	&= \sum_{k=1}^n (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\right) + \PP(A_{n+1})- \left( \sum_{k=1}^n (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\cap A_{n+1}\right) \right)\\
	&= \sum_{k=1}^n (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\right) + \PP(A_{n+1})+ \sum_{k=1}^n (-1)^{k+2}\sum_{\substack{n+1\in J\subset \interent{1 n}\\\card(J)=k+1}} \PP \left( \bigcap_{j\in J}A_j\right) \\
	&=\sum_{k=1}^n (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\right) + \PP(A_{n+1})+ \sum_{k=2}^{n+1} (-1)^{k+1}\sum_{\substack{n+1\in J\subset \interent{1 n}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\right)
\end{align*}
Remarquons que les deux sommes sur $J$ parcourt tous les sous-ensembles de cardinal $k$ de $\interent{1 n+1}$, la première pour les sous-ensembles ne contenant pas $n+1$, la deuxième le contenant. La deuxième somme commence à $k=2$, mais pour $k=1$, cela donnerait $\PP(A_{n+1})$ qui, justement, est présent. On peut finalement écrire :
\begin{align*}
\PP\left(\bigcup_{i=1}^{n+1} A_i\right) &= \sum_{k=1}^{n+1} (-1)^{k+1}\sum_{\substack{J\subset \interent{1 n+1}\\\card(J)=k}} \PP \left( \bigcap_{j\in J}A_j\right)
\end{align*}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
