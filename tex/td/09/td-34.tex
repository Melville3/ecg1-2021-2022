%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{15}{2}[Paradoxe des deux enfants]
Un couple a deux enfants. On suppose que les enfants ne sont pas nés en même temps, et que le sexe du premier enfant est indépendant du sexe du deuxième. Enfin, on suppose que chaque enfant est un garçon avec probabilité $\frac12$ et une fille avec une probabilité $\frac12$.
\begin{enumerate}
	\item L'aînée est une fille. Quelle est la probabilité que l'autre enfant soit aussi une fille ?
	\item L'un des enfants est une fille. Quelle est la probabilité que l'autre enfant soit aussi une fille ?
	\item On demande au couple d'indiquer le sexe de l'un des enfants et il répond qu'il a une fille. Quelle est la probabilité que l'autre enfant soit aussi une fille ?
	\item L'un des enfants est une fille née un jeudi (on suppose qu'il y a équiprobabilité qu'un enfant soit né un jour dans la semaine). Quelle est la probabilité que l'autre enfant soit aussi une fille ?
\end{enumerate}
\end{exoApp}

\begin{correction}
Dans l'ensemble de l'exercice, on pose $F_1$ l'événement \og{}l'aînée est une fille\fg{} et $F_2$ l'événement \og{}la cadette est une fille\fg{}. D'après l'énoncé, on a \[ \PP(F_1) = \PP(F_2) = \frac{1}{2} \]
et les événements $F_1$ et $F_2$ sont indépendants.
\begin{enumerate}
	\item On demande de calculer $\PP_{F_1}(F_2)$. Par définition :
	\begin{align*}
		\PP_{F_1}(F_2) &= \frac{\PP(F_1\cap F_2)}{F_1} \\
		&= \frac{\PP(F_1)\times \PP(F_2)}{\PP(F_1)} \text{  par indépendance de $F_1$ et $F_2$}\\
		&= \frac{1}{2}.
	\end{align*}
	Ainsi, comme l'intuition le laisse le présager, il y a une chance sur deux d'avoir une fille si l'aînée est une fille.
	\item Cette fois-ci, on sait qu'un des enfants est une fille. On note $F$ l'événement \og{}l'un des enfants est une fille\fg{}. Ainsi
	\[ \PP(F) = \PP(F_1\cup F_2) = \PP(F_1)+\PP(F_2) - \PP(F_1\cap F_2)= \frac{1}{2}+\frac{1}{2}-\frac{1}{4}=\frac{3}{4}. \]
	On note enfin $FF$ l'événement \og{}les deux enfants sont des filles\fg{}, et on a \[ \PP(FF) = \PP(F_1\cap F_2) = \frac{1}{4}. \]
	On peut répondre à la question : on veut calculer $\PP_F(FF)$ :
	\begin{align*}
		\PP_F(FF) &= \frac{\PP(F \cap FF)}{\PP(F)} \\
		&= \frac{\PP(FF)}{\PP(F)} \text{ car } FF \subset F \\
		&= \frac{\frac14}{\frac34} = \frac{1}{3}
	\end{align*}
	Ainsi, il y a une chance sur $3$ pour que le deuxième enfant soit également une fille.
	\item En fait, cette question est la même que la première; au lieu de considérer $F_1$ l'événement \og{}l'aîne est une fille\fg{} on va introduire $F_1$ l'événement \og{}le premier enfant montré par le couple est une fille\fg{}. Le raisonnement est alors le même et on  trouve $\frac{1}{2}$.
	\item \logoattention On a envie de dire que la probabilité vaut $\frac12$, puisque l'information du jour de naissance est \og{}inutile\fg{}. C'est bien sûr faux.

	On note $F_i^j$ l'événement \og{}le $i$-ième enfant de la famille est une fille, né le jour $j$\fg{} (avec par convention $j=1$ pour lundi et $j=7$ pour dimanche), et $F_i$ l'événement \og{}le $i$-ième enfant de la famille est une fille\fg{}. Par équiprobailité des sexes et des jours de la semaine, on a \[ \PP(F_i^j) = \frac{1}{2}\times \frac{1}{7} = \frac{1}{14} \] et la famille des $(F_i^j)_{\substack{1\leq i \leq 2\\1\leq j\leq 7}}$ est mutuellement indépendante.

	On souhaite ici calculer $\PP_{F_1^4 \cup F_2^4}(F_1\cap F_2)$. Alors :
	\begin{align*}
		\PP(F_1^4 \cup F_2^4) &= \PP(F_1^4) + \PP(F_2^4)-\PP(F_1^4F_2^4) \\
		&= \PP(F_1^4)+\PP(F_2^4)-\PP(F_1^4)\PP(F_2^4) \text{  par indépendance}\\
		&= \frac{1}{14}+\frac{1}{14} - \frac{1}{196} = \frac{27}{196}
	\end{align*}
	De plus, \[(F_1^4\cup F_2^4)\cap (F_1\cap F_2) = (F_1^4\cap F_1\cap F_2)\cup(F_2^4\cap F_1\cap F_2) = (F_1^4\cap F_2) \cup (F_2^4\cap F_1) \]
	puisque $F_1^4\subset F_1$ et $F_2^4 \subset F_2$. Alors :
	\begin{align*}
		\PP((F_1^4\cup F_2^4)\cap (F_1\cap F_2)) &= \PP((F_1^4\cap F_2) \cup (F_2^4\cap F_1))\\
		&= \PP(F_1^4\cap F_2) + \PP(F_2^4\cap F_1)- \PP((F_1^4\cap F_2)\cap(F_2^4\cap F_1))\\
		&= \PP(F_1^4)\PP(F_2) + \PP(F_2^4)\PP(F_1)-\PP(F_1^4\cap F_2^4) \text{  par indépendance du choix des sexes}\\
		&= \frac{1}{14}\frac{1}{2}+\frac{1}{14}\frac{1}{2} - \frac{1}{196} =\frac{13}{196}
	\end{align*}
	et finalement
	\[ \PP_{F_1^4\cup F_2^4}(F_1\cap F_2) = \frac{\frac{13}{196}}{\frac{27}{196}} = \frac{13}{27} \approx 0,481 \]
	Ainsi, il y a un peu moins d'une chance sur deux que le deuxième enfant soit une fille.
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
