\begin{exoApp}{20}{2}[Suite et matrice - II]
On considère la suite $u$ définie par $u_0=2$, $u_1=1$, $u_2=-1$ et pour tout $n$,\[u_{n+3}=2u_{n+2}+u_{n+1}-2u_n\]
 On définit les matrices $A=\matrice{2 & 1 & -2 \\ 1 & 0 & 0 \\  0 & 1 & 0 }$ et $P=\matrice{1 & 1 & 4 \\ 1 & -1 & 2 \\ 1 & 1 & 1}$.
 \begin{enumerate}
     \item Montrer que $P$ est inversible et calculer $P^{-1}$. Que vaut $D=P^{-1}AP$ ? En déduire $D^n$.
     \item Montrer que pour tout $n$, $D^n=P^{-1}A^nP$. En déduire les coefficients de $A^n$.
     \item Pour tout $n$, on pose $X_n=\matrice{ u_{n+2} \\ u_{n+1} \\ u_n }$.
     \begin{enumerate}
         \item Vérifier que pour tout $n$, $X_{n+1}=AX_n$. En déduire $X_n$ en fonction de $A^n$ et de $X_0$.
         \item Déterminer la valeur de $u_n$ en fonction de $n$.
     \end{enumerate}
 \end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
   \item On applique la méthode du pivot de Gauss tel qu'habituellement :
  \[(P|I_3) \sim \left( \begin{array}{ccc|ccc}
  					1&1&4&1&0&0\\
  					1&-1&2&0&1&0\\
  					1&1&1&0&0&1
 			 \end{array}
  \right)\sim
   \left( \begin{array}{ccc|ccc}
  					1&1&4&1&0&0\\
  					0&-2&-2&-1&1&0\\
  					0&0&-3&-1&0&1
 			 \end{array}
  \right)
  \]
  Les termes diagonaux sont tous non nuls, donc la matrice $P$ est inversible, et
  \[(P|I_3)\sim \left( \begin{array}{ccc|ccc}
  					1&1&4&1&0&0\\
  					0&-2&-2&-1&1&0\\
  					0&0&1&\frac{1}{3}&0&-\frac{1}{3}
 			 \end{array}
  \right)\sim \left( \begin{array}{ccc|ccc}
  					1&1&4&1&0&0\\
  					0&1&0&\frac{1}{6}&-\frac{1}{2}&\frac{1}{3}\\
  					0&0&1&\frac{1}{3}&0&-\frac{1}{3}
 			 \end{array}
  \right) \sim \left( \begin{array}{ccc|ccc}
  					1&0&0&-\frac{1}{2}&\frac{1}{2}&1\\
  					0&1&0&\frac{1}{6}&-\frac{1}{2}&\frac{1}{3}\\
  					0&0&1&\frac{1}{3}&0&-\frac{1}{3}
 			 \end{array}
  \right)\]
  Ainsi, $P^{-1}=\matrice{
  					-\frac{1}{2}&\frac{1}{2}&1\\
  					\frac{1}{6}&-\frac{1}{2}&\frac{1}{3}\\
  					\frac{1}{3}&0&-\frac{1}{3}
 			 }= \frac{1}{6}\matrice{-3&3&6\\1&-3&2\\2&0&-2}$.\\
 			 Après calcul, on obtient alors
 			 \[D=P^{-1}AP = \matrice{
 			 	1 & 0 & 0\\0&-1&0\\0&0&2
 			 }\]
 			 $D$ étant diagonale, on en déduit donc que
 			 \[\forall~n,~D^n = \matrice{ 1^n&0&0\\0&(-1)^n&0\\0&0&2^n }=\matrice{1&0&0\\0&(-1)^n&0\\0&0&2^n}\]
 	\item Montrons par récurrence sur $n$ la proposition $P_n$ définie pour tout entier $n$ par $P_n$ : ``$D^n=P^{-1}A^nP$''.
 		\begin{itemize}[label=\textbullet]
 		  \item Initialisation : pour $n=0$, on a $D^0 = I_3$ et $P^{-1}A^0P=P^{-1}I_3P=P^{-1}P=I_3$. Donc $P_0$ est vraie.
 		  \item Hérédité : supposons que la proposition $P_n$ est vraie pour un certain entier $n$, et montrons que $P_{n+1}$ est vraie.\\
 		  	 On constate que
 		  	 $D^{n+1}=D^{n}D \underbrace{=}_{\textrm{H.R.}} P^{-1}A^nPD$. Or $D=P^{-1}AP$. Donc
 		  	 \[D^{n+1} = P^{-1}A^nP (P^{-1}AP) = P^{-1}A^n\underbrace{PP^{-1}}_{=I_3}AP = P^{-1}A^nAP=P^{-1}A^{n+1}P\]
 		  	 $P_{n+1}$ est donc vraie.
 		\end{itemize}
 		D'après le principe de récurrence, la proposition $P_n$ est vraie pour tout $n$, et donc $\forall~n,~D^n=P^{-1}A^nP$. Mais alors
 		\[\forall~n,~A^n = PD^n P^{-1}\]
 		On connait $P$, $P^{-1}$ et $D^n$. On en déduit donc $A^n$ :
 		\[\boxed{\forall~n\in \N,~~A^n=\matrice{
  	\frac{-3+(-1)^n+2^{n+3}}{6} & \frac{3-3(-1)^n}{6} & \frac{6+2(-1)^n-2^{n+3}}{6} \\
  	\frac{-3-(-1)^n+2^{n+2}}{6} & \frac{3+3(-1)^n}{6} & \frac{6-2(-1)^n-2^{n+2}}{6} \\
  	\frac{-3+(-1)^n+2^{n+1}}{6} & \frac{3-3(-1)^n}{6} & \frac{6+2(-1)^n-2^{n+1}}{6}
  }}\]
 	\item \begin{enumerate}
 				\item Constatons que
 				\[AX_n=\matrice{ 2u_{n+2}+u_{n+1}-2u_n\\u_{n+2}\\u_{n+1} } = \matrice{ u_{n+3}\\u_{n+2}\\u_{n+1}} = X_{n+1}\]
 				Mais alors, on peut montrer par récurrence sur $n$ que
 				\[\forall~n\in \N,~X_n=A^nX_0\]
 				Soit $Q_n$ la proposition définie pour tout entier $n$ par $Q_n$: ``$X_n=A^nX_0$''.
 				\begin{itemize}[label=\textbullet]
   					\item Initialisation : pour $n=0$, $A^0X_0=I_3X_0=X_0$. Donc la proposition $Q_0$ est vraie.
   					\item Hérédité : supposons que la proposition $Q_n$ est vraie pour un certain entier $n$, et montrons que $Q_{n+1}$ est vraie.\\
   						On vient de voir que $X_{n+1}=AX_n$. Par hypothèse de récurrence, on a donc
   						\[X_{n+1}=AX_n=A(A^nX_0)=A^{n+1}X_0\]
   						La proposition $Q_{n+1}$ est donc vraie.
 				\end{itemize}
 				D'après le principe de récurrence, la proposition $Q_n$ est donc vraie pour tout $n$, et $\forall~n,~X_n=A^nX_0$.
 				\item Puisque $X_n=A^nX_0$, et qu'à la question 2., nous avons déterminé $A^n$, on peut en déduire ainsi que
 				\[\forall~n\in \N,~u_n=\frac{-3+(-1)^n+2^{n+1}}{6}u_2+\frac{3-3(-1)^n}{6}u_1+\frac{6+2(-1)^n-2^{n+1}}{6}u_0\]
 				c'est-à-dire
 				\[\boxed{\forall~n\in \N,~u_n=\frac{3-(-1)^n-2^{n+1}}{6}+\frac{3-3(-1)^n}{6}+2\frac{6+2(-1)^n-2^{n+1}}{6} = 3-2^n}\]
 			\end{enumerate}
 \end{enumerate}


 \begin{remarque}
Cet exercice est très classique. Il utilise ce que l'on appelle la diagonalisation de la matrice $A$ (en écrivant $A=PDP^{-1}$ avec $D$ une matrice diagonale) pour calculer les puissances $n^{ème}$ de la matrice $A$, et en déduire un résultat sur une suite. Les récurrences présentes dans cet exercice sont à savoir faire et refaire rapidement.
\end{remarque}
\end{correction}
