%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{2}[ESCP 99]
	Soit $g$ la fonction définie sur $\R$ par $$g(x)=\int_x^{2x} \eu{-t^2}\dd t$$
\begin{enumerate}
    \item Justifier que $g$ est bien définie sur $\R$, et que la fonction $g$ est impaire.
    \item Déterminer le signe de $g(x)$ suivant les valeurs de $x$.
    \item Justifier que $g$ est de classe $\mathcal{C}^1$ et que pour tout $x\in \R$,
    $$g'(x)=2\eu{-4x^2}-\eu{-x^2}$$
    \item Dresser le tableau de variations de $g$. On précisera $g(0)$.
    \item Montrer que pour tout $x>0$, $x\eu{-4x^2} \leq g(x) \leq x\eu{-x^2}$. En déduire la limite de $g$ en $+\infty$.
\end{enumerate}
\end{exoApp}

\begin{correction}
	\begin{enumerate}
	\item ~
	\begin{methode}
		Pour une fonction définie par une intégrale, la parité se fera (presque) toujours par le changement de variable $x=-t$.
	\end{methode}
	La fonction $f:x\mapsto \eu{-x^2}$ est définie et continue sur $\R$. La fonction $g$ est donc bien définie sur $\R$.\\
		$\R$ est symétrique par rapport à $0$, et pour tout réel $x$, on a
		$$g(-x)=\int_{-x}^{-2x} \eu{-t^2}\dd t$$
		Faisons le changement de variable $u=-t$, soit $t=-u$ qui est de classe $\mathcal{C}^1$, et $\dd t=-\dd u$. Lorsque $t=-x$, $u=x$ et lorsque $t=-2x$, $u=2x$. Par changement de variable
		$$g(-x)=\int_{x}^{2x} \eu{-(-u)^2}(-\dd u)=-\int_x^{2x} \eu{-u^2}\dd u=-g(x)$$
		Ainsi, la fonction $g$ est impaire.
	\item ~\begin{remarque}
	On fera toujours attention à l'ordre des bornes avant d'utiliser la positivité de l'intégrale.
	\end{remarque}
		La fonction $f$ est positive sur $\R$. Il faut donc s'intéresser aux bornes de l'intégrale :
		\begin{itemize}
			\item[$\bullet$] Pour $x>0$, on a $x<2x$. Par positivité de l'intégrale, $$g(x)=\int_x^{2x} \eu{-t^2}\dd t \geq 0$$
			\item[$\bullet$] Pour $x<0$, on a par contre $x>2x$. Par positivité de l'intégrale, on a cette fois-ci $$g(x)=\int_x^{2x} \eu{-t^2}\dd t\leq 0$$
		\end{itemize}
		\textbf{Bilan} : $g$ est positive sur $\R^+$ et négative sur $\R^-$.
	\item Soit $F$ une primitive de $f$ (qui existe car $f$ est continue sur $\R$). $F$ est de classe $\mathcal{C}^1$, et on peut alors écrire, pour tout réel $x$, $$g(x)=F(2x)-F(x)$$
		Par somme et composée de fonctions de classe $\mathcal{C}^1$, $g$ est $\mathcal{C}^1$ sur $\R$, et $$g'(x)=2F'(2x)-F'(x)=2f(2x)-f(x)$$ c'est-à-dire
		$$\forall~x\in \R,~g'(x)=2\eu{-(2x)^2}-\eu{-x^2}=2\eu{-4x^2}-\eu{-x^2}$$
	\item On a alors
		$$g'(x)\geq 0 \Leftrightarrow 2\eu{-4x^2} \geq \eu{-x^2} \Leftrightarrow \eu{-3x^2} \geq \frac{1}{2}$$
		Soit, par croissance de la fonction $\ln$ sur $\R^*_+$ :
		$$-3x^2 \geq \ln\left(\frac{1}{2}\right) \Leftrightarrow x^2\leq \frac{\ln(2)}{3} $$
		Ainsi, $g'$ est négative sur $\left]-\infty; -\sqrt{\frac{\ln(2)}{3}}\right]$ et sur $\left[\sqrt{\frac{\ln(2)}{3}};+\infty\right[$, positive sinon. On obtient le tableau de variations suivant
		\begin{center}
		\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $g'(x)$ / 1, $g$ / 2}{$-\infty$, $-\sqrt{\frac{\ln(2)}{3}}$, $\sqrt{\frac{\ln(2)}{3}}$, $+\infty$}
   \tkzTabLine{, -, z, +, z, -, }
   \tkzTabVar{+/,-/,+/,-/}
\end{tikzpicture}
		\end{center}
	\item Soit $x>0$. Sur $\interff{x 2x}$, la fonction $f:t\mapsto \eu{-t^2}$ est décroissante. Ainsi
		$$\forall t\in \interff{x 2x},\quad  f(2x) \leq   f(t) \leq f(x)\iff \eu{-4x^2} \leq f(t) \leq \eu{-x^2}$$
		Par positivité de l'intégrale (car pour $x>0, ~x<2x$), on a alors
		$$\int_x^{2x} \eu{-4x^2}\dd t \leq \int_x^{2x} f(t)\dd t \leq \int_x^{2x} \eu{-x^2}\dd t$$
		soit
		$$\eu{-4x^2}\left [t \right]_x^{2x} \leq g(x) \leq \eu{-x^2}\left[t \right]_x^{2x}$$
		et donc
		$$x\eu{-4x^2} \leq g(x) \leq x\eu{-x^2}$$
		Or, par croissance comparée, $$\lim_{x\rightarrow +\infty} x\eu{-x^2} = \lim_{x\rightarrow +\infty } \frac{1}{x} \underbrace{x^2\eu{-x^2}}_{\tendversen{+\infty} 0\textrm{ (CC)}} = 0 \textrm{  par produit}$$
		De même, $\displaystyle{\lim_{x\rightarrow +\infty} x\eu{-4x^2}=0}$. Par encadrement, $\displaystyle{\lim_{x\rightarrow +\infty} g(x)}$ existe et
		$$\boxed{\lim_{x\rightarrow +\infty} g(x)=0}$$
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
