%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{1}[Premières intégrales]
Montrer l'existence, puis calculer chacune des intégrales suivantes :
\begin{multicols}{3}
\begin{description}
	\item $\ds{A=\int_1^2 \frac{\ln(x)}{x}\dd x}$
	\item $\ds{B=\int_0^3 (2u+1)\eu{u^2+u+1}\dd u}$
	\item $\ds{C=\int_0^1 \frac{\eu{t}+1}{\eu{t}+t}\dd t}$
	\item $\ds{D=\int_\E^{2\E} \frac{\dd t}{t\sqrt{\ln(t)}}}$
	\item $\ds{E=\int_0^1 \frac{x}{x+1}\dd x}$
	\item $\ds{F=\int_{-1}^1 |x^2-x|\dd x}$
	\item $\ds{G=\int_{-1/\sqrt{2}}^0 \frac{1}{3+2t^2}\dd t}$
	\item $\ds{H=\int_1^2 \eu{u}\left(\frac{1}{u}+\ln(u)\right)\dd u}$
	\item $\ds{I=\int_0^{\frac{\pi}{2}} \left(2\sin^3(x)-3\sin^2(x)+5\right)\cos(x)\dd x}$
\end{description}
\end{multicols}
\end{exoApp}

\begin{correction}
L'idée de cet exercice est de trouver les primitives des fonctions sous le signe intégrale. On se ramènera aux primitives usuelles, ou aux formules de dérivations ($u'\E^u, \frac{u'}{u},\cdots$).\\Remarquons tout d'abord  que chaque intégrale existe car toutes les fonctions considérées sont continues sur les intervalles d'intégration.
\[A=\int_1^2 \frac{\ln(x)}{x}\dd x = \left[ \frac{1}{2}\left(\ln(x)\right)^2 \right]_1^2 = \frac{\left(\ln(2)\right)^2}{2} \textrm{  en reconnaissant } \frac{\ln(x)}{x} = \frac{1}{x}\ln(x) = u'(x)u(x) \textrm{ avec } u(x)=\ln(x)\]
\[B=\int_0^3 (2u+1)\eu{u^2+u+1}\dd u = \left[ \eu{u^2+u+1}\right]_0^3 = \eu{13}-\E \textrm{  en reconnaissant } v'(u) \eu{v(u)} \textrm{ avec } v(u)=u^2+u+1\]
\[C=\int_0^1 \frac{\E^t+1}{\E^t+t}\dd t = \left[ \ln\left|\E^t+t\right| \right] _0^1 = \ln(1+\E) \textrm{  en reconnaissant } \frac{u'(t)}{u(t)} \textrm{ avec } u(x)=\E^t+t\]
\[D=\int_\E^{2\E} \frac{\dd t}{t\sqrt{\ln(t)}} = \left[ 2\sqrt{\ln(t)} \right]_E^{2\E} =  2\sqrt{\ln(2)+1}-2 \textrm{  en reconnaissant } \frac{u'(t)}{\sqrt{u(t)}} \textrm{ avec } u(t)=\ln(t)\]
Pour le $E$, on ne reconnait pas de primitives usuelles. On transforme alors l'écriture de l'intégrale :
\[E=\int_0^1 \frac{x}{x+1}\dd x = \int_0^1 \frac{x+1-1}{x+1}\dd x = \int_0^1 1-\frac{1}{x+1}\dd x = \left[ x-\ln|x+1|\right]_0^1 = 1-\ln(2)\]
\begin{methode}
	Dans le cas d'une intégrale avec une valeur absolue, il faut utiliser la relation de Chasles pour pouvoir enlever, suivant les intervalles considérés, les valeurs absolues.
\end{methode}
Remarquons que $x^2-x=x(x-1)$ est positif sur $[-1;0]$ et est négatif sur $[0;1]$. Ainsi,
\[F=\int_{-1}^1 |x^2-x|\dd x = \int_{-1}^0 |x^2-x|\dd x + \int_0^1 |x^2-x| \dd x = \int_{-1}^0 x^2-x\dd x + \int_0^1 -(x^2-x)\dd x\]
en utilisant le fait que $|u|=u$ si $u\geq 0$, $|u|=-u$ sinon.
Ainsi, \[F=\left[\frac{x^3}{3}-\frac{x^2}{2}\right]_{-1}^0 + \left[ -\left(\frac{x^3}{3}-\frac{x^2}{2}\right)\right]_0^1 = 1\]
Pour la $G$, on reconnait presque la dérivée d'$\arctan$. On ré-écrit :
\begin{align*}
 G &= \int_{-\frac{1}{\sqrt{2}}}^0 \frac{1}{3\left(1+\frac{2}{3}t^2\right)} \dd t \\
 &= \int_{-\frac{1}{\sqrt{2}}}^0 \frac{1}{3} \frac{1}{1+\left(\sqrt{\frac{2}{3}}t\right)^2}\dd t \\
 &= \frac{1}{3} \left [\sqrt{\frac{3}{2}} \arctan\left(\sqrt{\frac{2}{3}}t\right)   \right]_{-\frac{1}{\sqrt{2}}}^0 \\
 &= \frac{1}{3}\sqrt{\frac{3}{2}} \left( \arctan(0) - \arctan\left(-\frac{1}{\sqrt{3}}\right)\right) \\
 &= \frac{1}{\sqrt{6}} \left( 0 + \frac{\pi}{6}\right) = \frac{\pi}{6\sqrt{6}}
\end{align*}
Concernant la $H$, on peut constater que l'on a une dérivée simple :
\[ x\mapsto \eu{u}\left(\frac{1}{u}+\ln(u)\right)=\eu{u}\ln'(u)+\eu{u}\ln(u) \]
et on reconnait ainsi la dérivée de $u\mapsto \eu{u}\ln(u)$. Ainsi
\begin{align*}
	H &= \left[ \eu{u}\ln(u) \right]_1^2  \\
	&= \eu{2}\ln(2)
\end{align*}
Enfin, pour la dernière, on reconnait des termes de la forme $u'u^n$ avec $u=\sin$. On peut calculer directement une primitive :
\begin{align*}
	I &= \int_0^{\frac{\pi}{2}} 2\sin^3(x)\cos(x) - 3 \sin^2(x)\cos(x) + 5\cos(x) \dd x\\
	&= \left[2 \frac{\sin^4(x)}{4} - 3 \frac{\sin^3(x)}{3}+5\sin(x)  \right]_0^{\frac{\pi}{2}} \\
	&= \frac{1}{2}-1+5 = \frac{9}{2}
\end{align*}
\end{correction}
%%% Fin exercice %%%
%%% Fin exercice %%%
