%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

  \begin{exoApp}{60}{3}[EDHEC 2005]
  Un mobile se déplace sur les points à coordonnées entières d'un axe d'origine $O$. Au départ, le mobile est à l'origine. Le mobile se déplace selon la règle suivante : s'il est sur le point d'abscisse $k$ à l'instant $n$, alors à l'instant $n+1$ :
  \begin{itemize}
    \item il sera sur le point d'abscisse $k+1$ avec une probabilité $p$ ($0<p<1$)
    \item il sera sur le point $0$ avec une probabilité $1-p$.
  \end{itemize}
  Pour tout $n$, on note $X_n$ l'abscisse de ce point à l'instant $n$, et l'on a donc $X_0=0$.\\
  On admet que, pour tout $n\in \N$, $X_n$ est une variable aléatoire définie sur un espace probabilisé $(\Omega, \partie(\Omega),P)$.\\
  Par ailleurs, on note $T$ l'instant auquel le mobile se trouve pour la première fois à l'origine (sans compter son positionnement au départ).\\
  Par exemple, si les abscisses successives du mobile après son départ sont $0,0,1,2,0,0,1$, alors $T=1$. Si les abscisses successives sont $1,2,3,0,0,1$ alors $T=4$.\\On admet que $T$ est une variable aléatoire définie sur $(\Omega, \partie(\Omega), P)$.
  \begin{enumerate}
      \item \begin{enumerate}
          \item Pour tout $k\in \N^*$, exprimer l'événement $(T=k)$ en fontion d'événements mettant en jeu certaines des variables $X_i$.
          \item Donner la loi de $X_1$.
          \item En déduire $P(T=k)$ pour tout $k$ de $\N^*$.
      \end{enumerate}
      \item \begin{enumerate}
          \item Montrer par récurrence que, pour tout entier naturel $n$, $X_n(\Omega) = \llbracket 0,n \rrbracket$.
          \item Pour tout $n$ de $\N^*$, utiliser le système complet d'événements $(X_{n-1}=k)_{0\leq k \leq n-1}$ pour montrer que :\\
          $P(X_n=0)=1-p$
      \end{enumerate}
      \item \begin{enumerate}
          \item Etablir que
          $$\forall~n \in \N, \forall~k\in \{1,2,\cdots, n+1\}, P(X_{n+1}=k)=pP(X_n=k-1)$$
          \item En déduire que
          $\forall~n \in \N^*, \forall~k\in \{0,1,2,\cdots, n-1\},~P(X_n=k)=p^k(1-p)$.\\ En déduire également la valeur de $P(X_n=n)$. Donner une explication probabiliste de ce dernier résultat.
          \item Vérifier que $\displaystyle{\sum_{k=0}^n P(X_n=k)=1}$.
      \end{enumerate}
      \item \begin{enumerate}
              \item Montrer que pour tout $n\geq 2$,
              $$\sum_{k=1}^{n-1} kp^{k-1} = \frac{(n-1)p^n-np^{n-1}+1}{(1-p)^2}$$
              \item En déduire que $E(X_n)=\frac{p(1-p^n)}{1-p}$.
          \end{enumerate}
      \item \begin{enumerate}
              \item Montrer en utilisant la question $3a)$ que
              $$\forall~n\in \N, E(X_{n+1}^2)=p(E(X_n^2)+2E(X_n)+1)$$
              \item Pour tout entier naturel $n$, on pose
              $\displaystyle{u_n=E(X_n^2)+(2n-1)\frac{p^{n+1}}{1-p}}$
              Montrer que $u_{n+1}=pu_n+\frac{p(1+p)}{1-p}$.
              \item En déduire l'expression de $u_n$, puis celle de $E(X_n^2)$ en fonction de $p$ et $n$.
              \item Montrer enfin que
              $$V(X_n)=\frac{p}{(1-p)^2}(1-(2n+1)p^n(1-p)-p^{2n+1})$$

          \end{enumerate}
  \end{enumerate}
  \end{exoApp}

  \begin{correction}
  \iffalse
  \begin{enumerate}
  	\item~\begin{enumerate}
  			\item Soit $k\in \N^*$. Par définition, $(T=k)$ représente l'événement où on retourne pour la première fois en $0$ à l'instant $k$. Ainsi, $$(T=k)=(X_1=1)\cap(X_2=2)\cap \cdots \cap (X_{k-1}=k-1) \cap (X_k=0)=\bigcap_{i=1}^{k-1} (X_i=i) \cap (X_k=0)$$
  			\item D'après l'énoncé, $X_1(\Omega)=\{0,1\}$ : $\PP(X=1)=p$ et $\PP(X=0)=1-p$. Ainsi $X_1 \suit \mathcal{B}(p)$
  			\item D'après l'énoncé, $\PP_{(X_1=1)\cap \cdots \cap (X_{i-1}=i-1)}(X_i=i) = p$. Ainsi, d'après la formule des probabilités composées
  				$$\PP(T=k)=\PP(X_1=1)\PP_{(X_1=1)}(X_2=2)\cdots \PP_{(X_1=1)\cap \cdots \cap (X_{k-2}=k-2)}(X_{k-1}=k-1) \PP_{(X_1=1)\cap \cdots \cap (X_{k-1}=k-1)}(X_k=0)$$
  				soit $$\PP(T=k)=p\times p \times \cdots \times p \times (1-p)=p^{k-1}(1-p)$$
  				Ainsi, $\boxed{T \suit \mathcal{G}(p)}$.
  		  \end{enumerate}
  				\remarqueL{Ce résultat n'est pas surprenant : $T$ est le temps d'attente du premier échec.}
  	\item ~\begin{enumerate}
  				\item Montrons par récurrence le résultat $P_n$ : ``$X_n(\Omega)=\llbracket 1,n \rrbracket$''.\\
  				\textbf{Initialisation} : pour $n=0$, on a $X_0(\Omega)=\{0\}$ donc $X_0(\Omega)=\llbracket 0,0 \rrbracket$ : $P_0$ est vraie.
  				\\\textbf{Hérédité} : supposons le résultat vraie pour un certain entier $n$, et montrons que $P_{n+1}$ est vraie.\\
  				Ainsi, $X_n(\Omega)=\llbracket 0,n\rrbracket$ par hypothèse de récurrence. Deux cas possibles pour $X_{n+1}$ : soit le mobile retourne à $0$, soit le mobile se déplace, et dans ce cas, il passe du point d'abscisse $i$ au point d'abscisse $i+1$, pour $i\in \llbracket 0,n\rrbracket$ : ainsi, il se retrouvera sur un point d'abscisse comprise dans $\llbracket 1,n+1\rrbracket$.\\
  					Donc, $X_{n+1}(\Omega)= \{0\} \cup \llbracket 1,n+1\rrbracket = \llbracket 0,n+1\rrbracket$ : $P_{n+1}$ est donc vraie.\\
  					On a donc bien montré par récurrence que pour tout entier $n$, $X_n(\Omega)=\llbracket 0,n\rrbracket$.
  				\item Soit $n\in \N^*$. D'après ce qui précède, $X_{n-1}(\Omega)=\llbracket 0,n-1\rrbracket$. Puisque $X_{n-1}$ est une variable aléatoire discrète, l'ensemble $(X_{n-1}=k)_{0\leq k\leq n-1}$ est un système complet d'événement. D'après la formule des probabilités totales :
  					$$\PP(X_n=0)=\sum_{k=0}^{n-1} \PP(X_{n-1}=k)\PP_{(X_{n-1}=k)}(X_n=0)$$
  					Or, d'après l'énoncé, $\PP_{(X_{n-1}=k)}(X_n=0)=1-p$ car à chaque instant, il retourne au point d'abscisse $0$ avec probabilité $1-p$. Ainsi
  					$$\PP(X_n=0)=\sum_{k=0}^{n-1} \PP(X_{n-1}=k)(1-p) = (1-p)\underbrace{\sum_{k=0}^{n-1} \PP(X_{n-1}=k)}_{=1 \textrm{ car v.a.}}=1-p$$
  			\end{enumerate}
  		\item ~\begin{enumerate}
  					\item Soit $n\in \N$ et $k\in \{1,\cdots ,n+1\}$. Utilisons le système complet d'événements $(X_n=i)_{0\leq i\leq n}$ et appliquons la formule des probabilités totales :
  						$$\PP(X_{n+1}=k)=\sum_{i=0}^n \PP(X_n=i)\PP_{(X_n=i)}(X_{n+1}=k)$$
  						Or, si $i\neq k-1$, $\PP_{(X_n=i)}(X_{n+1}=k)=0$ car il est impossible que le mobile soit à l'instant $n+1$ en $k$ sans qu'il soit à l'instant $n$ en $k-1$. De plus, $\PP_{(X_n=k-1)}(X_{n+1}=k)=p$ d'après l'énoncé. Ainsi
  						$$\PP(X_{n+1}=k)=\PP(X_n=k-1)p$$
  					\item Démontrons le résultat par récurrence : soit $P_n$: ``pour tout $k\in \{0,\cdots,n-1\}$, $\PP(X_n=k)=p^k(1-p)$''.\\\textbf{Initialisation} : pour $n=0$, on a bien $\PP(X_0=0)=1-p = p^0(1-p)$. Ainsi, $P_0$ est vraie.
  						\\\textbf{Hérédité} : supposons la propriété $P_n$ vraie pour un certain $n$, et montrons que $P_{n+1}$ est vraie. Soit $k \in \{1,\cdots,n\}$. D'après la question précédente, $\PP(X_{n+1}=k)=p\PP(X_n=k-1)$. Par hypothèse de récurrence,
  						$$\PP(X_{n+1}=k)=p (p^{k-1}(1-p)) = p^k(1-p)$$
  						De plus, $\PP(X_{n+1}=0)=1-p = p^0(1-p)$ d'après l'énoncé. Donc $P_{n+1}$ est vraie.
  						\\Ainsi, pour tout entier $n$ et tout entier $k\in \{1?\cdots, n-1\}$, $\PP(X_n=k)=p^k(1-p)$. \\~\\
  					Remarquons de plus que la relation vue en $3a$ amène que
  					$$\PP(X_n=n)=p\PP(X_{n-1}=n-1)$$
  					Ainsi, la suite $u$ définie pour tout $n$ par $u_n=\PP(X_n=n)$ est une suite géométrique de raison $p$.
  					Ainsi, on a  $$\forall~n,~~\PP(X_n=n)=p^n\PP(X_0=0)=p^n$$
  					ce qui est cohérent, car pour que au $n^{ième}$ instant le mobile soit au point d'abscisse $n$, il faut qu'il ne soit jamais retourné au point d'abscisse $0$.
  					\item $X_n$ étant une variable aléatoire de support $\llbracket 0,n\rrbracket$, on doit avoir $\displaystyle{\sum_{k=0}^n \PP(X_n=k)=1}$. Or
  							$$\sum_{k=0}^{n-1} \PP(X_n=k) + \PP(X_n=n)=\sum_{k=0}^{n-1} p^k(1-p)+p^n =(1-p)\frac{1-p^{n}}{1-p}+p^n=1 $$
  				\end{enumerate}
  		\item ~\begin{enumerate}
  					\item~ \remarqueL{Cette démonstration est à retenir : elle tombe souvent au concours.} Pour tout $n\geq 2$, on a
  						$$\sum_{k=0}^{n-1} p^k = \frac{1-p^n}{1-p}$$
  						comme somme des termes d'une suite géométrique ($p\neq 1$). En dérivant cette relation par rapport à $p$, et en remarquant que $p^0=1$ on a
  						$$\sum_{k=1}^{n-1} kp^{k-1} = \frac{(-np^{n-1})(1-p)-(1-p^n)(-1)}{(1-p)^2}$$
  						soit
  						$$\sum_{k=1}^{n-1} kp^{k-1}= \frac{ np^n-np^{n-1}+1-p^n  }{(1-p)^2}=\frac{p^n(n-1)-np^{n-1}+1}{(1-p)^2}$$
  					\item $X_n$ étant à support fini, elle admet une espérance. On a
  					$$\esperance(X_n)=\sum_{k=0}^n k\PP(X_n=k) = \sum_{k=0}^{n-1} k p^k(1-p) + np^n=p(1-p)\sum_{k=0}^{n-1} kp^{k-1}+np^n$$
  					D'après le résultat précédent, on a donc
  					$$\esperance(X_n)=p(1-p)\frac{(n-1)p^n-np^{n-1}+1}{(1-p)^2} + np^n=\frac{(n-1)p^{n+1}-np^{n}+p}{1-p}+np^n$$
  					Soit après mise au même dénominateur
  					$$\esperance(X_n)=\frac{(n-1)p^{n+1}-np^{n}+p + np^n-np^{n+1}}{1-p}$$
  					et donc
  					$$\boxed{\esperance(X_n)=\frac{p-p^{n+1}}{1-p}=\frac{p(1-p^n)}{1-p}  }$$
  				\end{enumerate}
  		\item ~\begin{enumerate}
  					\item Les espérances existent car les variables aléatoires sont à support fini. D'après la formule de transfert,
  					$$\esperance(X_{n+1}^2) = \sum_{k=0}^{n+1} k^2\PP(X_{n+1}=k)$$
  					D'après la question $3a)$, on a donc
  					$$\esperance(X_{n+1}^2) = 0 + \sum_{k=1}^{n+1} k^2 p\PP(X_n=k-1)$$
  					En posant $i=k-1$, on peut alors écrire
  					$$\esperance(X_{n+1}^2) = p\sum_{i=0}^n (i+1)^2 \PP(X_n=i) = \sum_{i=0}^n (i^2+2i+1) 	\PP(X_n=i)$$
  					soit, en développant
  					$$\esperance(X_{n+1}^2)= p \left( \sum_{i=0}^n i^2\PP(X_n=i) + 2\sum_{k=0}^n i\PP(X_n=i) + \sum_{i=0}^n \PP(X_n=i)\right)$$
  					Or, d'après la formule de transfert, $\displaystyle{\sum_{i=0}^n i^2\PP(X_n=i) = \esperance(X_n^2)}$. De plus, $\displaystyle{\sum_{i=0}^n i\PP(X_n=i)=\esperance(X_n)}$ et $\displaystyle{\sum_{i=0}^n \PP(X_n=i) = 1}$. Ainsi,
  					$$\esperance(X_{n+1}^2)=p(\esperance(X_n^2)+2\esperance(X_n)+1)$$
  					\item D'après la question précédente, on constate que
  					$$u_{n+1}=\esperance(X_{n+1}^2)+(2(n+1)-1)\frac{p^{n+1+1}}{1-p}=p\esperance(X_n^2)+2p\esperance(X_n)+p+(2n+1)\frac{p^{n+2}}{1-p}$$
  					soit
  					$$u_{n+1}=pu_n-p(2n-1)\frac{p^{n+1}}{1-p}+2p\esperance(X_n)+p+(2n+1)\frac{p^{n+2}}{1-p}$$
  					avec $\esperance(X_n)=\frac{p(1-p^n)}{1-p}$. On obtient donc

  					$$u_{n+1}=pu_n+\frac{-p(2n-1)p^{n+1}+2p^2(1-p^n)+p(1-p)+(2n+1)p^{n+2}}{1-p}$$
  					et donc
  					$$u_{n+1}=pu_n+\frac{-2np^{n+2}+p^{n+2}+2p^2-2p^{n+2}+p(1-p)+2np^{n+2}+p^{n+2}}{1-p}$$
  					$$u_{n+1}=pu_n+\frac{2p^2+p-p^2}{1-p}=pu_n+\frac{p(1+p)}{1-p}$$
  				\item La suite $(u_n)$ est donc une suite arithmético-géométrique. Après étude classique, on constate que la suite $v$ définie pour tout $n$ par $v_n=u_n-\frac{p(1+p)}{(1-p)^2}$ est géométrique, de raison $p$, et de premier terme $$v_0=u_0-\frac{p(1+p)}{(1-p)^2} =\underbrace{\esperance(X_0^2)}_{=0 \textrm{ car } X_0=0}-\frac{p}{1-p}-\frac{p(1+p)}{(1-p)^2}=\frac{-2p}{(1-p)^2}$$
  				 Ainsi,
  				 $$\boxed{\forall~n\in \N,~~u_n=\frac{-2p}{(1-p)^2}p^n+\frac{p(1+p)}{(1-p)^2}}$$
  				 et
  				 $$\boxed{\esperance(X_n^2)=u_n-(2n-1)\frac{p^{n+1}}{1-p} = \frac{-2p}{(1-p)^2}p^n+\frac{p(1+p)}{(1-p)^2}-(2n-1)\frac{p^{n+1}}{1-p}}$$
  		\item D'après la formule de Koenig-Huygens, puisque la variable aléatoire $X_n$ est à support fini, elle admet une variance, qui est donnée par
  			$$\Var(X_n)=\esperance(X_n^2)-\esperance(X_n)^2 =\frac{-2p}{(1-p)^2}p^n+\frac{p(1+p)}{(1-p)^2}-(2n-1)\frac{p^{n+1}}{1-p} - \left(\frac{p(1-p^n)}{1-p}\right)^2$$
  			soit, après factorisation
  			$$\Var(X_n)=\frac{p}{(1-p)^2} \left( -2p^n+(1+p)-(2n-1)p^n(1-p) - p(1-p^n)^2\right)$$
  			$$\Var(X_n)=\frac{p}{(1-p)^2} \left( -2p^n+1+p-(2n-1)p^n(1-p)-p+2p^{n+1}-p^{2n+1}\right)$$
  			et donc
  			$$\Var(X_n)=\frac{p}{(1-p)^2} \left( 1 -2p^n(1-p) -p^{2n+1}-(2n-1)p^n(1-p) \right)$$
  			soit
  			$$\boxed{\Var(X_n)=\frac{p}{(1-p)^2} \left( 1-p^n(1-p)(2n+1) -p^{2n+1} \right)}$$
  		\end{enumerate}
  \end{enumerate}
  \fi
  \end{correction}
%%% Fin exercice %%%
