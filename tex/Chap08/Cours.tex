\chapter{Combinatoire}
%!TeX root=../../encours.nouveau.tex

\objectifintro{On introduit des notions de base sur le dénombrement : cardinal, liste, combinaison et nombres combinatoires.}

\begin{extrait}{Sébastien Le Prestre de Vauban (1633 -- 1707). \emph{Les Oisivetés de Monsieur de Vauban}}
Il n'y a donc qu'à débrouiller le revenu de chacun, et le mettre en évidence, afin de voir comment il doit être taxé.\\
Ce que je dois dire à cet égard suppose un dénombrement exact de toutes les personnes qui habitent le royaume.
%Imaginons que, pour le caractère donné, par exemple le système sanguin Rhésus, le père et la mère soient dotés de deux gènes distincts, a et b pour un ensemble de deux caractères, $23 = 9$ combinaisons pour un ensemble de $200$ caractères, ce nombre est de $3^{200}$, ce qui est pratiquement infini puisque ce nombre est des milliards de fois plus grand que le nombre total d'atomes de notre univers.
\end{extrait}

\begin{objectifs}
\begin{numerote}
        %\item Concernant les limites :
        %       \begin{itemize}[label=\textbullet]
                        \item \hyperref[objectif-09-1]{Connaître les formules liées au cardinal d'un ensemble}\dotfill $\Box$
                        \item \hyperref[objectif-09-2]{Connaître la différence entre une permutation, une liste sans répétition et une liste avec répétition}\dotfill $\Box$
                        \item \hyperref[objectif-09-3]{Savoir dénombrer les différents ensembles précédents}\dotfill $\Box$
                        \item \hyperref[objectif-09-4]{Connaître la définition d'une combinaison}\dotfill $\Box$
                        \item \hyperref[objectif-09-5]{Savoir l'expression du nombre de combinaison $\binom{n}{p}$}\dotfill $\Box$
                        \item \hyperref[objectif-09-6]{Connaître les formules liées aux nombres de combinaisons}\dotfill $\Box$
                        \item \hyperref[objectif-09-7]{Connaître la formule du binôme de Newton et savoir la démontrer}\dotfill$\Box$
        %       \end{itemize}
\end{numerote}
\end{objectifs}


\section{Cardinaux}

\begin{definition}
Un ensemble $E$ est dit \textbf{fini} s'il est soit vide, soit composé d'un nombre fini d'éléments distincts $e_1, \cdots, e_n$. Dans ce cas, on appelle $n$ son \textbf{cardinal} (i.e. son nombre d'éléments), que l'on note $|E|$ ou $\Card(E)$.\\Par convention, $\Card(\vide)=0$.
\end{definition}

\begin{remarque}
	Faire du dénombrement, c'est déterminer le nombre d'éléments d'un ensemble, sans avoir à connaître la liste des éléments de $E$.
\end{remarque}

\label{objectif-09-1}
\begin{propriete}
	Soient $E$ et $A$ deux ensembles, tels que $A\subset E$ et $E$ est un ensemble fini. Alors
\begin{itemize}
    \item $A$ est également fini;
    \item $\Card(A) \leq \Card(E)$.
\end{itemize}
Si, de plus, $\Card(A)=\Card(E)$, alors $A=E$
\end{propriete}

\begin{remarque}\logoattention
Si $\Card(A) =\Card(E)$ sans avoir $A\subset E$ on ne peut pas conclure ! Par exemple $A=\{1,2\}$, $E=\{2, 3\}$. Alors $\Card(A) =\Card(E)$ mais $A\neq E$
\end{remarque}

\begin{theoreme}[Formule du Crible de Poincaré]
\begin{itemize}
\item Soient $A$ et $B$ deux sous-ensembles d'un ensemble fini $E$. Alors
\[\Card(A\cup B)=\Card(A)+\Card(B)-\Card(A\cap B)\]
\item Soient $A$, $B$ et $C$ trois sous-ensembles d'un ensemble fini $E$. Alors
\[\hspace{-1cm}\Card(A\cup B\cup C)=\Card(A)+\Card(B)+\Card(C)-\Card(A\cap B)-\Card(A\cap C)-\Card(B\cap C)+\Card(A\cap B \cap C)\]
\item Soient $A_1, \cdots, A_n$ des sous-ensembles d'un ensemble fini $E$ deux à deux disjoints. Alors \[\Card\left(\bigcup_{k=1}^n A_k\right)=\sum_{k=1}^n \Card(A_k)\]
\end{itemize}
\end{theoreme}

\begin{proposition}
	Soient $A$ et $B$ deux sous-ensembles d'un ensemble fini $E$. On note $A\backslash B$ l'ensemble des éléments qui sont dans $A$ mais pas dans $B$.\\
Alors \[\Card(A\backslash B)=\Card(A)-\Card(A\cap B)\]
\end{proposition}

\ifprof
\begin{demonstration}
Remarquons que les ensembles $A\backslash B$ et $A\cap B$ sont disjoints, de réunion $A$. D'après le théorème précédent
\[\Card(A)=\Card\left((A\cap B)\cup (A\backslash B)\right)=\Card(A\cap B)+\Card(A\backslash B)\]
\end{demonstration}
\else
\lignes{4}
\fi

\begin{proposition}
	Soient $E$ et $F$ deux ensembles finis. Alors \[\Card(E\times F)=\Card(E)\times \Card(F)\]
\end{proposition}

\ifprof
\begin{demonstration}
On note $n$ le cardinal de $E$ et $p$ celui de $F$. $E\times F$ est constitué des couples $(x;y)$ avec $x \in E$ et $y \in F$. Pour chaque élément $x$ de $E$, il y a $p$ couples possibles (un couple par élément de $F$). Puisqu'il y a $n$ éléments dans $E$, on a donc
\[\underbrace{p+\cdots +p}_{n \textrm{ fois}}=np \textrm{ éléments dans } E \times F\]
\end{demonstration}
\else
\lignes{5}
\fi

\afaire{Exercices \lienexo{01} et \lienexo{02}.}

\section{Dénombrement}

Dans cette partie, nous allons considérer des listes et des ensembles.

\begin{definition}
	On appelle \textbf{liste} de $p$ éléments d'un ensemble $E$ une suite \underline{ordonnée} de $p$ éléments.
\end{definition}

\begin{exemple}
	Ainsi, les listes $(1;2;3)$ et $(1;3;2)$ sont deux listes distinctes, et les ensembles $\{1;2;3\}$ et $\{1;3;2\}$ sont identiques.
\end{exemple}

	\subsection{Permutation}

\begin{definition}
	Soit $E$ un ensemble non vide à $n$ éléments. On appelle \textbf{permutation} de $E$ une liste (sans répétition) des $n$ éléments de $E$.
\end{definition}

\begin{exemple}
	Si $E=\{a;b;c\}$, alors $(a;c;b)$ et $(b;a;c)$ sont deux permutations de $E$.
\end{exemple}

\begin{definition}
	On note $\mathcal{S}(E)$ l'ensemble des permutations de l'ensemble $E$. En particulier, on note $\mathcal{S}_n$ l'ensemble des permutations de l'ensemble $\{1;\cdots;n\}$.
\end{definition}

\begin{theoreme}
	Le nombre de permutations d'un ensemble de $n$ éléments, $n\geq 1$, est égal à
\[n!=n\times(n-1)\times(n-2)\times\cdots\times 2\times 1\]
\end{theoreme}

\ifprof
\begin{demonstration}
Supposons qu'on dispose de $n$ cases, numérotées de $1$ à $n$. Dans la case numéro $1$, on peut mettre un des $n$ éléments de $E$. Une fois la case $1$ remplie, il ne reste que $n-1$ éléments à choisir. On en prend un qu'on met dans la case $2$. Il ne reste alors que $n-2$ éléments. Et on réitère.
\end{demonstration}
\else
\lignes{4}
\fi

\begin{exo}
	On dispose de $4$ personnes , à disposer sur $4$ chaises. Combien y a-t-il de possibilités ?
\end{exo}


\solution[4]{On doit placer $4$ personnes sur $4$ chaises. Il faut donc faire une permutation de ces $4$ personnes. Il y a donc
 \[4! = 4\times 3 \times 2 \times 1 =24 \textrm{ possibilités}\]}

	\subsection{Liste sans répétitions de $p$ éléments de $E$}

\begin{definition}
	Une \textbf{liste sans répétitions} (ou \textbf{arrangement}) de $p$ éléments de $E$ est une liste de $p$ éléments de $E$ deux à deux distincts ($1\leq p \leq n$).
\end{definition}

\begin{theoreme}
	Soit $E$ un ensemble à $n$ éléments, $n\geq 1$ et  $p$ un entier $1\leq p \leq n$. Le nombre de listes sans répétitions de $p$ éléments de $E$ est égal à
\[A_n^p=n\times (n-1)\times (n-2)\times\cdots\times (n-(p-1))=\frac{n!}{(n-p)!}\]
\end{theoreme}

\begin{demonstration}
On utilise le même raisonnement avec les cases, sauf qu'au lieu de mettre les $n$ éléments de $E$, on n'en met que $p$, en utilisant $p$ cases.
\end{demonstration}

\begin{exo}
	Une association ayant $20$ membres souhaite élire leur bureau, composé d'un président, d'un vice-président et d'un trésorier. Combien de bureaux est-il possible de composer ?
\end{exo}

\solution[5]{Dans cet exercice, l'ordre est important (on ne choisit pas $3$ personnes parmi les $20$, on choisit très exactement un président, un vice-président et un trésorier parmi les $20$). Quand l'ordre compte, on parle donc d'arrangement.\\
Ici, on veut donc des listes de $3$ éléments d'un ensemble à $20$ éléments. Il y en a donc
\[A_{20}^3 = 20\times 19 \times 18 = 6840 \textrm{ bureaux possibles}\]}

\begin{exo}
	Toujours dans la même association, il y a $12$ hommes et $8$ femmes. On impose que le trésorier soit une femme. Combien y a-t-il de bureaux possibles ?
\end{exo}

\solution[5]{Le poste de trésorier est une femme. Il faut donc choisir une femme parmi les $8$, soit $8$ possibilités.\\Pour les deux autres postes, comme ce qui précède, on a $\displaystyle{A_{19}^2 = 19\times 18 = 342}$ bureaux possibles (sachant que la personne trésorière n'aura pas d'autres postes). Cela donne donc
\[8 \times A_{19}^2 = 2736 \textrm{ bureaux possibles}\]}

	\subsection{Liste avec répétitions de $p$ éléments de $E$}

\begin{theoreme}
	Soit $p\geq 1$. Il y a $n^p$ listes avec répétitions de $p$ éléments de $E$.
\end{theoreme}

\ifprof
\begin{demonstration}
En effet, si on possède $p$ cases, on peut mettre dans chacune des cases l'un des $n$ éléments de $E$.
\end{demonstration}
\else
\lignes{2}
\fi

\begin{exo}
	Dans une classe de $30$ élèves, on décide que chaque jour pendant $3$ jours, une personne va nettoyer le tableau, sachant qu'une personne ayant déjà été de corvée peut y retourner. Combien y a-t-il de possibilités ?
\end{exo}

\solution[3]{Il faut donc choisir $3$ élèves, avec répétition. Il y a donc
\[30^3 = 27000 \textrm{ possibilités}\]}

\section{Combinaisons}

	\subsection{Définition}

\begin{definition}
\label{objectif-09-4}
	Soit $E$ un ensemble à $n$ éléments, et $p$ un entier tel que $0\leq p\leq n$.\\Une \textbf{combinaison} de $p$ éléments de $E$ est un sous-ensemble (ou une partie) de $E$ qui contient $p$ éléments.
\end{definition}

\begin{exemple}
	Si $E=\{a;b;c\}$ et $p=2$, les combinaisons de deux éléments de $E$ sont les parties $\{a;b\},\{a;c\}$ et $\{b;c\}$.
\end{exemple}

\begin{notation}
  Le nombre de combinaisons de $p$ éléments d'un ensemble à $n$ éléments est noté $\displaystyle{\binom{n}{p}}$ et on lit ``$p$ parmi $n$''. On note aussi $\mathrm{C}^p_n$.
\end{notation}

\begin{exemple}
	D'après l'exemple précédent, $\displaystyle{\binom{3}{2}=3}$.
\end{exemple}

\begin{remarque}
	Pour tout entier $n$, on obtient rapidement :
	\begin{multicols}{3}
	\begin{description}
	\item 	$\ds{\binom{n}{0} =1}$
	\item $\ds{\binom{n}{1}= n }$
	\item $\ds{\binom{n}{n}=1}$
	\end{description}
	\end{multicols}
\end{remarque}

\afaire{Exercice \lienexo{03}.}

	\subsection{Nombre de combinaisons}


\begin{theoreme}
\label{objectif-09-5}
	Pour tout entier $n\geq 1$, et pour tout entier $p$ tel que~ $0\leq p \leq n$, on a
\[\binom{n}{p}=\frac{n!}{p!(n-p)!}=\frac{n(n-1)\cdots(n-p+1)}{p!}\]
Pour $p> n$, on a $\binom{n}{p}=0$.
\end{theoreme}

\ifprof
\begin{demonstration}
\begin{itemize}
	\item Pour $p=0$, il n'existe qu'une seule partie sans élément : la partie vide. Donc \[\binom{n}{0}=1=\frac{n!}{0!(n-0)!}\]
	\item Supposons $p>0$. Prenons une partie $F$ de $p$ éléments de $E$. On constate qu'il y a $p!$ permutations de $F$, et une permutation de $F$ est une liste sans répétition de $p$ éléments.\\Si on fait de même avec toutes les parties de $E$ à $p$ éléments, on va décrire toutes les listes sans répétition de $p$ éléments, et une seule fois (deux parties distinctes de $E$ vont engendrer des listes distinctes nécessairement). On a donc
	\[\textrm{(nb de partie à }p\textrm{ éléments de }E)\times p!=\textrm{nb de listes sans répétition de }p\textrm{ éléments}\]
	soit
	\[\binom{n}{p}\times p! = \frac{n!}{(n-p)!}\]
\end{itemize}
\end{demonstration}
\else
\lignes{8}
\fi

\begin{remarque}
  On retrouve la définition vue dans le chapitre $3$ du nombre combinatoire, ce qui est rassurant.
\end{remarque}

\begin{exemple}
	Le nombre de partie à $4$ éléments d'un ensemble à $21$ éléments est
\[\binom{21}{4}=\frac{21!}{4!(21-4)!}=5985\]
\end{exemple}

\begin{exo}
	Dans une association de $12$ hommes et $8$ femmes, on crée un comité Hygiène et Sécurité, composé de $3$ personnes.
\begin{itemize}
    \item Combien y a-t-il de comités possibles ?
    \item Combien y a-t-il de comités sachant qu'une des personnes doit être une femme ?
\end{itemize}
\end{exo}

\solution[6]{
\begin{itemize}[label=\textbullet]
    \item Il nous faut choisir (sans ordre) $3$ personnes parmi $20$. Il y a donc
    \[\binom{20}{3} = 1140 \textrm{ comités possibles}\]
    \item On veut au moins une femme.\\\logoattention~Il n'y a pas $\displaystyle{\binom{8}{1}\binom{19}{2}}$ comités possibles avec au moins une femme, car en comptant ainsi, certains comités sont comptés plusieurs fois !\\
    Notons $A$ l'ensemble des comités ayant au moins une femme, $B$ l'ensemble des comités n'ayant que des hommes, et $C$ l'ensemble de tous les comités possibles. Alors $A \cup B = C$ et $A\cap B = \vide$. Or
    \[\Card(B)=\binom{12}{3} = 220 \textrm{ comités}\]
    \[\Card(C)= \binom{20}{3}=1140 \textrm{ comités}\]
    Donc
    \[\Card(A)=\Card(C)-\Card(B) =  920 \textrm{ comités}\]
\end{itemize}
}

\begin{methode}
\label{objectif-09-2} \label{objectif-09-3}
Dans un exercice, il faut déterminer en premier lieu si on va devoir utiliser les listes sans répétition, avec répétition ou les combinaisons. On retiendra que :
\begin{itemize}
	\item si on s'intéresse à un choix ordonné (par exemple, un classement à un jeu, ou bien le choix de différents postes dans une association), on utilisera les \emph{listes}, sans répétition (cas général où une personne ne peut pas être à deux endroits en même temps), ou avec répétition (si au contraire on l'accepte).
	\item si on s'intéresse à la sélection \textbf{simultanée} (donc on ne tient pas compte de l'ordre), on utilisera \emph{les combinaisons}.
\end{itemize}
\end{methode}

\afaire{Exercices \lienexo{04}, \lienexo{05}, \lienexo{06} et \lienexo{07}.}

\section{Formules}

	\subsection{Formules de base}\label{objectif-09-6}

\begin{theoreme}
	Pour tous naturels $n$ et $p$ tels que ~$0\leq p \leq n$, on a \[\binom{n}{n-p}=\binom{n}{p}\]
(\emph{Formule du triangle de Pascal})~Pour tous naturels $n$ et $p$ tels que ~$1\leq p \leq n-1$ on a
\[\binom{n-1}{p-1}+\binom{n-1}{p}=\binom{n}{p}\]
\end{theoreme}

\ifprof
\begin{demonstration}
\begin{itemize}[label=\textbullet]
	\item En effet, \[\binom{n}{n-p}=\frac{n!}{(n-p)!(n-(n-p))!}=\frac{n!}{(n-p)!p!}=\binom{n}{p}\]
	\item On a
	\[\binom{n-1}{p-1}+\binom{n-1}{p}=\frac{(n-1)!}{(p-1)!(n-p)!}+\frac{(n-1)!}{p!(n-p-1)!}\]
	En mettant au dénominateur commun $p!(n-p)!$, on a alors
	\[\begin{aligned}\binom{n-1}{p-1}+\binom{n-1}{p}&=\frac{p(n-1)!}{p!(n-p)!}+\frac{(n-p)(n-1)!}{p!(n-p)!}\\
	&=\frac{(n-1)!\left[p+(n-p)\right]}{p!(n-p)!}\\&=\frac{n!}{p!(n-p)!}=\binom{n}{p}\end{aligned}\]
\end{itemize}
\end{demonstration}
\else
\lignes{9}
\fi

\begin{remarque}
	La deuxième formule nous permet d'obtenir tous les nombres combinatoires de proche en proche, dans le \textbf{Triangle de Pascal} :
\begin{center}\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
$n \backslash p$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 \\
\hline
0 & 1 & & & & & &\\
1 & 1 & 1& & & & &\\
2 & 1 & 2 & 1 &&&&\\
3 & 1 & 3 & 3 & 1 & & &\\
4 & 1 & 4 & 6 & 4 & 1 & &\\
5 & 1 & 5 & 10 & 10 & 5 & 1 &\\
6 & 1 & 6 & 15 & 20 & 15 & 6 & 1\\
\hline
\end{tabular}
\end{center}
\end{remarque}

\begin{proposition}
	Pour tout entier $n$ strictement positif, et tout entier $k$ avec $1\leq k \leq n$, on a
\[k\binom{n}{k} = n \binom{n-1}{k-1}\]
\end{proposition}

\ifprof
\begin{demonstration}
On a, en effet :
\[n \binom{n-1}{k-1} = n \frac{(n-1)!}{(k-1)!((n-1)-(k-1))!} = \frac{n (n-1)!}{(k-1)! (n-k)!} = \frac{n!}{ (k-1)!(n-k)!}\]
soit
\[n \binom{n-1}{k-1} = \frac{k\times n!}{k \times (k-1)! (n-k)!}=k \frac{n!}{k!(n-k)!}=k\binom{n}{k}\]
\end{demonstration}
\else
\lignes{5}
\fi


	\subsection{Formule de Vandermonde}

Une première formule intéressante liant les nombres combinatoires est la formule de Vandermonde, formule que l'on va démontrer de manière purement combinatoire.

\begin{theoreme}[Formule de Vandermonde]
Soient $m$ et $n$ deux entiers strictement positifs. Pour tout entier $k$ tel que $k\leq m$ et $k\leq n$, on a
\[\binom{m+n}{k} = \sum_{j=0}^k \binom{m}{j}\binom{n}{k-j}\]
\end{theoreme}

\ifprof
\begin{demonstration}
On dispose de $n$ jetons blancs, et $m$ jetons noirs, tous indiscernables au toucher. On tire simultanément $k$ jetons et on note $E$ l'ensemble des tirages possibles. \\ Par définition de $E$, on a \[\card(E)=\binom{m+n}{k}\]
	Notons alors $E_j$ (pour $j$ entier entre $0$ et $k$) l'ensemble des tirages de $k$ jetons ayant $j$ jetons noirs. Puisqu'il y a $j$ jetons noirs, il y a $k-j$ jetons blancs dans $E_j$. Ainsi \[\card(E_j) = \binom{m}{j}\binom{n}{k-j}\]
	Constatons enfin que, par définition, $\displaystyle{E=\bigcup_{j=0}^k E_j}$ et les $E_j$ sont deux-à-deux disjoints. Ainsi,
	\[ \card(E) = \sum_{j=0}^k \card(E_j) \] ce qui donne
	\[ \binom{m+n}{k} = \sum_{j=0}^k \binom{m}{j} \binom{n}{k-j} \]
\end{demonstration}
\else
\lignes{10}
\fi

\begin{experiencehistorique}
La formule de Vandermonde, nommée d'après \textbf{Alexandre-Théophile Vandermonde}, est utilisée en probabilité pour déterminer l'espérance d'une loi particulière, appelée loi \textbf{hypergéométrique}.
\end{experiencehistorique}

\begin{exo}
	En utilisant la formule de Vandermonde, déterminer $\displaystyle{\sum_{j=0}^n \binom{n}{j}^2 }$.
\end{exo}

\solution[5]{En appliquant la formule de Vandermonde au cas particulier $k=m=n$, on obtient
\[ \binom{2n}{n} = \sum_{j=0}^n \binom{n}{j} \binom{n}{n-j} \]
Or, $\displaystyle{\binom{n}{n-j} = \binom{n}{j}}$. Donc
\[ \binom{2n}{n} = \sum_{j=0}^n \binom{n}{j} \binom{n}{j} = \sum_{j=0}^n \binom{n}{j}^2 \]
}

	\subsection{Formule du binôme de Newton}

Une autre formule est une relation importante qui servira avec les matrices.

\begin{theoreme}[Formule du binôme de Newton]\label{objectif-09-7}
	Pour tous nombres réels $a$ et $b$, et pour tout entier naturel $n\geq 1$, on a
\[(a+b)^n=\binom{n}{0}a^n+\binom{n}{1}a^{n-1}b+\cdots+\binom{n}{p}a^{n-p}b^p+\cdots +\binom{n}{n-1}ab^{n-1}+\binom{n}{n}b^n\]
soit \[(a+b)^n=\sum_{k=0}^n \binom{n}{k} a^{n-k}b^k=\sum_{k=0}^n \binom{n}{k}a^k b^{n-k} \]
\end{theoreme}

\ifprof
\begin{demonstration}
Soit $P_n$ la proposition ``$(a+b)^n=\sum\limits_{k=0}^n \binom{n}{k} a^{n-k}b^k$'' définie pour tout entier $n\geq 1$ (\textit{le résultat est également vrai pour $n=0$}).
\begin{itemize}[label=\textbullet]
	\item Pour $n=1$ le résultat est vrai car $(a+b)^1=a+b=\binom{1}{0}a+\binom{1}{1}b$.
	\item Supposons la proposition $P_n$ vraie pour un entier $n\geq 1$, et calculons $(a+b)^{n+1}$ :
	\[\begin{aligned}
		(a+b)^{n+1} &= (a+b)(a+b)^n\\
				    &= a(a+b)^n + b(a+b)^n\\
				    &=\binom{n}{0}a^{n+1}+\binom{n}{1}a^{n}b+\cdots+\binom{n}{p}a^{n-p+1}b^p+\cdots +\binom{n}{n}ab^n\\
				    &~~~~~~~~~~~~~~~~+ \binom{n}{0}a^nb+\cdots+\binom{n}{p-1}a^{n-p+1}b^{p}+\cdots +\binom{n}{n-1}ab^{n}+\binom{n}{n}b^{n+1}
		\end{aligned}\]
Or pour tout entier $1\leq p \leq n$, on a $\displaystyle{\binom{n-1}{p-1}+\binom{n-1}{p}=\binom{n}{p}}$ (formule du triangle de Pascal), donc
\[(a+b)^{n+1}=\binom{n}{0}a^{n+1}+\binom{n+1}{1}a^nb+\cdots+\binom{n+1}{p}a^{n-p+1}b^p+\cdots \binom{n+1}{n}ab^n+\binom{n}{n}b^{n+1}\]
ce qui donne le résultat annoncé, puisque $\ds{\binom{n}{0}=\binom{n+1}{0}=1}$ et $\ds{\binom{n}{n}=\binom{n+1}{n+1}=1}$.
\end{itemize}
\end{demonstration}
\else
\lignes{25}
\fi

\begin{exo}
	Calculer \[A_n=\sum_{k=0}^n \binom{n}{k} \textrm{ et } B_n=\sum_{k=0}^n \binom{n}{k}2^k\]
\end{exo}

\solution[6]{\begin{itemize}[label=\textbullet]
    \item Prenons $a=b=1$ et appliquons la formule du binôme de Newton :
    \[(1+1)^n = \sum_{k=0}^n \binom{n}{k} 1^{k} 1^{n-k} = \sum_{k=0}^n \binom{n}{k}\]
    Donc $A_n=(1+1)^n=2^n$.
    \item Prenons $a=2$ et $b=1$ et appliquons la formule du binôme de Newton :
    \[(1+2)^n = \sum_{k=0}^n \binom{n}{k} 2^k 1^{n-k} = \sum_{k=0}^n \binom{n}{k} 2^k\]
    Donc $B_n=(1+2)^n =3^n$.
\end{itemize}
}

\afaire{Exercice \lienexo{08}.}
