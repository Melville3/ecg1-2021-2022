\chapter{Matrices}
%!TeX root=../../encours.nouveau.tex

\objectifintro{On introduit la notion de matrices, qui nous servira plus tard dans l'année. On voit également le lien avec les systèmes linéaires. On étudiera enfin la méthode de Gauss-Jordan pour l'inversibilité d'une matrice.}

\begin{extrait}{Paracelse (1493 -- 1541)}
Il y a des temps pour toutes choses ; et les temps sont les matrices de toutes choses. Ils ne suivent donc pas une seule voie, mais empruntent des milliers de chemins.
\end{extrait}

\begin{objectifs}
\begin{numerote}
                                \item Savoir calculer avec les matrices (sommes, produits, transposés)\dotfill $\Box$
                                \item Connaître définition et propriétés des matrices inversibles\dotfill $\Box$
                                \item Savoir déterminer le rang d'une matrice\dotfill $\Box$
                                \item Savoir faire le lien entre système et matrice associée\dotfill $\Box$
                                \item Savoir déterminer la puissance $n^\text{ième}$ d'une matrice...
                                \begin{itemize}[label=\textbullet]
                                        \item par récurrence, en conjecturant l'allure générale\dotfill $\Box$
                                        \item par la formule du binôme de Newton\dotfill $\Box$
                                        \item par diagonalisation, lorsque celle-ci est donnée\dotfill $\Box$
                                \end{itemize}
\end{numerote}
\end{objectifs}

 \section{Matrices}

     \subsection{Définition}

 \begin{definition}
 Soient $n$ et $p$ deux entiers non nuls. On appelle \textbf{matrice} à $n$ lignes et $p$ colonnes à coefficients réels un tableau rectangulaire de nombres réels comportant $n$ lignes et $p$ colonnes.

 \[\underbrace{\matrice{ ~&~\\~&~}}_{p \textrm{ colonnes}}\left.\begin{array}{c}~\\~\end{array}\right\} n \textrm{ lignes}\]

 En général, lorsque $A$ est une matrice à $n$ lignes et $p$ colonnes, le coefficient situé à l'intersection de la $i\up{ème}$ ligne et de la $j\up{ème}$ colonne se note $a_{i,j}$. On écrit alors
 \[A=
 \matrice{
 a_{1,1}& a_{1,2} & \hdots & a_{1,j} & \hdots & a_{1,p}\\
 a_{2,1}& a_{2,2} & \hdots & a_{2,j} & \hdots & a_{2,p}\\
 \vdots & \vdots & & \vdots & & \vdots \\
 a_{i,1}& a_{i,2} & \hdots & a_{i,j} & \hdots & a_{i,p}\\
 \vdots & \vdots & & \vdots & & \vdots \\
 a_{n,1}& a_{n,2} & \hdots & a_{n,j} & \hdots & a_{n,p}
 }
  \textrm{ ou } A = (a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}}\]
 \end{definition}


 \begin{notation}
 L'ensemble des matrices à $n$ lignes et $p$ colonnes à coefficients réels est noté $\MM_{n,p}(\R)$.
 \end{notation}


 \begin{definition}
 	\begin{itemize}[label=\textbullet]
      \item On appelle \textbf{matrice ligne} $\matrice{.&.&.}$ un élément de $\MM_{1,p}(\R)$.
      \item On appelle \textbf{matrice colonne} $\matrice{.~\\.~\\.~}$ un élément de $\MM_{n,1}(\R)$.
      \item On appelle \textbf{matrice nulle} de $\MM_{n,p}(\R)$, notée $0_{n,p}$ (ou $0$ quand il n'y a pas d'ambiguïté), la matrice de $\MM_{n,p}(\R)$ dont tous les coefficients sont nuls.
      \item Pour $i\in \interent{1 n}$ et $j\in \interent{1 p}$, on note $E_{i,j}\in \MM_{n,p}(\R)$ la matrice ayant tous ses coefficients nuls, sauf celui en ligne $i$, colonne $j$, qui vaut $1$.
      \input{elementaire}
      Les $\left(E_{i,j}\right)_{\substack{1\leq i \leq n\\1\leq j\leq p}}$ sont appelées les \textbf{matrices élémentaires}.
  \end{itemize}
 \end{definition}

     \subsection{L'algèbre des matrices}

         \subsubsection{Addition de matrices}

 \begin{definition}
Soient $A=(a_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ et $B=(b_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ deux matrices de $\MM_{n,p}(\R)$. On appelle \textbf{somme} de la matrice $A$ et de la matrice $B$ la matrice de $\MM_{n,p}(\R)$, notée $A+B$ définie par
 \[A+B=(c_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}} \textrm{ où } \forall i\in \llbracket 1,n \rrbracket,~\forall j\in \llbracket 1,p \rrbracket,~ c_{i,j}=a_{i,j}+b_{i,j}\]
 \end{definition}


 \begin{exemple}
Si $A=\matrice{1 & 2\\2 & -1\\ 1& 0}$ et $B=\matrice{0 & 1\\-1 & 1 \\ 2& 1}$, alors
  $A+B=\matrice{1 & 3 \\ 1 & 0 \\ 3 & 1}$
 \end{exemple}

         \subsubsection{Multiplication par un réel}

 \begin{definition}
Soient $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ une matrice de $\MM_{n,p}(\R)$ et $\lambda$ un nombre réel. On appelle \textbf{produit} de la matrice $A$ par le réel $\lambda$ la matrice de $\MM_{n,p}(\R)$, notée $\lambda A$, définie par
 \[\lambda A=(c_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}} \textrm{ où } \forall i\in \llbracket 1,n \rrbracket,~\forall j\in \llbracket 1,p \rrbracket,~ c_{i,j}=\lambda a_{i,j}\]
 \end{definition}

 \begin{exemple}
Si $A=\matrice{1 & 2\\2 & -1\\ 1& 0}$ alors $2A=\matrice{2 & 4\\4 & -2\\ 2& 0}$
 \end{exemple}

         \subsubsection{Premières propriétés}

 \begin{propriete}
Soient $A, B$ et $C$ trois éléments de $\MM_{n,p}(\R)$, et $\lambda, \mu$ deux nombres réels.
 \begin{itemize}[label=\textbullet]
     \item $A+B=B+A$ (commutativité de l'addition).
     \item $0+A=A+0=A$ ($0$ est le neutre de l'addition)
     \item $A+(-A)=(-A)+A=A-A=0$ ($-A$ est l'opposé de la matrice $A$.)
     \item $(\lambda+\mu)A=\lambda A +\mu A$, $\lambda(A+B)=\lambda A+ \lambda B$ et $\lambda (\mu A) = (\lambda \mu)A = \lambda \mu A$ (distributivités)
 \end{itemize}
 \end{propriete}

 \begin{remarque}
Les différentes propriétés précédentes font de l'ensemble $\MM_{n,p}(\R)$, muni de l'addition et la multiplication par un réel, un \textbf{espace vectoriel}. Nous y reviendrons plus tard dans l'année.
 \end{remarque}

 \begin{remarque}
On peut manipuler, pour ces opérations, ainsi les matrices comme les nombres réels. Par exemple,  l'équation $X+A=B$ d'inconnue la matrice $X$, admet comme unique solution $X=B-A$. De même, l'équation $2X=A$ d'inconnue la matrice $X$ admet comme unique solution $X=\frac{1}{2}A$.
 \end{remarque}

Les matrices élémentaires sont intéressantes : on peut écrire toute matrice en fonction de celles-ci.

\begin{proposition}
  Soit $A=\left(a_{i,j}\right)_{\substack{1\leq i \leq n\\1\leq j \leq p}} \in \MM_{n,p}(\R)$. Alors, on peut écrire $A$ sous la forme
  \begin{align*}
A &= \sum_{i=1}^n \sum_{j=1}^p a_{i,j} E_{i,j}
  \end{align*}
On dira dans le chapitre suivant que $(E_{i,j})$ forme une \textit{base} de l'espace vectoriel des matrices.
\end{proposition}


         \subsubsection{Produit matriciel}

 \begin{definition}
 Soient $A=(a_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ une matrice $\MM_{n,p}(\R)$ et $B=(b_{i,j})_{\substack{1\leq i \leq p\\1 \leq j \leq m}}$ une matrice $\MM_{p,m}(\R)$.
 On appelle \textbf{produit} de la matrice $A$ par la matrice $B$ la matrice de $\MM_{n,m}(\R)$, notée $A\times B$ ou $AB$, définie par
 \[AB=(c_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq m}} \textrm{ où } \forall i\in \llbracket 1,n \rrbracket,~\forall j\in \llbracket 1,m \rrbracket,~ c_{i,j}=a_{i,1}b_{1,j}+a_{i,2}b_{2,j}+\cdots +a_{i,p}b_{p,j}=\sum_{k=1}^p a_{i,k}b_{k,j}\]
 \end{definition}


 Par exemple :
 \begin{center}
 \input{mult.tex}
 \end{center}

 \begin{remarque}
 	\begin{itemize}[label=\textbullet]
     \item \logoattention Pour multiplier deux matrices, il faut qu'elles soient compatibles : lorsque l'on calcule $AB$ il faut que le nombre de colonnes de $A$ soit égal au nombre de lignes de $B$.
     \item \logoattention La multiplication des matrices n'est pas \textbf{commutative} : en général, $AB \neq BA$. Par exemple
 \[\matrice{ 1&1\\ 0&0} \matrice{1 & 0 \\ 1 & 0} = \matrice{2 & 0 \\ 0 & 0} \textrm{ et } \matrice{ 1 & 0 \\ 1 & 0}\matrice{1&1\\ 0&0}=\matrice{1 & 1 \\ 1 & 1}\]
     \item \logoattention  Contrairement à ce qui se passe dans $\R$, on peut avoir $AB=0$ sans pour autant que  $A$ et $B$ soient nuls. Par exemple
 \[\matrice{1&0\\ 0&0}\matrice{0 & 0 \\ 1 & 1} = \matrice{0 & 0 \\ 0 & 0}\]
 On dit que $\MM_{n,p}(\R)$ n'est pas \textbf{intègre}.
 \end{itemize}
 \end{remarque}

\begin{exemple}
Soient $\displaystyle{A=\matrice{1&0\\1&1\\-1&2}}$ et $\displaystyle{B=\matrice{1&1&0\\1&-1&1}}$. Alors
 \[AB=\matrice{ 1 & 1 & 0 \\ 2 & 0 & 1\\1 & -3 & 2} \textrm{ et } BA = \matrice{ 2 & 1 \\-1 & 1}\]
\end{exemple}

\begin{propriete}
Soient $A,B,C$ trois matrices (que l'on considère compatibles pour les multiplications envisagées), et $\lambda$ un réel.
 \begin{enumerate}[label=\textbullet]
     \item $A(BC)=(AB)C=ABC$ (associativité de la multiplication)
     \item $(\lambda A)B=A(\lambda B) = \lambda AB$
     \item ] $(A+B)C=AC+BC$ et $C(A+B)=CA+CB$ (distributivités)
 \end{enumerate}
\end{propriete}

\afaire{Exercice \lienexo{01}.}


         \subsubsection{Transposition}

 \begin{definition}
 	Soit $A=(a_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ une matrice $\MM_{n,p}(\R)$.
 On appelle \textbf{transposée} de la matrice $A$ la matrice de $\MM_{p,n}(\R)$, notée $\,^tA$ ou $A^\mathsf{T}$, définie par
 \[\,^tA=(c_{i,j})_{\substack{1\leq i \leq p\\1\leq j \leq n}} \textrm{ où } \forall i\in \llbracket 1,p \rrbracket,~\forall j\in \llbracket 1,n \rrbracket,~ c_{i,j}=a_{j,i}\]
 Ainsi, la matrice $\,^t A$ est la matrice obtenue à partir de $A$ par symétrie, en échangeant les lignes et les colonnes.
 \end{definition}

\begin{exemple}
	Si $A=\matrice{ 1&2\\-1&-3\\0&4}$, alors
 $\,^tA=\matrice{1 & -1 & 0 \\ 2 & -3 & 4}$.
\end{exemple}

\begin{propriete}
Soient $A$ et $B$ deux matrices (que l'on considère compatibles pour les multiplications envisagées) et $\lambda$ un réel.
 \begin{itemize}[label=\textbullet]
     \item $\,^t (A+B) = \,^t A+\,^t B$ et $\,^t (\lambda A)=\lambda \,^t A$.
     \item $\,^t (\,^t A)=A$ et $\,^t (AB)= \,^t B \,^t A$.
 \end{itemize}
\end{propriete}

\preuve[10]{
  Notons $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ et $B=(b_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ deux matrices de $\MM_{n,p}(\R)$. Alors
\[ A+B = (a_{i,j}+b_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}} \qeq \lambda A = (\lambda a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}} \]
et donc
\[ \,^t (A+B) =  (a_{j,i}+b_{j,i})_{\substack{1\leq i \leq n\\1\leq j \leq p}} = \,^t A + \,^t B \]
et
\[ \,^t (\lambda A) = (\lambda a_{j,i})_{\substack{1\leq i \leq n\\1\leq j \leq p}} = \lambda \,^t A. \]
Enfin, soient  $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ et $B=(b_{i,j})_{\substack{1\leq i \leq p\\1\leq j \leq q}}$. Notons $C=AB$, $C=(c_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq q}}$, $D=\,^t B\,^t A$ et $D=(d_{i,j})_{\substack{1\leq i \leq q\\1\leq j \leq n}}$. On a, par définition
\[ \forall i\in \interent{1 n},\, \forall j\in \interent{1 q},\quad  c_{i,j} = \sum_{k=1}^p a_{i,k} b_{k,j}. \]
et
\[ \forall i\in \interent{1 q},\, \forall j\in \interent{1 n},\quad  d_{i,j} = \sum_{k=1}^p b_{k,i} a_{j,k}. \]
On constate alors que
\[ \forall i\in \interent{1 q},\, \forall j\in \interent{1 n},\quad  d_{i,j} = \sum_{k=1}^p b_{k,i} a_{j,k}=\sum_{k=1}^p a_{j,k}b_{k,j} = c_{j,i} \]
Ainsi,
\[ \,^t B \,^t A = \,^t (AB). \]
}

\afaire{Exercice \lienexo{02}.}

 \section{Matrices carrées}

     \subsection{Définitions}

         \subsubsection{Matrices carrées}

 \begin{definition}
Une \textbf{matrice carrée} d'ordre $n$ est une matrice à $n$ lignes et à $n$ colonnes. On notera $\MM_n(\R)$ l'ensemble des matrices carrées d'ordre $n$, plutôt que $\MM_{n,n}(\R)$. De même, on notera $0_n$ la matrice nulle de $\MM_n(\R)$.
 \end{definition}

 \begin{exemple}
La matrice $\matrice{ 1&2\\-1&-3}$ est une matrice carrée d'ordre $2$.
 \end{exemple}

\begin{definition}
Si $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq n}}$ est une matrice carrée, on appelle \textbf{diagonale} de $A$ les coefficients $(a_{i,i})_{1\leq i \leq n}$.
\end{definition}

\begin{exemple}
Dans l'exemple précédent, la diagonale est $(1, -3)$.
\end{exemple}

         \subsubsection{Matrices diagonales et triangulaires}

\begin{definition}
	\begin{itemize}[label=\textbullet]
     \item Une \textbf{matrice diagonale} d'ordre $n$ est une matrice carrée d'ordre $n$ où tous les coefficients sont nuls sauf éventuellement ceux de la diagonale :
     \[ \matrice{ a_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n} }\]
     On écrira plus simplement $\mathrm{Diag}(\lambda_1,\hdots,\lambda_n)$ pour désigner la matrice $\matrice{ \lambda_1 & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & \lambda_n }$.
     \item La \textbf{matrice identité} de $\MM_n(\R)$, notée $I_n$, est la matrice diagonale avec des $1$ sur la diagonale :
      \[I_n=\matrice{1 & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & 1 } = \mathrm{Diag}(1,\hdots,1).\]
      \item Une \textbf{matrice triangulaire supérieure} $(a_{i,j})$ est une matrice telle que
      \[\forall i\in \llbracket 1,n \rrbracket, \forall j\in \llbracket 1,n \rrbracket, i>j \implies a_{i,j}=0\]
      Ainsi, elle est de la forme
      \[\matrice{
           a_{1,1} & a_{1,2} & \hdots & a_{1,n-1} & a_{1,n} \\
           0 & a_{2,2} & \hdots & \vdots & \vdots \\
           \vdots & 0 & \ddots & \vdots & \vdots \\
           \vdots & \vdots & \ddots & a_{n-1,n-1} & a_{n-1,n} \\
           0 & \hdots & 0 & 0 & a_{n,n}
       }\]
      \item Une \textbf{matrice triangulaire inférieure} $(a_{i,j})$ est une matrice telle que
      \[\forall i\in \llbracket 1,n \rrbracket, \forall j\in \llbracket 1,n \rrbracket, i<j \implies a_{i,j}=0\]
      Ainsi, elle est de la forme
      \[\matrice{
           a_{1,1} & 0 & \hdots & 0 & 0 \\
           a_{2,1} & a_{2,2} & 0 & \hdots & \vdots \\
           \vdots & a_{3,2} & \ddots & \ddots & \vdots \\
           \vdots & \vdots & \ddots & a_{n-1,n-1} & 0\\
           a_{n,1} & \hdots & \hdots & a_{n,n-1} & a_{n,n}
       }\]
 \end{itemize}
\end{definition}

\begin{notation}[Ensembles particuliers]
  On note traditionnellement $\mathscr{D}_{n}(\R)$ (respectivement $\mathscr{T}^+_{n}(\R)$, $\mathscr{T}^-_{n}(\R)$) l'ensemble des matrices diagonales d'ordre $n$ (resp. triangulaires supérieures, triangulaire inférieure).
\end{notation}

\begin{remarque}
La matrice identité $I_n$ est \textbf{neutre} pour la multiplication : quelle que soit la matrice $A$ de $\MM_n(\R)$, on a $AI_n=I_nA=A$.
\end{remarque}


             \subsubsection{Matrices symétriques}

\begin{definition}
Soit $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq n}}$ une matrice carrée.
\begin{itemize}[label=\textbullet]
	\item $A$ est dite \textbf{symétrique} si
\[\forall (i,j)\in \llbracket 1,n\rrbracket^2,~a_{i,j}=a_{j,i}\]
Ainsi, une matrice est symétrique si et seulement si $\,^tA=A$.
	\item $A$ est dite \textbf{antisymétrique} si $\,^tA=-A$.
\end{itemize}
On note $\mathcal{S}_n(\K)$ l'ensemble des matrices d'ordre $n$ symétriques, et $\mathcal{A}_n(\K)$ l'ensemble des matrices d'ordre $n$ antisymétiques.
\end{definition}

\begin{exemple}
La matrice $A=\matrice{ 1 & 2 \\ 2 & 3 }$ est symétrique.
\end{exemple}

     \subsection{Puissances d'une matrice carrée}


		\subsubsection{Définition de la puissance d'une matrice}

 Si $A$ et $B$ sont deux matrices de $\MM_n(\R)$, on peut alors calculer $AB$ et $BA$ (elles sont compatibles) et le produit est encore dans $\MM_n(\R)$. On peut alors définir la puissance $n^{\textrm{ième}}$ d'une matrice.

\begin{definition}
Soit $A$ une matrice carrée d'ordre $n$. Soit $k$ un entier. On définit $A^k$ de la manière suivante :
 \begin{itemize}[label=\textbullet]
     \item Si $k=0$, $A^0=I_n$.
     \item Si $k>0$, $A^k = \underbrace{A\times A \times \cdots \times A}_{k \textrm{ fois}}$.
 \end{itemize}
\end{definition}

\begin{propriete}
Par définition, pour tout matrice $A \in \MM_n(\R)$, et pour tous entiers $p$ et $q$, $A^{p}\times A^q = A^{p+q}$.
\end{propriete}

\begin{remarque}\logoparcoeur
Si $A$ et $B$ sont deux matrices carrées d'ordre $n$ diagonales, alors le produit $AB$ est facile à calculer; en effet, si $A=\displaystyle{\matrice{ a_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n} }}$ et $B=\displaystyle{\matrice{ b_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & b_{n,n} }}$, alors
\[ AB = \matrice{ a_{1,1}\times b_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n}\times b_{n,n} } \]
Ainsi, si $A$ est une matrice diagonale $A=\displaystyle{\matrice{ a_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n} }}$, alors pour tout entier $p$, on a
\[ A^p = \matrice{ a_{1,1}^p & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n}^p } \]
 \end{remarque}

		\subsubsection{Formule du binôme de Newton}

\begin{attention}
Puisque la multiplication des matrices n'est pas commutative, on n'a pas $(AB)^k=A^kB^k$. En effet, $(AB)^k=(AB)(AB)\cdots (AB)$ et il faut que $AB=BA$ pour pouvoir obtenir $A^kB^k$.
\end{attention}

Cela arrive cependant dans certains cas, ce qui permet de simplifier certains calculs :

 \begin{definition}
 Soient $A$ et $B$ deux matrices de $\MM_n(\R)$. On dit que $A$ et $B$ \textbf{commutent} si $AB=BA$.
 \end{definition}

\begin{exemple}
La matrice $I_n$ commute avec toutes les matrices de $\MM_n(\R)$. En effet, $AI_n=I_nA=A$.
\end{exemple}

\begin{theoreme}[Formule du binôme de Newton]
Soient deux matrices $A$ et $B$ de $\MM_n(\R)$ qui \underline{commutent}. Alors, pour tout entier $n$, on a
 \[(A+B)^n = \sum_{k=0}^n \binom{n}{k} A^k B^{n-k}\]
 \end{theoreme}

L'utilité principale de la formule du binôme des matrices est de pouvoir calculer la puissance de certaines matrices de manière \og rapide \fg{}.

		\subsubsection{Méthodes de calculs}

\begin{methode}[Calcul de puissance avec la formule du binôme]
Pour calculer $A^p$, on peut parfois utiliser la formule du binôme de Newton, en décomposant $A$ sous la forme $I_n+B$ avec $B$ une matrice dont les puissances sont faciles à calculer.
\end{methode}

\begin{exemple}
Soit $A=\matrice{1 & 0 & 1 \\ 0 & 1 & 0 \\ 0 & 0 & 1 }$. Calculer $A^n$ pour tout entier $n$.
\end{exemple}

 \solution[20]{On constate que $A=I_3+ B$ avec \[B=\matrice{ 0 & 0 & 1 \\ 0 & 0 & 0 \\ 0 & 0 & 0 }\]
 et $B^2=0$. Puisque $I_3$ et $B$ commutent (\textit{car $I_3$ commute avec toutes les matrices}), on en déduit que, pour tout entier $n\geq 2$
 \[A^n = (I_3+B)^n = \sum_{k=0}^n \binom{n}{k} B^k I_3^{n-k} = \binom{n}{0} B^0 + \binom{n}{1} B^1 + \underbrace{\binom{n}{2} B^2 +\cdots + \binom{n}{n}B^n}_{=0}\]
 Ainsi, pour tout entier $n\geq 2$,
 \[A^n = I_3 + nB = \matrice{ 1 & 0 & n \\ 0 & 1 & 0 \\ 0 & 0 & 1 }\] résultat qui est également vrai pour $n=0$ et $n=1$.
 }

\afaire{Exercice \lienexo{12}.}

\begin{methode}[Calcul de puissance par récurrence]
Pour calculer $A^p$, on peut également essayer de calculer les premières puissances, puis en déduire le résultat par récurrence sur $p$.
\end{methode}

\begin{exemple}
Soit $A=\matrice{1 & 1 \\0 & 1}$.  Calculer $A^n$ pour tout entier $n$.
\end{exemple}

 \solution[15]{On constate que \[A^0=I_2~~~~A^1=\matrice{ 1 & 1 \\0 & 1 }~~~~A^2=\matrice{1 & 2 \\0 & 1}~~~~A^3=\matrice{1 & 3 \\0 & 1 }\]
 Soit alors $P_n$ la proposition définie pour tout entier $n$ par \[P_n: A^n = \matrice{ 1 & n \\0 & 1}\]
 que l'on démontre par récurrence sur $n$ :
 \begin{itemize}[label=\textbullet]
     \item Initialisation : puisque $A^0=I_2 = \matrice{ 1 & 0 \\0 & 1 }$, $P_0$ est vraie.
     \item Hérédité : supposons que la proposition $P_n$ est vraie pour un certain entier $n$ fixé. Montrons alors $P_{n+1}$ :
     \[A^{n+1} = A^n \times A \underbrace{=}_{\textrm{par H.R.}} \matrice{1 & n \\0 & 1 } \matrice{ 1 & 1 \\0 & 1} = \matrice{1 & n+1\\0 & 1}\]
     La proposition $P_{n+1}$ est donc vraie.
 \end{itemize}
 On a ainsi démontré par récurrence que
 \[\forall n\geq 0,~~A^n=\matrice{1 & n \\0 & 1}\]
 }

\afaire{Exercices \lienexo{10} et \lienexo{11}.}

  \subsection{Polynômes de matrice}

A l'aide des résultats précédents, on peut effectuer des calculs polynomiaux avec les matrices :

\begin{definition}
On appelle \textbf{polynôme de matrice} toute matrice carrée qui s’écrit sous la forme $P(A)$ avec $P$ un polynôme à coefficients dans $\R$, et $A\in \MM_n(\R)$.

Si $P(X)=\sum\limits_{k=0}^p a_kX^k$, avec $(a_0,\hdots,a_p)\in \R^{p+1}$, alors
\[ P(A) = a_0I_n + a_1A + \hdots + a_nA^n. \]
On dit que $P(A)$ est un \textbf{polynôme} en $A$.
\end{definition}

\begin{exemple}
  Si $P(X)=2X^2+3X-4$, alors $P(A)=2A^2+3A-4I_n$.

 Si $P(X)=X^k$, alors $P(A)=A^k$ si $k\geq 1$, $P(A)=I_n$ si $P(X)=1$.
\end{exemple}

\begin{propriete}[Opérations sur les polynômes de matrices]
  Soient $A$ une matrice de $\MM_n(\R)$, $P, Q$ deux polynômes à coefficients réels et $\lambda \in \R$.
  \begin{itemize}
    \item $(\lambda P)(A) = \lambda P(A)$,
    \item $(P+Q)(A) = P(A)+Q(A)$,
    \item $(PQ)(A) = P(A)Q(A) = Q(A)P(A)$.
  \end{itemize}
La dernière proposition indique que deux polynômes d'une même matrice $A$ commutent.
\end{propriete}

Ce qui nous intéressera régulièrement, ce sont des polynômes qui annulent une matrice :

\begin{definition}[Polynôme annulateur]
  Soit $A\in \MM_n(\R)$ et $P$ un polynôme à coefficient réels. On dit que $P$ est un \textbf{polynôme annulateur} de $A$ si $P(A)=0_n$.
\end{definition}

\begin{exemple}
  Le polynôme $P(X)=X^2-X+1$ est annulateur de la matrice $A=\matrice{0&-1\\1&1}$.
\end{exemple}

\solution[5]{
  En effet :
  \begin{align*}
    P(A) &= A^2-A+I_2 \\
    &= \matrice{-1&-1\\1&0} - \matrice{0&-1\\1&1} + \matrice{1&0\\0&1} = \matrice{0&0\\0&0}.
  \end{align*}
}

\begin{remarque}
  Nous verrons au second semestre que toute matrice admet au moins un polynôme annulateur non nul. Trouver un tel polynôme, en revanche, n'est pas forcément aisé.
\end{remarque}

On utilisera les polynômes de matrices dans de nombreuses situations : le calcul de puissances $n$-ièmes, par exemple, mais aussi la détermination de l'inverse d'une matrice, que nous allons introduire.

\section{Matrices inversibles}

     \subsection{Définition}

\begin{definition}
Soit $A$ une matrice carrée d'ordre $n$. On dit que $A$ est \textbf{inversible} s'il existe une matrice $B$ de $\MM_n(\R)$ vérifiant \[AB=BA=I_n\]
 Dans ce cas, $B$ est appelée \textbf{matrice inverse} de $A$, et est notée $B=A^{-1}$.
\end{definition}

\begin{notation}
On note $GL_n(\R)$ l'ensemble des matrices carrée d'ordre $n$ inversibles.
\end{notation}

\begin{exemple}
La matrice $\matrice{ 1 & 1 \\ 2 & 3}$ est inversible. En effet,
     \[\matrice{ 1 & 1 \\ 2 & 3 } \matrice{ 3 & -1 \\ -2 & 1 }=
     \matrice{ 3 & -1 \\ -2 & 1 }
     \matrice{ 1 & 1 \\ 2 & 3 }
     =I_2\]
\end{exemple}

\begin{remarque}
La matrice $I_n$ est inversible et $I_n^{-1}=I_n$. En effet, $I_nI_n=I_nI_n=I_n$.
\end{remarque}

     \subsection{Propriétés}

\begin{propriete}
Soient $A$ et $B$ deux matrices carrées de $\MM_n(\R)$.
     \begin{itemize}[label=\textbullet]
         \item Si $A\in GL_n(\R)$, alors $A^{-1}\in GL_n(\R)$ et $(A^{-1})^{-1}=A$.
         \item Si $A$ et $B$ sont inversibles, alors $AB$ est également inversible, et on a $(AB)^{-1}=B^{-1}A^{-1}$, et ainsi pour tout entier $k\in \N$, $\left(A^k\right)^{-1}=\left(A^{-1}\right)^k$.
         \item Si $A$ est inversible, alors $\,^t A$ est inversible et $\left(\,^t A\right)^{-1} = \,^t \left( A^{-1}\right)$.
     \end{itemize}

\end{propriete}

\preuve[7]{Pour le premier point, on a en effet $A^{-1}A=AA^{-1}=I_n$ donc $A^{-1}$ est inversible et son inverse est $A$.

 Pour le second point, on a $AB(B^{-1}A^{-1})=AI_nA^{-1}=AA^{-1}=I_n$ et $(B^{-1}A^{-1})AB=B^{-1}I_nB=B^{-1}B=I_n$. Donc $AB$ est inversible et son inverse est $B^{-1}A^{-1}$.

Enfin, en utilisant les propriétés de la transposée :
\begin{align*}
  \,^t A \times  \,^t\left( A^{-1}\right) &= \,^t\left( A^{-1} A \right) = \,^t I_n = I_n,\\
  \text{ et }\,^t\left( A^{-1}\right)\times \,^tA &= \,^t\left( AA^{-1} \right) = \,^t I_n = I_n.
\end{align*}
Ainsi, $\,^tA$ est inversible et $\left(\,^t A\right)^{-1}=\,^t\left(A^{-1}\right)$.
 }

 Pour démontrer qu'une matrice n'est pas inversible, on peut utiliser le théorème suivant :

\begin{theoreme}
Soit $A \in \MM_n(\R)$ une matrice non nulle. S'il existe une matrice $B\in \MM_n(\R)$ non nulle telle que $AB=0_n$ alors $A$ n'est pas inversible.
\end{theoreme}

\ifprof
 \preuve{Faisons un raisonnement par l'absurde, et supposons que $A$ soit inversible, d'inverse $A^{-1}$. Alors
 \[AB=0_n \Rightarrow A^{-1}(AB)=O_n \Rightarrow B=0_n\]
 ce qui est absurde, puisque $B$ n'est pas nulle.}
\else
\lignes{3}
\fi

\begin{remarque}
Si $A$ et $B$ sont toutes les deux non nulles, telles que $AB=0_n$ alors ni $A$ ni $B$ ne sont inversibles.
\end{remarque}

Pour démontrer qu'une matrice est inversible, on dispose d'un théorème un peu similaire au précédent, appelé critère du noyau (nous donnerons un sens à ce nom plus tard dans l'année).

\begin{theoreme}[Critère du noyau]
  Soit $A\in \MM_n(\R)$ non nulle. $A$ est inversible si et seulement si
  \[ \forall X \in \MM_{n,1}(\R),\quad AX=0_{n,1} \implies X=0_{n,1}. \]
\end{theoreme}

\preuve[8]{
Si $A$ est inversible, et si $AX=0_{n,1}$, par multiplication par $A^{-1}$ à gauche, on en déduit $A^{-1}AX=A0_{n,1}$, c'est-à-dire $X=0_{n,1}$.

Si $A$ n'est pas inversible, d'après le théorème précédent, il existe une matrice $B$ non nulle telle que $AB=0_n$. Puisque $B$ n'est pas nulle, il existe $Y\in \MM_{n,1}(\R)$ tel que $BY\neq 0_{n,1}$. Mais alors $ABY=0$ c'est-à-dire $A(BY)=0_{n,1}$. En notant $X=BY\neq 0_{n,1}$, on a donc $X$ tel que $AX=0_{n,1}$. Par contraposée, on en déduit que si pour tout $X \in \MM_{n,1}(\R),\quad AX=0_{n,1} \implies X=0_{n,1}$, alors $A$ est inversible.
}

     \subsection{Règles de calcul}

\begin{propriete}
Soient $A$ et $B$ deux matrices de $\MM_n(\R)$, et $C \in GL_n(\R)$. Alors
\begin{align*}
	&AC=B \Leftrightarrow A=BC^{-1} &&&&CA=B \Leftrightarrow A=C^{-1}B\\
	&AC=BC \Leftrightarrow A=B &&&&CA=CB \Leftrightarrow A=B
\end{align*}
\end{propriete}

\begin{attention}
Cela n'est valable que si $C$ est inversible ! Ce n'est pas forcément vrai si $C$ n'est pas inversible. Par exemple, si $A=\matrice{1 & 3 \\ -1 & 1 }$, $B=\matrice{ 1 & -4 \\ -1 & 2 }$, et $C=\matrice{ 1 & 1 \\ 0 & 0 }$, on a $AC=BC$ et pourtant $A\neq B$.
\end{attention}

Pour démontrer qu'une matrice est inversible, il est suffisant de démontrer qu'elle est inversible d'un seul côté :

\begin{theoreme}
\begin{itemize}[label=\textbullet]
     \item Soit $A \in M_n(\R)$. S'il existe une matrice $B\in M_n(\R)$ telle que $AB=I_n$, alors $A$ est inversible, d'inverse $B$.
     \item Soit $A \in M_n(\R)$. S'il existe une matrice $B\in M_n(\R)$ telle que $BA=I_n$, alors $A$ est inversible, d'inverse $B$.
 \end{itemize}
\end{theoreme}

 Ainsi, il n'est pas nécessaire de vérifier $AB=I_n$ \textsc{et} $BA=I_n$. Seul un des sens est nécessaire.

\begin{methode}
Pour montrer qu'une matrice $A$ est inversible, on peut chercher une matrice $B$ telle que $AB=I_n$. On pourra conclure que $A$ est inversible, et que $A^{-1}=B$.
\end{methode}

\begin{exemple}
Soit $A=\matrice{ 1&1\\0&1}$. On note également $B=\matrice{ 0 & -1\\0 & 0}$.
 \begin{enumerate}
     \item Calculer $A(I_2+B)$.
     \item En déduire que $A$ est inversible, et calculer $A^{-1}$.
 \end{enumerate}
\end{exemple}

 \solution[7]{
 \begin{enumerate}
     \item On constate que \[A(I_2+B)=\matrice{1&1\\0&1}\matrice{1&-1\\0&1}=\matrice{1&0\\0&1}=I_2\]
     \item D'après ce qui précède, $A$ est inversible, et $A^{-1}=I_2+B=\matrice{1&-1\\0&1}$.
 \end{enumerate}
 }

 \afaire{Exercices \lienexo{20}, \lienexo{21} et \lienexo{22}.}

     \subsection{Ensemble $GL_2(\R)$}

L'inverse d'une matrice de $\MM_2(\R)$ est facile à obtenir :

\begin{theoreme}\logoparcoeur
Soit $A=\matrice{ a & b \\ c & d} \in \MM_2(\R)$. Alors $A$ est inversible si et seulement si $ad-bc \neq 0$. On a alors
 \[A^{-1}=\frac{1}{ad-bc}\matrice{ d & -b \\ -c & a}\]
 Le réel $ad-bc$ est appelé \textbf{déterminant} de la matrice $A$ et est noté $\det(A)$.
\end{theoreme}


\ifprof
 \preuve{Notons $A=\matrice{ a&b\\c&d}$ et $B=\matrice{d & -b \\ -c & a }$. On suppose que $A\neq 0$ et donc $B\neq 0$. Alors
 \[AB=\matrice{ ad-bc & 0 \\ 0 & ad-bc} = (ad-bc)I_2\]
 \begin{itemize}[label=\textbullet]
   \item Si $ad-bc=0$, alors $AB=0_2$. Puisque $B\neq 0$, d'après un résultat précédent, $A$ ne peut pas être inversible.
   \item Si $ad-bc\neq 0$, alors $A \times \left( \frac{1}{ad-bc} B \right) = I_2$. D'après un résultat précédent, $A$ est donc inversible, et
   		\[A^{-1}=\frac{1}{ad-bc}B = \frac{1}{ad-bc} \matrice{ d & -b\\-c&a}\]
 \end{itemize}}
\else
\lignes{10}
\fi

\begin{exemple}
Montrer que la matrice $A=\matrice{1&2\\1&1}$ est inversible et déterminer son inverse.
\end{exemple}

\solution[5]{Son déterminant vaut $\det(A)=1-2=-1$. Il est non nul, donc $A$ est inversible et son inverse est
\[ A^{-1} = \frac{1}{-1}\matrice{1&-2\\-1&1} = \matrice{-1&2\\1&-1}\]}

  \subsection{Inverse et polynôme annulateur}

Lorsqu'on dispose d'un polynôme annulateur, on peut en déduire, si elle est inversible, un inverse.

Si $P(X)=\sum\limits_{k=0}^p a_kX^k$ est un polynôme annulateur de la matrice $A$, avec $a_0\neq 0$, et $p\geq 2$, alors :
\begin{align*}
 a_0I_n + a_1A+ \hdots + a_pX^p =0 &\Leftrightarrow a_1A+\hdots +a_pA^p = -a_0I_n \\
 &\Leftrightarrow -\frac{1}{a_0}A\left(a_1I_n+\hdots +a_pA^{p-1}\right)=I_n.
\end{align*}
Ainsi, la matrice $-\frac{1}{a_0}\left(a_1I_n+\hdots +a_pA^{p-1}\right)$ est l'inverse de la matrice $A$.

\begin{exo}
Soit $A=\matrice{-2&-2&1\\-2&1&-2\\1&-2&-2}$ et $P(X)=X^2-9$. Montrer que $P$ est un polynôme annulateur de $A$, puis déterminer l'inverse de $A$.
\end{exo}

\solution[6]{En calculant rapidement :
\begin{align*}
P(A) &= \matrice{9&0&0\\0&9&0\\0&0&9} - \matrice{9&0&0\\0&9&0\\0&0&9} =0_3(\R)
\end{align*}
Ainsi, $P$ est bien annulateur de $A$. Mais alors :
\[ A^2-9I_3 = 0_3(\R) \Leftrightarrow A\times \left(\frac{1}{9}A\right) = I_n.\]
Ainsi, $A$ est inversible et $A^{-1}=-\frac{1}{9}A$.
}

 \section{Systèmes linéaires et matrices}

     \subsection{Ecriture matricielle d'un système linéaire}

\begin{exemple}
On s'intéresse au système
 \[(S) \left \{ \begin{array}{ccccccc}
   2x & - & 3y & + & z &=&1\\
   x & + & y & - & 2z &=& 2\\
   -x & - & 2y & + & z & =& -1
  \end{array}
 \right.\]

 Notons alors $A=\matrice{ 2 & -3 & 1 \\ 1 & 1 & -2 \\ -1 & -2 & 1}$, $X=\matrice{ x\\y\\z}$ et $Y=\matrice{1 \\ 2 \\ -1 }$.
 On a alors
 \[(S) \Leftrightarrow AX=Y\]

 La matrice $A$ est appelée \textbf{matrice associée} au système $(S)$. Résoudre le système $(S)$, c'est donc trouver le vecteur colonne $X$.
\end{exemple}

\begin{definition}[Matrice associée à un système]
Soit $(S)$ un système $n\times p$ de la forme
 \[\left \{ \begin{array}{ccccccccc} a_{11}x_1&+&a_{12}x_2&+&\cdots &+& a_{1p}x_p &=& b_1 \\ \vdots & & \vdots & &\vdots & & \vdots & & \vdots\\a_{n1}x_1&+&a_{n2}x_2&+&\cdots &+& a_{np}x_p&=&b_n\end{array}\right.\]
 On appelle \textbf{matrice associée} à $(S)$ la matrice
 \[A=\matrice{ a_{11} & a_{12} & \cdots & a_{1p}\\\vdots & \vdots & \cdots & \vdots\\a_{n1}&a_{n2}&\cdots &a_{np} }\]
 Le système $(S)$ s'écrit alors $AX=Y$, avec $X=\matrice{x_1\\\vdots\\x_n}$ et $Y=\matrice{b_1\\\vdots\\b_n}$.
\end{definition}


     \subsection{Inverse d'une matrice et système}

\begin{theoreme}
Soit $A \in \MM_n(\R)$. Soit $B \in \MM_{n,1}(\R)$ une matrice colonne. Le système $(S)~~AX=B$ admet une unique solution si, et seulement si, la matrice $A$ est inversible.\\Dans ce cas, $X=A^{-1}B$.
\end{theoreme}

\begin{remarque}
Ainsi, pour résoudre un système $(S)$, on peut introduire la matrice associée et résoudre une équation matricielle $AX=B$. Cela permet en général de simplifier les notations.
\end{remarque}

\afaire{Exercice \lienexo{30}.}

\begin{consequence}
Une matrice triangulaire supérieure $A$ est inversible si et seulement si tous les termes de la diagonale sont non nuls.
\end{consequence}


\ifprof
 \preuve{En effet, un système triangulaire est de Cramer si et seulement si tous ses pivots sont non nuls.}
\else
\lignes{2}
\fi

\begin{methode}
Pour montrer qu'une matrice $A$ est, ou n'est pas inversible, sans calculer son inverse, on résout matriciellement l'équation $AX=0$ en appliquant la méthode du pivot de Gauss. Si on obtient une diagonale sans terme nul, la matrice sera inversible. On peut simplifier les écritures en écrivant $(A | 0)$ pour ne pas s'encombrer des inconnues.
\end{methode}

\begin{exemple}
	Montrer que la matrice
 $
     A=\matrice{
         1 & 2 & 0 \\
         2 & 1 & 0 \\
         0 & 1 & 0
     }
 $
 n'est pas inversible.
 Montrer que la matrice $B=
 	\matrice{
         1 & 7 & 2 \\
         2 & 4 & 1 \\
         -1 & 1 & 0
 	}
 $
 est inversible.
\end{exemple}

 \solution[15]{On résout :
 \[\left(\begin{array}{ccc|c}1 & 2 & 0 & 0\\2 & 1 & 0 & 0\\0 & 1 & 0 & 0 \end{array}\right)~\sim
 \left(\begin{array}{ccc|c}1 & 2 & 0 & 0\\0 & -3 & 0 & 0\\0 & 1 & 0 & 0 \end{array}\right) \begin{array}{c}\textrm{ligne pivot} \\ L_2\leftarrow L_2-2L_1\\~\end{array}~\sim
 \left(\begin{array}{ccc|c}1 & 2 & 0 & 0\\0 & -3 & 0 & 0\\0 & 0 & 0 & 0 \end{array}\right) \begin{array}{c}\\~\textrm{ligne pivot} \\ L_3\leftarrow 3L_3+L_2\end{array} \]
 Puisqu'un des termes sur la diagonale est nul, la matrice $A$ n'est pas inversible.\\Pour $B$ :
 \[\left(
     \begin{array}{ccc|c}
         1 & 7 & 2 & 0 \\
         2 & 4 & 1 & 0\\
         -1 & 1 & 0 & 0
     \end{array}
 \right) \sim
 \left(
     \begin{array}{ccc|c}
         1 & 7 & 2 & 0 \\
         0 & -10 & -3  & 0\\
         0 & 8 & 2 & 0
     \end{array}
 \right) \sim
 \left(
     \begin{array}{ccc|c}
         1 & 7 & 2 & 0 \\
         0 & -10 & -3  & 0\\
         0 & 0 & -4 & 0
     \end{array}
 \right)
 \]
 Les termes sur la diagonale étant non nuls, la matrice $B$ est bien inversible.
 }

\begin{methode}
Pour déterminer l'inverse d'une matrice, on peut utiliser la méthode du pivot de Gauss, mais en simplifiant les écritures. On écrit $(M | I_n)$ et on cherche à remplacer, par des opérations sur les lignes, $M$ par $I_n$. A la place du $I_n$ de départ, on aura alors $M^{-1}$.
\end{methode}

\begin{exemple}
Déterminer l'inverse de
 $\matrice{
      -3 & 5 & 6 \\
      -1 & 2 & 2 \\
      1 & -1 & -1
 }$.

\end{exemple}

 \solution[20]{On a :
 \[\left( \begin{array}{ccc|ccc}
      -3 & 5 & 6 & 1 & 0 & 0\\
      -1 & 2 & 2 & 0 & 1 & 0\\
      1 & -1 & -1 & 0 & 0 & 1
  \end{array}
 \right) \sim
 \left( \begin{array}{ccc|ccc}
      1 & -1 & -1 & 0 & 0 & 1\\
      -1 & 2 & 2 & 0 & 1 & 0\\
      -3 & 5 & 6 & 1 & 0 & 0
  \end{array}
 \right) L_1 \leftrightarrow L_3
 \]
 \[
 \sim\left( \begin{array}{ccc|ccc}
      1 & -1 & -1 & 0 & 0 & 1\\
      0  & 1 & 1 & 0 & 1 & 1\\
      0  & 2 & 3 & 1 & 0 & 3
  \end{array}\right)
   \begin{array}{c}~\\ L_2 \leftarrow L_2+L_1\\ L_3\leftarrow L_3+3L_1\end{array}
 \]
 \[
 \sim\left( \begin{array}{ccc|ccc}
      1 & -1 & -1 & 0 & 0 & 1\\
      0  & 1 & 1 & 0 & 1 & 1\\
      0  & 0 & 1 & 1 & -2 & 1
  \end{array}\right)
   \begin{array}{c}~\\ ~\\ L_3\leftarrow L_3-2L_2\end{array}
 \]
 \[
 \sim\left( \begin{array}{ccc|ccc}
      1 & -1 & -1 & 0 & 0 & 1\\
      0  & 1 & 0 & -1 & 3 & 0\\
      0  & 0 & 1 & 1 & -2 & 1
  \end{array}\right)
   \begin{array}{c}~\\ L_2\leftarrow L_2-L_3\\ ~\end{array}
 \]
 \[
 \sim\left( \begin{array}{ccc|ccc}
      1 & 0 & 0 & 0 & 1 & 2\\
      0  & 1 & 0 & -1 & 3 & 0\\
      0  & 0 & 1 & 1 & -2 & 1
  \end{array}\right)
   \begin{array}{c}L_1\leftarrow L_1+L_2+L_3\\ ~\\ ~\end{array}
 \]
 Ainsi, $\displaystyle{
   \matrice{
      -3 & 5 & 6 \\
      -1 & 2 & 2 \\
      1 & -1 & -1
 }}$ est inversible, et son inverse est
 \[\matrice{
   0 & 1 & 2\\
   -1 & 3 & 0\\
   1 & -2 & 1
 }\]

}

\afaire{Exercices \lienexo{23}, \lienexo{24} et \lienexo{25}.}

\begin{experiencehistorique}
Cette méthode de détermination de l'inverse d'une matrice est appelée \textbf{réduction de Gauss-Jordan}, en hommage à  \textit{Carl Friedrich Gauss} et \textit{Wilhelm Jordan}, mais était connue des Chinois au $1^{\textrm{er}}$ siècle de notre ère, sous le nom \textit{Fang cheng} dans \textit{Les Neuf Chapitres sur l'art mathématique}.
\end{experiencehistorique}

\begin{definition}
On appelle \textbf{rang} d'une matrice $A$, et on note $\rg(A)$ le rang du système associée $AX=0$, où $X$ représente le vecteur colonne des inconnues, c'est-à-dire le nombre de lignes non nulles après réduction de Gauss-Jordan.
\end{definition}

\begin{propriete}
On dispose des propriétés suivantes :
	\begin{itemize}[label=\textbullet]
		\item $\rg(A)=0$ si et seulement si la la matrice est nulle.
		\item $\rg(A)=1$ si et seulement si toutes les colonnes de $A$ sont colinéaires.
		\item Si la matrice $A$ est carrée d'ordre $n$, $\rg(A)=n$ si et seulement si la matrice est inversible.
	\end{itemize}
\end{propriete}
