\chapter{Séries}
%!TeX root=../../encours.nouveau.tex

\objectifintro{Dans ce chapitre, on introduit la notion de série, une suite liée à une somme de termes d'une suite. On y verra des méthodes de calculs, mais aussi des théorèmes pour montrer des convergences ou divergences de séries, sans pouvoir nécessairement calculer la somme de la série.}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Georg Wilhelm Friedrich Hegel (1770--1831). \emph{Encyclopédie des sciences philosophiques}}
L'homme n'est rien d'autre que la série de ses actes.
\end{extrait}

\begin{objectifs}
	\begin{numerote}
		\item Connaître la notion de série :
				\begin{itemize}
					\item \lienobj{1}{connaître la définition d'une série, d'une somme et du reste d'une série}
					\item \lienobj{2}{connaître les différentes opérations usuelles}
					\item \lienobj{3}{connaître le lien entre suite et série}
					\item \lienobj{4}{connaître la condition nécessaire de convergence}
					\item \lienobj{5}{connaître les séries de référence}
				\end{itemize}
		\item Concernant les théorèmes de convergence :
				\begin{itemize}
					\item \lienobj{6}{connaître le théorème de comparaison}
					\item \lienobj{7}{connaître le théorème d'équivalence et de négligeabilité}
				\end{itemize}
		\item Concernant l'absolue convergence :
				\begin{itemize}
					\item \lienobj{8}{connaître la définition}
					\item \lienobj{9}{connaître le lien entre absolue convergence et convergence}
				\end{itemize}
	\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

\section{Définition}

    \subsection{Séries}

\begin{definition}
\labelobj{1}
\vspace*{.1cm}Soit $(u_n)$ une suite réelle. On appelle \textbf{série de terme général} $u_n$, et on note $\displaystyle{\sum_{n\geq 0} u_n}$ ou plus simplement $\sum u_n$, la suite des sommes partielles $(S_n)$ définie par $$S_n=\sum_{k=0}^n u_k$$
\end{definition}

\begin{remarque}
Si la suite $(u_n)$ n'est définie qu'à partir d'un certain rang $n_0$, la série de terme général $u_n$ n'est également définie qu'à partir de $n_0$, ce que l'on note $\displaystyle{\sum_{n\geq n_0} u_n}$. La suite des sommes partielles est $(S_n)_{n\geq n_0}$, avec $\displaystyle{S_n=\sum_{k={n_0}}^n u_k}$.
\end{remarque}

\begin{attention}
	La série $\sum u_n$ est bien une suite ! C'est la suite des sommes partielles $(S_n)_{n\geq n_0}$.
\end{attention}

\begin{exemple}
Soit $u$ la suite définie pour tout $n\geq 1$ par $u_n=\frac{1}{n}$. La série de terme général $u_n$ est notée $\displaystyle{\sum_{n\geq 1} \frac{1}{n}}$ est appelée la \textbf{série harmonique}.
\end{exemple}

\begin{exo}
Déterminer les premiers termes de la suite des sommes partielles de la série harmonique.
\end{exo}

\solution[2]{On note $(S_n)_{n\geq 1}$ la suite des sommes partielles associées à la série harmonique, c'est-à-dire $\ds{\forall n\geq 1,\,S_n=\sum_{k=1}^n \frac1k}$. Alors on a
\[ S_1=1,\quad S_2=1+\frac12=\frac32,\quad S_3=1+\frac12+\frac13=\frac{11}{6}, \quad S_4=1+\frac12+\frac13+\frac14=\frac{25}{12} \]
}


    \subsection{Convergence}

La série $\sum u_n$ étant une suite, on peut s'intéresser à sa convergence.

\begin{definition}
Soit $(u_n)$ une suite réelle. On dit que la série $\sum u_n$ \textbf{converge} si la suite des sommes partielles $(S_n)$ converge. Dans ce cas :
\begin{itemize}[label=\textbullet]
	\item la limite de la suite $(S_n)$ est alors appelée \textbf{somme} de la série, et est notée $\ds{\sum_{k=0}^{+\infty} u_k}$. On a ainsi
$$\sum_{k=0}^{+\infty} u_k = \lim_{n\rightarrow +\infty} \sum_{k=0}^n u_k$$
	\item on appelle \textbf{reste} de la série la suite $(R_n)$ définie par \[R_n=\sum_{k=n+1}^{+\infty} u_k = \sum_{k=0}^{+\infty} u_k-S_{n} \]
\end{itemize}
\end{definition}

\begin{consequence}
Si la série $\sum u_n$ converge, alors le reste $(R_n)$ tend vers $0$.
\end{consequence}

\preuve[2]{
En effet, puisque la série converge, $\ds{S_n\tendversen{n\to +\infty} \sum_{k=0}^{+\infty} u_k}$ et donc $R_n\tendversen{n\to +\infty} 0$.
}

\begin{attention}
L'écriture $\displaystyle{\sum_{k=0}^{+\infty} u_k}$ n'a de sens que si la série converge, alors que l'écriture $\sum u_n$ a bien un sens, puisqu'elle désigne une suite.
\end{attention}

\begin{remarque}
Les sommes infinies ne se manipulent pas comme les sommes finies (puisqu'en réalité, ce sont des limites, et il faut donc toujours s'assurer de la convergence). C'est pourquoi on calculera (presque) toujours les sommes partielles, qui sont des sommes finies, avant de passer à la limite.
\end{remarque}

\begin{remarque}
	\textbf{\'Etudier} la série $\sum u_n$, c'est déterminer si la série converge ou diverge.

	On dit que deux séries ont \textbf{même nature} si elles sont toutes les deux convergentes, ou toutes les deux divergentes.
\end{remarque}

\begin{proposition}
	Soient $u$ et $v$ deux suites qui ne différent que d'un nombre fini de termes. Alors $\sum u_n$ et $\sum v_n$ sont de même nature.

	Ainsi, modifier un nombre fini de termes d'une série ne change pas sa nature. On peut donc faire des suppositions sur les premiers termes d'une suite pour déterminer la nature d'une série.
\end{proposition}

\preuve[7]{Supposons que pour tout $n\geq N$, $u_n=v_n$. Alors, pour $n\geq N$ :
\[ \sum_{k=0}^n u_k = \sum_{k=0}^n v_k + \sum_{k=0}^N (u_k-v_k). \]
c'est-à-dire, en notant respectivement $(S_n)$ et $(T_n)$ les suites des sommes partielles associées à $u$ et $v$ :
\[ S_n = T_n + \sum_{k=0}^N (u_k-v_k). \]
Le terme $\ds{\sum_{k=0}^N (u_k-v_k)}$ étant fini, $(S_n)$ converge si et seulement si $(T_n)$ converge.
}

    \subsection{Premiers exemples}

\begin{exemple}
Soit $(u_n)$ la suite définie pour tout $n$ par $\ds{u_n=\left(\frac{1}{2}\right)^n}$. Etudier la série $\displaystyle{\sum u_n}$.
\end{exemple}

\solution[10]{
Notons $\displaystyle{S_n=\sum_{k=0}^n \left(\frac{1}{2}\right)^k}$. Alors

$$\forall n,~S_n=\sum_{k=0}^n \left(\frac{1}{2}\right)^k=\frac{1-\left(\frac{1}{2}\right)^{n+1}}{1-\frac{1}{2}} = 2\left(1-\left(\frac{1}{2}\right)^{n+1}\right)$$

Puisque $-1< \frac{1}{2} < 1$, on a $\displaystyle{\lim_{n\rightarrow +\infty} \left(\frac{1}{2}\right)^{n+1}=0}$. Par somme et produit, on en déduit donc que la série $\displaystyle{\sum_{n\geq 0} \left(\frac{1}{2}\right)^n}$ converge, et on a $$\lim_{n\rightarrow +\infty} S_n=\sum_{k=0}^{+\infty} \left(\frac{1}{2}\right)^k=2$$}


\begin{exemple}
Montrer que la série harmonique, de terme général $\frac{1}{n}$, $\displaystyle{\sum_{n\geq 1} \frac{1}{n}}$, est divergente.
\end{exemple}

\solution[10]{Pour tout $n\geq 1$, notons $H_n=\displaystyle{\sum_{k=1}^n \frac{1}{k}}$.
\\Nous avons vu dans le chapitre sur le calcul différentiel que l'on a, pour tout $k\geq 1$, $$\ln(k+1)-\ln(k)\leq \frac{1}{k}$$
En additionnant ces inégalités, on obtient alors
$$\sum_{k=1}^n (\ln(k+1)-\ln(k)) \leq \sum_{k=1}^n \frac{1}{k}=H_n$$
Or, on a
$$\sum_{k=1}^n (\ln(k+1)-\ln(k)) =  \ln(n+1)-\ln(1)=\ln(n+1) \textrm{, les termes se téléscopant.}$$
Puisque $\displaystyle{\lim_{n\rightarrow +\infty} \ln(n+1)=+\infty}$, par comparaison, on en déduit que
$$\lim_{n\rightarrow +\infty} \sum_{k=1}^n \frac{1}{k} = +\infty$$
}

\afaire{Exercices \lienexo{01} et \lienexo{02}.}

\section{Propriétés}

    \subsection{Opérations sur les séries}

Les opérations sur les sommes finies se transposent, dans certains cas, aux séries :
\begin{theoreme}[Linéarité]
\labelobj{2}
Soient $(u_n)$ et $(v_n)$ deux suites réelles, et $\lambda$ un réel non nul.
\begin{itemize}
    \item Les séries $\sum u_n$ et $\sum \lambda u_n$ sont de même nature (c'est-à-dire qu'elles sont soit toutes les deux convergentes, soit toutes les deux divergentes). Si elles sont convergentes, on a alors
    \[\sum_{k=0}^{+\infty} \lambda u_k= \lambda \sum_{k=0}^{+\infty} u_k\]
    \item Si les séries $\sum u_n$ et $\sum v_n$ sont toutes les deux convergentes, alors la série $\sum (u_n+v_n)$ est également convergente, et on a
    \[\sum_{k=0}^{+\infty} (u_k+v_k)=\sum_{k=0}^{+\infty} u_k+\sum_{k=0}^{+\infty} v_k\]
		\item Si $\sum u_n$ est convergente, et $\sum v_n$ est divergente, alors $\sum (u_n+v_n)$ est également divergente.
\end{itemize}
\end{theoreme}

\begin{attention}
La réciproque du deuxième point n'est pas vraie. Par exemple, si pour tout $n\geq 1$, $u_n=\frac{1}{n}$ et $v_n=-\frac{1}{n}$, alors la série $\sum u_n+v_n$ converge (vers $0$) alors que ni $\sum u_n$ ni $\sum v_n$ ne convergent.

Il faudra donc toujours s'assurer que les séries convergent avant de séparer les sommes.
\end{attention}

\preuve[10]{Cela repose sur la linéarité de la somme. Soit $n\in \N*$ fixé. Alors
{\allowdisplaybreaks
 \begin{align*}
	 \sum_{k=0}^n \lambda u_k &= \lambda \sum_{k=0}^n u_k \tendversen{n\to +\infty} \lambda \sum_{k=0}^{+\infty} u_k
 \end{align*}
 Donc la série $\sum \lambda u_n$ converge et sa somme vaut $\ds{\lambda \sum_{k=0}^{+\infty} u_k}$.
 \begin{align*}
	 \sum_{k=0}^n (u_k+v_k) &= \sum_{k=0}^n u_k +\sum_{k=0}^n v_k
 \end{align*}
 Si les deux séries $\sum u_n$ et $\sum v_n$ convergent, le résultat précédent garantit que $\sum (u_n+v_n)$ converge, et que sa somme vaut $\ds{\sum_{k=0}^{+\infty}u_k+ \sum_{k=0}^{+\infty} v_k}$.

 Si $\sum u_n$ converge, et $\sum v_n$ diverge, alors $\sum (u_n+v_n)$ ne peut converger (sinon, par différence, $\sum v_n$ converge également, ce qui est absurde).
}
}

\begin{consequence}
L'ensemble des séries convergentes forme un $\R$-espace vectoriel.
\end{consequence}

    \subsection{Suite et série}

\begin{theoreme}[Théorème suite série]
\labelobj{3}
Soit $(u_n)$ une suite. Alors $(u_n)$ converge si, et seulement si, la série $\sum (u_{n+1}-u_n)$ converge. Dans ce cas, en notant $\ell$ la limite de $(u_n)$, on a $$\sum_{k=0}^{+\infty} (u_{k+1}-u_k)=\ell-u_0$$
\end{theoreme}

\preuve[4]{Notons $\displaystyle{S_n=\sum_{k=0}^n (u_{k+1}-u_k)}$. On constate que $S_n=u_{n+1}-u_0$  par telescopage. Ainsi, $(S_n)$ converge si et seulement si $(u_{n})$ converge. Si $(u_n)$ converge vers $\ell$, par passage à la limite, on obtient bien $\displaystyle{\sum_{k=0}^{+\infty} (u_{k+1}-u_k)=\ell-u_0}$.}


\section{Conditions de convergence}

    \subsection{Limite de la suite et convergence}

\begin{theoreme}
\labelobj{4}
Soit $(u_n)$ une suite réelle. Si la série $\sum u_n$ est convergente, alors $\displaystyle{\lim_{n\rightarrow +\infty} u_n=0}$.
\end{theoreme}

\preuve[5]{Pour tout $n$, notons $\displaystyle{S_n=\sum_{k=0}^n u_k}$. Alors, pour tout $n\geq 1$, on a $u_n=S_n-S_{n-1}$. Si la série $\sum u_n$ converge, alors la suite $(S_n)$ admet, par définition, une limite que l'on note $\ell$. Mais alors
$$\lim_{n\rightarrow +\infty} S_n=\lim_{n\rightarrow +\infty} S_{n-1}=\ell$$
et donc
$$\lim_{n\rightarrow +\infty} u_n = \ell-\ell = 0$$}

\begin{remarque}
La contraposée du théorème est intéressante : si la suite $(u_n)$ ne converge pas vers $0$, alors la série $\sum u_n$ n'est pas convergente. Par exemple, la série $\displaystyle{\sum \frac{2n}{n+1}}$ diverge, car son terme général ne tend pas vers $0$.

On dit dans ce cas que la série \textbf{diverge grossièrement}.
\end{remarque}

\begin{attention}
Cette condition est nécessaire, mais pas suffisante : en effet, on a $\displaystyle{\lim_{n\rightarrow +\infty} \frac{1}{n}=0}$ et pourtant la série harmonique $\displaystyle{\sum_{n\geq 1} \frac{1}{n}}$ diverge.
\end{attention}

\begin{exemple}
Soit $q$ un réel. On s'intéresse à la série $\sum q^n$. Alors, pour que la série $\sum q^n$ converge, il faut que $\displaystyle{\lim_{n\rightarrow +\infty} q^n =0}$, c'est à dire $|q|<1$. On verra plus tard que la réciproque, dans ce cas, est vraie.
\end{exemple}

    \subsection{Séries à termes positifs}

Le cas des séries à termes positifs est plus simple à étudier.

\begin{definition}[Séries à termes positifs]
On dit qu'une série $\sum u_n$ est \textbf{à termes positifs} si, pour tout $n$, $u_n\geq 0$.
\end{definition}

\begin{theoreme}
Soit $(u_n)$ une suite à termes positifs. Alors la série $\sum u_n$ est convergente, si et seulement si, la suite des sommes partielles $(S_n)$ est majorée.
\end{theoreme}

\preuve[3]{Notons $\displaystyle{S_n=\sum_{k=0}^n u_k}$. On a alors $S_{n+1}-S_n=u_{n+1}\geq 0$ : la suite $(S_n)$ est donc croissante. D'après les théorèmes sur les suites monotones, $(S_n)$ converge si et seulement si la suite $(S_n)$ est majorée.}

\section{Séries de référence}
\labelobj{5}

    \subsection{Séries géométriques et dérivées}

\begin{definition}[Série géométrique]
\vspace*{.1cm}Pour tout entier $p$, la série $\ds{\sum_{n\geq p} q^n}$ s'appelle \textbf{série géométrique} de raison $q$.
\end{definition}

\begin{theoreme}
La série $\sum q^n$ est convergente si et seulement si $|q|<1$. Dans ce cas,
$$\sum_{n=0}^{+\infty} q^n=\frac{1}{1-q}$$

Plus généralement, la série $\displaystyle{\sum_{n\geq p} q^n}$ est convergente si et seulement si $|q|<1$, et dans ce cas, $\displaystyle{\sum_{n=p}^{+\infty} q^n=\frac{q^p}{1-q}}$
\end{theoreme}

\preuve[10]{La suite $(q^n)$ converge vers $0$ si et seulement si $|q|<1$. Par condition nécessaire de convergence, la série $\sum q^n$ ne peut pas converger si $|q|\geq 1$.

Supposons alors que $|q|<1$. Notons, pour $n\geq p$, $\displaystyle{S_n=\sum_{k=p}^n q^k}$. On a $\displaystyle{S_n=\frac{q^p-q^{n+1}}{1-q}}$. Or, $\displaystyle{\lim_{n\rightarrow +\infty} q^{n+1}=0}$ car $|q|<1$. Donc la suite $(S_n)$ converge vers $\frac{q^p}{1-q}$ : la série converge, et sa somme vaut $\frac{q^p}{1-q}$.
}

On dispose également de deux séries, appelées séries géométriques dérivées première et deuxième :
\begin{theoreme}[Série géométrique dérivée]
    \begin{itemize}
        \item[$\bullet$] \textbf{Série géométrique dérivée première}\\ La série $\displaystyle{\sum_{n\geq 1} nq^{n-1}}$ converge si et seulement si $|q|<1$. Dans ce cas, $$\sum_{n=1}^{+\infty} nq^{n-1} = \frac{1}{(1-q)^2}$$
        \item[$\bullet$] \textbf{Série géométrique dérivée seconde}\\ La série $\displaystyle{\sum_{n\geq 2} n(n-1)q^{n-2}}$ converge si et seulement si $|q|<1$. Dans ce cas, $$\sum_{n=2}^{+\infty} n(n-1)q^{n-2} = \frac{2}{(1-q)^3}$$

    \end{itemize}
\end{theoreme}


\preuve[15]{Démontrons le premier résultat. Si $|q|\geq 1$, la suite $(nq^{n-1})$ ne converge pas vers $0$, donc la série $\displaystyle{\sum_{n\geq 1} nq^{n-1}}$ ne peut converger.\\
Pour tout $x \in \interoo{-1 1}$, notons $\displaystyle{T_n(x)=\sum_{k=0}^{n} x^{k}}$. $T_n$ est une fonction dérivable sur $]-1;1[$, et on a
$$T'_n(x)=\sum_{k=1}^{n} kx^{k-1}$$
D'autre part, pour tout $x\in \interoo{-1,1}$, $\displaystyle{T_n(x)=\frac{1-x^{n+1}}{1-x}}$. On a donc également
$$\forall x\in \interoo{-1 1},\quad T'_n(x)=\frac{-(n+1)x^{n}(1-x)-(1-x^{n+1})(-1)}{(1-x)^2}=\frac{1-(n+1)x^{n}+nx^{n+1}}{(1-x)^2}$$
Puisque $\displaystyle{\lim_{n\rightarrow +\infty} (n+1)x^{n}=\lim_{n\rightarrow +\infty} nx^{n+1}=0}$ (car $|x|<1$ et par croissances comparées), on en déduit que $\displaystyle{\lim_{n\rightarrow +\infty} T'_n(x)=\frac{1}{(1-x)^2}}$.\\
Ainsi, la série $\displaystyle{\sum_{n\geq 1} nx^{n-1}}$ converge, et on a bien $$\sum_{n=1}^{+\infty} nx^{n-1} = \frac{1}{(1-x)^2}$$
}

\begin{remarque}
On remarque que $nq^n = q nq^{n-1}$ et que $n(n-1)q^n=q^2 n(n-1)q^{n-2}$. Ainsi, si $|q|<1$, les séries $\sum nq^n$ et $\sum n(n-1)q^n$ convergent également, et
$$\sum_{n=1}^{+\infty} nq^n = \frac{q}{(1-q)^2} \textrm{ ~~~ et ~~~  } \sum_{n=2}^{+\infty} n(n-1)q^n = \frac{2q^2}{(1-q)^3}$$
Les sommes peuvent commencer à $0$, puisque les termes manquants sont nuls.
\end{remarque}

\begin{exo}
	Montrer que la série $\sum (n^2+n+1)2^{-n}$ converge, et déterminer sa somme.
\end{exo}

\solution[10]{On se ramène à des séries géométriques, éventuellement dérivée, en utilisant la remarque $n^2 = n(n-1)+n$.
\begin{align*}
 (n^2+n+1)2^{-n} &= (n(n-1)+n +n +1)2^{-n} \\
 &= n(n-1)\left(\frac12\right)^n + 2n \left(\frac12\right)^n + \left(\frac12\right)^n
\end{align*}
Les séries $\ds{\sum n(n-1)\left(\frac12\right)^n}$, $\ds{\sum n\left(\frac12\right)^n}$ et $\ds{\left(\frac12\right)^n}$ sont convergentes, puisque $-1<\frac12<1$. Par linéarité, la série $\sum (n^2+n+1)2^{-n}$ converge, et on a :
\begin{align*}
	\sum_{n=0}^{+\infty} (n^2+n+1)2^{-n} &= \sum_{n=0}^{+\infty} n(n-1)\left(\frac12\right)^n + 2\sum_{n=0}^{+\infty} n\left(\frac12\right)^n + \sum_{n=0}^{+\infty} \left(\frac12\right)^n \\
	&= \frac{2\left(\frac12\right)^2}{\left(1-\frac12\right)^3}+2 \frac{\frac12}{\left(1-\frac12\right)^2}+ \frac{1}{1-\frac12}\\
	&= 4 + 4 + 2 = 10
\end{align*}
}

    \subsection{Séries de Riemann}

\begin{definition}[Série de Riemann]
\vspace*{.1cm}La série de terme général $\dfrac{1}{n^\alpha}$ ($\alpha \in \R$) est appelée \textbf{série de Riemann}.
\end{definition}

\begin{theoreme}
\vspace*{.1cm}La série de Riemann $\displaystyle{\sum_{n\geq 1} \frac{1}{n^\alpha}}$ converge si et seulement si $\alpha >1$.
\end{theoreme}

\preuve[20]{Nous avons déjà traité le cas $\alpha=1$. Si $\alpha< 0$, $\frac{1}{n^\alpha} \tendversen{n\to +\infty} +\infty$ et si $\alpha=0$, $\frac{1}{n^\alpha} \tendversen{n\to +\infty} 1$. Ainsi, si $\alpha \leq 0$, la série est grossièrement divergente.

Prenons $\alpha >0$. On note $\ds{S_n=\sum_{k=1}^n \frac{1}{k^{\alpha}}}$.

La fonction $t\mapsto \frac{1}{t^\alpha}$ est alors décroissante sur $\R>$.

Pour tout $k\in \N*$ et $t\in \interent{k k+1}$, on a, par croissance de l'intégrale :
\allowdisplaybreaks
\begin{align*}
	\frac{1}{(k+1)^\alpha} \leq \frac{1}{t^\alpha} \leq \frac{1}{k^\alpha} &\implies \int_k^{k+1} \frac{1}{(k+1)^{\alpha}} \D t\leq \int_k^{k+1} \frac{\D t}{t^\alpha}\leq \int_k^{k+1} \frac{1}{k^\alpha}\D t\\
	&\implies \frac{1}{(k+1)^\alpha}\leq \left[ \frac{t^{-\alpha+1}}{-\alpha+1}\right]_k^{k+1} \leq \frac{1}{k^\alpha}\\
	&\implies \frac{1}{(k+1)^\alpha} \leq \frac{1}{1-\alpha}\left(\frac{1}{(k+1)^{\alpha-1}}-\frac{1}{k^{\alpha-1}}\right) \leq \frac{1}{k^\alpha}
\end{align*}
En ajoutant ces inégalités, et par télescopage :
\begin{align*}
 \sum_{k=1}^n \frac{1}{(k+1)^{\alpha}} \leq \frac{1}{1-\alpha}\sum_{k=1}^n\left(\frac{1}{(k+1)^{\alpha-1}} - \frac{1}{k^{\alpha-1}}\right)\leq \sum_{k=1}^n \frac{1}{k^\alpha} &\implies S_{n+1}-1 \leq \frac{1}{1-\alpha}\left(\frac{1}{(n+1)^{\alpha-1}} - 1\right) \leq S_n
\end{align*}
\allowdisplaybreaks[0]
\begin{itemize}
	\item Si $\alpha\in \interoo{0 1}$, $\alpha-1<0$ et donc \[  \frac{1}{1-\alpha} \left(\frac{1}{(n+1)^{\alpha-1}}-1\right)\tendversen{n\to +\infty} +\infty. \]
	Par comparaison, $S_n\tendversen{n\to +\infty} +\infty$ : la série diverge.
	\item Si $\alpha\in \interoo{1 +\infty}$, $\alpha-1>0$ et l'inégalité précédente devient
	\[ S_{n+1}-1 \leq \frac{1}{\alpha-1}. \]
De plus, la suite $(S_n)$ est croissante, puisque \[ S_{n+1}-S_n=\frac{1}{(n+1)^\alpha}>0. \]
La suite $(S_n)$ est donc croissante et majorée; d'après le théorème de convergence monotone, la suite $(S_n)$ converge, et la série converge donc.
\end{itemize}
}

\begin{remarque}
Même si la série converge, on ne connait pas explicitement la valeur de la somme $\displaystyle{\sum_{n\geq 1} \frac{1}{n^\alpha}}$, sauf dans certains rares cas.
\end{remarque}

\afaire{Exercice \lienexo{11}}

    \subsection{Série exponentielle}

\begin{definition}[Série exponentielle]
\vspace*{.1cm}La série de terme général $\frac{x^n}{n!}$ ($x\in \R$) est appelée série exponentielle.
\end{definition}

\begin{theoreme}
\vspace*{.1cm}Pour tout réel $x$, la série exponentielle $\ds{\sum \frac{x^n}{n!}}$ converge, et on a $$\sum_{n= 0}^{+\infty} \frac{x^n}{n!}=\E^x$$
\end{theoreme}

\preuve{Vu dans le chapitre 21 : c'est une application de la formule de Taylor-Lagrange.}

\begin{methode}
Pour déterminer si une série converge ou non, et éventuellement calculer sa limite, on essaiera si possible de se ramener à une des séries usuelles (géométriques, Riemann ou exponentielle).
\end{methode}

\begin{exemple}
Déterminer la nature de la série de terme générale $u_n=\frac{(-3)^{n+1}}{n!}$.
\end{exemple}

\solution[6]{Remarquons tout d'abord que $$u_n=\frac{(-3)^n(-3)}{n!}=-3\frac{(-3)^n}{n!}$$
La série $\displaystyle{\sum_{n\geq 0} \frac{(-3)^n}{n!}}$ converge puisqu'il s'agit de la série exponentielle, et sa somme vaut $\eu{-3}$. Par produit, la série $\displaystyle{\sum_{n\geq 0} u_n}$ converge, et
$$\sum_{n=0}^{+\infty} \frac{(-3)^{n+1}}{n!} = -3\eu{-3}$$
}

\begin{remarque}
Par décalage d'indice, on a également, pour tout réel $x$, $\displaystyle{\sum_{n\geq 1} \frac{x^{n-1}}{(n-1)!}}$ converge également, et $$\sum_{n=1}^{+\infty} \frac{x^{n-1}}{(n-1)!} = \E^x$$
et de manière plus générale
$$\sum_{n=p}^{+\infty} \frac{x^{n-p}}{(n-p)!} = \E^x$$
\end{remarque}

\afaire{Exercice \lienexo{10}}


\section{Théorèmes de convergence}

\subsection{Théorème de comparaison}

Pour majorer $(S_n)$, on commence en général par majorer $(u_n)$. On somme alors ces majorants pour en déduire un majorant de $(S_n)$. On dispose ainsi du théorème suivant :

\begin{theoreme}[Théorème de comparaison]
\labelobj{6}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que pour tout $n$,
$$0 \leq u_n \leq v_n$$
Alors, si la série $\sum v_n$ est convergente, la série $\sum u_n$ est également convergente. Dans ce cas,
$$\sum_{k=0}^{+\infty} u_k \leq \sum_{k=0}^{+\infty} v_k$$
\end{theoreme}

\preuve[8]{Si on note $\displaystyle{S_n=\sum_{k=0}^n u_k}$ et $\displaystyle{T_n=\sum_{k=0}^n v_k}$, on a, pour tout $n$, $S_n \leq T_n$ (addition des inégalités). De plus, la suite $(T_n)$ est également croissante, de limite $T$. Donc pour tout $n$, $T_n\leq T$. Donc
$$\forall n, S_n \leq T_n \leq T$$
La suite $(S_n)$ est donc majorée, et d'après le théorème précédent, la série $\sum u_n$ converge. L'inégalité précédente donne alors
$$\sum_{n=0}^{+\infty} u_n=\lim_{n\rightarrow +\infty} S_n \leq T=\lim_{n\rightarrow +\infty} T_n=\sum_{n=0}^{+\infty} v_n$$}

\begin{exemple}
Soit $(u_n)$ une suite à termes positifs vérifiant, pour tout $n$, $\displaystyle{u_n\leq \frac{1}{2^n}}$. Puisque la série $\displaystyle{\sum \left(\frac{1}{2}\right)^n}$ est une série convergente, la série $\sum u_n$ est donc convergente, et on a $\displaystyle{\sum_{n=0}^{+\infty} u_n\leq 2}$.
\end{exemple}

On dispose également d'un critère de divergence, qui est la contraposée du théorème précédent :

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites à \textbf{termes positifs}. On suppose que pour tout $n$, $0\leq u_n \leq v_n$. Alors, si la série $\sum u_n$ diverge vers $+\infty$, alors la série $\sum v_n$ diverge également vers $+\infty$.
\end{theoreme}

\begin{exemple}
Soit $(u_n)$ une suite à termes positifs vérifiant pour tout entier $n\geq 1$, $u_n\geq \frac{1}{n}$. Alors, puisque la série $\sum \frac{1}{n}$ est divergente, la série $\sum u_n$ est également divergente.
\end{exemple}

\afaire{Exercices \lienexo{10} et \lienexo{11}.}


		\subsection{Equivalence et négligeabilité}
\labelobj{7}
On peut utiliser les équivalents :

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que $\ds{u_n \equi_{+\infty} v_n}$.
Alors, la série $\sum v_n$ est convergente si et seulement si la série $\sum u_n$ est également convergente.
\end{theoreme}

\preuve[10]{La suite $(v_n)$ est non nulle à partir d'un certain rang (pour pouvoir parler d'équivalent). Puisque $\frac{u_n}{v_n} \tendversen{n\to \infty} 1$, il existe un rang $N$ à partir duquel $\frac{u_n}{v_n}\leq \frac32$.

Mais alors, pour tout $n\geq N$, puisque $(v_n)$ est à termes positifs :
\[ 0 \leq u_n \leq \frac{3}{2}v_n. \]
La série $\sum v_n$ étant convergente, on peut alors écrire
\[ 0 \leq \sum_{k=N}^n u_k \leq \frac{3}{2}\sum_{k=N}^n v_k \leq \frac{3}{2}\sum_{k=0}^{+\infty} v_k. \]
Finalement, la suite des sommes partielles $\left(\sum_{k=0}^n u_k\right)$ est croissante (car série à terme positif) et majorée : elle converge et la série $\sum u_n$ converge donc.
}

\begin{exemple}
Montrer que $\ds{\sum_{n\geq 0} \frac{1}{2^n+3^n}}$ converge.
\end{exemple}

\solution[4]{Remarquons que \[ \frac{1}{2^n+3^n} \sim \frac{1}{3^n}\]
Puisque les deux suites sont à termes positifs, et que la série $\ds{\sum \frac{1}{3^n}}$ converge (série géométrique), on en déduit que par équivalent, la série $\ds{\sum_{n\geq 0} \frac{1}{2^n+3^n}}$ converge.
}

On dispose également d'un critère en cas de négligeabilité :

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que $\ds{u_n =\mathrm{o}_{+\infty}(v_n)}$.

Si la série $\sum v_n$ est convergente, alors la série $\sum u_n$ est également convergente.

Si la série $\sum u_n$ est divergente, alors la série $\sum v_n$ est également divergente.
\end{theoreme}

\preuve[10]{La suite $(v_n)$ est non nulle à partir d'un certain rang (pour pouvoir parler de négligeabilité). Puisque $\frac{u_n}{v_n} \tendversen{n\to \infty} 0$, il existe un rang $N$ à partir duquel $\frac{u_n}{v_n}\leq 1$.

Mais alors, pour tout $n\geq N$, puisque $(v_n)$ est à termes positifs :
\[ 0 \leq u_n \leq v_n. \]
La série $\sum v_n$ étant convergente, on peut alors écrire
\[ 0 \leq \sum_{k=N}^n u_k \leq \sum_{k=N}^n v_k \leq \sum_{k=0}^{+\infty} v_k. \]
Finalement, la suite des sommes partielles $\left(\sum_{k=0}^n u_k\right)$ est croissante (car série à terme positif) et majorée : elle converge et la série $\sum u_n$ converge donc.
}

\begin{exemple}
Montrer que $\ds{\sum_{n\geq 0} \eu{-n^2}}$ est une série convergente.
\end{exemple}


\solution[5]{Remarquons que $\ds{\eu{-n^2}=\petito{\frac{1}{n^2}}}$. En effet, \[ \mylim[n]{+\infty}{\frac{\eu{-n^2}}{1/n^2}}=\mylim[n]{+\infty}{n^2\eu{-n^2}} = 0 \text{ par croissances comparées}\]
Les deux suites $(\eu{-n^2})$ et $\left(\frac{1}{n^2}\right)$ sont positives, et la série $\sum \frac{1}{n^2}$ est convergente (Riemann). Par comparaison de série à termes positifs, on en déduit que la série $\sum \eu{-n^2}$ converge.}

\begin{remarque}
Dans les cas où on ne peut pas calculer la somme de la série en se ramenant à une somme usuelle, on peut essayer, via équivalence ou négligeabilité, montrer la convergence ou la divergence de la série en se ramenant, comme dans l'exemple précédent, à une somme de Riemann.

Si $u_n=o_{+\infty}\left(\frac{1}{n^\alpha}\right)$ avec $\alpha>1$, et si la série est à terme positif, alors la série $\sum u_n$ converge. Le cas $\alpha=2$ est souvent utilisé, mais pas toujours !

Si $\frac{1}{n^\alpha}= o_{+\infty}(u_n)$ avec $\alpha<1$, et si la série est à terme positif, alors la série $\sum u_n$ diverge.
\end{remarque}

\afaire{Exercice \lienexo{12}.}

\begin{exo}
Déterminer la nature des séries $\ds{\sum_{n\geq 1} \frac{1}{n^2\ln n}}$ et $\ds{\sum_{n\geq 1} \frac{1}{\sqrt{n}\ln n}}$.
\end{exo}

\solution[10]{Ce sont des cas particuliers de séries appelées les séries de Bertrand.

Remarquons tout d'abord que les deux séries sont à termes positifs. De plus :
\[ \frac{1}{n^2\ln n} = o_{+\infty}\left( \frac{1}{n^{3/2}}\right) \text{ puisque } \frac{\frac{1}{n^2\ln n}}{\frac{1}{n^{3/2}}} = \frac{1}{n^{1/2}\ln n}\tendversen{n\to +\infty} 0. \]
Puisque la série $\ds{\sum_{n\geq 1} \frac{1}{n^{3/2}}}$ converge (car $3/2>1$), par négligeabilité, la série $\ds{\sum_{n\geq 1} \frac{1}{n^2\ln n}}$ converge.

De même
\[ \frac{1}{n^{3/4}} = o_{+\infty}\left(\frac{1}{\sqrt{n}\ln n}\right)\text{ puisque } \frac{ \frac{1}{n^{3/4}} }{ \frac{1}{\sqrt{n}\ln n}} = \frac{\ln n}{n^{1/4}}\tendversen{n\to +\infty} 0 \text{ par croissance comparée}.\]
Puisque $\ds{\sum_{n\geq 1} \frac{1}{n^{3/4}}}$ diverge (car $\frac34 < 1$), par comparaison, la série $\ds{\sum_{n\geq 1} \frac{1}{\sqrt{n}\ln n}}$ diverge.
}

\afaire{Exercice \lienexo{40}.}

\section{Convergence absolue}

    \subsection{Définition}

\begin{definition}
\labelobj{8}
Soit $(u_n)$ une suite réelle. On dit que la série $\sum u_n$ est \textbf{absolument convergente} si la série $\sum |u_n|$ est convergente.
\end{definition}

\begin{exemple}
La série $\displaystyle{\sum_{n\geq 1} \frac{(-1)^n}{n^2}}$ est absolument convergente : en effet, la série $\displaystyle{\sum_{n\geq 1} \left|\frac{(-1)^n}{n^2}\right| = \sum_{n\geq 1} \frac{1}{n^2}}$ est convergente (série de Riemann).
\end{exemple}

    \subsection{Absolue convergence et convergence}

\begin{theoreme}\label{theo:absolue_conv}
\labelobj{9}
Soit $(u_n)$ une suite réelle. Si la série $\sum u_n$ est absolument convergente, alors elle est convergente.
\end{theoreme}

\preuve[10]{On note $x$ et $y$ les suites définies pour tout $n$ par
\[ x_n  = \frac{u_n+|u_n|}{2} \qeq y_n=\frac{|u_n|-u_n}{2}. \]
Par construction et définition de la valeur absolue,
\[ \forall n\in \N,\quad 0\leq x_n \leq |u_n| \qeq 0\leq y_n\leq |u_n|. \]
Puisque la série est absolument convergente, et que les suites $(x_n)$ et $(y_n)$ sont à termes positifs, par comparaison, on peut en déduire que $\sum x_n$ et $\sum y_n$ convergent.

Mais alors
\[ \forall n,\quad  u_n = \frac{x_n-y_n}{2} \]
et par linéarité, la série $\sum u_n$ converge également.
}

\begin{remarque}
Pour démontrer qu'une série de signe quelconque est convergente, il peut ainsi être judicieux de montrer qu'elle est absolument convergente, et se ramener donc à une série à termes positifs.
\end{remarque}

Les séries absolument convergentes sont \og{}stables\fg{}, comme on peut le voir avec ce théorème (hors-programme) :

\begin{theoreme}
Soit $\sum u_n$ une série absolument convergente. Soit $\sigma:\N\to \N$ une bijection. Alors la série $\sum u_{\sigma(n)}$ est absolument convergente, et \[ \sum_{n=0}^{+\infty} u_{\sigma(n)} = \sum_{n=0}^{+\infty} u_n. \]

Ainsi, changer l'ordre de sommation d'une série absolument convergente ne change ni sa convergence, ni la valeur de la somme.
\end{theoreme}

    \subsection{Convergence et absolue convergence}

\begin{remarque}
La réciproque du theoreme \ref{theo:absolue_conv} n'est pas vraie : une série peut être convergente sans être absolument convergente. On dit, dans ce cas, que la série est \textbf{semi-convergente}.

Par exemple, nous avons vu dans un exercice du chapitre $20$, que la série $\displaystyle{\sum_{n\geq 1} \frac{(-1)^{n-1}}{n}}$ est convergente (de limite $\ln(2)$). En revanche, elle n'est pas absolument convergente, puisque la série $$\sum_{n\geq 1} \left| \frac{(-1)^n}{n}\right| = \sum_{n\geq 1} \frac{1}{n}$$ n'est pas convergente.
\end{remarque}

Les séries semi-convergentes sont, quant à elles, très \og{}instables\fg{}, comme le montre le théorème suivant (hors-programme) :

\begin{theoreme}[Théorème de réarrangement de Riemann]
Soit $\sum u_n$ une série semi-convergente, et $\ell \in \R\cup \{\pm \infty\}$. Alors il existe une bijection $\sigma:\N\to\N$ telle que \[ \sum_{n=0}^{+\infty} u_{\sigma(n)} = \ell.\]
En réarrangeant les termes de la série, on peut ainsi obtenir toute valeur possible.
\end{theoreme}

    \subsection{Etudier une série}

\begin{methode}
	Pour étudier une série $\sum u_n$, on suit différentes étapes :
\begin{enumerate}
    \item On vérifie si la suite $(u_n)$ tend vers $0$. Si non, la série est divergente.
    \item On s'intéresse à la suite $(u_n)$ et on regarde si elle est équivalente, négligeable devant, ou majorée par une suite dont on sait que la série converge (par exemple, parce que c'est une série de référence). Dans ce cas, on peut conclure quant à la convergence (mais pas sur la somme de la série)
    \item Si on demande de déterminer la somme, on pose la suite des sommes partielles $(S_n)$ et on vérifie si on peut la calculer. Si oui, on peut conclure quant à la convergence, et la valeur de la somme le cas échéant.
    \item Si tout ce qui précède n'a pas abouti, on essaie de majorer (ou minorer) les sommes partielles $(S_n)$ si la série est à termes positifs, ou alors on s'intéresse à l'absolue convergence sinon.
\end{enumerate}
\end{methode}

\begin{remarque}
On ne peut pas forcément calculer la somme de la série, même si on arrive à prouver que la série converge.
\end{remarque}

\begin{exemple}
Soit $u$ la suite définie pour $n\geq 1$ par $\displaystyle{u_n=\frac{1}{n^2(n^2+1)}}$. Etudier la nature de la série $\displaystyle{\sum_{n\geq 1} u_n}$
\end{exemple}

\solution[10]{Plusieurs méthodes pour cette série.
\begin{enumerate}
	\item \textbf{Théorème de comparaison} : on constate que \[ u_n\sim \frac{1}{n^4} \]
	La suite $(u_n)$ et la suite $\left(\frac{1}{n^4}\right)$ sont des suites à termes positifs, et la série $\ds{\sum \frac{1}{n^4}}$ converge (série de Riemann avec $4>1$). Par équivalence de suite à termes positifs, la série $\ds{\sum_{n\geq 1} u_n}$ converge.
	\item \textbf{Majoration} : on constate que \[ n^2(n^2+1)=n^4+n^2\geq n^2 \text{ donc } 0\leq \frac{1}{n^2(n^2+1)} \leq \frac{1}{n^2} \]
		La suite $(u_n)$ est à termes positifs et majorée par la suite $\left(\frac{1}{n^2}\right)$ dont la série converge (série de Riemann avec $2>1$). Par majoration, la série $\ds{\sum_{n\geq 1} u_n}$ converge.
	\item \textbf{Calcul des sommes partielles} :
	Remarquons tout d'abord que la suite $(u_n)$ converge vers $0$. On peut écrire $u_n$ sous la forme
$$u_n=\frac{1}{n^2}-\frac{1}{n^2+1}=v_n-w_n$$
Or, la série $\displaystyle{\sum_{n\geq 1} v_n}$ converge (série de Riemann). De plus, pour tout $n\geq 1$,
$$0<w_n\leq \frac{1}{n^2}=v_n$$
Par comparaison, $(w_n)$ étant à termes positifs et la série $\displaystyle{\sum_{n\geq 1} v_n}$ étant convergente, on en déduit que la série $\displaystyle{\sum_{n\geq 1} w_n}$ converge également.\\
Par somme, la série $\displaystyle{\sum_{n\geq 1} u_n}$ converge également, et
$$\sum_{n\geq 1} \frac{1}{n^2(n^2+1)} = \sum_{n\geq 1} \frac{1}{n^2} - \sum_{n\geq 1} \frac{1}{n^2+1}$$
\end{enumerate}
}

\afaire{Exercices \lienexo{30}  et \lienexo{32}}



%%% Fin du cours %%%
