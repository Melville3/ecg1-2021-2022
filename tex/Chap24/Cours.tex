\chapter{Fonctions convexes}
%!TeX root=../../encours.nouveau.tex

\objectifintro{Dans ce chapitre, on généralise la notion d'intégrale, vue sur un segment, au cas d'un intervalle quelconque. On verra des méthodes pour s'assurer que ces intégrales existent, et pour les calculer.}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Karl Kraus (1874--1936).}
La différence entre les psychiatres et les autres malades mentaux, c'est un peu le rapport entre la folie convexe et la folie concave.
\end{extrait}

\begin{objectifs}
	\begin{numerote}
		\item Connaître la définition d'intégrale sur un intervalle :
				\begin{itemize}
					\item \lienobj{01}{connaître la définition}
					\item \lienobj{02}{connaître les différentes propriétés usuelles}
				\end{itemize}
		\item Concernant les théorèmes d'existence :
				\begin{itemize}
					\item \lienobj{03}{connaître le théorème de majoration des fonctions positives}
					\item \lienobj{04}{savoir utiliser les équivalents de fonctions de signe constant}
					\item \lienobj{05}{connaître les intégrales de référence (Riemann, exponentielle)}
					\item \lienobj{06}{savoir appliquer un changement de variable à une intégrale généralisée}
				\end{itemize}
		\item Connaître la notion de convergence absolue :
				\begin{itemize}
					\item \lienobj{07}{connaître la définition}
					\item \lienobj{08}{l'inégalité triangulaire, et le cas d'une fonction continue dont l'intégrale converge absolument vers $0$}
				\end{itemize}
	\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

Dans l'ensemble de ce chapitre, $I$ désigne un intervalle de $\R$ non vide et non réduit à un point. 

\section{Paramétrisation}

Soient $a$ et $b$ deux réels de $I$ tels que $a<b$. Soit $f$ une fonction définie sur $I$. On munit le plan d'un \ron, et on note $\CC_f$ la courbe représentative de $f$.

On peut \textit{paramétriser} un segment :

\begin{proposition}
  L'ensemble des réels de la forme $(1-t)a+tb$, où $t$ parcourt $\interff{0 1}$, est le segment \interff{a b}.
\end{proposition}

\preuve[5]{On procède par double inclusion :
\begin{itemize}
  \item Si $t\in \interff{0 1}$, alors $(b-a)t\in \interff{0 b-a}$ et finalement $(1-t)a+tb=a + t(b-a) \in \interff{a b}$.
  \item Réciproquement, si $u\in \interff{a b}$, alors $t=\frac{u-a}{b-a}$ vérifie 
  \[ (1-t)a+tb = \frac{b-u}{b-a} a + \frac{u-a}{b-a}b=u\frac{b-a}{b-a}=u. \]
\end{itemize}
}

\begin{remarque}
De la même manière, $\left \{ t a + (1-t)b,\, t\in \interff{0 1}\right \} = \interff{a b}$ également.
\end{remarque}

De la même manière, on a :
\begin{itemize}
  \item $u\in f\left(\interff{a b}\right)$ si et seulement s'il existe $t\in \interff{0 1}$ tel que $u = f((1-t)a+tb)$.
  \item $u \in \interff{f(a) f(b)}$ si et seulement s'il existe $t\in \interff{0 1}$ tel que $u=(1-t)f(a)+tf(b)$, en considérant que si $f(a)>f(b)$, $\interff{f(a) f(b)}=\interff{f(b) f(a)}$.
\end{itemize}

\begin{definition}
Soient $A$ et $B$ les deux points du plan, de coordonnées respectives $(a, f(a))$ et $(b, f(b))$. 

On appelle \textbf{arc} de $\CC_f$ entre $a$ et $b$ la portion de $\CC_f$ comprise entre $A$ et $B$.
On appelle \textbf{corde} de cet arc le segment $\interff{A B}$.
\begin{center}
  \includegraphics{arc-corde.mps}
\end{center}
\end{definition}

\section{Convexité, concavité}

    \subsection{Définition}

\begin{definition}
  Soit $I$ un intervalle, $f:I\rightarrow \R$ une fonction, et $\CC_f$ sa courbe représentative dans un repère orthonormé.
\begin{itemize}
    \item On dit que $f$ est \textbf{convexe} si tout arc de $\CC_f$ est en dessous de sa cordes.
    \item On dit que $f$ est \textbf{concave} si tout arc de $\CC_f$ est au dessus de sa cordes.
\end{itemize}
\end{definition}

\begin{center}
\begin{tabular}{cc}
	\includegraphics[width=7cm]{convexe}&\includegraphics[width=7cm]{concave}\\ \textit{Fonction convexe} & \textit{Fonction concave}
\end{tabular}
\end{center}

% \begin{remarque}
%   Si $I=[a;b]$, on peut constater que $g(t)=ta+(1-t)b$ parcourt le segment \interff{a b} si $t$ parcourt $\interff{0 1}$.\\De même, $tf(a)+(1-t)f(b)$ parcourt la corde liant $(a,f(a))$ et $(b,f(b))$. Ainsi, cette définition peut également s'écrire, en traduisant mathématiquement :
% \end{remarque}

% \begin{center}
% \includegraphics[width=9cm]{convexite}
% \end{center}


En utilisant la paramétrisation des arcs et cordes:  

\begin{definition}
  Soit $I$ un intervalle, et $f:I \rightarrow \R$ une fonction.
\begin{itemize}
    \item La fonction $f$ est convexe sur $I$, si, pour tout $a,b \in I$ tels que $a<b$ on a
\[\forall t \in \interff{0 1},\quad f((1-t)a+tb) \leq (1-t) f(a)+tf(b)\]
    \item La fonction $f$ est concave sur $I$, si, pour tout $a,b \in I$ tels que $a<b$ on a
\[\forall t \in \interff{0 1},\quad f(t(1-t)a+tb) \geq (1-t) f(a)+tf(b)\]

\end{itemize}
\end{definition}

\begin{remarque}
De même, $f$ est convexe sur $I$ si, pour tout $(a, b)\in I^2$, tels que $a<b$, on a 
\[ \forall t\in \interff{0 1},\quad f(ta + (1-t)b) \leq tf(a) + (1-t)f(b).\] 
\end{remarque}

\begin{exemple}
  La fonction $f:x\mapsto x^2$ est convexe sur $\R$. En effet, soient $a$ et $b$  deux réels tels que $a<b$ et  $t \in \interff{0 1}$. On a
\begin{align*}
  f(ta+(1-t)b)&=(ta+(1-t)b)^2=t^2a^2+2t(1-t)ab+(1-t)^2b^2
\end{align*}
On a alors
\begin{align*}
  f(ta+(1-t)b)-(tf(a)+(1-t)f(b) )&= t(t-1)a^2+t(1-t)2ab+(1-t)tb^2\\&=t(t-1)(a-b)^2 \leq 0 \textrm{ puisque } t \in \interff{0 1}
\end{align*}
\end{exemple}

\subsection{Convexité, continuité et dérivabilité}

\labelobj{10}
\begin{theoreme}
Une fonction convexe sur un intervalle ouvert $I$ est continue, et admet des dérivées à droite et à gauche en tout point.
\end{theoreme}

\preuve{Résultat admis.}

\begin{remarque}
Ainsi, par contraposée du théorème précédent, si une fonction n'est pas continue sur un intervalle $I$, elle ne peut \textit{a fortiori} pas être convexe sur cet intervalle
\end{remarque}

\section{Cas des fonctions dérivables}


    \subsection{Convexité et dérivée}

\begin{theoreme}
  Soit $I$ un intervalle ouvert, et $f:I\rightarrow \R$ une fonction dérivable sur $I$.\\
Alors $f$ est convexe (respectivement concave) sur $I$ si, et seulement si, $f'$ est croissante (resp. décroissante).
\end{theoreme}


\preuve{Résultat admis.}

\begin{exemple}
  \begin{itemize}
    \item La fonction $\exp$ est convexe sur $\R$. En effet, $\exp'=\exp$ qui est bien une fonction croissante.
    \item La fonction $\ln$ est concave sur $\R^+_*$. En effet, pour tout $x>0$, $\ln'(x)=\frac{1}{x}$ et la fonction inverse est décroissante sur $\R^*_+$.
\end{itemize}
\end{exemple}

        \subsection{Inégalité de convexité}

\labelobj{11}
\begin{theoreme}
  Une fonction dérivable est convexe si et seulement si elle est au dessus de chacune de ses tangentes. Elle est concave si et seulement si elle est en dessous de chacune ses tangentes.
\end{theoreme}


\begin{center}
    \includegraphics[width=10cm]{convexite2}
\end{center}

\begin{application}
  Comme la fonction $\exp$ est convexe, la courbe de la fonction $\exp$ est toujours au dessus de ses tangentes. En particulier, elle est au-dessus de la tangente en $0$, d'équation $y=x+1$. Ainsi, \[\forall x,~\mathrm{e}^x \geq 1+x\]
De même, la fonction $f:x\mapsto \ln(x+1)$ est concave sur $]-1;+\infty[$, donc la courbe de $f$ est toujours en dessous de ses tangentes, et en particulier sa tangente en $0$, d'équation $y=x$. Ainsi,
\[\forall x\in ]-1;+\infty[,~\ln(x+1)\leq x\]
\end{application}


    \subsection{Convexité et signe de $f''$}

\begin{theoreme}
  Soit $f$ une fonction deux fois dérivable sur un intervalle ouvert $I$. Alors $f$ est convexe (respectivement concave) si et seulement si $f''$ est positive (resp. négative).
\end{theoreme}

\preuve{Supposons la fonction $f$ deux fois dérivable sur $I$ et convexe. D'après le théorème précédent, la fonction $f'$ est donc croissante. Puisque $f'$ est elle-même dérivable, $f'$ est croissante si et seulement si $f''$ est positive.}

\begin{exemple}
  Soit $f$ la fonction définie sur $\R^*_+$ par $f(x)=x^2-\ln(x)$. Alors, $f$ est deux fois dérivables sur $\R^*_+$, et on a
\[f''(x)=2+\frac{1}{x^2}\]
La dérivée seconde étant positive sur $\R^*_+$, la fonction $f$ est convexe sur $\R^*_+$.
\end{exemple}

\begin{methode}
  Pour montrer qu'une fonction est convexe, ou concave, tout dépend de sa régularité :
\begin{itemize}
    \item si elle est de classe $\CC^2$ (ou, au moins, deux fois dérivable), on calcule sa dérivée seconde, et on s'intéresse à son signe.
    \item si elle n'est pas deux fois dérivable, mais au moins dérivable, on la dérive et on vérifie le sens de variation de sa dérivée.
    \item si elle n'est pas dérivable, on part sur la définition de base, ou on se ramène à des fonctions connues.
\end{itemize}
\end{methode}

\afaire{Exercices \lienexo{5} et \lienexo{4}.}


    \subsection{Point d'inflexion}

\begin{definition}
  Un \textbf{point d'inflexion} de la courbe $\CC$ est un point où la courbe $\CC$ traverse sa tangente en ce point. Lorsque sa courbe franchit un point d'inflexion, la convexité change de sens.
\end{definition}


\begin{exemple}
  Soit $f:x\mapsto x^3$. Alors la tangente au point d'abscisse $0$ coupe la courbe donc $0$ est un point d'inflexion.
\begin{center}
    \includegraphics[width=8cm]{cube}
\end{center}
\end{exemple}

\begin{theoreme}
  Soit $f$ une fonction de classe $\CC^2$ sur $I$. Si la dérivée seconde de $f$ s'annule en changeant de signe en $x_0$, alors le point de $\CC$ d'abscisse $x_0$ est un point d'inflexion.
\end{theoreme}


\begin{attention}
 Le point d'inflexion peut exister sans que la fonction soit de classe $\CC^2$ : c'est donc une condition suffisante, mais pas nécessaire.
\end{attention}


\begin{methode}
  Pour déterminer l'existence potentielle d'un point d'inflexion, si la fonction est de classe $\CC^2$ :
\begin{itemize}
    \item on détermine la dérivée seconde de la fonction,
    \item on dresse le tableau de signe de la dérivée seconde
    \item on conclut, en cherchant les réels pour lesquels la dérivée seconde s'annule en changeant de signe.
\end{itemize}
\end{methode}

\begin{exemple}
  Soit $f$ la fonction définie sur $\R$ par $f(x)=x^4-6x^2$. Déterminer les éventuels points d'inflexion de $f$.
\end{exemple}

\solution[6]{$f$ est de classe $\CC^2$ sur $\R$ en tant que polynôme, et on a \[f''(x)=12x^2-12=12(x-1)(x+1)\]
En dressant le tableau de signe de $f''$, on constate que la dérivée seconde s'annule en changeant de signe en $-1$ et en $1$. Ainsi, la courbe de $f$ admet deux points d'inflexion.}

\afaire{Exercice \lienexo{6}}.
%%% Fin du cours %%%
