\chapter{Introduction aux espaces vectoriels}
%!TeX root=../../encours.nouveau.tex


\objectifintro{
Ce chapitre est très important et tombe régulièrement au concours. Il est abstrait, mais pas difficile. Il sera enrichi dans un prochain chapitre, et approfondi l'année prochaine. \\Il doit être maitrisé dans son ensemble.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Cédric Chapeaucou}
  Les mathématiques sont le vecteur de l'humanité.
\end{extrait}

\begin{objectifs}
\begin{numerote}
  \item Connaître la définition d'espaces vectoriels et de sous-espaces vectoriels :
\begin{itemize}
  \item \lienobj{1}{Savoir démontrer qu'un ensemble est un sous-espace vectoriel d'un espace vectoriel}
  \item \lienobj{2}{Savoir montrer qu'un vecteur est une combinaison linéaire d'autres vecteurs}
\end{itemize}
  \item Maitriser la notion de base :
\begin{itemize}
  \item \lienobj{3}{Savoir montrer qu'une famille est libre}
  \item \lienobj{4}{Savoir montrer qu'une famille est génératrice}
  \item \lienobj{5}{Savoir montrer qu'une famille est une base d'un espace vectoriel}
  \item \lienobj{6}{Connaître les bases canoniques des espaces usuels}
  \item \lienobj{7}{Savoir déterminer la dimension d'un sous-espace vectoriel}
%* \hyperref[objectif-18-8]{Savoir déterminer les coordonnées d'un vecteur dans une base}
\end{itemize}
\end{numerote}
\end{objectifs}





%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
\section{Espaces vectoriels}

\subsection{Généralités}

\begin{definition}
Soit $E$ un ensemble non vide.
\begin{itemize}
  \item On dit que la loi $+$ est une \textbf{loi de composition interne} sur $E$ si $\forall~(x,y)\in E^2, x+y \in E$.
  \item On dit que la loi $\cdot$ est une \textbf{loi de composition externe} sur $E$ si $\forall~x\in E,\forall~\lambda \in \R, \lambda\cdot x \in E$.
\end{itemize}
\end{definition}

\begin{exemple}
L'exemple le plus classique est l'ensemble des matrices $\MM_{n,p}(\R)$. La loi d'addition de matrices est une loi de composition interne, et la multiplication par un réel est une loi de composition externe.
\end{exemple}

\begin{definition}
Soit $E$ un ensemble non vide, muni d'une loi interne, noté $+$, et d'une loi externe, noté $\cdot$. On dit que $E$ est un \textbf{espace vectoriel} sur $\R$ si les lois vérifient les propriétés suivantes :

\begin{itemize}

  \item
  \begin{tikzpicture}[remember picture, overlay]
    \draw [decorate,decoration={brace,amplitude=7pt},xshift=-4pt,yshift=0pt]
  (-0.4,-2.1) -- (-0.4,0.2) node [black,midway,xshift=-0.6cm] {};
   \node [align=center, rotate=90] at (-1.05, -0.9) {\small Loi $+$};
   \draw [decorate,decoration={brace,amplitude=7pt},xshift=-4pt,yshift=0pt]
 (-0.4,-4.4) -- (-0.4,-2.6) node [black,midway,xshift=-0.6cm] {};
  \node [align=center,rotate=90] at (-1.05, -3.5) {\small Loi $\cdot$};
  \end{tikzpicture}

  (commutativité de $+$) : $\forall~(x,y) \in E^2, x+y=y+x$.
  \item (associativité de $+$) : $\forall~(x,y,z)\in E^3, (x+y)+z=x+(y+z)$
  \item (neutre pour $+$) : il existe un élément, noté $0_E$, tel que $\forall~x \in E, x+0_E=0_E+x=x$.
  \item  (symétrique pour $+$) : pour tout $x \in E$, il existe un élément $y \in E$, tel que $x+y=y+x=0_{E}$.  Cet élément est appelé \textit{symétrique} de $x$.
  \item (neutre pour $\cdot$) $\forall~x\in E, 1.x = x$
  \item (distributivité de $\cdot$) $\forall~\lambda\in \R, \forall~(x,y)\in E^2, \lambda.(x+y)=\lambda.x+\lambda.y$
  \item (distributivité de $\cdot$) $\forall~(\lambda, \mu)\in \R^2, \forall~x\in E, (\lambda+\mu).x=\lambda.x+\mu.x$
  \item $\forall~(\lambda, \mu)\in \R^2, \forall~x\in E, \lambda.(\mu.x)=(\lambda\times\mu).x$.
\end{itemize}
\end{definition}

\begin{remarque}
Si $E$ est un espace vectoriel, les éléments de $E$ sont alors appelés les \textbf{vecteurs}, et les réels sont appelés les \textbf{scalaires}. L'élément $0_{E}$ est appelé vecteur nul.

Au lieu de dire \og{}espace vectoriel sur $\R$\fg{} ou \og{}$\R$-espace vectoriel\fg{}, on dira plus simplement \og{}espace vectoriel\fg{}.
\end{remarque}

\begin{remarque}
Les quatre premières propriétés font de $(E,+)$ ce qu'on appelle un \textbf{groupe abélien} ou groupe commutatif.
\end{remarque}

\begin{remarque}
Le symbole $\cdot$ de la loi de composition externe est très souvent omis. On notera plus souvent $2x$ plutôt que $2\cdot x$.

De même, on écrira $x-y$ plutôt que $x+ (-y)$.
\end{remarque}

\begin{propriete}
  Soit $E$ un $\R$-espace vectoriel. Le vecteur nul $0_E$ est unique.

  Quand la notation n'est pas ambigüe, on notera $0$ plutôt que $0_E$.
\end{propriete}

\preuve[3]{Soient $0_E$ et $0'_E$ deux éléments neutres. Alors
\begin{itemize}
  \item Puisque $0_E$ est neutre, $0_E+0'_E=0'_E$;
  \item puisque $0'_E$ est neutre, $0_E+0'_E=0_E$.
\end{itemize}
On en déduit $0'_E=0_E$.
}

\begin{propriete}
  Soit $E$ un espace vectoriel, et $x\in E$. Le symétrique de $x$ pour l'addition est unique, et est noté $-x$.
\end{propriete}

\preuve[3]{En effet, soient $y$ et $z$ deux symétriques de $x$. Alors
\[ y+(x+z) = y+ 0_E = y \qeq (y+x)+z = 0_E+z = z. \]
Par associativité, $y=z$.
}


\subsection{Règles de calculs}

On se place ici dans un espace vectoriel $E$.

\begin{proposition}
Pour tous $(\lambda,\mu) \in \R^2$ et $(x,y,z)\in E^3$, on a
\begin{itemize}
  \item $\lambda\cdot 0_E=0_E$ et $0\cdot x=0_E$.
  \item Si $x+y=x+z$ alors $y=z$.
  \item $\lambda \cdot(x-y)=\lambda \cdot x-\lambda \cdot y$ et $(\lambda-\mu)\cdot x=\lambda \cdot x-\mu \cdot x$.
  Ainsi, \[ (-\lambda)\cdot x =\lambda \cdot (-x)=-\lambda \cdot x. \]
\end{itemize}
\end{proposition}

\preuve[10]{
\begin{itemize}
  \item On utilise la linéarité :
  \[ \lambda \cdot 0_E = \lambda \cdot (0_E+0_E) = \lambda \cdot 0_E + \lambda \cdot 0_E \]
  soit, en ajoutant $-\lambda \cdot 0_E$, on obtient $\lambda \cdot 0_E=0_E$. De la même manière
  \[ 0 \cdot x = (0+0)\cdot x = 0\cdot x + 0\cdot x\]
  et donc $0\cdot x = 0_E$.
  \item On ré-écrit astucieusement :
  \[ (x+y) + (-x) = (x+(-x))+y=y \qeq (x+z)+(-x) =(x+(-x))+z = z \]
  Puisque $x+y=x+z$, on en déduit que $(x+y)+(-x)=(x+z)+(-x) $ et donc $y=z$.
  \item Toujours les mêmes astuces d'écriture :
  \[ \lambda \cdot x = \lambda \cdot (x-y+y) = \lambda \cdot (x-y) + \lambda \cdot y \]
  et donc $\lambda \cdot (x-y) = (\lambda \cdot x)-(\lambda \cdot y)$. De même
  \[ \lambda \cdot x = (\lambda -\mu+\mu)\cdot x = (\lambda-\mu)\cdot x + \mu \cdot x\]
  et ainsi $(\lambda-\mu)\cdot x = \lambda\cdot x- \mu\cdot x$.
En utilisant alors les deux relations précédentes
\[ (-\lambda)\cdot x = (0-\lambda)\cdot x = 0\cdot x-\lambda\cdot x = -\lambda \cdot x\]
et
\[ \lambda \cdot (-x) = \lambda \cdot (0_E-x) = \lambda \cdot 0_E - \lambda \cdot x = -\lambda \cdot x.\]
\end{itemize}
}

\begin{theoreme}
Soit $\lambda \in \R$, et $x\in E$. Alors
\[\lambda.x=0_E \Leftrightarrow x=0_E\quad \textrm{ou}\quad \lambda=0\]
\end{theoreme}

\preuve[3]{Le sens $\Leftarrow$ a été montré précédemment. Pour le sens direct, supposons $\lambda \cdot x=0_E$ et $\lambda \neq 0$. Alors
\[ x = 1\cdot x = \left(\frac{1}{\lambda}\lambda\right)\cdot x = \frac{1}{\lambda} \cdot (\lambda \cdot x) = \frac{1}{\lambda} \cdot 0_E = 0_E. \]
}

\subsection{Exemples fondamentaux}

  \subsubsection{Ensembles $\R^n$}

Pour $n\geq 1$, on munit $\R^n$ des deux opérations suivantes :
\begin{itemize}
  \item Si $x=(x_1,\hdots,x_n)\in \R^n$ et $y=(y_1,\hdots, y_n)\in \R^n$, on note \[x+y=(x_1+y_1,\hdots, x_n+y_n).\]
  \item Si $x=(x_1,\hdots,x_n)\in \R^n$ et $\lambda \in \R$, on note \[ \lambda \cdot u = (\lambda x_1,\hdots, \lambda x_n).\]
\end{itemize}

\begin{proposition}
  $\R^n$, muni des deux lois précédentes, forme un $\R$-espace vectoriel, dont le vecteur nul est $(0,\hdots, 0)$.
\end{proposition}

\preuve[10]{Les propriétés de la loi $+$ découle des propriétés sur $\R$. L'élément neutre est $(0,\cdot,0)$ et tout vecteur $x=(x_1,\hdots,x_n)$ admet pour opposé $-x=(-x_1,\hdots,-x_n)$. Il nous faut démontrer les propriétés liant $+$ et $\cdot$. Montrons-en une, les autres se démontrant de la même manière. Soit $\lambda \in \R$ et $x=(x_1,\hdots,x_n)$ et $y=(y_1,\hdots,y_n)$ deux éléments de $\R^n$. Alors
\begin{align*}
 \lambda \cdot (x+y) &= \lambda \cdot (x_1+y_1,\hdots,x_n+y_n)\\
 &= \left( \lambda(x_1+y_1),\hdots, \lambda(x_n+y_n)\right) \\
 &= (\lambda x_1+\lambda y_1,\hdots, \lambda x_n+\lambda y_n) \\
 &= (\lambda x_1,\hdots, \lambda x_n)+(\lambda y_1,\hdots, \lambda y_n) = \lambda \cdot x +\lambda \cdot y
\end{align*}
}


  \subsubsection{Ensemble de matrices}

\begin{proposition}
[Matrices colonnes] Les ensembles $\MM_{n,1}(\R)$ pour $n\geq 1$, munis de l'addition de matrices, et de la multiplication par un réel, sont des espaces vectoriels.
\end{proposition}

\preuve[20]{
En effet, si
$A=\matrice{a_1 \\\vdots \\a_n} \qeq B=\matrice{b_1 \\\vdots \\b_n}$alors
\begin{align*}
  A+B&=\matrice{a_1+b_1 \\\vdots \\a_n+b_n }=\matrice{b_1+a_1 \\\vdots \\b_n+a_n}=B+A\\
  (A+B)+C&=\matrice{(a_1+b_1)+c_1 \\\vdots \\(a_n+b_n)+c_n} =
  \matrice{a_1+(b_1+c_1) \\\vdots \\a_n+(b_n+c_n)}=A+(B+C)\\
  A+0_{n,1} &= \matrice{a_1+0 \\\vdots \\a_n+0} =\matrice{0+a_1 \\\vdots \\0+a_n}=\matrice{a_1 \\\vdots \\a_n}=A\\
  A+(-A)&=\matrice{a_1+(-a_1) \\\vdots \\a_n+(-a_n)} =\matrice{0 \\\vdots \\0}=0_{n,1}\\
  1\cdot A&=\matrice{1.a_1 \\\vdots \\1.a_n} =\matrice{a_1 \\\vdots \\a_n}=A\\
  \lambda\cdot (A+B)&=\lambda.\matrice{a_1+b_1 \\\vdots \\a_n+b_n} =\matrice{\lambda (a_1+b_1) \\\vdots \\\lambda (a_n+b_n)}=\matrice{\lambda a_1+\lambda b_1 \\\vdots \\\lambda a_n+\lambda b_n} =\lambda.\matrice{a_1 \\\vdots \\a_n} + \lambda.\matrice{b_1 \\\vdots \\b_n}=\lambda\cdot A+\lambda \cdot B\\
  (\lambda+\mu)\cdot A&=\matrice{(\lambda+\mu)a_1 \\\vdots \\(\lambda+\mu)a_n}=\matrice{\lambda a_1+\mu a_1 \\\vdots \\\lambda a_n+\mu a_n}=\lambda\cdot A+\mu\cdot A\\
  \lambda\cdot (\mu\cdot A) &= \lambda.\matrice{\mu a_1 \\\vdots \\\mu a_n} = \matrice{\lambda \mu a_1 \\\vdots \\\lambda \mu a_n} =(\lambda\times\mu)\cdot A
\end{align*}

}

On peut, de manière plus générale, montrer que :
\begin{proposition}[$\MM_{n,p}(\R)$]
Pour $(n, p)\in \left(\N^*\right)^2$, l'ensemble des matrices $\MM_{n,p}(\R)$ forme un espace-vectoriel, dont le vecteur nul est la matrice nulle.
\end{proposition}

  \subsubsection{Ensemble de fonctions}

Enfin, on dispose des résultats suivants :

\begin{proposition}
  \begin{itemize}
  \item Pour tout entier $n$, $\R[-n]$, et $\R[0]$ forment des espaces vectoriels, de vecteur nul le polynôme nul.
  \item L'ensemble des suites réelles $\R^{\N}$ est un espace vectoriel, de vecteur nul la suite constante nulle.
  \item Si $I$ est un intervalle, l'ensemble $\mathcal{F}(I, \R)$ des fonctions définies sur $I$ forme un espace vectoriel, de vecteur nul la fonction nulle sur $I$.
\end{itemize}
\end{proposition}

On peut même avoir des ensembles plus généraux :

\begin{proposition}
Soient $A$ un ensemble quelconque, et $(E, +, \cdot)$ un $\R$-espace vectoriel. Pour tout $(f, g)\in \mathcal{F}(A, E)^2$ et $\lambda \in \R$, on définit $f+g\in \mathcal{F}(A, E)$ et $\lambda\cdot f\in \mathcal{F}(A,E)$ par
\[ \forall x\in A,\quad (f+g)(x)=f(x)+g(x) \qeq ()\lambda \cdot f)(x)=\lambda \cdot f(x). \]
Alors $\mathcal{F}(A,E)$, muni de ces deux lois, forme un $\R$ espace vectoriel.
\end{proposition}

\subsection{Combinaison linéaire}

Soit $E$ un espace vectoriel.

\begin{definition}
On appelle \textbf{famille finie de vecteurs} de $E$ une $n$-liste $(e_1,\hdots, e_n)$ d'éléments de $E$.
\end{definition}


\begin{exemple}
Si $A=\left(\begin{array}{c} 1\\2 \end{array}\right)$ et
$B=\left(\begin{array}{c} 4\\-5 \end{array}\right)$, alors $(A,B)$ désigne une famille de deux vecteurs de $\MM_{2,1}(\R)$.
\end{exemple}

\begin{definition}
Soient $(e_1, \hdots, e_p)$ une famille de $p$ vecteurs de $E$. Soit $x$ un vecteur de $E$. On dit que $x$ est une \textbf{combinaison linéaire} de la famille $(e_1,\hdots, e_p)$ s'il existe des réels $(\lambda_1, \hdots, \lambda_p)$ tels que
\[x=\sum_{i=1}^p \lambda_i e_i = \lambda_1 e_1+\hdots+ \lambda_p e_p\]
Les réels $\lambda_1,\hdots, \lambda_p$ sont alors les \textbf{coefficients} de la combinaison linéaire.
\end{definition}

\begin{remarque}
Il n'y a pas forcément unicité de la combinaison linéaire.
\end{remarque}

\begin{attention}
  Dans un $\R$-espace vectoriel, on peut faire des combinaisons linéaires, mais pas forcément de produit entre vecteurs.
\end{attention}

\begin{exemple}
Soient $A=\left( \begin{array}{c}1\\2\end{array}\right)$, $B=\left( \begin{array}{c}-3\\1\end{array}\right)$. Alors, $2.A-3.B=\left( \begin{array}{c}11\\1\end{array}\right)$ est une combinaison linéaire de $A$ et $B$.

Dans $\R[0]$, $P=2X^4+X^3+2X^2-1$ est combinaison linéaire des vecteurs $X^4+X^2$ et $X^3-1$.
\end{exemple}

\begin{methode}
\labelobj{2}
Pour montrer qu'un vecteur $x$ est combinaison linéaire d'une famille $(e_1,\hdots,e_p)$, on écrit $\ds{x=\sum_{i=1}^p \lambda_i e_i}$ et on résout un système pour déterminer (ou non) les réels $\lambda_1,\hdots,\lambda_p$.
\end{methode}

\begin{exo}
Notons $A=\left( \begin{array}{c}1\\2\end{array}\right)$, $B=\left( \begin{array}{c}-3\\1\end{array}\right)$ et $X=\left( \begin{array}{c}-5\\4\end{array}\right)$. Montrer que $X$ est combinaison linéaire de $A$ et $B$.
\end{exo}

\solution[7]{
On écrit $X=\lambda A + \mu B$. On a alors
\[\left( \begin{array}{c}-5\\4\end{array}\right)=\left( \begin{array}{c}\lambda -3\mu\\2\lambda+\mu\end{array}\right)\]
On résout alors le système :
\[\left\{ \begin{array}{ccccc} \lambda&-&3\mu&=&-5 \\ 2\lambda&+&\mu&=&4 \end{array}\right. \Leftrightarrow
\left\{ \begin{array}{ccccc} \lambda&-&3\mu&=&-5 \\ &&7\mu&=&14 \end{array}\right.
  \Leftrightarrow
  \left\{ \begin{array}{ccc} \lambda&=&1 \\ \mu&=&2 \end{array}\right.
\]
Ainsi, $X=A+2B$.
}

\section{Sous-espace vectoriel}

\subsection{Définition}

\begin{definition}
Soit $E$ un espace vectoriel. Soit $F$ un sous ensemble de $E$ \underline{non vide}. On dit que $F$ est un \textbf{sous-espace vectoriel} de $E$ si les restrictions des lois $+$ et $\cdot$ à $F$ font de $F$ un espace vectoriel.
\end{definition}

\begin{exemple}
Si $E$ est un espace vectoriel, alors $\{0_E\}$ et $E$  sont des sous-espaces vectoriels de $E$.
\end{exemple}

\begin{propriete}
Soit $F$ un sous-espace vectoriel d'un espace vectoriel $E$. Alors $0_E \in F$ et $F$ est un espace vectoriel.
\end{propriete}

\begin{demonstration}
Par définition, $F$ est un espace vectoriel. Par propriété, si $x\in F$ (puisque $F$ est non vide), alors $0.x \in F$ c'est-à-dire $0_E \in F$.
\end{demonstration}

\begin{remarque}
  On prendra soin, lorsqu'on manipule des sous-espaces vectoriels, de noter les vecteurs de $E$ de manière cohérente. Par exemple :
  \begin{itemize}
    \item Si $E=\R^2$, on écrira $(x,y)$, $(x',y')$, $(x_1,y_1),\hdots$.
    \item Si $E=\R[0]$, on notera $P, Q, R, P_1,\hdots$ et on évitera la notation $P', Q'$ pour ne pas confondre avec la dérivée.
    \item De même pour des espaces de fonctions, on notera $f,g ,h,\hdots$ et on évitera $f', g'\hdots$
    \item Si $E=\R^{\N}$, on notera $u, v$ ou bien $(u_n), (v_n)\hdots$
    \item Enfin, si $E=\MM_{n,p}(\R)$, on notera $A, B, M, N, \hdots$.
  \end{itemize}
\end{remarque}


\begin{proposition}
Soit $F$ un sous ensemble d'un espace vectoriel $E$. Alors, $F$ est un sous-espace vectoriel de $E$ si et seulement si
\begin{itemize}
  \item $F \neq \vide$
  \item $\forall~(x, y) \in F^2, x+y\in F$ ($F$ est stable par addition)
  \item $\forall~x\in F, \lambda \in \R, \lambda.x \in F$ ($F$ est stable par multiplication par un scalaire)
\end{itemize}
\end{proposition}

\begin{remarque}
\begin{itemize}
  \item La première propriété se vérifie en général en montrant que le neutre $0_E$ est dans $F$.
  \item Les deux dernières propriétés peuvent être regroupées en une seule :
    \[\forall~(x, y)\in F^2, \forall~\lambda \in \R, \lambda.x+y\in F\]
\end{itemize}
\end{remarque}

\begin{methode}
\labelobj{1}
On utilise la proposition précédente pour démontrer qu'un ensemble est un sous-espace vectoriel.
\end{methode}

\begin{exo}
Montrer que $F=\left\{ \left(\begin{array}{c} t \\ 0 \end{array}\right),~t\in \R \right\}$ est un sous-espace vectoriel de $\MM_{2,1}(\R)$.
\end{exo}

\solution[7]{
En effet,
\begin{itemize}
  \item $0_{2,1} \in F$ : il suffit de prendre $t=0$.
  \item Si $A=\left(\begin{array}{c} a \\ 0 \end{array}\right), B=\left(\begin{array}{c} b \\ 0 \end{array}\right)$ et $\lambda \in \R$, alors
    \[\lambda.A+B= \left(\begin{array}{c} \lambda a+b \\ 0 \end{array}\right) \in F\]
    en prenant $t=\lambda a+b$.
\end{itemize}
}

\begin{exo}
  Soit $F=\{ (x,y)\in \R^2,\quad 2x+3y=0 \}$, $G= \{ (u_n)\in \R^{\N},\quad u_0=0\}$, $H=\{ P\in \R[0],\quad P(1)=P(-1)\}$ et $I$ l'ensemble des applications croissantes de $\R$ dans $\R$.

  Montrer que $F$ est un s.e.v. de $\R^2$, $G$ est un s.e.v de $\R^{\N}$, $H$ est un s.e.v. de $\R[0]$, mais que $I$ n'est pas un s.e.v. de $\mathcal{F}(\R,\R)$.
\end{exo}

\solution[15]{
\begin{itemize}
  \item $F$ contient $(0,0)$ puisque $2\times 0+3\times 0=0$. Soient $(x, y)$ et $(x',y')$ deux éléments de $F$ et $\lambda \in \R$. Alors $\lambda (x,y)+(x',y')=(\lambda x+x', \lambda y+y')$ vérifie :
  \[ 2(\lambda x+x') + 3(\lambda y+y') = \lambda(\underbrace{2x+3y}_{=0}) + (\underbrace{2x'+3y'}_{=0}) = 0\]
  Ainsi, $\lambda(x,y)+(x',y')\in F$.
  \item La suite nulle est dans $G$. Soient $u, v$ deux suites de $G$ et $\lambda$ un réel. Alors $\lambda u+v = (\lambda u_n+v_n)_{n\in \N}$ et \[ (\lambda u+v)_0=\lambda u_0+v_0 =0. \]
  Ainsi, $\lambda u+v \in G$.
  \item Le polynôme nul vérifie $P(1)=P(-1)$ donc est dans $H$. Soient $P, Q$ deux polynômes de $H$, et $\lambda$ un réel. On constate alors que :
  \[ (\lambda P+Q)(1) = \lambda P(1)+Q(1) = \lambda P(-1)+Q(-1)=(\lambda P+Q)(-1) \]
  et donc $\lambda P+Q\in H$.
  \item La fonction nulle est dans $I$ mais $I$ n'est pas stable : si $f\in I$, $-f$ est décroissante donc n'est pas dans $I$.
\end{itemize}
}


\begin{theoreme}
Soit $(\mathcal{S})$ un système linéaire homogène de $n$ équations à $n$ inconnues. L'ensemble des solutions de $(\mathcal{S})$ forme un sous-espace vectoriel de $\MM_{n,1}(\R)$
\end{theoreme}

\preuve[15]{
Soit $M\in \MM_{n}(\R)$ la matrice associée au système $\mathcal{S}$. Alors l'ensemble des solutions $F$ du système $(\mathcal{S})$  s'écrit également
\[F=\left \{ X \in \MM_{n,1}(\R),~MX=0_{n,1} \right \}\]
avec $0_{n,1} = \left( \begin{array}{c} 0\\\vdots \\0 \end{array}\right)$.
\begin{itemize}
  \item $F \subset \MM_{n,1}(\R)$.
  \item $0_{n,1} \in F$. En effet, $M.0_{n,1} = 0_{n,1}$ par définition de la matrice nulle.
  \item Soient $X$ et $Y$ dans $F$, et $\lambda \in \R$. Alors
    \[M.(\lambda X + Y) = \lambda M.X + M.Y = 0_{n,1} + 0_{n,1} = 0_{n,1}\]
    puisque $MX=MY=0_{n,1}$. Donc $\lambda X + Y \in F$.
\end{itemize}
$F$ est bien un sous-espace vectoriel de $\MM_{n,1}(\R)$.
}

 \afaire{Exercices \lienexo{01}, \lienexo{02}, \lienexo{03} et \lienexo{04}}

\begin{proposition}[Inclusions]
  Soit $E$ un $\R$-espace vectoriel.
  \begin{itemize}
    \item Si $G$ est un sous-espace vectoriel de $E$, et si $F$ est un sous-espace vectoriel de $G$, alors $F$ est un sous-espace vectoriel de $E$.
    \item Si $F$ et $G$ sont deux sous-espaces vectoriels de $E$, tels que $F\subset G$, alors $F$ est un sous-espace vectoriel de $G$.
  \end{itemize}
\end{proposition}

 \subsection{Sous-espaces vectoriels usuels}

Les espaces suivants sont des sous-espaces vectoriels à connaître, et si besoin, à savoir démontrer.

\begin{itemize}
  \item $\R[-n]$ est un sous-espace vectoriel de $\R[0]$.
  \item L'ensemble des suites réelles convergente est un sous-espace vectoriel de $\R^{\N}$.
  \item Si $I$ est un intervalle de $\R$, alors les ensembles des fonctions continues $\CC^0(I,\R)$, dérivables $\DD^1(I,\R)$,  $\CC^1(I,\R)$ sont des sous-espaces vectoriels de $\mathcal{F}(I,\R)$.
  \item Les ensembles des matrices diagonales d'ordre $n$, triangulaires supérieures (resp. inférieures), symétriques et antisymétriques sont des sous-espaces vectoriels de $\MM_n(\R)$.
\end{itemize}

\begin{attention}
  $\mathrm{GL}_n(\R)$ n'est pas un sous-espace vectoriel de $\MM_n(\R)$, puisqu'il ne contient pas la matrice nulle.
\end{attention}

\subsection{Intersection de sous-espace vectoriel}

Une propriété importante : l'intersection de sous-espaces vectoriels reste un sous-espace vectoriel.

\begin{proposition}
  Soit $E$ un espace vectoriel.

  Soit $(F_i)_{i\in I}$ une famille de sous-espaces vectoriels de $E$. Alors
$F = \bigcap\limits_{i\in I} F_i$ est un sous-espace vectoriel de $E$.
\end{proposition}

\preuve[4]{
Tout d'abord, puisque $0_E\in F_i$ pour tout $i\in I$, on a $0_E\in F$. Ainsi, $F\neq \vide$.

Soient $\lambda \in \R$ et $(x,y)\in F^2$. Alors, pour tout $i\in I$, $x\in F_i$ et $y\in F_i$. $F_i$ étant un sous-espace vectoriel de $E$, on en déduit que $\lambda x+y\in F_i$.

Ceci étant vrai pour tout $i\in I$, cela signifie que $\lambda x+y\in F$.
}

\begin{attention}
  L'union de deux sous-espaces vectoriels n'est pas (en général) un sous-espace vectoriel.
\end{attention}


\subsection{Sous-espaces engendrés}

On se donne un espace vectoriel $E$.

\begin{definition}
Soient $(e_1, \hdots, e_p)$ une famille de vecteurs de $E$. On appelle \textbf{sous-espace vectoriel engendré par} $(e_1,\hdots,e_p)$, et on note $\Vect(e_1,\hdots, e_p)$, l'ensemble formé de toutes les combinaisons linéaires de $(e_1,\hdots, e_p)$ :
\[ \Vect(e_1,\hdots, e_p) = \left \{ \sum_{i=1}^p \lambda_i e_i,\, (\lambda_1,\hdots, \lambda_p)\in \R^p \right \}. \]
 Ainsi,
\[x \in \Vect(e_1,\hdots,e_p) \Leftrightarrow\exists~(\lambda_1,\hdots, \lambda_p) \in \R^p,~~x=\lambda_1e_1+\hdots+\lambda_pe_p\]
Par convention, si $p=0$, $\Vect(\vide)=\{0\}$.
\end{definition}

\begin{exemple}
Soit $A=\left( \begin{array}{c} 1 \\ 2 \end{array}\right)$. Alors \[\Vect(A) = \{ \lambda.A,~\lambda\in \R\}= \left\{ \left( \begin{array}{c} \lambda \\ 2\lambda \end{array}\right), \lambda \in \R\right\}\]
$\Vect(A)$ est appelée \textbf{droite vectorielle}.
\end{exemple}

\begin{remarque}
 Si un polynôme $P\in \R[-n]$, alors $P=a_0+\hdots +a_nX^n$ avec $(a_0,\hdots, a_n)\in \R^{n+1}$. Ainsi, $P\in \Vect(1,X,\hdots, X^n)$. On a donc \[ \R[-n] = \Vect(1, X,\hdots, X^n). \]
\end{remarque}

\begin{theoreme}
Soit $(e_1, \hdots, e_p)$ une famille de vecteurs de $E$. Alors $\Vect(e_1,\hdots,e_p)$ est un sous-espace vectoriel de $E$, contenant les vecteurs $e_1,\hdots, e_p$.
\end{theoreme}

\begin{propriete}
Soit $(e_1, \hdots, e_p)$  une famille de vecteurs de $E$. Alors
\begin{itemize}
  \item $\Vect(e_1,\hdots,e_p)$ est le plus petit sous-espace vectoriel contenant la famille $(e_1,\hdots, e_p)$. Ainsi, si $F$ est un sous-espace vectoriel contenant $e_1,\hdots, e_p$, nécessairement, \\$\Vect(e_1,\hdots,e_p) \subset F$.
  \item $\Vect(e_1,\hdots,e_p)$ est l'intersection de tous les sous-espaces vectoriels de $E$ contenant les vecteurs $e_1,\hdots, e_p$.
  \item Si $F=\Vect(e_1,\hdots, e_p, e_{p+1})$ et si $e_{p+1}$ est combinaison linéaire des vecteurs $e_1,\hdots, e_p$, alors $F=\Vect(e_1,\hdots,e_p)$.
  \item Si $(a_1,\hdots, a_p)$ sont des réels tous non nuls, et si $F=\Vect(a_1 e_1,\hdots, a_p e_p)$, alors \\$F=\Vect(e_1,\hdots, e_p)$ et $F=\Vect\left(e_1+\sum\limits_{i=1}^p a_ie_i, e_2,\hdots, e_p\right)$.
\end{itemize}
\end{propriete}

\preuve[10]{
Par définition, $\Vect(e_1,\hdots,e_p)$ est un sous-espace vectoriel, contenant la famille $(e_1,\hdots,e_p)$. Si $F$ est un sous-espace vectoriel de $E$ contenant $(e_1,\hdots, e_p)$, alors il contient toute combinaison linéaire des $(e_1,\hdots, e_n)$, et donc $\Vect(e_1,\hdots,e_p)\subset F$.

Remarquons alors que, puisque tout sous-espace vectoriel de $E$ contenant $(e_1,\hdots,e_p)$ contient $\Vect(e_1,\hdots,e_p)$, alors \[ \Vect(e_1,\hdots,e_p) \subset \bigcap\limits_{\substack{F\text{ s.e.v.}\\(e_i)\in F}} F. \]
Comme $\Vect(e_1,\hdots,e_p)$ est un s.e.v. contenant les $(e_i)$, \[ \bigcap\limits_{\substack{F\text{ s.e.v.}\\(e_i)\in F}} F \subset \Vect(e_1,\hdots,e_p).\]
D'où l'égalité.

Les deux derniers résultats viennent de la définition de $\Vect(e_1,\hdots,e_p)$.
}

\begin{exemple}
\begin{itemize}
  \item Soit $A=\left( \begin{array}{c} 2 \\ -4 \end{array}\right)$. Alors $\Vect(A)=\Vect\left(\left( \begin{array}{c} 1 \\ -2 \end{array} \right) \right)$.
  \item Soient $B=\left( \begin{array}{c} 0 \\ 1 \end{array}\right)$ et $C=\left( \begin{array}{c} 2 \\ -3 \end{array}\right)$. Alors $\Vect(A,B,C)=\Vect(A,B)$ puisque $C=A+B$.
  \item Soit $F$ un sous-espace vectoriel contenant les vecteurs $A$ et $B$ précédents. Alors, nécessairement, $\Vect(A,B) \subset F$.
\end{itemize}
\end{exemple}

\begin{remarque}
Les propriétés précédentes permettent de simplifier l'écriture des sous-espaces vectoriels engendrés :
\begin{itemize}
  \item On peut enlever un vecteur nul de la famille.
  \item On peut enlever un vecteur qui est combinaison linéaire des autres.
  \item On peut ajouter/soustraire un vecteur à un autre vecteur pour simplifier l'écriture.
\end{itemize}
\end{remarque}

\begin{exemple}
  Soit $F=\Vect(X+X^2+X^3, 1, X, X^2+1, X^3)$. On remarque que,
  \begin{align*}
    F &= \Vect(X+X^2+X^3\textcolor{red}{+1}, 1, X, X^2+1\textcolor{red}{-1}, X^3)\\
    &= \Vect(X+X^3+(X^2+1), 1, X, X^2, X^3)\\
    &= \Vect( 1, X, X^2, X^3) = \R[-3]
\end{align*}
\end{exemple}

 \afaire{Exercices \lienexo{05} et \lienexo{06}}

\section{Familles libres, génératrices, bases d'un espace vectoriel}

\subsection{Définition}

\begin{definition}
Soit $E$ un espace vectoriel. Soit  $\mathcal{B}=(e_1,\hdots, e_p)$ une famille de vecteurs de $E$. \\
On dit que $\mathcal{B}$ est une \textbf{base} de $E$ si, pour tout vecteur $x$ de $E$, il existe une \textit{unique} $n$-liste $(\lambda_1,\hdots, \lambda_p)$ de réels tels que
\[x=\sum_{i=1}^p \lambda_i e_i\]
Les réels $(\lambda_1,\hdots, \lambda_p)$ sont appelés les \textbf{coordonnées} de $x$ dans la base $\mathcal{B}$
\end{definition}

\begin{exemple}
Si $F=\left\{ \left(\begin{array}{c} t \\ 0 \end{array}\right),~t\in \R \right\}$, alors tout élément de $F$ s'écrit de manière unique sous la forme $t.\left(\begin{array}{c} 1 \\ 0 \end{array}\right)$. Ainsi, la famille composée du vecteur $\left(\begin{array}{c} 1 \\ 0 \end{array}\right)$ est une base de $F$.
\end{exemple}

\begin{methode}
\labelobj{5}
Pour montrer qu'une famille $(e_1,\hdots,e_p)$ est une base d'un espace vectoriel $E$, on prend $x\in E$ et on résout $\lambda_1e_1+\hdots +\lambda_pe_p = x$. S'il existe une unique solution, alors $(e_1,\hdots, e_p)$ est bien une base de $E$.
\end{methode}

\begin{exemple}
Montrer que $\left(\left(\begin{array}{c}1\\2\end{array}\right), \left(\begin{array}{c} 1\\3 \end{array}\right)\right)$ est une base de $\MM_{2,1}(\R)$.
\end{exemple}

\solution[8]{
Soit $\left(\begin{array}{c} x\\y\end{array}\right)$ un élément de $\MM_{2,1}(\R)$. On cherche $a$ et $b$ tels que
\[a\left(\begin{array}{c}1\\2\end{array}\right)+b \left(\begin{array}{c} 1\\3 \end{array}\right)=\left(\begin{array}{c} x\\y\end{array}\right) \Leftrightarrow \left(\begin{array}{c}a+b\\2a+3b \end{array}\right)=\left(\begin{array}{c}x\\y\end{array}\right)\]
 On résout le système
 \[\left\{ \begin{array}{ccccc}a&+&b&=&x \\2a&+&3b&=&y \end{array}\right.
 \Leftrightarrow \left\{ \begin{array}{ccccc}a&+&b&=&x \\&&b&=&y-2x \end{array}\right.\]
 Le système est de Cramer, donc il possède une unique solution. \\
 \textbf{Bilan} : la famille $\left(\left(\begin{array}{c}1\\2\end{array}\right), \left(\begin{array}{c} 1\\3 \end{array}\right)\right)$ est bien une base de de $\MM_{2,1}(\R)$.
}

\begin{exo}
  Montrer que la famille $(1, X-1, (X-1)^2)$ forme une base de $\R[-2]$.
\end{exo}

\solution[6]{Soit $P\in \R[-2]$. Il existe $a, b$ et $c$ trois réels tels que $P(X)=aX^2+bX+c$. On cherche alors trois réels $x,y,z$ tels que
\[ aX^2+bX+c = x \times 1 + y\times (X-1) + z\times (X-1)^2 \Longleftrightarrow aX^2+bX+c = zX^2+ (y-2z)X + x-y+z. \]
On doit donc résoudre le système
\[ \systeme{z=a,y-2z=b,x-y+z=c} \Leftrightarrow \systeme{x-y+z=c,y-2z=b,z=a}\]
qui est de Cramer. Il possède donc une unique solution, et $(1, X-1, (X-1)^2)$ forme une base de $\R[-2]$.
}

 \afaire{Exercices \lienexo{12} et \lienexo{13}}

 \subsection{Famille libre, famille génératrice}

Dans la définition d'une base, il y a deux éléments importants : l'existence d'une combinaison linéaire, et l'unicité de celle-ci. Cela nous amène à définir deux concepts :

\begin{definition}
Soit $E$ un espace vectoriel. Soit  $(e_1,\hdots, e_p)$ une famille de vecteurs de $E$.
\begin{itemize}
  \item On dit que la famille $(e_1,\hdots, e_p)$ est \textbf{libre} si
    \[\lambda_1 e_1+\hdots +\lambda_p e_p = 0_E \Rightarrow \lambda_1=\hdots=\lambda_p = 0\] Si elle n'est pas libre, on dit qu'elle est \textbf{liée}.
  \item On dit que la famille $(e_1,\hdots, e_p)$ est \textbf{génératrice} si $\Vect(e_1,\hdots,e_p)=E$.
\end{itemize}
\end{definition}

\begin{remarque}
Lorsqu'une famille est libre, cela implique que s'il y a une combinaison linéaire de ses éléments, celle-ci est forcément unique.

En effet, supposons que l'on puisse écrire \[ x = \sum_{i=1}^p \lambda_i e_i = \sum_{i=1}^p \mu_i e_i\]
Alors, en soustrayant les deux expressions de $x$, on obtient :
\[ \sum_{i=1}^p (\lambda_i-\mu_i) e_i = 0 \]
Par définition de la liberté, cela implique que pour tout $i\in \ll 1, p\rr$, $\lambda_i-\mu_i=0$, c'est-à-dire $\lambda_i=\mu_i$ : il y a bien unicité de la décomposition.
\end{remarque}

\begin{methode}
\labelobj{3}
Pour montrer qu'une famille $(e_1,\hdots,e_p)$ est libre, on écrit $\lambda_1e_1+\hdots \lambda_pe_p=0_E$ et on résout le système. S'il admet comme seule solution $(0,\hdots,0)$ alors elle est libre, sinon elle est liée.
\end{methode}

\begin{exemple}\label{\thechapter-exemple-libre-2}
Montrer que la famille $\left(\begin{array}{c}1\\1\\1\end{array}\right),\left(\begin{array}{c}2\\1\\0\end{array}\right)$ est libre.
\end{exemple}

\solution[6]{
On cherche $a$ et $b$ deux réels tels que
\[a\left(\begin{array}{c}1\\1\\1\end{array}\right)+b\left(\begin{array}{c}2\\1\\0\end{array}\right)=\left(\begin{array}{c}0\\0\\0\end{array}\right)\]
Ainsi,
\[\left(\begin{array}{c} a+2b\\a+b\\a \end{array}\right) = \left(\begin{array}{c}0\\0\\0\end{array}\right)\]
On obtient alors $a=0$ puis $b=0$. Ainsi, la famille est libre.
}

\begin{remarque}[Cas d'une famille de deux vecteurs]
Lorsqu'une famille est composée de deux vecteurs $(u,v)$, celle-ci est liée si et seulement si il existe $(\alpha, \beta)$ tels que $\alpha u +\beta v=0$, c'est-à-dire si un des vecteurs s'exprime comme un multiple de l'autre. Ainsi, si on voit rapidement que ce n'est pas le cas, on peut conclure : on signalera que les vecteurs ne sont pas \textbf{colinéaires}.

Par exemple, dans l'exercice \thechapter.\ref{\thechapter-exemple-libre-2}, les deux vecteurs ne sont clairement pas colinéaires (par exemple, en regardant le $0$ en 3\ieme position). Ainsi, la famille est libre.
\end{remarque}


\begin{remarque}[Cas des polynômes]
  Dans un cas particulier, on pourra rapidement signaler qu'une famille de polynôme est libre : si elle est échelonnée en degré.
\end{remarque}

\begin{definition}
On dit qu'une famille de vecteurs de $\R[0]$ est \textbf{échelonnée en degré} si elle est constituée de polynômes de degrés deux à deux distincts.
\end{definition}

\begin{exemple}
  Les familles $(1, X-1, (X-1)^2)$ et $(X, X^3, X^5)$ sont échelonnées en degré.
\end{exemple}

Alors :

\begin{theoreme}[Liberté d'une famille échelonnée en degré]
  Toute famille de polynômes \textbf{non nul} échelonnée en degré est libre.
\end{theoreme}

\preuve[10]{Soit $(P_1,\hdots, P_n)$ une famille de polynômes non nuls échelonnée en degré, dans laquelle on suppose qu'on a ordonné les polynômes en ordre croissant de degré.

Soit $(\lambda_1,\hdots, \lambda_n)$ des réels tels que $\lambda_1P_1 + \hdots + \lambda_nP_n=0$, et supposons que l'un des $\lambda_i$ au moins est non nul. On note alors \[ i_0 = \max \{ i\in \interent{1 n},\, \lambda_i \neq 0\}. \]
Mais alors \[ \lambda_1 P_1+\hdots + \lambda_{i_0} P_{i_0} =0 \]
ce qui est absurde puisque $\lambda_{i_0}\neq 0$ et $P_{i_0}\neq 0$. Ainsi, $(\lambda_1,\hdots, \lambda_n)=(0,\hdots,0)$ et la famille est libre.
}

 \afaire{Exercices \lienexo{10} et \lienexo{11}}

\begin{methode}
\labelobj{4}
Pour montrer qu'une famille $(e_1,\hdots,e_p)$ est génératrice, on écrit $\lambda_1e_1+\hdots \lambda_pe_p=x$ avec $x\in E$ et on résout le système. S'il admet \textbf{au moins} une solution alors elle est génératrice, sinon elle ne l'est pas et on exhibe alors un contre-exemple.
\end{methode}

\begin{remarque}
\begin{itemize}
  \item Une famille libre est une famille dans laquelle aucun vecteur ne peut être exprimé comme combinaison linéaire des autres vecteurs. En effet, si c'est le cas, il existe un entier $i$ et des réels $(\lambda_1, \hdots, \lambda_{i-1},\lambda_{i+1},\hdots \lambda_p)$ tels que \[ e_i=\lambda_1e_1+\hdots+ \lambda_{i-1}e_{i-1}+\lambda_{i+1}e_{i+1}+\hdots +\lambda_pe_p \]
  et donc la famille est liée.


  Ainsi, tout vecteur est \og{}utile\fg{} dans la famille.
  \item Une famille génératrice est une famille qui permet de récupérer, par combinaison linéaire, tout vecteur de E; en revanche, plusieurs combinaisons linéaires peuvent mener au même vecteur : il n'y a pas forcément unicité de la décomposition.
\end{itemize}
\end{remarque}

\begin{methode}Pour déterminer une famille génératrice d'un sous-espace vectoriel $F$, on se donne un vecteur $x$ de $F$ et on cherche des vecteurs $(e_1,\hdots, e_n)$ de $F$ \textbf{qui ne dépendent pas de} $x$, et des réels $(\lambda_1,\hdots, \lambda_n)$ tels que \[ x=\lambda_1e_1+\hdots+\lambda_ne_n. \]
On conclut alors que $F=\Vect(e_1,\hdots, e_n)$ et $(e_1,\hdots, e_n)$ est génératrice de $F$.
\end{methode}

\begin{exemple}
Soit $F=\{ (x,y,z)\in \R^3,\, x+y+z=0\}.$. Déterminer une famille génératrice de $F$.
\end{exemple}

\solution[7]{On constate que
\begin{align*}
  F &= \{ (x,y,z)\in \R^3,\, x = -y-z\} \\
  &= \{ (-y-z, y, z), \, (y,z)\in \R^2\}
\end{align*}
\textbf{Attention} : on ne peut pas écrire $F=\Vect((y-z,y,z))$ puisque cela dépend des coordonnées $(x,y,z)$ du vecteur ! Il faut trouver des vecteurs qui n'en dépendent pas. On continue donc :
\begin{align*}
  F &= \{ y(-1,1,0) + z(-1,0,1),\, (y,z)\in \R^2 \}
\end{align*}
Ainsi, $(-1,1,0)$ et $(-1,0,1)$ ne dépendent pas des coordonnées du vecteur; on peut écrire \[ F = \Vect( (-1,1,0), (-1, 0, 1)).\]
}

\begin{theoreme}[Lien entre famille libre, génératrice et base]
Soit $E$ un espace vectoriel. Soit  $(e_1,\hdots, e_p)$ une famille de vecteurs de $E$. $(e_1,\hdots, e_p)$ est une base si et seulement si elle est libre et génératrice.
\end{theoreme}

\preuve[4]{
Si $(e_1,\hdots, e_p)$ est une base, elle est génératrice par définition. De plus, elle est libre, puisque le vecteur nul n'admet qu'une unique représentation $(0,\hdots, 0)$.

Réciproquement, si $(e_1,\hdots, e_p)$ est libre et génératrice de $E$, tout vecteur de $E$ peut s'écrire comme combinaison linéaire des $(e_1,\hdots, e_p)$, et nous avons vu que la liberté implique que cette combinaison est unique : $(e_1,\hdots, e_p)$ est donc une base de $E$.
}

\begin{remarque}
  Si $F=\Vect(e_1,\hdots, e_n)$, alors par définition, $(e_1,\hdots, e_n)$ est une famille génératrice de $F$. Si de plus elle est libre, c'est une base de $F$.
\end{remarque}



\subsection{Bases canoniques usuelles }

\begin{definition}
\labelobj{6}
\begin{itemize}
\item Soit $n\in \N^*$. On note $e_1=(1, \hdots, 0)$, $e_2=(0, 1, \hdots, 0 ), \hdots, e_n=(0, \hdots, 0, 1)$ des éléments de $\R^n$. Alors la famille de vecteurs $(e_1, \hdots, e_n)$ forme une \textbf{base}, appelée \textbf{base canonique} de $\R^n$.
\item Soit $n\in \N$. La famille $(1, X, \hdots, X^n)$ forme une base de $\R[-n]$, appelée base canonique de $\R[0]$.
\item Soient $n$ et $p$ deux entiers non nuls. Alors la famille $(E_{i,j})_{\substack{1\leq i\leq n\\1\leq j\leq p}}$ est matrices élémentaires forme une base de $\MM_{n,p}(\R)$, appelée base canonique de $\MM_{n,p}(\R)$.
\end{itemize}
\end{definition}

\begin{remarque}
Il n'y a pas unicité de la base. Par exemple, dans $\MM_{2,1}(\R)$, les vecteurs $\left(\begin{array}{c} 2 \\ 0\end{array}\right)$ et $\left(\begin{array}{c} 0\\ -1 \end{array}\right)$ forment également une base.
\end{remarque}

\begin{exemple}
Le polynôme $P=-X^2+4X-2$ s'écrit également $P=
1+2(X-1)-(X-1)^2$. Ainsi :
\begin{itemize}
  \item Dans la base canonique $(1, X, X^2)$ les coordonnées de $P$ sont $(-2, 4, -1)$;
  \item Dans la base $(1, X-1, (X-1)^2)$, ses coordonnées sont $(1, 2 -1)$.
\end{itemize}
\end{exemple}



\begin{definition}
Nous verrons plus tard que, lorsqu'une base existe, toute base d'un espace vectoriel possède le même nombre d'éléments : ainsi, dans $\MM_{2,1}(\R)$, les bases possèdent $2$ vecteurs. On appelle ce nombre la \textbf{dimension} de l'espace vectoriel, et on le notera $\dim(E)$.
\end{definition}

\begin{exemple}
$\MM_{2,1}(\R)$ est de dimension $2$, et plus généralement $\MM_{n,p}(\R)$ est de dimension $n\times p$.

$\R[-n]$ est de dimension $n+1$, et $\R^n$ est de dimension $n$.
\end{exemple}

\begin{methode}\labelobj{7}
Pour déterminer la dimension d'un espace vectoriel, on détermine d'abord une base, et on conclut. Pour cela, on essaie de l'expliciter comme un espace vectoriel engendré par une certaine famille de vecteurs. On vérifie ensuite si celle-ci est libre.
\end{methode}

\begin{exemple}
Soit $F=\left\{ \left(\begin{array}{c}x\\y\end{array}\right),~x+y=0 \right \}$. Déterminer la dimension de $F$.
\end{exemple}

\solution[9]{
Remarquons qu'on peut écrire
\[F=\left\{ \left(\begin{array}{c}x\\y\end{array}\right),~y=-x \right \}\]
soit
\[F=\left\{ \left(\begin{array}{c}x\\-x\end{array}\right),~x\in \R \right \}\]
Ainsi,
\[F=\left\{ x\left(\begin{array}{c}1\\-1\end{array}\right),~x\in \R \right \}\]
et donc $\left(\begin{array}{c}1\\-1\end{array}\right)$ est une base de $F$ :
\[F=\Vect\left(\left(\begin{array}{c}1\\-1\end{array}\right)\right)\]
et $\dim(F) = 1$.
}

\afaire{Exercice \lienexo{14}}
%
%%% Fin du cours %%%
