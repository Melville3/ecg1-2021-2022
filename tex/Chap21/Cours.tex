\chapter{Développements limités}
%!TeX root=../../encours.nouveau.tex


\objectifintro{Dans ce chapitre, on introduit la notion de développement limité, en lien avec le chapitre précédent. Nous verrons ensuite des applications de ceux-ci, dans le calcul de limite et de position relative.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Anton Pavlovitch Tchekhov (1860--1904). \emph{Le Moine noir}}
La joie est-elle un sentiment surnaturel ? Ne doit-elle pas être l'état normal de l'homme ? Plus le développement intellectuel et moral d'un homme est élevé, plus l'homme est libre, et plus la vie lui donne de satisfaction.
\end{extrait}

\begin{objectifs}
\begin{numerote}
	\item Concernant les développements limités :
			\begin{itemize}
				\item \lienobj{1}{connaître la définition d'un développement limité}
				\item \lienobj{2}{savoir obtenir des développements limités (somme, produit, composée, troncature, quotient, intégration)}
				\item \lienobj{1}{connaître les développements limités usuels}
			\end{itemize}
	\item \lienobj{1}{Savoir utiliser la formule de Taylor-Young pour déterminer des développements limités}
	\item Savoir utiliser les développements limités :
			\begin{itemize}
				\item \lienobj{1}{pour déterminer des limites et des équivalents}
				\item \lienobj{1}{pour déterminer des positions relatives}
				\item \lienobj{1}{pour déterminer des asymptotes et position relatives}
			\end{itemize}
\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Développements limités}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Définition}
	%%%%%%%%%%%%%%%%%%%%%%%

\begin{definition}[Développement limité en $x_0$]
Soit $f$ une fonction définie au voisinage de $x_0$, et $n$ un entier naturel. On dit que $f$ admet un \textbf{développement limité} à l'ordre $n$ au voisinage de $x_0$ s'il existe des réels $a_0, a_1\hdots, a_n$ tels qu'au voisinage de $x_0$ on ait
\[ f(x) = a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\hdots +a_n(x-x_0)^n + o_{x_0}\left ( (x-x_0)^n\right)\]
La partie $a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\hdots +a_n(x-x_0)^n$ est appelée \textbf{partie régulière} du développement limité, et $o_{x_0}\left ( (x-x_0)^n\right)$ le \textbf{terme complémentaire} ou le \textbf{reste}.

On abrégera régulièrement ``développement limité à l'ordre $n$ au voisinage de $x_0$'' en $\text{DL}_n(x_0)$.
\end{definition}


\begin{remarque}
	L'idée d'un développement limité est de trouver une approximation locale d'une fonction $f$ par un polynôme. Le terme complémentaire est essentiel, puisqu'il permet d'indiquer l'ordre du développement limité.
\end{remarque}

\begin{remarque}
Pour que $f$ admette un développement limité en $x_0$, il faut qu'elle soit définie au voisinage de $x_0$ : c'est une propriété locale.

Plutôt que d'écrire le reste $o_{x_0}\left((x-x_0)^n\right)$, on pourra écrire $(x-x_0)^n\eps(x)$ avec $\ds{\lim_{x\to x_0} \eps(x)=0}$.
\end{remarque}

\begin{exemple}
	On a le développement limité à l'ordre $1$ suivant : $\ln(1+x)=x+o_0(x)$. En effet,
\[ \lim_{x\rightarrow 0} \frac{\ln(1+x)-x}{x}=\lim_{x\rightarrow 0} \frac{\ln(1+x)}{x}-1=0 \]
\end{exemple}

\begin{exo}
	Montrer qu'on a le développement limité suivant :
$\ds{\E^x=1+x+o_0(x)}$.
\end{exo}


\solution[3]{En effet
\[ \frac{\E^x-1-x}{x} = \frac{\E^x-1}{x}-1 \underset{x\rightarrow 0}{\longrightarrow} 1-1=0 \]
}

Citons le cas particulier, que l'on va souvent utiliser, du développement limité au voisinage de $0$ :

\begin{definition}[Développement limité en $0$]
	Soit $f$ une fonction définie au voisinage de $0$, et $n$ un entier naturel. On dit que $f$ admet un \textbf{développement limité} à l'ordre $n$ au voisinage de $0$ s'il existe des réels $a_0, a_1\hdots, a_n$ tels qu'au voisinage de $x_0$ on ait
	\[ f(x) = a_0 + a_1x+a_2x^2+\hdots +a_nx^n + o_{0}\left ( x^n\right)\]
\end{definition}

\begin{remarque}
	$f$ admet un développement limité en $x_0$ si et seulement si la fonction $h\mapsto f(x_0+h)$ admet un développement limité en $0$ (par rapport à la variable $h$). Dans la suite, on n'énoncera ainsi que les résultats pour les développements limités en $0$.
\end{remarque}

\begin{exo}
	Montrer que $\cos(x)=1-\frac{x^2}{2}+o_0(x^2)$. Déterminer alors un développement limité à l'ordre $2$ de $\sin$ en $\frac{\pi}{2}$.
\end{exo}

\solution[8]{
On a en effet, d'après un résultat du chapitre précédent
\[ \cos(x)-1 \underset{0}{\sim} -\frac{x^2}{2} \]
donc \[ \frac{\cos(x)-1}{x^2}\tendversen{x\to 0}-\frac12. \]
Ainsi
\begin{align*}
 \frac{\cos(x)-1+\frac{x^2}{2}}{x^2} &= \frac{\cos(x)-1}{x^2} +\frac{1}{2}\tendversen{x\to 0} 0
\end{align*}
On a donc bien $\cos(x)=1-\frac{x^2}{2}+o_0(x^2)$. Mais alors, pour $x$ au voisinage de $\frac{\pi}{2}$, posons $x=h+\frac{\pi}{2}$, avec $h$ au voisinage de $0$. On obtient
\begin{align*}
	\sin(x) &= \sin\left(h+\frac{\pi}{2}\right) \\
	&= \cos(h) = 1-\frac{h^2}{2}+o_0(h^2)\\
	&= 1-\frac{\left(x-\frac{\pi}{2}\right)^2}{2}+o_{\frac{\pi}{2}}\left(\left(x-\frac{\pi}{2}\right)^2\right)
\end{align*}
}

\begin{proposition}
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $x_0$. Alors la partie régulière est unique.
\end{proposition}


\preuve[5]{Montrons-le dans le cas d'un voisinage de $0$. Ecrivons $f(x)=P(x)+o_0(x^n) =Q(x)+o_0(x^n)$ avec $P,Q$ deux polynômes de degrés inférieurs ou égaux à $n$. Mais alors
\[ P(x)-Q(x)=o_0(x^n) \quad\text{soit}\quad \lim_{x\rightarrow 0} \frac{P(x)-Q(x)}{x^n}=0 \]
Or, puisque $\deg(P-Q)\leq n$, ce résultat est impossible, sauf si $P-Q=0$. En effet, si $P-Q\neq 0$, alors $P(x)-Q(x) \underset{0}{\sim} \alpha_px^p$ avec $p\leq n$ et $\alpha_p\neq 0$. Mais alors \[ \frac{P(x)-Q(x)}{x^n} \underset{0}{\sim} \alpha_p x^{p-n} \tendversen{x\to 0} \alpha_p\neq 0 \text{ ou }\pm \infty. \]
Ainsi, $P=Q$ et la partie régulière est unique.
}

\begin{exo}
	Déterminer un $\text{DL}_n(0)$ de $f:x\mapsto \frac{1}{1-x}$ (on pourra s'intéresser à $1+x+\hdots +x^n$).
\end{exo}

\solution[6]{Remarquons que, pour $x\neq 1$ : \[1+x+\hdots + x^n = \frac{1-x^{n+1}}{1-x}= \frac{1}{1-x}-\frac{x^{n+1}}{1-x} \]
Ainsi, pour $x\neq 1$ :
\[ \frac{1}{1-x} = 1+x+\hdots + x^n + \frac{x^{n+1}}{1-x} \]
Or
\[ \lim_{x\rightarrow 0} \frac{ \frac{x^{n+1}}{1-x}}{x^n} = \lim_{x\rightarrow 0} \frac{x}{1-x} = 0 \]
Ainsi, $\ds{\frac{x^{n+1}}{1-x} = o_0(x^n)}$ et donc
\[ \frac{1}{1-x} = 1+x+\hdots + x^n + o_0(x^n) \]
}


	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Propriétés des DL}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{proposition}[Troncature]
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $x_0$ de la forme
\[ f(x) = a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\hdots +a_n(x-x_0)^n + o_{x_0}\left ( (x-x_0)^n\right)\]
et soit $0\leq p<n$. Alors $f$ admet un développement limite à l'ordre $p$, donné par
\[ f(x) = a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\hdots +a_p(x-x_0)^p + o_{x_0}\left ( (x-x_0)^p\right)\]
\end{proposition}

\preuve[10]{
Supposons que l'on dispose d'un \devlim[x_0]{n} :
\[ f(x) = a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\hdots +a_n(x-x_0)^n + o_{x_0}\left ( (x-x_0)^n\right)\]
Alors, si $0\leq p<n$ :
\begin{align*}
 f(x) &= a_0+ a_1(x-x_0)+\hdots + a_p(x-x_0)^p + a_{p+1}(x-x_0)^{p+1}+\hdots + a_n(x-x_0)^n + o_{x_0}\left ( (x-x_0)^n\right)\\
      &=  a_0+ a_1(x-x_0)+\hdots + a_p(x-x_0)^p + (x-x_0)^{p}\left(a_{p+1}(x-x_0)+\hdots + a_n(x-x_0)^{n-p}\right)+ o_{x_0}\left ( (x-x_0)^n\right)
\end{align*}
Or
$o_{x_0}\left ( (x-x_0)^n\right)= o_{x_0}\left ( (x-x_0)^p\right)$ et
\[ a_{p+1}(x-x_0)+\hdots + a_n(x-x_0)^{n-p}\tendversen{x\to x_0} 0. \]
Ainsi,
\[ (x-x_0)^{p}\left(a_{p+1}(x-x_0)+\hdots + a_n(x-x_0)^{n-p}\right)+ o_{x_0}\left ( (x-x_0)^n\right) = o_{x_0}\left ( (x-x_0)^p\right). \]
}

\begin{exemple}
	Ainsi, si $f(x)=1+x+x^2+x^3+o_0(x^3)$ alors $f(x)=1+x+x^2+o(x^2)$.
\end{exemple}

\begin{proposition}[Opérations sur les DL]
Soient $f$ et $g$ deux fonctions admettant un développement limité à l'ordre $n$ au voisinage de $0$. On écrit
\[ f(x)=P(x)+o_0(x^n) \qeq g(x)=Q(x)+o_0(x^n) \]
avec $P,Q$ deux polynômes de $\R_n[X]$.
\begin{itemize}
	\item Pour tout réel $\lambda$, $\lambda f+g$ admet un développement limité à l'ordre $n$ au voisinage de $0$, dont la partie régulière est $\lambda P+Q$.
	\item $f\times g$ admet un développement limité à l'ordre $n$ au voisinage de $0$, dont la partie régulière est la troncature du polynôme $P\times Q$ à l'ordre $n$.
	\item Si $f(0)=0$, $g\circ f$ admet un développement limité à l'ordre $n$ au voisinage de $0$, dont la partie régulière est la troncature du polynôme $Q\circ P$ à l'ordre $n$.
\end{itemize}
\end{proposition}


\preuve[7]{Le premier point se fait aisément, en utilisant les règles de prépondérance :
\[ \lambda f(x)+g(x)=\lambda P(x)+o_0(x^n)+Q(x)+o_0(x^n) = (\lambda P+Q)(x) + o_0(x^n) \]
Pour le deuxième point, on écrit astucieusement :
\[ f\times g-P\times Q=f\times g-f\times Q + f\times Q-Q\times P = f\times (g-Q) + Q\times (f-P) \]
Mais alors
\[ f\times g-P\times Q = fo_0(x^n) + Qo_0(x^n) = o_0(x^n) \]
Le dernier point est admis.
}

\begin{attention}
	Pour $f\times g$, $P\times Q$ a un degré $2n$, mais n'apporte qu'une information jusqu'à l'ordre $n$, et donc tout monôme de degré strictement supérieur à $n$ sont à supprimer.\\
Ces résultats sont valables de manière générale en $x_0$, quitte à poser $f(x_0+h)$.
\end{attention}

\begin{exo}
	Déterminer un $\text{DL}_n(0)$ de $x\mapsto \frac{1}{1+x}$ et $x\mapsto \frac{1}{1+x^2}$.
\end{exo}

\solution[6]{On part du développement limité
\[ \frac{1}{1-x}= 1+x+\hdots + x^n + o_0(x^n) \]
et on compose $f:x\mapsto -x$ (qui vérifie bien $f(0)=0$) par $x\mapsto\frac{1}{1-x}$  :
\[ \frac{1}{1+x}=\frac{1}{1-(-x)} = 1-x+x^2+\hdots +(-1)^n x^n + o_0(x^n) \]
De même, on compose $x\mapsto x^2$ par le précédent et on tronque le résultat à l'ordre $n$ :
\[ \frac{1}{1+x^2} = 1-x^2+x^4+\hdots + (-1)^px^{2p} + \hdots + o_0(x^n) \]
}

\begin{proposition}[Parité]
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $0$.
\begin{itemize}
	\item Si $f$ est paire, alors sa partie régulière ne contient que des termes de degrés pairs.
	\item Si $f$ est impaire, alors sa partie régulière ne contient que des termes de degrés impairs.
\end{itemize}
\end{proposition}

\preuve[5]{Ecrivons $f(x)=P(x)+I(x)+o(x^n)$, où $P$ contient les termes pairs, et $I$ les termes impairs du développement limité. Mais alors $f(-x)=P(x)-I(x)+o(x^n)$. Par unicité du développement limité, par exemple si $f$ est paire, on a nécessairement $I(x)=0$.}

\subsection{Quotient}

\begin{methode}
Pour déterminer le développement limité d'un quotient $\frac{f}{g}$, on écrira $\frac{f}{g}=f\times \frac{1}{g}$ et on se ramènera au développement limité de $x\mapsto \frac{1}{1+x}$ par composée.
\end{methode}

\begin{exo}
	Déterminer un $\text{DL}_3(0)$ de $x\mapsto \frac{1}{-x^2+x+1}$.
\end{exo}

\solution[5]{On utilise le développement limité à l'ordre $3$ de $\frac{1}{1+x}$ et on compose $x\mapsto x-x^2$ par celui-ci, en tronquant à l'ordre $3$ :
$\ds{\frac{1}{1+x} = 1-x+x^2-x^3 +o_0(x^3)}$ donc
\[ \frac{1}{1+x-x^2} = 1-(x-x^2)+(x-x^2)^2 -(x-x^2)^3 + o_0(x^3) \]
soit, après développement et troncature
\[ \frac{1}{1+x-x^2} = 1-x+x^2 + (x^2-2x^3) - x^3 + o_0(x^3) = 1-x+2x^2-3x^3 +o_0(x^3) \]}

Dans le cas d'un produit ou d'un quotient par un monôme, on a des résultats plus intéressants :
\begin{proposition}[Cas particulier des monômes]
\begin{itemize}
	\item Si $f(x)=a_0+a_1x+\hdots + a_nx^n + o_0(x^n)$ alors, pour tout $p\in\N$ :
\[ x^pf(x) = a_0x^p + a_ax^{p+1}+\hdots + a_nx^{n+p}+ o_0(x^{n+p}) \]
	\item Si $f(x)=a_px^p + \hdots + a_nx^n + o_0(x^n)$ alors
	\[ \frac{f(x)}{x^p} = a_p + \hdots + a_nx^{n-p} + o_0(x^{n-p}). \]
\end{itemize}
\end{proposition}

\begin{exemple}
	On a vu que $\cos(x)=1-\frac{x^2}{2}+o_0(x^2)$. On peut alors écrire
	\[ \frac{\cos(x)-1}{x} = -\frac{x}{2}+o_0(x). \]
\end{exemple}

	\subsection{Développement limité, continuité et dérivabilité}

\begin{proposition}
$f$ admet un développement limité à l'ordre $0$ au voisinage de $x_0$ de la forme $f(x)=\ell+o_{x_0}(1)$ si et seulement si $f$ est continue (ou prolongeable par continuité) en $x_0$ et $f(x_0)=\ell$.
\end{proposition}

\preuve[3]{En effet, $f(x)-\ell=o_{x_0}(1) \Longleftrightarrow \ds{\lim_{x\rightarrow x_0} \frac{f(x)-\ell}{1}=0}$.}

\begin{proposition}
$f$ admet un développement limité à l'ordre $1$ au voisinage de $x_0$ de la forme $f(x)=a+b(x-x_0)+o_{x_0}(x-x_0)$ si et seulement si $f$ est dérivable en $x_0$ et $f(x_0)=a$ et $f'(x_0)=b$.
\end{proposition}

\preuve[4]{En effet, par troncature, $f(x)=a+o_{x_0}(1)$ si et seulement si $f$ est continue en $x_0$ et $f(x_0)=a$. Enfin
\[ f(x)=a+b(x-x_0)+o_{x_0}(x-x_0) \Longleftrightarrow  \frac{f(x)-a}{x-x_0}=b+o_{x_0}(1)\tendversen{x\to x_0} b \]
}

\begin{attention}
Ce résultat, vrai si $f$ est continue ou dérivable, n'est plus vrai pour les dérivées d'ordre supérieur. Ainsi, une fonction peut avoir un développement limité d'ordre $2$ au voisinage de $0$ sans être deux fois dérivable en $0$.
\end{attention}

\begin{exemple}
	Soit $f:x\mapsto x^3\sin\left(\frac{1}{x}\right)$. Montrer que $f(x)=o_0(x^2)$ mais que $f$ n'est pas deux fois dérivable en $0$.
\end{exemple}


\solution[15]{Rapidement, pour $x\neq 0$, on a $\frac{f(x)}{x^2}=x\sin\left(\frac{1}{x}\right) \longrightarrow 0$ en $0$ (par encadrement classique). Donc $f$ est dérivable en $0$ et $f'(0)=0$. $f$ est de classe $\CC^1$ sur $]-\infty;0[$ et $]0;+\infty[$. Pour $x\neq 0$, on a
\[ f'(x)=3x^2\sin\left(\frac{1}{x}\right) + x^3 \left(-\frac{1}{x^2}\right) \cos\left(\frac{1}{x}\right) =  3x^2\sin\left(\frac{1}{x}\right) - x\cos\left(\frac{1}{x}\right) \]
Calculons alors le taux d'accroissement de $f'$ en $0$ :
\[ \frac{f'(x)-f'(0)}{x-0}=\frac{f'(x)}{x}= 3x\sin\left(\frac{1}{x}\right) - \cos\left(\frac{1}{x}\right) \]
Le premier terme admet une limite en $0$ (qui vaut $0$). En revanche, le deuxième terme n'en admet pas (car $\cos$ n'admet pas de limite en $+\infty$ ou en $-\infty$). Ainsi, $f'$ ne peut être dérivable en $0$, et $f$ n'est donc pas deux fois dérivable en $0$.
}

	\subsection{Intégration}

\begin{proposition}
Soit $f$ une fonction continue sur un intervalle contenant $0$. On suppose que $f$ admet un développement limité à l'ordre $n$ au voisinage de $0$ :
\[ f(x)=\underbrace{a_0+a_1x+a_2x^2+\hdots +a_nx^n}_{=P(x)} + o_0(x^n) \]
Alors une primitive de $f$ admet un développement limité à l'ordre $n+1$ au voisinage de $0$, dont la partie régulière est la primitive de $P$ nulle en $F(0)$ :
\[ F(x)=F(0)+a_0x+\frac{a_1}{2}x^2+\frac{a_2}{3}x^3+\hdots +\frac{a_n}{n+1}x^{n+1}+o_0\left(x^{n+1}\right) \]
\end{proposition}


\preuve[10]{Posons $Q(x)=F(0)+a_0x+\frac{a_1}{2}x^2+\frac{a_2}{3}x^3+\hdots +\frac{a_n}{n+1}x^{n+1}$. La fonction $t\mapsto F(t)-Q(t)$ est de classe $\CC^1$ sur $[0;x]$ (pour $x$ bien choisi), donc par inégalité des accroissements finis
\[ \left | \frac{F(x)-Q(x) - (F(0)-Q(0))}{x-0} \right| \leq \underset{t\in [0;x]}{\max} |(F-Q)'(t)|=\underset{t\in [0;x]}{\max} |f(t)-P(t)| \]
Ainsi,
\[ \left | \frac{F(x)-Q(x)}{x^{n+1}} \right| \leq \frac{\underset{t\in [0;x]}{\max}|f(t)-P(t)|}{x^n}\tendversen{x\rightarrow 0} 0 \]
puisque $f(x)-P(x)=o_0(x^n)$.
}

\begin{exo}
	Déterminer un $\text{DL}_{n+1}(0)$ de $x\mapsto \ln(1+x)$ et un $\text{DL}_{2n+1}(0)$ de $x\mapsto \arctan(x)$.
\end{exo}

\solution[8]{On part d'un $\text{DL}_n(0)$ de $x\mapsto \frac{1}{1+x}$ que l'on intègre :
\[ \frac{1}{1+x} = 1-x+x^2+\hdots + (-1)^n x^n +o_0(x^n) \]
et donc
\[ \ln(1+x) = \underbrace{\ln(1+0)}_{=0} +x-\frac{x^2}{2}+\frac{x^3}{3}+\hdots + (-1)^n \frac{x^{n+1}}{n+1} +o_0(x^{n+1}) \]
De même, on écrit un $\text{DL}_{2n}(0)$ de $x\mapsto \frac{1}{1+x^2}$ que l'on intègre :
\[ \frac{1}{1+x^2} = 1-x^2+x^4+\hdots +(-1)^n x^{2n} + o_0(x^{2n}) \]
et donc
\[ \arctan(x)=\underbrace{\arctan(0)}_{=0} +x - \frac{x^3}{3}+\frac{x^5}{5}+\hdots + (-1)^n \frac{x^{2n+1}}{2n+1} + o_0(x^{2n+1}) \]
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Formule de Taylor-Young}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

	\subsection{Formule de Taylor-Young}

\begin{theoreme}[Formule de Taylor-Young]
Soit $f\in \CC^n(I)$ où $I$ est un intervalle contenant $x_0$. Alors $f$ admet un développement limité à l'ordre $n$ au voisinage de $x_0$ et
\[ f(x)=\sum_{k=0}^n \frac{f^{(k)}(x_0)}{k!}(x-x_0)^k + o_{x_0}((x-x_0)^n) \]
\end{theoreme}

%%%% TODO: démonstration
\preuve{La preuve est admise. Si $f$ est de classe $\CC^{n+1}$, il s'agit d'une conséquence de l'inégalité de Taylor-Lagrange.}

\begin{exo}
	Déterminer le $\text{DL}_n(0)$ de $\exp$.
\end{exo}


\solution[7]{$\exp$ est de classe $\CC^n(\R)$ pour tout entier $n$ (puisqu'en réalité, $\exp$ est de classe $\CC^{\infty}$) et on a \[ \forall~k\in \N,\quad \exp^{(k)} = \exp \quad\text{et donc}\quad \forall~k\in \N,\quad \exp^{(k)}(0)=\exp(0)=1 \]
Ainsi, $f$ admet un développement limité à l'ordre $n$ au voisinage de $0$ et
\[ \exp(x)=\sum_{k=0}^n \frac{\exp^{(k)}(0)}{k!} x^k + o_0(x^n) = \sum_{k=0}^n \frac{1}{k!} x^k + o_0(x^n) \]
}

	\subsection{Développements limités usuels}

En utilisant la formule de Taylor-Young, on obtient les développements limités au voisinage de $0$ suivants, qui sont à \emph{connaître par c\oe ur}.

	\subsubsection{$x\mapsto \frac{1}{1-x}$, logarithme et $\arctan$}

\begin{proposition}
	On dispose des \devlim{n} suivants :
{ \allowdisplaybreaks
\begin{align*}
\frac{1}{1-x} &= 1+x+x^2+\hdots +x^n +o_0(x^n) \\
\frac{1}{1+x} &= 1-x+x^2+\hdots +(-1)^nx^n +o_0(x^n) \\
\ln(1+x) &= x-\frac{x^2}{2}+\hdots +(-1)^{n-1}\frac{x^n}{n} +o_0(x^n) \\
\ln(1-x) &= -x-\frac{x^2}{2}-\hdots -\frac{x^n}{n} +o_0(x^n)\\
\frac{1}{1+x^2} &= 1-x^2+x^4+\hdots +(-1)^n x^{2n} +o_0(x^{2n}) \\
\arctan(x)&=x-\frac{x^3}{3}+\frac{x^5}{5}+\hdots +(-1)^n \frac{x^{2n+1}}{2n+1} + o_0\left(x^{2n+1}\right)
\end{align*}
}
\end{proposition}

\preuve[14]{
Pour le premier, on n'utilise pas la formule de Taylor-Young, mais plutôt le résultat des sommes géométriques :
\begin{align*}
 1+x+\hdots + x^n &= \frac{1-x^{n+1}}{1-x} \\
 &= \frac{1}{1-x} - x^n \underbrace{\frac{x}{1-x}}_{\tendversen{x\to 0}0} = \frac{1}{1-x}+o_0(x^n)
\end{align*}
En remplaçant $x$ par $-x$ (qui tend vers $0$ en $0$) on obtient
\begin{align*}
 \frac{1}{1+x} &= 1-x+x^2+\hdots + (-1)^nx^n + o_0(x^n).
\end{align*}
Par intégration du précédent :
\begin{align*}
 \ln(1+x) &= \ln(1)+x-\frac{x^2}{2}+\hdots + \frac{(-1)^n x^{n+1}}{n+1}+ o_0\left(x^{n+1}\right)
\end{align*}
et par substitution
\begin{align*}
 \ln(1-x) &= -x-\frac{x^2}{2}+\hdots - \frac{ x^{n+1}}{n+1}+ o_0\left(x^{n+1}\right).
\end{align*}
Par composition avec $x\mapsto x^2$ (qui tend vers $0$ en $0$) dans le deuxième développement limité :
\begin{align*}
 \frac{1}{1+x^2} &= 1-x^2+x^4+\hdots +(-1)^nx^{2n} + o_0\left(x^{2n}\right)
\end{align*}
et par intégration
\begin{align*}
 \arctan(x) &= \arctan(0) + x - \frac{x^3}{3}+\frac{x^5}{5}+\hdots +(-1)^n\frac{x^{2n+1}}{2n+1} + o_0\left(x^{2n+1}\right)
\end{align*}
}

	\subsection{Exponentielle et fonctions trigonométriques}

\begin{proposition}
 On dispose des \devlim{n} suivants :
 \begin{align*}
 \exp(x) &= 1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\hdots + \frac{x^n}{n!} + o_0(x^n) \\
 \cos(x) &= 1-\frac{x^2}{2!}+\frac{x^4}{4!}+\hdots (-1)^n\frac{x^{2n}}{(2n)!} + o_0(x^{2n+1}) \\
 \sin(x) &= x-\frac{x^3}{3!}+\frac{x^5}{5!}+\hdots +(-1)^n\frac{x^{2n+1}}{(2n+1)!} + o_0(x^{2n+2}) \\
 \tan(x) &= x+\frac{x^3}{3}+\frac{2x^5}{15}+o_0(x^5)
\end{align*}
\end{proposition}

\preuve[6]{
Les quatre découlent de la formule de Taylor-Young, les fonctions sont de classe $\CC^\infty$ sur un voisinage de $0$, donc admettent des développements limités en $0$ à tout ordre. Il suffit d'utiliser les dérivées vues dans le chapitre $19$.
}

\begin{remarque}
$\tan$ étant de classe $\CC^\infty$ au voisinage de $0$, elle admet un développement limité à tout ordre. Il n'y a cependant pas de formule \og{}simple\fg{} donnant le développement limité.
\end{remarque}

	\subsection{Puissances}

\begin{proposition}
	 Soit $\alpha\in \R$. On dispose des \devlim{n} suivants :
	 \[ (1+x)^\alpha = 1+\alpha x + \frac{\alpha(\alpha-1)}{2}x^2+\hdots + \frac{\alpha(\alpha-1)\hdots(\alpha-n+1)}{n!}x^n + o_0(x^n). \]
 \end{proposition}

\preuve[10]{
On applique la formule de Taylor-Young, la fonction $f:x\mapsto (1+x)^{\alpha}$ étant de classe $\CC^\infty$ au voisinage de $0$. On constate rapidement que, pour tout $n\in \N*$,
\begin{align*}
 f^{(n)}:x&\mapsto \alpha(\alpha-1)\hdots (\alpha-n+1)(1+x)^{\alpha-n} \qeq f^{(n)}(0)=\alpha(\alpha-1)\hdots (\alpha-n+1).
\end{align*}
La formule de Taylor-Young s'écrit alors
\begin{align*}
 (1+x)^{\alpha} &= 1 + \alpha x +\frac{\alpha(\alpha-1)}{2!}x^2 + \hdots +\frac{\alpha(\alpha-1)\hdots(\alpha-n+1)}{n!}x^n + o_0(x^n)
\end{align*}
}

\begin{remarque}
Le cas $\alpha=\frac{1}{2}$ est intéressant. On constate alors que, pour tout $n\in \N*$ :
{\allowdisplaybreaks
\begin{align*}
 \alpha(\alpha-1)\hdots(\alpha-n+1)&= \frac{1}{2}\left(\frac{1}{2}-1\right)\hdots \left(\frac12-n+1\right)\\
 &= \frac{1}{2}\frac{-1}{2}\frac{-3}{2}\hdots \frac{-2n+3}{2} \\
 &= \frac{(-1)^{n-1} 1\times 3\times \hdots \times (2n-3)}{2^n} \\
 &= \frac{(-1)^{n-1} (2n-2)!}{2^{n}\times 2\times 4\times \hdots \times (2n-2)}\\
 &= \frac{(-1)^{n-1} (2n-2)!}{2^{n}2^{n-1}(n-1)!}
\end{align*}
Pour les premières valeurs :
\begin{align*}
 \sqrt{1+x} &= 1+\frac{1}{2}x + o_0(x)\\
 &= 1+\frac{1}{2}x- \frac18x^2 + o_0\left(x^2\right) \\
 &= 1+\frac12 x -\frac18x^2 + \frac{1}{16}x^3 + o_0\left(x^3\right)
\end{align*}
}
\end{remarque}

\section{Applications}

	\subsection{Calcul de limite}

On peut utiliser les développements limités pour lever des formes indéterminées.

\begin{exo}
	Calculer
\[ \lim_{x\rightarrow 0} \frac{(x+1)(\E^x +1) - 3(\ln(1+x)+1)+1}{x^2} \qeq \lim_{x\rightarrow 0} \frac{\ln(1+x)-x}{x^2} \]
\end{exo}

\solution[7]{Dans le premier cas, on utilise le développement limité de $\exp$ et de $x\mapsto \ln(1+x)$ à l'ordre $2$ (car le $x^2$ au dénominateur nous donne l'intuition) :
\[ \frac{(x+1)(1+x+\frac{x^2}{2}+o_0(x^2) + 1)-3(x-\frac{x^2}{2}+o_0(x^2) +1)+1}{x^2} = \frac{3x^2+o_0(x^2)}{x^2} \underset{x\rightarrow 0}{\longrightarrow} 3 \]
De même, en effectuant un développement limité à l'ordre $2$ de $x\mapsto \ln(1+x)$ :
\[ \frac{\ln(1+x)-x}{x^2} = \frac{x-\frac{x^2}{2}+o_0(x^2)-x}{x^2}=\frac{-\frac{x^2}{2}+o_0(x^2)}{x^2}\underset{x\rightarrow 0}{\longrightarrow} -\frac{1}{2} \]
}

	\subsection{Recherche d'équivalents}

\begin{proposition}
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $0$ :
\[ f(x)=a_0+a_1x+\hdots +a_nx^n+o_0(x^n) \]
Soit $p$ l'indice du premier coefficient $a_p$ non nul. On appelle \textbf{forme normalisée} du développement limité l'écriture
\[ f(x)=x^p\left(a_p+a_{p+1}x+\hdots +a_nx^{n-p} + o_0(x^{n-p})\right) \]
et dans ce cas
\[ f(x) \underset{0}{\sim} a_px^p \]
\end{proposition}


\begin{remarque}
	On peut raisonner en dehors de $0$, en faisant un développement limité à l'ordre $n$ de $x\mapsto f(x_0+x)$.
\end{remarque}

\begin{exo}
	Déterminer un équivalent simple en $0$ de \[ f:x\mapsto \tan(x)-x\cos(x)\]
\end{exo}

\solution[7]{On utilise un développement limité à l'ordre 3 au voisinage de $0$ de chacune des fonctions :
\begin{align*}
 \tan(x) &= x+\frac{x^3}{3}+o_0(x^3) &&&  x\cos(x) &= x-\frac{x^3}{2} + o_0(x^3)
\end{align*}
et donc
\[ \tan(x) -x\cos(x) = \frac{5}{6}x^3 + o_0(x^3).\]
Ainsi,
\[ f(x) \underset{0}{\sim} \frac{5}{6} x^3 \]
}

\begin{exo}
	Déterminer un équivalent de la suite $1+n\ln\left(1-\frac1n\right)$.
\end{exo}

\solution[7]{$\frac{1}{n}\tendversen{n\to +\infty} 0$. Déterminons un développement limité au voisinage de $0$ :
\begin{align*}
 \ln(1-x) &= -x - \frac{x^2}{2}+ o_0(x^2)\\
 \text{donc } \ln(1-x)+x &= -\frac{x^2}{2}+o_0(x^2)
\end{align*}
Ainsi, \[ \ln(1-x)+x \eq{0} -\frac{x^2}{2}. \]
Par substitution,
\begin{align*}
	\ln\left(1-\frac1n\right)+\frac1n \eq{+\infty} -\frac{1}{2n^2} \\
	\text{puis } n\ln\left(1-\frac1n\right)+1\eq{+\infty} -\frac{1}{2n}
\end{align*}
}

	\subsection{Position relative de courbe}

\begin{methode}
Supposons que $f$ admette un $\text{DL}_1(x_0)$. Alors on obtient l'équation de la tangente à la courbe de $f$ au point d'abscisse $x_0$ en prenant la partie régulière du développement limité. En appliquant le résultat précédent, et sous réserve qu'un DL d'ordre suffisant existe, on peut écrire
\[ f(x)-(a+bx) \sim_{x_0} a_p(x-x_0)^p \]
Alors le signe de $a_p(x-x_0)^p$ permet de donner la position relative de la courbe et de la tangente au voisinage de $x_0$ : s'il est positif, la courbe est au dessus de sa tangente; sinon elle est au-dessous.
\end{methode}


\begin{attention}
	Le résultat n'est vrai que localement autour de $x_0$.
\end{attention}

\begin{exemple}
	Soit $f:x\mapsto \frac{\ln(1+x)-x}{x^2}$. Montrer que $f$ est dérivable en $0$, et étudier la position relative de la courbe de $f$ et de sa tangente au point d'abscisse $0$.
\end{exemple}

\solution[10]{Utilisons le développement limité de $x\mapsto \ln(1+x)$ au voisinage de $x_0$ :
\[ f(x)=\frac{x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4} + o_0(x^4)-x}{x^2}=-\frac{1}{2}+\frac{1}{3}x-\frac{1}{4}x^2+o_0(x^2) \]
D'une part, puisque $f$ admet (par troncature) un $\text{DL}_1(0)$, $f$ est dérivable en $0$ et $f'(0)=\frac{1}{3}$, mais de plus, $f(x)-\left(-\frac{1}{2}+\frac{1}{3}x\right) = -\frac{1}{4}x^2+o_0(x^2)$.\\
Puisque $-\frac{1}{4}x^2<0$ au voisinage de $0$, on en déduit que la tangente à la courbe de $f$ au voisinage de $0$ est localement toujours au-dessus de la courbe de $f$.
}

	\subsection{Asymptote}

\begin{definition}
On dit qu'une fonction $f$ admet un \textbf{développement asymptotique} à l'ordre $n$ au voisinage de $+\infty$ si on peut écrire
\[ f\left(t\right) = a_0+\frac{a_1}{t}+\hdots + \frac{a_n}{t^n} + o_{+\infty}\left(\frac{1}{t^n}\right) \]
\end{definition}

\begin{methode}
Pour obtenir un développement asymptotique au voisinage de $+\infty$ de $f(x)$, on cherche un développement limité au voisinage de $0$ de $f\left(\frac{1}{t}\right)$, puis on pose $t=\frac{1}{x}$.
\end{methode}


\begin{exo}
	Déterminer un développement asymptotique à l'ordre $2$ de \[ f:x\mapsto \frac{\sqrt{1+\frac{1}{x}}}{x} \]
\end{exo}

\solution[8]{Notons $u=\frac{1}{x}$. On a alors $f(u)=u\sqrt{1+u}$. Faisons un développement limité au voisinage de $0$ :
\[ f(u) =u\left(1+\frac{1}{2}u-\frac{1}{8}u^2+o_0(u^2)\right) = u+\frac{1}{2}u^2+u_0(u^2) \]
On obtient alors de développement asymptotique
\[ f(x) = \frac{1}{x}+\frac{1}{2x^2}+o_{+\infty}\left(\frac{1}{x^2}\right) \]
}

\begin{proposition}
Soit $f$ une fonction, telle que $\frac{f(x)}{x}$ admet un développement asymptotique au voisinage de $+\infty$ à l'ordre $n$ :
\[ \frac{f(x)}{x} = a_0+\frac{a_1}{x}+\hdots + \frac{a_n}{x^n} + o_{+\infty}\left(\frac{1}{x^n}\right)  \]
soit encore
\[ f(x)= a_0x+a_1+\frac{a_2}{x}+\hdots + \frac{a_n}{x^{n-1}}+o_{+\infty}\left(\frac{1}{x^{n-1}}\right) \]
Soit $p\geq 2$ l'indice du premier coefficient $a_p$ non nul. Alors $y=a_0x+a_1$ est asymptote à la courbe de $f$ au voisinage de $+\infty$, et le signe de $\frac{a_p}{x^{p-1}}$ donne la position de la courbe et de son asymptote au voisinage de $+\infty$.
\end{proposition}


\begin{exo}
	Déterminer l'asymptote au voisinage de $+\infty$ et la position relative avec la courbe de la fonction \[ f:x\mapsto x\sqrt{1+\frac{1}{x}}\]
\end{exo}

\solution[10]{En procédant comme précédemment, posons $u=\frac{1}{x}$. $f(u)=\frac{1}{u}\sqrt{1+u}$ et faisons un développement limité à l'ordre $2$ de $u\mapsto\sqrt{1+u}$ :
\[ f(u)=\frac{1}{u}\left(1+\frac{1}{2}u-\frac{1}{8}u^2+o_0(u^2)\right) = \frac{1}{u}+\frac{1}{2}-\frac{1}{8}u+o_0(u) \]
Soit, en revenant à $x$ :
\[ f(x)=x+\frac{1}{2}-\frac{1}{8x}+o_{+\infty}\left(\frac{1}{x}\right) \]
Ainsi, la courbe de $f$ admet la droite d'équation $y=x+\frac{1}{2}$ comme asymptote oblique, et puisque $-\frac{1}{8x}< 0$ au voisinage de $+\infty$, la courbe de $f$ est localement en dessous de son asymptote.
}

%%% Fin du cours %%%
