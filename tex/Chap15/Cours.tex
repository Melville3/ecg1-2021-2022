\chapter{Polynômes}
%!TeX root=../../encours.nouveau.tex

\objectifintro{Dans ce chapitre, on introduit un objet qui servira tant en première qu'en deuxième année, et qui a déjà été vu, en partie, au lycée.}

\begin{extrait}{Victor Hugo (1802 -- 1885). \emph{Proses philosophiques, L’âme}}
Rien n’est solitaire, tout est solidaire. L’homme est solidaire avec la planète, la planète est solidaire avec le soleil, le soleil est solidaire avec l’étoile, l’étoile est solidaire avec la nébuleuse, la nébuleuse, groupe stellaire, est solidaire avec l’infini. Ôtez un terme de cette formule, le polynôme se désorganise, l’équation chancelle, la création n’a plus de sens dans le cosmos et la démocratie n’a plus de sens sur la terre. Donc, solidarité de tout avec tout, et de chacun avec chaque chose. La solidarité des hommes est le corollaire invincible de la solidarité des univers. Le lien démocratique est de même nature que le rayon solaire.
\end{extrait}

\begin{objectifs}
\begin{numerote}
	\item \hyperref[objectif-03-1]{Savoir manipuler la notion de degré (définition, opérations)}\dotfill $\Box$
    \item \hyperref[objectif-03-2]{Savoir manipuler la notion de dérivation de polynôme}\dotfill $\Box$
	\item \hyperref[objectif-03-3]{Connaître le principe de la division euclidienne}\dotfill $\Box$
	\item \hyperref[objectif-03-4]{Connaître la notion de racine}\dotfill $\Box$
	\item \hyperref[objectif-03-5]{Savoir factoriser un polynôme dans $\R$}\dotfill $\Box$
	\item \hyperref[objectif-03-6]{Savoir résoudre des équations se ramenant à un polynôme}\dotfill $\Box$
\end{numerote}
\end{objectifs}


\section{Définitions}

    \subsection{Notion de polynômes}

			\subsubsection{Définition}

\begin{notation}
Pour tout $i \in \N^*$, on introduit les fonctions $\begin{array}{rrcl} X^i:&\R &\mapsto& \R\\ &x &\mapsto& x^i\end{array}$ et la fonction  $\begin{array}{rrcl}X^0: &\R &\mapsto& \R\\ &x &\mapsto& 1\end{array}$

\end{notation}

\begin{definitions}
On appelle \textbf{polynôme à coefficients réels} toute somme finie d'applications précédentes, c'est-à-dire toute fonction s'écrivant sous la forme
		\[P(X)=a_nX^n+a_{n-1}X^{n-1}+\cdots +a_1X+a_0=\sum_{k=0}^n a_kX^k\]
avec $n \in \N$ et $\forall k \in \ll 0,n \rr, a_k\in \R$.
\begin{itemize}[label=\textbullet]
    \item Les nombres $a_k$ sont appelés les \textbf{coefficients} du polynôme $P$.
    \item Si $a_n\neq 0$, on dit que $P$ est de \textbf{degré} $n$ et on note $\deg(P)=n$. Dans ce cas, $a_n$ est appelé \textbf{coefficient dominant} du polynôme $P$.
\end{itemize}
\end{definitions}

\begin{definitions}
Soit $P(X)=\sum\limits_{k=0}^n a_kX^k$ un polynôme à coefficients réels. La \textbf{fonction polynôme associée} au polynôme $P$ est la fonction, encore notée $P$, définie sur $\R$ par \\$P:x\mapsto a_nx^n+\cdots +a_1x+a_0$.

Un polynôme ne contenant qu'un terme, c'est-à-dire s'écrivant sous la forme $P=a_nX^n$ pour $n\in \N*$ et $a_n\in \R$ est appelé un \textbf{monôme}.
\end{definitions}

\begin{exemple}
Le polynôme $P(X)=X^4-2X^2+1$ est un polynôme de degré $4$ et de coefficient dominant $1$. Sa fonction polynôme associée est la fonction $P:x \mapsto x^4-2x^2+1$. Ainsi, $P(1)=1^4-2\times 1^2+1=0$.
\end{exemple}


\begin{remarque}
Attention : la notation $P(X)$ représente une fonction (c'est une représentation \textbf{formelle}), que l'on peut voir comme la composée de $P$ avec la fonction $X$ définie en préambule.

On peut ainsi parler du polynôme $P(X)=X^2-1$ plutôt que de parler de la fonction polynôme $P$ définie pour tout $x\in \R$ par $P(x)=x^2-1$.

On \textbf{évalue} un polynôme en un réel $x$ quand on donne la valeur de $P(x)$.
\end{remarque}

\begin{notation}
On note $\R[0]$, ou $\R\left[x\right]$, l'ensemble des polynômes à coefficients réels, et $\R[-n]$ l'ensemble des polynômes à coefficients réels de degré inférieur ou égal à $n$. \\Ainsi, $\R[-1]=\{aX+b, (a,b)\in \R^2\}$ et $\R[-2]=\{aX^2+bX+c, (a,b,c)\in \R^3\}$.
\end{notation}

\subsubsection{Egalité de polynômes}

L'écriture d'un polynôme n'est pas unique : on peut ajouter autant de termes nuls qu'on le souhaite :
\[ \sum_{k=0}^n a_kX^k = \sum_{k=0}^{n+p} a_kX^k \text{ avec } a_{n+1}=a_{n+2}=\hdots = a_{n+p} = 0. \]

En revanche, les coefficients d'un polynôme sont uniquement déterminés :

\begin{theoreme}[Unicité des coefficients d'un polynôme]
Soient $\ds{P(X)=\sum_{k=0}^n a_kX^k}$ et $\ds{Q(X)=\sum_{k=0}^m b_kX^k}$ deux polynômes. Alors $P=Q$ si et seulement si
\[n=m \qeq \forall k \in \interent{0 n}, \quad a_k=b_k\]
En particulier, la fonction constante égale à $0$ est l'unique polynôme dont tous les coefficients sont nuls. On l'appelle le \textbf{polynôme nul} et on le note $0$.
\end{theoreme}

\preuve[12]{
Supposons que $n\leq m$. On pose alors $a_{n+1}=a_{n+2}=\hdots = a_m=0$, de sorte que \[ \sum_{k=0}^m a_kX^k = \sum_{k=0}^m b_kX^k \iff \forall x\in \R,\, \sum_{k=0}^m(a_k-b_k)x^k=0. \]
On en revient à démontrer la propriété suivante : pour tout $n$, si $(c_0,\hdots, c_n)\in \R^{n+1}$ est tel que $\ds{\sum_{k=0}^n c_kX^k=0}$ alors $c_0=\hdots = c_n=0$. Notons $H_n$ cette propriété que l'on montre par récurrence.

$H_0$ est vraie (car le polynôme est constant). Supposons la propriété $H_n$ vraie pour un certain entier $n$ fixé.

Prenons $(c_0, \hdots, c_{n+1})\in \R^{n+2}$ tels que, pour tout $x\in \R$, $\ds{Q(x)=\sum_{k=0}^{n+1} c_kx^k=0}$. Si $c_{n+1}\neq 0$, remarquons alors que \[ \lim_{x\to +\infty} |Q(x)|=+\infty \]
ce qui est absurde, car $Q(x)=0$ pour tout réel $x$. Donc nécessairement, $c_{n+1}=0$. Mais alors, pour tout $x\in \R$, $\ds{\sum_{k=0}^n c_kx^k=0}$ et on peut appliquer l'hypothèse de récurrence pour en déduire que $c_0=\hdots=c_n=0$.

Le principe de récurrence nous permet alors de conclure que $H_n$ est vraie pour tout $n$.
}

\begin{exemple}
Soient $P(X)=2X^2+3X-1$ et $Q(X)=2X^2+aX-b$. Alors $P=Q$ si et seulement si $a=3$ et $b=1$.
\end{exemple}

 \subsubsection{Application à l'identification}

\begin{exemple}
Soit $P(X)=X^3-X^2-3X+2$. Déterminer trois réels $a,b,c$ tels que
\[\forall x\in\R, \quad P(x)=(x-2)(ax^2+bx+c)\]
\end{exemple}

\solution[10]{
Soit $Q(X)=(X-2)(aX^2+bX+c)$. En développant,
\[Q(X)=aX^3+bX^2+cX-2aX^2-2bX-2c=aX^3+(b-2a)X^2+(c-2b)X-2c\]
Pour que $P=Q$, par unicité de l'écriture d'un polynôme, on identifie les coefficients :
\[\left\{\begin{array}{rcl}
a &=& 1 \\
b-2a &=& -1 \\
c-2b &=& -3 \\
-2c &=& 2
\end{array}\right. \Longleftrightarrow
\left\{\begin{array}{rcl}
a &=& 1 \\
b &=& 1 \\
c &=& -1
\end{array}\right.\]
Ainsi, $P(X)=(X-2)(X^2+X-1)$.
}

\exercice{Trouver deux réels $a$ et $b$ tels que, \[\forall x\in\R\backslash\{-1;1\}, \quad \frac{1}{x^2-1}=\frac{a}{x-1}+\frac{b}{x+1}\]}

\solution[10]{En mettant au même dénominateur, on cherche $a$ et $b$ tels que, pour tout $x \in \R \backslash \{-1;1\}$, on a
\[\frac{1}{x^2-1} = \frac{a(x+1)+b(x-1)}{x^2-1}=\frac{(a+b)x+a-b}{x^2-1}\]
Par identification des coefficients, on a donc
\[\left \{ \begin{array}{ccc} a+b &=& 0 \\ a-b&=&1 \end{array}\right. \Longleftrightarrow \left \{ \begin{array}{ccc} a&=&\frac{1}{2}\\b&=&-\frac{1}{2} \end{array}\right.\]
Ains, pour tout réel $x$ différent de $1$ et $-1$, on a
\[\frac{1}{x^2-1}=\frac{1}{2(x-1)}-\frac{1}{2(x+1)}\]
}

\afaire{Exercice \lienexo{10}.}

    \subsubsection{Degré}

\begin{definition}[Degré]
Soit $\ds{P=\sum_{k=0}^n a_kX^k}$ un polynôme à coefficient réel.
\begin{itemize}
	\item Si $P$ n'est pas le polynôme nul, on appelle \textbf{degré} de $P$, et on note $\deg(P)$, le plus grand des indices des coefficients non nuls de $P$ :
	\[ \deg(P) = \max\left \{ k\in \interent{0 n},\quad a_k\neq 0 \right \}. \]
	Si $p=\deg(P)$, on appelle \textbf{coefficient dominant} de $P$ le terme $a_p$, et \textbf{terme} dominant de $P$ le terme $a_pX^p$. Si $a_p=1$, on dit que le polynôme est \textbf{unitaire}.
	\item Si $P$ est le polynôme nul, on choisit, par convention, de définir $\deg(0)=-\infty$.
\end{itemize}
\end{definition}

On utilisera, pour les règles suivantes, la convention, pour $a\in \N$ :
\[ a+(-\infty)=(-\infty)+a = (-\infty) + (-\infty) = -\infty \qeq \max(a, -\infty)=a. \]

\begin{propriete}
\label{objectif-03-1}
Soient $P(X)=\sum\limits_{k=0}^p a_kX^k$ et $Q(X)=\sum\limits_{k=0}^q b_kX^k$ deux polynômes.
\begin{itemize}[label=\textbullet]
    \item Le degré d'un polynôme constant non nul est $0$.
    \item $\deg(P+Q)\leq \max(\deg(P),\deg(Q))$, avec égalité si et seulement si $p\neq q$ ou bien $p=q$ et $a_p\neq -b_q$.
    \item $\deg(PQ)=\deg(P)+\deg(Q)$, et son coefficient dominant est $a_pb_q$.
\end{itemize}
\end{propriete}

\preuve[10]{
Supposons que $p\leq q$ (l'autre cas est similaire). Alors :
\begin{itemize}
	\item Posons $a_{p+1}=\hdots=a_q=0$, de sorte que \[ P+Q = \sum_{k=0}^q (a_k+b_k)X^k \in \R[0]. \]
	Remarquons alors que $P+Q$ est de degré au maximum $q=\max(p,q)$, et il est de degré $q$ si et seulement si $a_q+b_q\neq 0$, c'est-à-dire $a_q\neq -b_q$. C'est entre autre le cas si $a_q=0$, c'est-à-dire si $\deg(P)<\deg(Q)$.
	\item On suppose que $a_p\neq 0$ et $b_q\neq 0$. Calculons le produit $PQ$ :
	\begin{align*}
		PQ &= \left( a_pX^p + \sum_{k=0}^{p-1} a_kX^k\right)\left( b_qX^q+\sum_{i=0}^{q-1} b_iX^i \right) \\
		&= a_pb_qX^{p+q} + \underbrace{\sum_{k=0}^{p-1} a_kb_qX^{k+q}}_{\text{degré }\leq p+q-1} + \underbrace{\sum_{i=0}^{q-1} a_pb_iX^{p+i}}_{\text{degré }\leq p+q-1} + \underbrace{\sum_{k=0}^{p-1}\sum_{i=0}^{q-1} a_kb_iX^{k+i} }_{\text{degré }\leq p-1+q-1=p+q-2}
	\end{align*}
	Ainsi, puisque $a_pb_q\neq 0$, $PQ$ est de degré $p+q$, et de coefficient dominant $a_pb_q$.
\end{itemize}

}

\begin{corollaire}
On peut généraliser à la somme et au produit d'un nombre fini de polynômes. Si $P_1, \hdots, P_k$ sont des polynômes à coefficients réels, \[ \deg\left(\sum_{i=1}^k P_i\right) \leq \max_{1\leq i\leq k} \left(\deg(P_i)\right) \qeq \deg\left(\prod_{i=1}^k P_i\right) = \sum_{i=1}^k \deg(P_i).\]
Le coefficient dominant du produit est égal au produit des coefficients dominants.

Ainsi, si $P\in \R[0]$ et $k\in \N$ alors $P^k\in \R[0]$ et $\deg\left(P^k\right)=k\deg(P)$ et le coefficient dominant de $P^k$ est la puissance $k$-ième de celui de $P$.
\end{corollaire}

Les résultats précédents permettent de justifier que la somme et le produit par un réel dans $\R_n[X]$ sont des opérations internes : si $(P, Q)\in \R[-n]$ et si $(\lambda, \mu)\in \R^2$, alors \[ \lambda P + \mu Q \in \R[-n].\]

Les opérations sur les polynômes possèdent les propriétés usuelles qui découlent des opérations sur les réels. Par exemple, si $P, Q$ et $R$ sont des polynômes :
\begin{itemize}
	\item Commutativités : $P+Q=Q+P$ et $P\times Q= Q\times P$.
	\item Associativités : $(P+Q)+R=P+(Q+R)$ et $P(QR)=(PQ)R$.
	\item Neutres : $P+0=0+P=P$ ($0$ est le neutre de l'addition) et $P\times 1=1\times P=P$ (le polynôme constant $1$ est le neutre de la multiplication).
	\item Inverse : $P+(-P)=0$ : on dit que $-P$ est l'opposé de $P$, ou encore que $-P$ est \textbf{l'inverse pour l'addition} de $P$.
\end{itemize}

\begin{remarque}
	Ces propriétés font de $\R[0]$, muni de l'addition et de la multiplication, un \textbf{anneau}.
\end{remarque}

En revanche, un polynôme n'admet pas nécessairemet un inverse pour la multiplication :

\begin{proposition}
	Soient $P$ et $Q$ dans $\R[0]$. On a $PQ=1$ si et seulement si $P$ et $Q$ sont des polynômes constants non nuls, inverses l'un de l'autre.
\end{proposition}

\preuve[5]{
Soient $P$ et $Q$ deux polynômes vérifiant $PQ=1$. Tout d'abord, ils sont non nuls. Alors, $\deg(PQ)=\deg(P)+\deg(Q) = \deg(1)=0$ et donc nécessairement, $\deg(P)=\deg(Q)=0$. Ainsi, $P$ et $Q$ sont constants, et puisque $PQ=1$, ils sont constants inverses l'un de l'autre.

Réciproquement, si $P$ et $Q$ sont constants, inverses l'un de l'autre, $PQ=1$.
}

Enfin, on dispose d'une propriété importante : $\R[0]$ est \textbf{intègre} :

\begin{proposition}[Intégrité]
Soient $P$ et $Q$ des polynômes de $\R[0]$. $PQ=0$ si et seulement si $P=0$ ou $Q=0$.
\end{proposition}

\preuve[4]{Si $P=0$ ou $Q=0$, $PQ=0$.

Réciproquement, si $P$ et $Q$ sont tous les deux non nuls, alors $\deg(PQ)=\deg(P)+\deg(Q)\geq 0$ et donc $PQ\neq 0$. On en déduit le résultat par contraposée.
}

    \subsection{Composée et dérivée d'un polynôme}

			\subsubsection{Composition}

\begin{proposition}[Composition de polynômes]
Soient $P(X)=\sum\limits_{k=0}^p a_kX^k$ et $Q(X)=\sum\limits_{k=0}^q b_kX^k$ deux polynômes \textbf{non constants}. $P\circ Q$ est un polynôme et $\deg(P\circ Q)=\deg(P)\deg(Q)$. Le coefficient dominant de $P\circ Q$ est $a_b(b_q)^p$.
\end{proposition}

\preuve[7]{On suppose $a_p\neq 0$ et $b_q\neq 0$ (ainsi, $P$ est de degré $p$ et $Q$ de degré $q$).

Notons que, pour tout $k$, $Q^k$ est un polynôme, de degré $k\times q$ par produit. Alors
\begin{align*}
 P\circ Q &= \underbrace{a_pQ^p}_{\text{degré }pq} + \underbrace{\sum_{k=0}^{p-1} a_kQ^k}_{\text{degré }\leq (p-1)q}
\end{align*}
Ainsi, $P\circ Q$ est est degré $pq$ et de coefficient dominant $a_p b_q^p$.
}


	\subsubsection{Polynôme dérivé}

Remarquons que si $P=\sum\limits_{k=0}^n a_kX^k$ est un polynôme, la fonction polynomiale $x\mapsto P(x)$ est dérivable sur $\R$, et \[ \forall x\in \R,\quad P'(x)=\sum_{k=1}^n ka_kx^{k-1}=\sum_{k=0}^{n-1} (k+1)a_{k+1}x^k. \]
Ainsi, $P'$ est également un polynôme, qu'on appelle polynôme dérivé de $P$ :

\begin{definition}[Polynôme dérivé]
\label{objectif-03-2}
	Soit $P(X)=\sum\limits_{k=0}^n a_k X^k \in \R[0]$ un polynôme. On appelle \textbf{polynôme dérivé}, et on note $P'$, le polynôme défini par $P'=0$ si le polynôme $P$ est constant, ou sinon
\[P'(X)=\sum_{k=1}^n ka_kX^{k-1}=\sum_{k=0}^{n-1}(k+1)a_{k+1}X^k.\]
\end{definition}

\begin{remarque}
On peut dérivée à nouveau $P'$ et on note $P''$ ou $P^{(2)}$ la dérivée seconde de $P$. Plus généralement, on note $P^{(k)}$ la dérivée $k$-ième de $P$.
\end{remarque}

\begin{exemple}
Soit $P(X)=3X^3+2X^2-1$. Alors $P'(X)=9X^2+4X$, $P^{(2)}(X)=18X+4$, $P^{(3)}(X)=18$ et $P^{(4)}(X)=0$.
\end{exemple}

\begin{proposition}
	Soient $P$ et $Q$ deux polynômes et $\lambda \in \R$.
	\begin{itemize}[label=\textbullet]
    \item Si $\deg(P)\geq 1$, alors $\deg(P')=\deg(P)-1$.
    \item Si $\deg(P)=n$, alors $\forall k>n,\quad P^{(k)}=0$.
		\item $(P+Q)'=P'+Q'$, $(\lambda P)'=\lambda P'$, $(PQ)'=P'Q+PQ'$ et $(P\circ Q)' = Q'\times (P'\circ Q)$.
\end{itemize}
\end{proposition}

\afaire{Exercices \lienexo{01}, \lienexo{02} et \lienexo{03}.}

On peut relier les coefficients d'un polynôme à ses dérivées successives en un point : c'est l'objectif de la formule de Taylor :

	\subsection{Formule de Taylor}

\begin{theoreme}[Formule de Taylor pour les polynômes]
Soient $P$ un polynôme à coefficient réels de degré $n$ (avec $n\geq 1$), et $a\in \R$. Alors
\[ P = P(a)+ P'(a)(X-a)+P''(a)\frac{(X-a)^2}{2}+\hdots + P^{(n)}(a)\frac{(X-a)^n}{n!} = \sum_{k=0}^n P^{(k)}(a)\frac{(X-a)^k}{k!}. \]
\end{theoreme}

\preuve[10]{
Notons $Q$ le polynôme \[ Q = P - \left(\sum_{k=0}^n P^{(k)}(a)\frac{(X-a)^k}{k!}\right) \]
et montrons que celui-ci est nul.
Tout d'abord, $Q$ est de degré au plus $n$, comme différence de deux polynômes de degré $n$. Ainsi, $Q^{(n)}$ est un polynôme constant. Or, après dérivation,
\[ Q^{(n)} = P^{(n)} - P^{(n)}(a) \qeq Q^{(n)}(a)=0. \]
ainsi, $Q^{(n)}=0$. Donc $Q^{(n-1)}$ est lui-même constant. Or
\[ Q^{(n-1)} = P^{(n-1)} - P^{(n-1)}(a)-(X-a)P^{(n)}(a) \qeq Q^{(n-1)}(a) = 0. \]
Ainsi, $Q^{(n-1)}$ est nul. En réitérant ce procédé (par récurrence descendante pour être rigoureux), on en déduit à chaque étape que $Q^{(k)}$ est nul pour tout $k$, et finalement $Q=0$.
}

\begin{remarque}
	Si $P$ est de degré $n$, alors $P^{(k)}(a)=0$ pour tout $k>n$. Ainsi, on peut écrire par abus de notation
	\[ P = \sum_{k=0}^{+\infty} P^{(k)}(a)\frac{(X-a)^k}{k!}, \]
	les termes étant nuls à partir d'un certain rang.
\end{remarque}

\section{Arithmétique des polynômes}

	\subsection{Division euclidienne}

Dans l'exercice 22 du chapitre 02, nous avons introduit la notion de division euclidienne dans $\Z$ : si $a$ et $b$ sont deux entiers relatifs, avec $b\neq 0$, il existe un unique couple d'entiers $(q,r)\in \Z^2$ tel que \[ a = bq+r \qeq r\in \interent{0 b-1}. \]

Cette propriété confère à $\Z$ la notion d'\textbf{anneau euclidien}. Nous disposons de la même chose pour $\R[0]$ :

\begin{theoreme}[Division euclidienne]
Soient $A$ et $B$ deux polynômes de $\R[0]$, tels que $B\neq 0$. Il existe un unique couple $(Q,R)\in \left(\R[0]\right)^2$ tel que \[ A = BQ+R \qeq \deg(R)<\deg(B). \]
Le polynôme $Q$ est appelé le \textbf{quotient}, et $R$ est appelé le \textbf{reste} de la division euclidienne de $A$ par $B$.
\end{theoreme}

\preuve[25]{Montrons tout d'abord l'unicité. Supposons qu'il existe deux couples $(Q_1, R_1)$ et $(Q_2, R_2)$ vérifiant \[ A=BQ_1+R_1=BQ_2+R_2 \qeq \deg(R_1)<\deg(B) \text{ et } \deg(R_2)<\deg(B). \]
Alors, $R_1-R_2=B(Q_2-Q_1)$. Si $Q_1\neq Q_2$, alors $\deg(B(Q_2-Q_1))=\deg(B)+\deg(Q_2-Q_1) \geq \deg(B)$, mais en même temps \[ \deg(R_1-R_2)\leq \max(\deg(R_1),\deg(R_2))<\deg(B). \]
C'est absurde. Ainsi, $Q_1-Q_2=0$, c'est-à-dire $Q_1=Q_2$, puis $R_1=R_2$. Il y a ainsi unicité.

Nous allons montrer l'existence par récurrence. On fixe $\ds{B=\sum_{j=0}^p b_jX^j}$ avec $b_p\neq 0$ (ainsi $\deg(B)=p$). Pour tout entier $n$, on note $H_n$ la proposition : \[ \text{Pour tout } A\in \R[-n],\text{ il existe } (Q,R)\in \left(\R[0]\right)^2\text{ tel que } A= BQ+R \qeq \deg(Q)<\deg(B). \]
\begin{itemize}
	\item Pour $n=0$, $A\in \R[-0]$ est constant. On pose alors \[ Q=0, R=A \text{ si } \deg(B)\geq 1,\quad Q=\frac{1}{b_0}A, R=0 \text{ si } \deg(B)=0.\]
	\item On suppose que $H_n$ est vraie pour un certain entier $n$. On écrit $\ds{A=\sum_{k=0}^{n+1} a_kX^k\in \R[- n+1]}$. Si $n+1<p$, alors on peut écrire \[ A = B\times 0 + A \qeq \deg(A)<\deg(B). \]
	On peut prendre $Q=0$ et $R=A$. Si $n+1\geq p$ alors :
	\begin{itemize}
		\item Si $a_{n+1}=0$, en réalité $A\in \R[-n]$ et on peut appliquer l'hypothèse de récurrence à $A$, ce qui garantit l'existence.
		\item Si $a_{n+1}\neq 0$, posons alors
		\[ C = A-\frac{a_{n+1}}{b_p}X^{n+1-p}B. \]
		$C$ est de degré au maximum $n$, car c'est la somme de deux polynôme de degré $n+1$ dont les coefficients dominants sont opposés. Par hypothèse de récurrence appliquée à $C$, il existe $D$ et $R$ deux polynômes tels que \[ C = DB+R \qeq \deg(R)<\deg(B) \]
		 c'est-à-dire
		 \[ A-\frac{a_{n+1}}{b_p}X^{n+1-p}B = D B + R \qeq \deg(R)<\deg(B) \]
		 et finalement
		 \[ A = \left(\underbrace{D+\frac{a_{n+1}}{b_p}X^{n+1-p}}_{=Q}\right)B + R. \]
		 Puisque $\deg(R)<\deg(B)$, on a bien démontré l'existence du couple $(Q,R)$ qui convient.
	\end{itemize}
\end{itemize}
Par récurrence, on peut garantir que $H_n$ est vraie pour tout $n$, ce qui démontre l'existence de la division euclidienne.
}

\begin{exemple}
\label{objectif-03-3}
	On peut poser la division euclidienne comme on le fait dans $\Z$ :

\ifprof
\begin{equation*}
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{2pt}
  \begin{array}{rrrrr|rrr}
		X^4 & -2X^3 & +4X^2 & - 3X & +1 & X^2 & +1&\\
		\cline{6-8}
				-(X^4 &&+X^2)& & & X^2 & -2X &+3\\
				\cline{1-3}
			& -2X^3 & +3X^2 & -3X & +1 & & &\\
		 & -(-2X^3 & & -2X) & & & &\\
		\cline{2-4} & & 3X^2 & -X & +1 & & & \\
		& & -(3X^2 & & +3) & & &\\
		\cline{3-5} & & & -X & -2 & & &
% -X^3&+X^2 &   &  &X^2&-X&-2\\
% \cline{1-2}
%     &-1X^2 & -X  &  &   &   &  \\
%     &X^2&-X&  &   &   &  \\
%     \cline{2-3}
%     &     &-2X & +2 &   &   &  \\
%     &     &2X&-2&   &   &  \\
%               \cline{3-4}
%     &     &   &0&   &   &  \\
  \end{array}
\end{equation*}
On peut en déduire que $X^4-2X^3+4X^2-3X+1 = (X^2+1)(X^2-2X+3) -X-2$ : $X^2-2X+3$ est le quotient, et $-X-2$ est le reste.
\else
\begin{equation*}
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{2pt}
  \begin{array}{rrrrr|rrr}
		X^4 & -2X^3 & +4X^2 & - 3X & +1 & X^2 & +1&\\
		\cline{6-8}
				 &&& & &  &  &\\
				 &&& & &  &  &\\
				 &&& & &  &  &\\
				 &&& & &  &  &\\
				 &&& & &  &  &\\
				 &&& & &  &  &\\
				 &&& & &  &  &\\
  \end{array}
\end{equation*}
~\\
\lignes{2}
\fi
\end{exemple}

\begin{exo}
	Soit $n\in \N$ tel que $n\geq 2$. Déterminer le reste de la division euclidienne de $X^n+1$ par $X^2-1$.
\end{exo}

\solution[10]{Soit $n\in \N\setminus \{0, 1\}$.
Notons qu'il existe $Q$ et $R$ tels que \[ X^n+1 = (X^2-1)Q + R\qeq \deg(R)<\deg(X^2-1). \]
	Ainsi, $R$ est degré au plus $1$ : on peut l'écrire $R=aX+b$ avec $(a,b)\in \R^2$. Donc, pour tout réel $x$ :
	\[ x^n+1 = (x^2-1)Q(x) + ax+b. \]
	Prenons $x=1$ et $x=-1$. On obtient \[ 1^n+1 = 0 + a+b \qeq (-1)^n +1 = 0 + (-a+b). \]
	Ainsi
	\[ a = \frac{1-(-1)^n}{2} \qeq b =\frac{1+(-1)^n}{2}. \]
	Le reste de la division euclidienne de $X^n+1$ par $X^2-1$ est donc \[ \frac{1-(-1)^n}{2} X + \frac{1+(-1)^n}{2}. \]
}

Terminons par un résultat qui nous servira plus tard :

\begin{proposition}\label{prop.div.rac}
Soit $P\in \R[0]$. Pour tout réel $a$, le reste de la division euclidienne de $P$ par $X-a$ est $P(a)$.

Ainsi, si $P(a)=0$, le reste de la division euclidienne de $P$ par $X-a$ est nul.
\end{proposition}

\preuve[4]{
Par division euclidienne, il existe $Q$ et $R$ deux polynômes tels que $P=(X-a)Q+R$ et $\deg(R)<\deg(X-a)=1$. Ainsi, $\deg(R)\leq 0$ et $R$ est une constante $\lambda$. Mais alors, en évaluant en $a$:
\[ P(a)=Q(a)\times (a-a)+\lambda \implies \lambda=P(a).\]
}

	\subsection{Divisibilité dans $\R[0]$}

On va retrouver le vocabulaire classique des entiers, appliqué aux polynômes, avec des résultats très similaires.

\begin{definition}
Soient $A$ et $B$ deux polynômes à coefficients réels. On dit que $A$ est \textbf{divisible} par $B$ (ou que $B$ \textbf{divise} $A$, ou que $B$ est un \textbf{diviseur} de $A$, ou enfin que $A$ est un \textbf{multiple} de $B$) dans $\R[0]$ si le reste de la division euclidienne de $A$ par $B$ est le polynôme nul, c'est-à-dire s'il existe $Q\in \R[0]$ tel que $A=BQ$.

On note alors $B\mid A$.
\end{definition}

Si $B$ divise $A$, alors le polynôme $Q$ tel que $A=BQ$ est nécessairement unique, d'après le théorème de la division euclidienne.

\begin{exemple}
	$X-1$ divise $X^3-1$ car $X^3-1=(X-1)(X^2+X+1)$. De même, $X^2+2X+3$ divise $X^4+2X^3+4X^2+2X+3$ car $(X^2+2X+3)(X^2+1) = X^4 + 2X^3 +4X^2+2X + 3$.
\end{exemple}

\begin{exo}
	Démontrer que $X-1$ divise $X^3-2X^2-X+2$.
\end{exo}

\solution[8]{En posant la division :
\begin{equation*}
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{2pt}
  \begin{array}{rrrr|rrr}
 X^3&-2X^2 & -X  &+2&X  &-1 &  \\
\cline{5-7}
-X^3&+X^2 &   &  &X^2&-X&-2\\
\cline{1-2}
    &-1X^2 & -X  &  &   &   &  \\
    &X^2&-X&  &   &   &  \\
    \cline{2-3}
    &     &-2X & +2 &   &   &  \\
    &     &2X&-2&   &   &  \\
              \cline{3-4}
    &     &   &0&   &   &  \\
  \end{array}
\end{equation*}
Ainsi, $X^3-2X^2-X+2 = (X-1)(X^2-X-2)$ et $X-1$ divise bien $X^3-2X^2-X+2$.
}

\begin{propriete}
Soient $A, B$ et $C$ trois polynômes non nuls.
\begin{itemize}
	\item Si $B\mid A$, alors $\deg(B)\leq \deg(A)$. Il y a égalité si et seulement s'il existe $\lambda \in \R*$ tel que $A=\lambda B$ (on dit que $A$ et $B$ sont  \textbf{colinéaires}).
	\item (\petittitre{Réflexivité}) $A\mid A$.
	\item (\petittitre{Antisymétrie}) Si $A\mid B$ et $B\mid A$ alors il existe $\lambda \in \R$ tel que $A=\lambda B$.
	\item (\petittitre{Transitivité}) Si $A\mid B$ et $B\mid C$ alors $A\mid C$.
\end{itemize}
\end{propriete}

\section{Racines d'un polynôme}

    \subsection{Définition}

\begin{definition}
\label{objectif-03-4}
Soit $P\in \R[0]$ et $a$ un réel. On dit que $a$ est une \textbf{racine} de $P$ si $P(a)=0$.
\end{definition}

\begin{remarque}Chercher les racines de $P$, c'est donc résoudre l'équation $P(X)=0$.

	Le polynôme nul admet tout réel comme racine (il en a donc une infinité). Les autres polynômes constants n'admettent aucune racine.
\end{remarque}

\begin{exemple}
$1$ est une racine du polynôme $P(X)=X^2-1$. En effet, $P(1)=0$.
\end{exemple}

\begin{theoreme}
  Soit $P \in \R[0]$ et $a$ un réel. Alors
$a$ est une racine de $P$ si et seulement s'il existe un polynôme $Q$ tel que $P=(X-a)Q$, c'est-à-dire si et seulement si $X-a$ divise $P$.

$Q$ est alors unique, et on a $\deg(P)=\deg(X-a) +\deg(Q)$ c'est-à-dire $\deg(Q)=\deg (P)-1$.
\end{theoreme}

\preuve[3]{
D'après le résultat de la proposition \ref{prop.div.rac}, il existe $Q$ tel que $P=(X-a)Q + P(a)$. Le reste est nul si et seulement si $P(a)=0$, et donc si et seulement si $P=(X-a)Q$.
}

\begin{exemple}
	Soit $P$ le polynôme défini par $P(X)=X^3 - 2X +1$. Montrer que $1$ est racine, puis factoriser $P$ par $(X-1)$.
\end{exemple}

\solution[7]{On constate que $P(1)=0$. Ainsi, $1$ est racine. Par théorème, on peut donc factoriser $P$ par $X-1$ : il existe $Q$ un polynôme tel que $P(X)=(X-1)Q(X)$ avec
$\deg(Q)=\deg(P)-1=2$.

Deux possibilités pour conclure : la division euclidienne, ou bien l'identification.

Pour l'identification, on cherche donc $a, b$ et $c$ tel que
\[X^3-2X+1=(X-1)(aX^2+bX+c)\]
Après développement, on obtient
\[X^3-2X+1=aX^3+(b-a)X^2+(c-b)X-c\]
Par identification, on obtient $a=1,~b=1$ et $c=-1$. Ainsi
\[X^3-2X+1=(X-1)(X^2+X-1)\]
}

\begin{proposition}
Soit $P$ un polynôme à coefficient réels, et $a_1, \hdots, a_n$ des réels deux à deux distincts (avec $n\geq 1$). Si $a_1,\hdots, a_n$ sont des racines de $P$, alors $P$ est divisible par $\ds{\prod_{k=1}^n (X-a_k)}$.
\end{proposition}

\preuve[10]{
On procède par récurrence sur $n\geq 1$.
\begin{itemize}
	\item Si $n=1$, $a_1$ est racine de $P$ et d'après le théorème précédent, $X-a_1$ divise $P$.
	\item Supposons la proposition vraie pour un entier $n\geq 1$. Soit $P\in \R[0]$ admettant $n+1$ racines distinctes $a_1,\hdots, a_{n+1}$. Puisque $a_{n+1}$ est une racine de $P$, il existe un polynôme $Q$ tel que $P=(X-a_{n+1})Q$. $Q$ est alors un polynôme qui admet $n$ racines $a_1, \hdots, a_n$ comme racines; en effet, puisque les $(a_i)$ sont distincts, pour $j\in \interent{1 n}$ \[ Q(a_j)=  \frac{P(a_j)}{a_j-a_{n+1}} = 0 . \]
	D'après l'hypothèse de récurrence appliquée à $Q$, il existe un polynôme $S$ tel que $Q=(X-a_1)\hdots(X-a_n)S$, et finalement \[ P=(X-a_1)\hdots(X-a_n)(X-a_{n+1})S \] ce qui garantit que la proposition est vraie au rang $n+1$.
\end{itemize}
}

Enfin, un théorème important : un polynôme ayant \og{}trop\fg{} de racines est nécessairement nul :

\begin{theoreme}
Soient $n\in \N$ et $P\in \R[-n]$. Si $P$ admet au moins $n+1$ racines deux à deux distinctes, alors $P=0$.

Ainsi, un polynôme non nul de degré $n$ admet \textbf{au plus} $n$ racines deux à deux distinctes.
\end{theoreme}

\preuve[6]{
Supposons par l'absurde qu'un polynôme de degré $n$ admet au moins $n+1$ racines $(a_1, \hdots, a_{n+1})$. D'après le théorème précédent, on peut écrire
\[ P = \left(\prod_{i=1}^{n+1} (X-a_{n+1})\right) Q \text{ avec }\deg(Q)\geq 0. \]
Or \[ \deg\left(\left(\prod_{i=1}^{n+1} (X-a_{n+1})\right) Q\right) = \deg\left( \prod_{i=1}^{n+1} (X-a_{n+1}) \right) + \deg(Q) \geq n+1 \]
ce qui est absurde puisque $\deg(P)\leq n$.
}

Ce théorème admet des conséquences utiles :

\begin{consequence}
	\begin{itemize}
	\item Si $P$ admet une infinité de racines, alors $P$ est le polynôme nul.
	\item Soient $P$ et $Q$ deux polynômes de degré au plus $n$ (avec $n\geq 1$). $P=Q$ si et seulement si $P$ et $Q$ coïncident en au moins $n+1$ valeurs distinctes.
	\item Soient $P$ et $Q$ deux polynômes. $P=Q$ si et seulement si $P$ et $Q$ coïncident en une infinité de valeurs distinctes.
\end{itemize}
\end{consequence}

\preuve[2]{Le premier point découle directement du théorème. Quant aux deux suivants, $P=Q$ si et seulement si $P-Q=0$ et on utilise ensuite le théorème et le premier point.
}

On en déduit un principe de factorisation si un polynôme de degré $n$ admet $n$ racines distinctes :

\begin{consequence}\label{cons_fac_rac}
	Soit $P$ un polynôme de degré $n$ (avec $n\geq 1$), de coefficient dominant $a_n$. Si $a_1, \hdots, a_n$ sont des racines de $P$ deux à deux distinctes, alors \[ P = a_n \prod_{k=1}^n (X-a_k). \]
\end{consequence}

En général, il n'y a aucune raison pour qu'un polynôme ait des racines \emph{deux à deux distinctes}. C'est l'objet de la prochaine partie.

	\subsection{Multiplicité d'une racine}

Un réel peut être racine \og{}plusieurs fois\fg{} d'un polynôme $P$. Par exemple, si $P=(X-a)^2$, $a$ est une racine qui \og{}compte\fg{} deux fois : on dira qu'elle est double.

\begin{definition}[Ordre de multiplicité]
Soit $P$ un polynôme à coefficients réels non nul. Soit $a\in \R$.

On dit que $a$ est une racine \textbf{d'ordre $k$} ($k\in \N*$) si $(X-a)^k$ divise $P$, et $(X-a)^{k+1}$ ne divise pas $P$.

$k$ est alors appelé \textbf{l'ordre} de multiplicité de la racine $a$.
\end{definition}

Si l'ordre vaut $1$, on dira que la racine est simple; s'il vaut $2$, que la racine est double, \etc.

Remarquons que l'ordre de multiplicité d'une racine existe toujours. En effet, si $a$ est une racine de $P$, l'ensemble $\{ k\in \N*,\quad (X-a)^k \mid P\}$ est une partie non vide (comme $a$ est une racine, $1$ est dedans), et majorée (par $\deg(P)$) de $\N$ : elle admet un maximum.

\begin{attention}
	S'il existe $k\in \N*$ tel que $(X-a)^k \mid P$ on ne peut pas dire que $a$ est d'ordre $k$, mais que $k$ est d'ordre \textbf{au moins} $k$.
\end{attention}

Connaissant la multiplicité d'une racine, on peut alors \textbf{factoriser} le polynôme.

\begin{proposition}
	Soit $P$ un polynôme à coefficients réels non nul. Soit $a\in \R$.

	$a$ est de multiplicité $k$ (avec $k\in \N*$) si et seulement s'il existe un polynôme $Q$ tel que $P=(X-a)^k Q$ avec $Q(a)\neq 0$. $Q$ est alors unique.
\end{proposition}

\preuve[10]{Soit $P\in \R[0]$ et $a\in \R$.

Si $a$ est de multiplicité $k$, par définition, il existe un polynôme $Q$ tel que $P=(X-a)^kQ$ avec $Q(a)\neq 0$ puisque, sinon, $X-a\mid Q$ et finalement $(X-a)^{k+1}\mid P$. $Q$ est unique par factorisation.

S'il existe $Q\in \R[0]$ tel que $P=(X-a)^kQ$ et $Q(a)\neq 0$. Alors $(X-a)^k \mid P$ : $a$ est de multiplicité au moins $k$. Par division euclidienne, il existe $S\in \R[0]$ tel que $Q=(X-a)S + Q(a)$, et donc
\[ P = (X-a)^{k+1} S + Q(a)(X-a)^k \]
Puisque $\deg((X-a)^k) < \deg((X-a)^{k+1})$, il s'agit de la division euclidienne de $P$ par $(X-a)^{k+1}$. Or $Q(a)\neq 0$ donc le reste est non nul : ainsi, $(X-a)^{k+1}$ ne divise pas $P$ et $k$ est de multiplicité $k$.
}

On peut alors généraliser la conséquence \ref{cons_fac_rac} :

\begin{proposition}
Soit $P$ un polynôme à coefficients réels admettant $p$ racines $a_1,\hdots, a_p$ (avec $p\geq 1$) deux à deux distinctes, d'ordres de multiplicité respectifs $k_1, \hdots, k_p$. Alors
\[ \sum_{i=1}^p k_i \leq \deg(P) \qeq \prod_{i=1}^p (X-a_i)^{k_i} \mid P. \]
De plus, en notant $\alpha$ le coefficient dominant de $P$ :
\[ \sum_{i=1}^p k_m=\deg(P) \iff P=\lambda \prod_{i=1}^p (X-a_i)^{k_i}. \]
\end{proposition}

Enfin, on a un lien direct entre multiplicité d'une racine et dérivées du polynôme en cette racine :

\begin{proposition}[Lien entre multiplicité et dérivées]
Soient $P$ un polynôme à coefficient réel non nul, et $a\in \R$ une racine de $P$.

Alors $a$ est de multiplicité $k$ ($k\in \N*$) si et seulement si \[ \forall i\in \interent{0 k-1},\, P^{(i)}(a)=0 \qeq P^{(k)}(a)\neq 0. \]
\end{proposition}

\preuve[15]{
Soit $n$ le degré de $P$. Si $a$ est de multiplicité $k$, alors $(X-a)^k \mid P$. Or, d'après la formule de Taylor :
\[ P = \sum_{i=0}^n P^{(i)}(a)\frac{(X-a)^i}{i!} = \underbrace{\sum_{i=0}^{k-1} P^{(i)}(a)\frac{(X-a)^i}{i!}}_{\text{degré }< k} + (X-a)^k \sum_{i=k}^n P^{(i)}(a) \frac{(X-a)^{i-k}}{i!}.\]
Par unicité de la division euclidienne, $\ds{\sum_{i=0}^{k-1} P^{(i)}(a)\frac{(X-a)^i}{i!}}$ est le reste de la division euclidienne de $P$ par $(X-a)^k$ et est donc nul puisque $(X-a)^k \mid P$. Ainsi
\[ \sum_{i=0}^{k-1} P^{(i)}(a)\frac{(X-a)^i}{i!} = 0 \implies \forall i\in \interent{0 k-1},\quad P^{(i)}(a) = 0. \]

Réciproquement, si $\forall i\in \interent{0 k-1},\, P^{(i)}(a)=0 \qeq P^{(k)}(a)\neq 0$, alors d'après la formule de Taylor
\[ P = \sum_{i=0}^n P^{(i)}(a)\frac{(X-a)^i}{i!} = \underbrace{\sum_{i=0}^{k-1} P^{(i)}(a)\frac{(X-a)^i}{i!}}_{=0} + (X-a)^k \sum_{i=k}^n P^{(i)}(a) \frac{(X-a)^{i-k}}{i!} = (X-a)^k Q(X) .\]
avec $Q(a)=P^{(k)}(a)\neq 0$ : $a$ est de multiplicité $k$.
}

\subsection{Factorisation}

Un des objectifs est de factoriser le plus possible un polynôme (pour l'étudier, ou étudier ses racines par exemple).

Dans $\R[0]$, on ne peut pas factoriser tous les polynômes de manière simple : $X^2+1$ est un polynôme de degré $2$ qui n'a pas de racine, donc ne se factorise pas par un terme de la forme $X-a$.

    \subsubsection{Cas des polynômes de degré $2$.}

	Rappelons la méthode pour déterminer les racines et la factorisation d'un polynôme de degré deux à coefficients réels.

\begin{theoreme}
  Soit $P(X)=aX^2+bX+c$ un polynôme de degré $2$ à coefficients réels ($a\neq 0$). Soit $\Delta=b^2-4ac$ son discriminant.
\begin{itemize}[label=\textbullet]
    \item Si $\Delta>0$, le polynôme $P$ possède deux racines réelles distinctes
    \[x_1=\frac{-b-\sqrt{\Delta}}{2a} \qeq x_2=\frac{-b+\sqrt{\Delta}}{2a}\]
    On a alors $P(X)=a(X-x_1)(X-x_2)$.
    \item Si $\Delta=0$, le polynôme $P$ possède une unique racine réelle dite double
    \[x_0=-\frac{b}{2a}\]
    On a alors $P(X)=a(X-x_0)^2$.
    \item Si $\Delta<0$, le polynôme $P$ ne possède pas de racines réelles, et ne se factorise donc pas dans $\R[0]$.
\end{itemize}
\end{theoreme}

\begin{exemple}
Factoriser $P(X)=2X^2+2X-4$.
\end{exemple}

\solution[5]{On a $\Delta=36>0$ donc le polynôme $P$ possède deux racines réelles
\[x_1=\frac{-2-\sqrt{36}}{2\times 2}=-2 \qeq x_2=1\]
Donc $P(X)=2(X-1)(X+2)$.}

\begin{remarque}
Attention : on n'oubliera pas, lors de la factorisation, de mettre en facteur le coefficient de plus haut degré (dans l'exemple précédent, le $2$).
\end{remarque}

	  \subsubsection{Polynômes irréductibles}

Les polynômes irréductibles jouent le rôle, dans $\R[0]$, des nombres premiers de $\Z$. Un nombre premier, par exemple $13$, n'est divisible que par $1$ ou $13$ (ou leur opposé). On donne une définition similaire pour les polynômes :

\begin{definition}[Polynômes irréductibles]
	Un polynôme $P$ à coefficients réels non constant est dit \textbf{irréductible} s'il n'est divisible que par un polynôme constant, ou un multiple de lui-même :
	\[ Q \mid P \implies  \exists \lambda\in \R,\, Q = \lambda \ou Q=\lambda P. \]
\end{definition}

\begin{exemple}
	Tout polynôme $P$, de degré $1$, est irréductible dans $\R[0]$, puisqu'un polynôme $Q$ qui divise $P$ est de degré $0$ ou $1$, c'est-à-dire constant ou de la forme $\lambda P$.

	Le polynôme $X^2+1$ est irréductible dans $\R[0]$. En effet, si $Q$ divise $X^2+1$, $Q$ est de degré $0$ (donc constant), $2$ (donc nécessairement un multiple de $X^2+1$) ou $1$. Or, si $Q$ est de degré $1$, il admet une racine réelle (si $Q=aX+b$ avec $a\neq 0$, alors $-\frac{b}{a}$ est une racine) et donc, puisqu'on peut écrire $X^2+1=Q\times R$, alors cette racine est également racine de $X^2+1$, ce qui est absurde.
\end{exemple}

On dispose d'un théorème fondamental sur $\R[0]$ :

\begin{theoreme}[Polynômes irréductibles dans {$\R[0]$}]
	Les polynômes irréductibles dans $\R[0]$ sont les polynômes de degré $1$, ou les polynômes de degré $2$ n'admettant pas de racines réelles.
\end{theoreme}

\begin{demonstration}
	Théorème admis.
\end{demonstration}

On a alors le résultat de factorisation suivant :

\begin{theoreme}[Factorisation dans {$\R[0]$}]
	Tout polynôme $P$ non constant de $\R[0]$ peut s'écrire sous la forme
	\[ P = \lambda \prod_{i=1}^p (X-a_i)^{r_i} \prod_{j=1}^q (X^2+b_jX+c_j)^{s_j} \]
	où $\lambda$ est le coefficient dominant de $P$, et les polynômes $X^2+b_j+c_j$ sont irréductibles (c'est-à-dire $\Delta_j=b_j^2-4c_j < 0$).
\end{theoreme}

\begin{demonstration}
	Théorème admis.
\end{demonstration}

On termine par du vocabulaire :

\begin{vocabulaire}
Lorsqu'un polynôme s'écrit comme un produit de polynômes uniquement de degré $1$ (donc sans polynômes de degré $2$), $\ds{P=\lambda\prod_{i=1}^p (X-a_i)^{r_i}}$, on dit que le polynôme est \textbf{scindé}.

Si, de plus, pour tout $i\in \interent{1 p}$, $r_i=1$, on dit que le polynôme est \textbf{scindé à racines simples}.
\end{vocabulaire}

    \subsubsection{Cas général}

\begin{methode}
\label{objectif-03-5}
  Lorsqu'on doit factoriser un polynôme $P$ (ou déterminer ses racines) :
\begin{itemize}[label=\textbullet]
    \item On cherche des racines évidentes ($-2,-1,0,1,2$) pour se ramener à un (ou  des) polynôme(s) de degré $2$.
    \item On factorise le polynôme $P$ par $X-a$ où $a$ désigne une racine évidente.
    \item Une fois ramené à un (ou des) polynômes de degré $2$, on utilise la méthode classique du discriminant.
\end{itemize}
\end{methode}

\begin{exemple}
	Soit $P(X)=X^3-2X^2-X+2$. Déterminer les racines de $P$.
\end{exemple}

\solution[20]{
\begin{itemize}[label=\textbullet]
    \item Premier étape : on cherche une ``racine évidente'' : $-2, -1, 0, 1, 2$. Ici, on constate que $P(1)=1-2-1+2=0$ donc $1$ est racine évidente.
    \item Deuxième étape : on factorise $P$ par $X-1$ : on écrit $P(X)=(X-1)Q(X)$ avec $\deg(Q)=\deg(P)-1=2$. Donc $Q$ peut s'écrire $aX^2+bX+c$.\\
    On a alors
    \[P(X)=(X-1)(aX^2+bX+c)=aX^3+(b-a)X^2+(c-b)X-c\]
    Par unicité de l'écriture d'un polynôme, on identifie les coefficients :
    \[\left\{\begin{array}{rcl}
 a &=& 1 \\
 b-a &=& -2 \\
 c-b &=& -1 \\
 -c &=& 2
\end{array}\right. \Longleftrightarrow
\left\{\begin{array}{rcl}
 a &=& 1 \\
 b &=& -1 \\
 c &=& -2
\end{array}\right.\]
Donc $P(X)=(X-1)(X^2-X-2)$.
\item Troisième étape : on détermine les racines du polynôme $Q$. Ici, $Q$ est du second degré, dont le discriminant vaut $\Delta=9$. Donc $Q$ possède deux racines :
		\[x_1=\frac{1-\sqrt{9}}{2}=-1 \qeq x_2=2\]
Donc $Q(X)=(X-2)(X+1)$
    \item Quatrième étape : on conclut. On a donc
    \[P(X)=(X-1)(X-2)(X+1)\]
    et $P$ possède trois racines réelles : $1, 2$ et $-1$.
\end{itemize}
}

\afaire{Exercices \lienexo{20} et \lienexo{22}.}

	\subsection{\'Equations se ramenant à un polynôme}

Il est possible de résoudre des équations particulières en se ramenant à une équation polynomiale.

\begin{exemple}
Résoudre l'équation $\ln(x)^2-3\ln(x)+2=0$.
\end{exemple}

\begin{methode}
\label{objectif-03-6}
Pour résoudre une équation liée à un polynôme, on effectue un changement de variable : on posera, ici,  $u=\ln(x)$ et on se ramènera à un polynôme.
\end{methode}

\solution[10]{L'équation n'a de sens que sur $\interoo{0 +\infty}$. On pose alors $u=\ln(x)$. L'équation s'écrit alors \[ u^2-3u+2=0 \]
Le discriminant de ce polynôme vaut $\Delta=(-3)^2-4\times 2 = 1$. Les racines sont alors \[ u_1=1 \qeq u_2=2 \]
On revient alors à la variable de départ :
\begin{align*}
 u_1=1 &\Leftrightarrow \ln(x_1)=1 \\
 &\Leftrightarrow x_1=\eu{1}=\E \\
 \text{et }u_2=2 &\Leftrightarrow \ln(x_2)=2 \\
 &\Leftrightarrow x_2=\eu{2}
\end{align*}
Les solutions $x_1$ et $x_2$ sont bien dans $\interoo{0 +\infty}$. On peut alors conclure que l'ensemble des solutions de l'équation $\ln(x)^2-3\ln(x)+2=0$ est $\mathcal{S} = \left \{ \E, \eu{2} \right \}$.
}

\begin{exo}
Résoudre l'équation $\eu{2x}-2\eu{x}-3=0$.
\end{exo}

\solution[10]{L'équation est valable sur $\R$. On pose $u=\eu{x}$. En constatant que $\eu{2x}=\left(\eu{x}\right)^2$, l'équation devient alors $u^2-2u-3=0$.

Les racines sont $u_1=-1$ et $u_2=3$. On revient alors à la variable de départ. $u_1=-1$ devient $\eu{x_1}=-1$ ce qui est impossible. La deuxième devient $\eu{x_2}=3$ soit $x_2=\ln(3)$.

Ainsi, l'équation $\eu{2x}-2\eu{x}-3=0$ admet une unique solution : $\mathcal{S}=\left \{ \ln(3)\right \}$.
}

\afaire{Exercices \lienexo{21} et \lienexo{23}.}
