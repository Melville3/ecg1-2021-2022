\chapter{Intégrales généralisées}
%!TeX root=../../encours.nouveau.tex

\objectifintro{Dans ce chapitre, on généralise la notion d'intégrale, vue sur un segment, au cas d'un intervalle quelconque. On verra des méthodes pour s'assurer que ces intégrales existent, et pour les calculer.}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Eugène Ionesco (1909--1994). \emph{La leçon}}
Il ne faut pas uniquement intégrer. Il faut aussi désintégrer. C'est ça la vie. C'est ça la philosophie. C'est ça la science. C'est ça le progrès, la civilisation.
\end{extrait}

\begin{objectifs}
	\begin{numerote}
		\item Connaître la définition d'intégrale sur un intervalle :
				\begin{itemize}
					\item \lienobj{01}{connaître la définition}
					\item \lienobj{02}{connaître les différentes propriétés usuelles}
				\end{itemize}
		\item Concernant les théorèmes d'existence :
				\begin{itemize}
					\item \lienobj{03}{connaître le théorème de majoration des fonctions positives}
					\item \lienobj{04}{savoir utiliser les équivalents de fonctions de signe constant}
					\item \lienobj{05}{connaître les intégrales de référence (Riemann, exponentielle)}
					\item \lienobj{06}{savoir appliquer un changement de variable à une intégrale généralisée}
				\end{itemize}
		\item Connaître la notion de convergence absolue :
				\begin{itemize}
					\item \lienobj{07}{connaître la définition}
					\item \lienobj{08}{l'inégalité triangulaire, et le cas d'une fonction continue dont l'intégrale converge absolument vers $0$}
				\end{itemize}
	\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

\section{Intégrale sur un intervalle quelconque}

L'idée est d'étendre la notion d'intégrale, mais sur un intervalle infini, du type $\interoo{a +\infty}$, $\interoo{-\infty{} a}$ voire $\interoo{-\infty{} +\infty}$, ou sur un intervalle du type $\interof{a b}$ où la fonction n'est pas définie en $a$. 

    \subsection{Cas de \interfo{a b} et \interof{a b}}

\begin{definition}[Intégrale convergente sur \interfo{a b}]
	Soit $f$ une fonction continue, définie sur $\interfo{a b}$, où $a\in \R$ et $b\in \R\cup\{+\infty\}$. On dit que l'intégrale $\ds{\int_a^b f(t)\dd t}$ est \textbf{convergente} si et seulement si $\ds{\lim_{x\rightarrow b^-} \int_a^x f(t)\dd t}$ existe et est finie. Dans ce cas, on note \[ \int_{\interfo{a b}} f(t)\dd t=\int_a^b f(t)\dd t = \lim_{x\rightarrow b^-} \int_a^x f(t)\dd t \]
	Les intégrales $\ds{\int_a^x f(t)\dd t}$ pour $x\in \interfo{a b}$ sont appelées \textbf{intégrales partielles}.
\end{definition}

\begin{remarque}
	On définit de même le cas où $f$ est définie sur $\interof{a b}$ (et dans ce cas, on s'intéresse à la limite en $a^+$).
\end{remarque}

\begin{remarque}
	On dit que l'intégrale $\ds{\int_a^{b} f(t)\dd t}$ est une intégrale \textbf{impropre} (puisque, rigoureusement, si $f$ est continue, l'intégrale n'est définie que sur un segment). On parle également d'\textbf{intégrale généralisée}.

	Déterminer la nature de l'intégrale, c'est savoir si elle est convergente ou divergente.
\end{remarque}


\begin{exemple}
	Soit $f:t\mapsto \eu{-t}$ sur $[0;+\infty[$. Montrer que l'intégrale $\ds{\int_0^{+\infty} \eu{-t}\dd t}$ converge et déterminer sa valeur.
\end{exemple}

\solution[5]{La fonction $t\donne \eu{-t}$ est continue sur $\R+$. Soit $x>0$. Alors
\[\int_0^x f(t)\dd t = [-\eu{-t}]_0^x = 1-\eu{-x}\]
et $\ds{\lim_{x\rightarrow +\infty} 1-\eu{-x} = 1}$. Donc $\ds{\int_0^{+\infty} f(t)\dd t}$ existe, et \[\int_0^{+\infty} \eu{-t}\dd t = 1\]
}

\begin{exo}\label{exo:nature_int_t2}
	Déterminer la nature des intégrales $\ds{\int_0^1 \frac{1}{t}\dd t}$ et $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$.
\end{exo}

\solution[8]{La fonction $t\donne \frac1t$ est continue sur $\interof{0 1}$. Pour tout $x\in \interof{0 1}$ :
\begin{align*}
	\int_x^1 \frac1t \D t &= \left[ \ln(|t|)\right]_x^1 = -\ln(x) \tendversen{x\to 0} +\infty.
\end{align*}
Ainsi, l'intégrale $\ds{\int_0^1 \frac{1}{t}\D t}$ diverge.

De même, $t\donne \frac{1}{t^2}$ est continue sur $\interfo{1 +\infty}$. Pour tout $x\in \interfo{1 +\infty}$,
\begin{align*}
  \int_1^x \frac{1}{t^2}\D t &= \left[ -\frac{1}{t} \right]_1^x = 1-\frac{1}{x}\tendversen{x\to +\infty} 1.
\end{align*}
Ainsi, l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\D t}$ converge et \[ \int_1^{+\infty} \frac{1}{t^2}\D t = 1. \]
}

\begin{exo}
	Déterminer la nature de l'intégrale $\ds{\int_0^1 \frac{1}{\sqrt{1-t}}\D t}$.
\end{exo}

\solution[5]{La fonction $t\donne  \frac{1}{\sqrt{1-t}}$ est continue sur $\interfo{0 1}$. L'intégrale est donc impropre en $1$. Pour tout $x\in \interfo{0 1}$,
\begin{align*}
	 \int_0^x \frac{1}{\sqrt{1-t}}\D t &= \left[ -2\sqrt{1-t} \right]_0^x=2-2\sqrt{1-x}\tendversen{x\to 1} 2.
\end{align*}
Ainsi, l'intégrale $\ds{\int_0^1 \frac{1}{\sqrt{1-t}}\D t}$ converge et \[ \int_0^1 \frac{1}{\sqrt{1-t}}\D t = 2. \]
}

Si la fonction $f$ est prolongeable par continuité en $b$, alors l'intégrale est convergente :

\begin{proposition}[Intégrale faussement impropre]
Soit $f$ une fonction continue sur $\interfo{a b}$. Si $f$ admet une limite finie en $b^-$, alors l'intégrale $\ds{\int_a^b f(t)\D t}$ est convergente, et est égale à l'intégrale de $\tilde{f}$, prolongement par continuité de $f$ sur $\interff{a b}$.

On dit alors que l'intégrale est \textbf{faussement impropre}.
\end{proposition}

\begin{exemple}
	Montrer que l'intégrale $\ds{\int_0^1 \frac{\eu{t}-1}{t}\D t}$ est convergente.
\end{exemple}

\solution[5]{La fonction $f:t\donne \frac{\eu{t}-1}{t}$ est continue sur $\interof{0 1}$. En $0$, on constate que
\[ \frac{\eu{t}-1}{t} \tendversen{t\to 0} 1 \text{ (taux d'accroissement)}. \]
Ainsi, $f$ est prolongeable par continuité en $0$ : l'intégrale $\ds{\int_0^1 \frac{\eu{t}-1}{t}\D t}$ est convergente.
}

\begin{attention}
 Contrairement à ce qu'il se passe dans le cadre des séries, l'intégrale $\ds{\int_a^{+\infty} f(t)\D t}$ peut être convergente sans que la fonction $f$ tende vers $0$ en $+\infty$.

 Réciproquement, si $f(t)\tendversen{t\to +\infty} 0$, cela ne garantit pas que $\ds{\int_a^{+\infty} f(t) \D t}$ converge.
\end{attention}

	\subsection{Définition dans le cas \interoo{a b}}

Dans le cas où les deux bornes sont impropres, on va s'intéresser à deux limites :

\begin{definition}
	Soient $a\in \R\cup\{-\infty\}$ et $b\in \R\cup \{+\infty\}$ avec $a<b$.

	Soit $f$ une fonction continue sur \interoo{a b}. On dit que l'intégrale généralisée $\ds{\int_a^b f(t)\D t}$ converge si et seulement s'il existe $c_0 \in \interoo{a b}$ tel que les intégrales $\ds{\int_a^{c_0} f(t)\D t}$ et $\ds{\int_{c_0}^b f(t)\D t}$ sont convergentes. On note alors
	\[ \int_a^b f(t)\D t = \int_a^{c_0} f(t)\D t + \int_{c_0}^b f(t)\D t = \lim_{x\to a^+} \int_x^{c_0} f(t)\D t + \lim_{x\to b^-} \int_{c_0}^x f(t)\D t.\]

	Dans ce cas, pour tout $c\in \interoo{a b}$, les intégrales $\ds{\int_a^{c} f(t)\D t}$ et $\ds{\int_{c}^b f(t)\D t}$ sont convergentes.

	Dans le cas contraire, on dit que l'intégrale $\ds{\int_a^b f(t)\D t}$ diverge.
\end{definition}

\preuve[5]{S'il existe $c_0\in \interoo{a b}$ tel que les intégrales soient convergentes, alors pour tout $(c,x)\in \interoo{a b}^2$, par la relation de Chasles :
\begin{align*}
	 \int_x^c f(t)\D t &= \int_x^{c_0}f(t)\D t + \int_{c_0}^c f(t) \D t \tendversen{x\to a^+} \int_a^{c_0} f(t)\D t + \int_{c_0}^c f(t)\D t \\
	 \int_c^x f(t)\D t &= \int_c^{c_0}f(t)\D t+ \int_{c_0}^x f(t)\D t\tendversen{x\to b^-} \int_c^{c_0}f(t)\D t + \int_{c_0}^b f(t)\D t
\end{align*}
Les deux intégrales sont bien convergentes.
}

\begin{exemple}
	Déterminer la nature de $\ds{\int_{-\infty}^{+\infty} \frac{1}{1+t^2}\D t}$.
\end{exemple}

\solution[8]{La fonction $t\donne \frac{1}{1+t^2}$ est continue sur $\R$. L'intégrale est impropre en $-\infty$ et $+\infty$. Or, pour $x\in \R$ :
\begin{align*}
  \int_x^0 \frac{1}{1+t^2}\D t &= \left[\arctan(t)\right]_x^0 = -\arctan(x)\tendversen{x\to -\infty} \frac{\pi}{2},\\
	\int_0^x \frac{1}{1+t^2}\D t &= \left[\arctan(t)\right]_0^x = \arctan(x)\tendversen{x\to +\infty} \frac{\pi}{2}.
\end{align*}
Ainsi, l'intégrale $\ds{\int_{-\infty}^{+\infty} \frac{1}{1+t^2}\D t}$ converge, et
\[ \int_{-\infty}^{+\infty} \frac{1}{1+t^2}\D t = \pi. \]
}

\begin{exo}
 Déterminer la nature de $\ds{\int_0^{+\infty} \frac{1}{t^2}\D t}$.
\end{exo}

\solution[7]{La fonction $t\donne \frac{1}{t^2}$ est continue sur $\R>$. L'intégrale est impropre en $0$ et en $+\infty$.

On a vu dans l'exercice \ref{exo:nature_int_t2} que l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\D t}$ converge.

Soit $x\in \interof{0 1}$.
\begin{align*}
 \int_x^1 \frac{1}{t^2}\D t &= \left[ -\frac{1}{t}\right]_x^1 = \frac{1}{x}-1\tendversen{x\to 0^+} +\infty.
\end{align*}
L'intégrale $\ds{\int_0^1 \frac{1}{t^2}\D t}$ diverge, et par conséquent, $\ds{\int_0^{+\infty} \frac{1}{t^2}\D t}$ diverge également.
}

\begin{attention}
Pour déterminer la convergence d'une intégrale du type $\ds{\int_{-\infty}^{+\infty} f(t)\D t}$, on doit bien calculer deux limites. On ne peut pas calculer
\[ \lim_{x\to +\infty} \int_{-x}^x f(t)\D t\]
pour conclure : ce n'est pas suffisant.

Par exemple, par imparité, pour tout $x\in \R+$, $\ds{\int_{-x}^x t^3\D t=0}$, donc converge vers $0$, et pourtant $\ds{\int_{-\infty}^{+\infty} t^3\D t}$ diverge.
\end{attention}

	\subsection{Extensions}

On garde la même convention dans le cas où les bornes ne sont pas dans le bon sens :
\begin{definition}
Soit $a, b$ deux éléments de $\R \cup \{-\infty, +\infty\}$ avec $a>b$.

Soit $f$ une fonction continue sur $\interfo{b a}$ (ou bien \interof{b a}, ou bien \interoo{a b}). On dit que $\ds{\int_a^b f(t)\D t}$ converge si et seulement si $\ds{\int_b^a f(t)\D t}$ converge. Dans ce cas
\[ \int_a^b f(t)\D t = -\int_b^a f(t)\D t.\]
\end{definition}

Enfin, il arrive qu'on s'intéresse à l'intégrale d'une fonction définie sur un intervalle sauf un nombre fini de points. Dans ce cas, on découpera en des intervalles où on peut se ramener aux cas précédents :

\begin{definition}
Soient $a=a_0 < a_1< \hdots < a_n=b$ des éléments tels que $a\in \R\cup\{-\infty\}$, $(a_1,\hdots, a_{n-1})\in \R^{n-1}$ et $b\in \R\cup\{+\infty\}$.

Soit $f$ une fonction définie et continue sur \interoo{a_i a_{i+1}} pour tout $i\in \interent{0 n-1}$.

On dit que l'intégrale $\ds{\int_a^b f(t)\D t}$ converge si et seulement si les intégrales généralisées $\ds{\int_{a_i}^{a_{i+1}} f(t)\D t}$ convergent pour tout $i\in \interent{0 n-1}$. Dans ce cas, on note
\[ \int_a^b f(t)\D t = \sum_{i=0}^{n-1} \int_{a_i}^{a_{i+1}} f(t)\D t. \]
\end{definition}

\begin{exemple}
	Déterminer la nature de l'intégrale $\ds{\int_0^{+\infty} \frac{1}{t-1}\D t}$.
\end{exemple}

\solution[8]{La fonction $t\donne \frac{1}{t-1}$ est définie et continue sur $\interfo{0 +\infty} \setminus \{1\}$. Il nous faut donc déterminer la nature des deux intégrales $\ds{\int_0^1 \frac{1}{t-1}\D t}$ et $\ds{\int_1^{+\infty} \frac{1}{t-1}\D t}$. Or, pour tout $u\in \interfo{0 1}$ :
\begin{align*}
  \int_0^u \frac{1}{t-1}\D t &= \left [ \ln |t-1|\right]_0^u = \ln(1-u) \tendversen{u\to 1^-} -\infty.
\end{align*}
Ainsi, l'intégrale $\ds{\int_0^1 \frac{1}{t-1}\D t}$ diverge, de même que $\ds{\int_0^{+\infty} \frac{1}{t-1}\D t}$.
}


\section{Propriétés}

Nous allons utiliser les propriétés des intégrales sur un segment pour en déduire des propriétés sur les intégrales généralisées. Dans l'ensemble de cette partie, on énonce les propriétés pour les intégrales sur \interfo{a b}, mais elles sont également valables pour \interof{a b} et \interoo{a b}.

Dans l'ensemble de cette partie, on se fixe $a\in \R$, et $b \in \R \cup \{+\infty\}$ tels que $a<b$.


		\subsection{Linéarité}

\begin{propriete}[Linéarité]
	Soient $f$ et $g$ deux fonctions continues sur $\interfo{a b}$, et $\lambda \in \R$.
\begin{itemize}
    \item Si $\ds{\int_a^{b} f(t)\dd t}$ converge, alors $\ds{\int_a^{b} \lambda f(t)\dd t}$ converge, et
    \[\int_a^{b} \lambda f(t)\dd t=\lambda\int_a^{b} f(t)\dd t\]
    \item Si $\ds{\int_a^{b} f(t)\dd t}$ et $\ds{\int_a^{b} g(t)\dd t}$ convergent, alors $\ds{\int_a^{b} (f(t)+g(t))\dd t}$ converge également, et
    \[\int_a^{b} (f(t)+g(t))\dd t=\int_a^{b} f(t)\dd t+\int_a^{b} g(t)\dd t\]
\end{itemize}
\end{propriete}

\preuve[10]{Soit $x\in \interfo{a b}$. Alors, par linéarité de l'intégrale sur un segment, et convergence :
\begin{align*}
 \int_a^x \lambda f(t)\D t &= \lambda \int_a^x f(t)\D t \tendversen{x\to b^-} \lambda\int_a^b f(t) \D t.
\end{align*}
Ainsi, $\ds{\int_a^b \lambda f(t)\D t}$ converge et $\ds{\int_a^b \lambda f(t)\D t = \lambda \int_a^b f(t)\D t}$.

De même,
\begin{align*}
 \int_a^x (f(t)+g(t))\D t &=  \int_a^x f(t)\D t +\int_a^x g(t)\D t\tendversen{x\to b^-} \int_a^b f(t) \D t + \int_a^b g(t)\D t.
\end{align*}
Ainsi, $\ds{\int_a^b (f(t)+g(t))\D t}$ converge et $\ds{\int_a^b (f(t)+g(t))\D t=\int_a^b f(t)\D t + \int_a^b g(t)\D t}$.
}

\begin{consequence}\vspace*{.1cm}
	L'ensemble des fonctions $f$, continues sur \interfo{a b}, dont l'intégrale généralisée $\ds{\int_a^b f(t)\D t}$ converge est un $\R$-espace vectoriel.
\end{consequence}

On dispose d'un résultat qui est une conséquence directe :
\begin{proposition}
Soient $f$ et $g$ deux fonctions continues sur \interfo{a b}. On considère les trois intégrales généralisées
\[ \int_a^b f(t)\D t,\quad \int_a^b g(t)\D t\qeq \int_a^b (f(t)+g(t))\D t. \]
Si deux de ces intégrales sont convergentes, alors la troisième l'est.
\end{proposition}


\begin{attention}
	L'intégrale $\ds{\int_a^{b} (f(t)+g(t))\dd t}$ peut exister, sans pour autant que $\ds{\int_a^{b} f(t)\dd t}$ et $\ds{\int_a^{b} g(t)\dd t}$ \\n'existent.\\Par exemple, $\ds{\int_1^{+\infty} \frac{1}{t}\dd t}$  et $\ds{\int_1^{+\infty} \frac{1}{t^2}-\frac{1}{t}\dd t}$ ne convergent pas, et pourtant la somme $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ converge.
\end{attention}

		\subsection{Relation de Chasles}

\begin{propriete}[Relation de Chasles]
	Soit $c\in \interfo{a b}$.

 Soit $f$ une fonction continue sur $\interfo{a b}$. Alors $\ds{\int_a^b f(t)\dd t}$ converge si et seulement si $\ds{\int_c^b f(t)\dd t}$ converge. Dans ce cas
\[\int_a^b f(t)\dd t=\int_a^c f(t)\dd t+\int_c^b f(t)\dd t\]
\end{propriete}

\preuve[8]{Soit $u \in \interoo{c b}$. D'après la relation de Chasles :
\begin{align*}
 \int_a^u f(t) \D t &= \int_a^c f(t)\D t + \int_c^u f(t)\D t.
\end{align*}
Ainsi, $\ds{\int_a^u f(t)\D t}$ a une limite quand $u$ tend vers $b$ si et seulement si $\ds{\int_c^u f(t)\D t}$ en a une (puisque $\ds{\int_a^c f(t)\D t}$ est un réel). Dans ce cas,
\[ \underbrace{\int_a^b f(t)\D t}_{=\lim\limits_{u\to b^-} \int\limits_a^u f(t)\D t } = \int_a^c f(t)\D t + \underbrace{\int_c^b f(t)\D t}_{=\lim\limits_{u\to b^-}\int\limits_c^u f(t)\D t}. \]
}

		\subsection{Positivité et croissance de l'intégrale}

\begin{proposition}[Positivité et croissance]\vspace*{.1cm}
Soient $f$ et $g$ deux fonctions continues sur l'intervalle $\interfo{a b}$. On suppose que $\ds{\int_a^b f(t)\dd t}$ et $\ds{\int_a^b g(t)\dd t}$ convergent.
\begin{itemize}
	\item \petittitre{Positivité} : si $f$ est positive sur $\interfo{a b}$ (avec $a\leq b$),  alors $\ds{\int_a^b f(t)\dd t\geq 0}$.
	\item \petittitre{Croissance} : si, pour tout $t\in \interfo{a b}$, $f(t)\leq g(t)$ (avec $a\leq b$) alors \[ \int_a^b f(t)\dd t \leq \int_a^b g(t)\dd t \]
\end{itemize}
\end{proposition}

\preuve[10]{Puisque $f$ est continue sur \interfo{a b}, elle y admet des primitives. Soit $F$ une primitive de $f$ sur \interfo{a b}. Puisque $F'=f\geq 0$, la fonction $F$ est croissante. Ainsi, pour tout $x\in \interfo{a b}$, $F(x)\geq F(a) $, c'est-à-dire $F(x)-F(a)\geq 0$. Puisque l'intégrale $\ds{\int_a^b f(t)\D t}$ converge, par passage à la limite, on en déduit que \[\int_a^b f(t)\D t = \lim_{x\to b^-} F(x)-F(a) \geq 0.\]
Enfin, si $f\leq g$, la fonction $g-f$ est positive. D'après le cas précédent, et par linéarité et convergence :
\begin{align*}
 \int_a^b (g(t)-f(t))\D t\geq 0 &\Longleftrightarrow \int_a^b g(t)\D t \geq \int_a^b f(t)\D t.
\end{align*}
}


\begin{attention}
	Comme dans le cas de l'intégrale sur un segment, il faut absolument que $a\leq b$. Si ce n'est pas le cas, les inégalités sont inversées.
\end{attention}

\begin{proposition}[Fonction positive d'intégrale nulle]\vspace*{.1cm}
Soit $f$ une fonction continue et \textbf{positive }sur $\interfo{a b}$. Si l'intégrale $\ds{\int_a^b f(t)\D t}$ est convergente et est nulle, alors la fonction $f$ est nulle sur $\interfo{a b}$.
\end{proposition}

\preuve[10]{Puisque $f$ est continue sur \interfo{a b}, elle y admet des primitives. Soit $F$ une primitive de $f$ sur \interfo{a b}. Puisque $F'=f\geq 0$, la fonction $F$ est croissante. Ainsi, pour tout $x\in \interfo{a b}$, $F(x)-F(a) \geq 0$.

Puisque l'intégrale est convergente, $F$ admet une limite en $b^-$ et \[ \int_a^b f(t)\D t = \lim_{x\to b^-} F(x)-F(a) = 0. \]
Cela implique que $\lim_{x\to b^-} F(x)=F(a)$. Or, $F$ étant croissante, pour tout $x$ et $y$ dans $\interfo{a b}$ tels que $x\leq y$ :
\[ F(a) \leq F(x) \leq F(y) \tendversen{y\to b^-} F(a). \]
Ainsi, $F$ est constante égale à $F(a)$ sur \interfo{a b}, et ainsi, $f=F'$ est nulle sur $\interfo{a b}$.
}

	\subsection{Parité}

Lorsque la fonction est paire ou impaire, on peut se simplifier la recherche de convergence.

\begin{proposition}[Cas d'une fonction paire ou impaire]
Soit $f$ une fonction continue sur $\R$. On suppose que $f$ est paire ou impaire. Alors l'intégrale $\ds{\int_{-\infty}^{+\infty} f(t)\D t}$ converge si et seulement si $\ds{\int_0^{+\infty} f(t)\D t}$ converge. Dans ce cas,
\begin{itemize}
	\item si $f$ est impaire, alors $\ds{\int_{-\infty}^{+\infty} f(t)\D t=0}$;
	\item si $f$ est paire, alors \[ \int_{-\infty}^{+\infty} f(t)\D t = 2 \int_0^{+\infty} f(t)\D t = 2\int_{-\infty}^0 f(t)\D t.\]
\end{itemize}
\end{proposition}


	\subsection{Fonction définie par une intégrale généralisée}

Traitons, enfin, le cas d'une fonction définie par une intégrale.

\begin{proposition}
	Soient $a$ et $b$ deux éléments, tels que $-\infty \leq x <b \leq +\infty$.
Soit $f$ une fonction continue sur $\interoo{a b}$, telle que $\ds{\int_a^b f(t)\D t}$ converge. Alors
\begin{itemize}
	\item la fonction $\ds{x\donne \int_a^x f(t)\D t}$ est dérivable sur $\interoo{a b}$, de dérivée $f$;
	\item la fonction $\ds{x\donne \int_x^b f(t)\D t}$ est dérivable sur $\interoo{a b}$, de dérivée $-f$;
\end{itemize}
\end{proposition}

\preuve[7]{Puisque l'intégrale $\ds{\int_a^b f(t)\D t}$ converge, pour tout $x\in \interoo{a b}$, on peut écrire \[ \int_a^x f(t)\D t = F(x)-\lim_{t\to a^+} F(t) \qeq \int_x^b f(t)\D t = \lim_{t\to b^-} F(t) - F(x)\quad (\star)\]
où $F$ est une primitive de $f$ sur \interoo{a b}. Les deux limites existent puisque l'intégrale est convergente. Par exemple, pour $y\in \interoo{a b}$ :
\[ F(x)-F(y)= \int_y^x f(t)\D t \tendversen{y\to a^+} \int_a^x f(t)\D t \] et donc
 $\ds{F(y) \tendversen{y\to a^+} F(x)-\int_a^x f(t)\D t}$.

Il suffit alors de dériver les propriétés $(\star)$ pour conclure.
}

\section{Intégrales de référence}

			\subsection{Intégrales de Riemann}

\begin{theoreme}[Intégrale de Riemann]
La fonction $f:t\mapsto \frac{1}{t^\alpha}$ est intégrable sur $\interfo{1 +\infty}$ si et seulement si $\alpha >1$. Dans ce cas
\[\int_1^{+\infty} \frac{1}{t^\alpha}\dd t = \frac{1}{\alpha -1}\]
\end{theoreme}


\preuve[12]{Dans le cas où $\alpha \neq 1$, on a
\[\int_1^x \frac{1}{t^\alpha}\dd t = \left[ \frac{1}{1-\alpha}\frac{1}{t^{\alpha-1}}\right]_1^x = \frac{1}{(1-\alpha)x^{\alpha-1}}-\frac{1}{1-\alpha}\]
\begin{itemize}[label=\textbullet]
	\item Si $\alpha>1$ : quand $x$ tend vers $+\infty$, cette intégrale converge vers $\ds{\frac{1}{\alpha-1}}$
	\item Si $\alpha < 1$ : \[\lim_{x\rightarrow +\infty} \frac{1}{x^{\alpha - 1 }} = +\infty\]
	donc l'intégrale diverge.
	\item Si $\alpha=1$, on a \[\int_1^x \frac{1}{t}\dd t=\ln(x) \textrm{ et } \lim_{x\rightarrow +\infty} \ln(x)=+\infty\] L'intégrale diverge donc.
\end{itemize}
}

\begin{remarque}
	On reste dans le cas de l'intégrale de Riemann si la borne n'est pas $1$ mais un réel $c\in \R>$. Par exemple, $\ds{\int_3^{+\infty} \frac{1}{t^2}\D t}$ est une intégrale de Riemann convergente.

	En revanche, ce n'est pas valable si $c=0$, comme on va le voir.
\end{remarque}

Par parité :

\begin{theoreme}[Intégrale de Riemann - $\R<$]
	La fonction $f:t\donne \frac{1}{|t|^\alpha}$ est intégrable sur \interof{-\infty -1} si et seulement si $\alpha>1$.
\end{theoreme}

En $0$, on dispose d'un résultat très différent :

\begin{theoreme}[Intégrale de Riemann - 2]
La fonction $f:t\mapsto \frac{1}{t^\alpha}$ est intégrable sur $\interof{0 1}$ si et seulement si $\alpha<1$. Dans ce cas,
\[ \int_0^1 \frac{1}{t^\alpha}\dd t = \frac{1}{1-\alpha} \]
\end{theoreme}

\preuve[10]{Pour $\alpha \neq 1$, on a, pour tout $u\in\interoo{0 1}$,
\[ \int_u^1 \frac{1}{t^\alpha}\dd t = \left[ \frac{1}{1-\alpha} \frac{1}{t^{\alpha-1}} \right]_u^1 = \frac{1}{1-\alpha}- \frac{1}{1-\alpha} \frac{1}{u^{\alpha-1}}\]
Si $\alpha>1$, $\ds{\lim_{u\rightarrow 0} \frac{1}{u^{\alpha-1}} = +\infty}$. L'intégrale diverge donc. Si $\alpha<1$, $\ds{\lim_{u\rightarrow 0} \frac{1}{u^{\alpha-1}} = 0}$ donc l'intégrale converge et \[\int_0^1 \frac{1}{t^\alpha}\dd t=\frac{1}{1-\alpha}\]
Si $\alpha=1$, pour $u\in \interoo{0 1}$, \[ \int_u^1 \frac{1}{t}\dd t=\ln(1)-\ln(u)=-\ln(u) \tendversen{u\to 0} +\infty \]
Donc l'intégrale diverge également.
}

\begin{remarque}
	On reste dans le cas de l'intégrale de Riemann si la borne n'est pas $1$ mais un réel $c\in \R>$. Par exemple, $\ds{\int_0^{2} \frac{1}{\sqrt{t}}\D t}$ est une intégrale de Riemann convergente.
\end{remarque}

On dispose enfin d'intégrales de Riemann, qui sont une variante des précédentes :

\begin{theoreme}[Intégrale de Riemann - 3]
Soit $\alpha \in \R$ et $(a,b)\in \R^2$ tels que $a< b$.
\begin{itemize}
	\item $\ds{\int_a^b \frac{\dd t}{(t-a)^\alpha}}$ converge si et seulement si $\alpha <1$.
	\item $\ds{\int_a^b \frac{\dd t}{(b-t)^\alpha}}$ converge si et seulement si $\alpha <1$.
\end{itemize}
\end{theoreme}

	\subsection{Exponentielle et logarithme}

\begin{theoreme}
La fonction $f:t\mapsto \eu{-\alpha t}$ est intégrable sur \interfo{0 +\infty} si et seulement si $\alpha >0$. Dans ce cas,
\[\int_0^{+\infty} \eu{-\alpha t}\dd t =\frac{1}{\alpha}\]
\end{theoreme}

\preuve[9]{Si $\alpha=0$, $\eu{-\alpha t} = 1$ et la fonction constante égale à $1$ n'est pas intégrable sur $[0;+\infty[$. Sinon,
\[\int_0^x \eu{-\alpha t}\dd t = \left[ \frac{\eu{-\alpha t}}{-\alpha} \right]_0^x = -\frac{\eu{-\alpha x}}{\alpha} +\frac{1}{\alpha}\]
Cette intégrale converge si et seulement si $\alpha > 0$, et dans ce cas
\[\lim_{x \rightarrow +\infty} \int_0^x \eu{-\alpha t}\dd t = \frac{1}{\alpha}\]}

Plus généralement :

\begin{theoreme}
	Soit $u\in \R+$.	La fonction $f:t\donne \eu{-\alpha t}$ est intégrable sur $\interfo{u +\infty}$ si et seulement si $\alpha>0$. Dans ce cas, \[ \int_u^{+\infty} \eu{-\alpha t} = \frac{\eu{-\alpha u}}{\alpha}. \]
\end{theoreme}

Enfin, on dispose d'un dernier résultat concernant la fonction $\ln$.

\begin{theoreme}
La fonction $f:t\mapsto \ln(t)$ est intégrable sur $\interof{0 1}$, et on a
\[ \int_0^1 \ln(t)\dd t=-1 \]
\end{theoreme}

\preuve[7]{
Soit $a\in \interoo{0 1}$. Par intégration par parties, ou en utilisant une primitive de $\ln$, on a
\[ \int_a^1 \ln(t)\dd t = \left[ t\ln(t)-t \right]_a^1 = -1-a\ln(a)+a \]
Or, on a $\ds{\lim_{a\rightarrow 0} a\ln(a) = 0}$ (croissance comparée). Par somme, $\ds{\lim_{a\rightarrow 0} \int_a^1 \ln(t)\dd t=-1}$. Ainsi, l'intégrale converge, et \[ \int_0^1 \ln(t)\dd t=-1 \]
}

\section{Théorèmes d'existence des intégrales de fonctions positives}

Dans le cas de fonctions positives, on dispose de différentes possibilités pour démontrer la convergence.

	\subsection{Intégrale partielle}

\begin{theoreme}[Majoration des intégrales partielles]
	Soient $a\in \R$ et $b\in \R \cup \left\{+\infty\right\}$ tels que $a<b$.

Soit $f$ une fonction continue et \textbf{positive} sur \interfo{a b}. L'intégrale généralisée $\ds{\int_{a}^{b} f(t) \dd t}$ converge si et seulement si la fonction $\ds{x \donne \int_{a}^{x} f(t) \dd t}$ est majorée sur \interfo{a b}.

Dans ce cas, \[\int_{a}^{b} f(t) \dd t=\sup _{x \in\interfo{a b}} \int_{a}^{x} f(t) \dd t.\] \end{theoreme}

\preuve[7]{
%TODO: démonstration
}

\begin{remarque}
	On dispose du même résultat sur $\interof{a b}$ avec $a\in \R \cup \{-\infty\}$ et $b\in \R$ : l'intégrale $\ds{\int_a^b f(t)\dd t}$ converge si et seulement si la fonction $\ds{x\donne \int_x^b f(t)\dd t}$ est majorée sur \interof{a b}, et dans ce cas \[ \int_a^b f(t)\dd t = \sup_{x\in \interof{a b}} \int_x^b f(t)\dd t. \]
\end{remarque}

\begin{exo}
	Montrer que $\ds{\int_0^{1} \left|\sin\left(\frac{1}{t}\right)\right|\dd t}$ converge.
\end{exo}

\solution[7]{La fonction $t\donne \left|\sin\left(\frac{1}{t}\right)\right|$ est continue et positive sur $\interof{0 1}$. De plus, pour tout $x\in \interof{0 1}$ :
\begin{align*}
	\int_x^{1} \left|\sin\left(\frac{1}{t}\right)\right|\dd t &\leq \int_x^1 1 \dd t \\
	&\leq 1-x\leq 1.
\end{align*}
Ainsi, la fonction $\ds{x\donne \int_x^1 \left|\sin\left(\frac{1}{t}\right)\right|\dd t}$ est majorée : l'intégrale $\ds{\int_0^1 \left|\sin\left(\frac{1}{t}\right)\right|\dd t}$ est convergente.
}

\begin{remarque}
	Le résultat est valable si la fonction est négative sur les domaines considérés, quitte à s'intéresser à $-f$.
\end{remarque}

	\subsection{Majoration}

\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues et \textbf{positives} sur l'intervalle $\interfo{a b}$. On suppose que \[\forall~x\in \interfo{a b}, 0\leq f(x)   \leq g(x)\]
Alors:
\begin{itemize}
	\item  si $\ds{\int_a^{b} g(t)\dd t}$ est convergente, $\ds{\int_a^{b} f(t)\dd t}$ est également convergente, et on a
\[0\leq \int_a^{b} f(t)\dd t \leq \int_a^{b} g(t)\dd t;\]
\item si $\ds{\int_a^b f(t)\D t}$ est divergente, $\ds{\int_a^b g(t)\D t}$ est également divergente.
\end{itemize}
\end{theoreme}


\begin{remarque}
	Attention : il faut absolument que $f$ et $g$ soient positives.
\end{remarque}

\begin{remarque}
	Il suffit que l'inégalité $f(x)\leq g(x)$ soit vraie au voisinage de $b$ pour que le résultat puisse tout de même s'appliquer.
\end{remarque}

\begin{exemple}
	Montrer que $\ds{\int_1^{+\infty} \frac{1}{t+t^2}\dd t}$ converge.
\end{exemple}

\solution[10]{Tout d'abord, $t\mapsto \dfrac{1}{t+t^2}$ est continue sur $\interfo{1 +\infty}$. Remarquons que pour tout $t\geq 1$ on a $t+t^2\geq t^2$, soit
\[0\leq \frac{1}{t+t^2} \leq \frac{1}{t^2}\]
Or, l'intégrale $\ds{\int_1^{+\infty}\frac{1}{t^2}\dd t}$ converge (intégrale de Riemman avec $\alpha=2>1$). Par comparaison, l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t+t^2}\dd t}$ converge.
}

    \subsection{Equivalences et négligeabilité}

\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues, \textbf{positives} sur l'intervalle $\interfo{a b}$. On suppose que \[\ f(x) \eq{b} g(x)\]
~\\Alors, $\ds{\int_a^{b} g(t)\dd t}$ converge, si et seulement si $\ds{\int_a^{b} f(t)\dd t}$ est également convergente.
\end{theoreme}


\begin{remarque}
	Attention : il faut absolument que $f$ et $g$ soient positives, ou a minima, de signe constant (si $f$ et $g$ sont négatives, on peut raisonner sur $-f$ et $-g$).
\end{remarque}

\begin{exo}
	Traiter le cas de l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t+t^2}\dd t}$ à l'aide d'équivalents.
\end{exo}

\solution[6]{Par équivalences usuelles :
\[ \frac{1}{t+t^2} \eq{+\infty} \frac{1}{t^2} \]
Les deux fonctions sont continues et positives sur $\interfo{1 +\infty}$, et l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ est convergente (Riemann avec $\alpha=2>1$). Par comparaison de fonctions positives, l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ converge.

}

\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues, \textbf{positives} sur l'intervalle $\interfo{a b}$. On suppose que \[f(x) =\petito[b]{g(x)}.\]
\begin{itemize}
	\item Si $\ds{\int_a^{b} g(t)\dd t}$ est convergente, alors $\ds{\int_a^{b} f(t)\dd t}$ est également convergente.
	\item Si $\ds{\int_a^{b} f(t)\dd t}$ est divergente, alors $\ds{\int_a^{b} g(t)\dd t}$ est également divergente.
\end{itemize}
\end{theoreme}

\begin{remarque}
	Attention : il faut absolument que $f$ et $g$ soient positives.
\end{remarque}

\begin{methode}
La plupart du temps, on essaiera de comparer à l'une des intégrales de références, et principalement les intégrales de Riemann convergentes.
\end{methode}


\begin{exemple}
	Montrer que l'intégrale $\ds{\int_1^{+\infty} t\eu{-t}\dd t}$ converge.
\end{exemple}

\solution[10]{$t\mapsto t\eu{-t}$ est continue sur $\interfo{1 +\infty}$. On constate que
\[ \frac{t\eu{-t}}{1/t^2}=t^3\eu{-t}\underset{t\rightarrow +\infty}{\longrightarrow} 0 \]
Ainsi, $\ds{t\eu{-t} = o_{+\infty}\left(\frac{1}{t^2}\right)}$. Puisque les deux fonctions sont positives, et que l'intégrale $\ds{\int_1^{+\infty} \frac{\dd t}{t^2}}$ converge (intégrale de Riemann), par comparaison de fonctions positives, on en déduit que l'intégrale  $\ds{\int_1^{+\infty} t\eu{-t}\dd t}$ converge également.
}

\subsection{Méthode d'étude}

\begin{methode}
Pour montrer qu'une intégrale impropre $\ds{\int_a^b f(t)\dd t}$ converge :
\begin{numerote}
	\item On étudier tout d'abord la continuité de la fonction sur $\interoo{a b}$.
	\item On vérifie en quel(s) point(s) $a$, $b$ ou les deux, l'intégrale est impropre.
	\item On utilise les théorèmes de majoration ou d'équivalence pour montrer que l'intégrale converge.
\end{numerote}
\end{methode}


\begin{exemple}
	Montrer que l'intégrale $\ds{\int_0^{+\infty} \eu{-t^2}\dd t}$ converge.
\end{exemple}

\solution[10]{La fonction $t\mapsto \eu{-t^2}$ est continue et positive sur $[0;+\infty[$. L'intégrale est donc impropre en $+\infty$. Or, au voisinage de $+\infty$ :
\[ \eu{-t^2} = \petito[+\infty]{\frac{1}{t^2}}\]
En effet,
\[ \frac{\eu{-t^2}}{1/t^2} = t^2\eu{-t^2} \underset{t\rightarrow +\infty}{\longrightarrow} 0 \text{ par croissance comparée} \]
Puisque $t\mapsto \frac{1}{t^2}$ est positive, et que l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ converge, par comparaison de fonctions positives, on en déduit que l'intégrale $\ds{\int_0^{+\infty} \eu{-t^2}\dd t}$ converge.
}

\begin{theoreme}[Intégrale de Gauss]
	La fonction $f:x\mapsto \eu{-x^2}$ est intégrable sur $\R$ et
\[\int_{-\infty}^{+\infty} \eu{-t^2}\dd t =\sqrt{\pi} \qeq \int_{0}^{+\infty} \eu{-t^2}\dd t =\frac{\sqrt{\pi}}{2} \]
\end{theoreme}

\preuve{Théorème admis.}

	\subsection{Changement de variable}

L'intégration par partie et le changement de variable, sous condition de convergence de chacune des intégrales, sont encore valables.

\begin{proposition}
Soit $f$ une fonction continue sur $\interoo{a b}$. Soit $\varphi$ une fonction \textbf{strictement croissante} de classe $\CC^1$ sur $\interoo{\alpha{} \beta}$, avec \[ \lim_{t\rightarrow \alpha} \varphi(t)=a \qeq \lim_{t\rightarrow \beta} \varphi(t)=b \]
Alors les intégrales $\ds{\int_a^b f(t)\dd t}$ et $\ds{\int_\alpha^\beta f(\varphi(u))\varphi'(u)\dd u}$ sont de même nature. Si elles sont convergentes, on a alors
\[ \int_a^b f(t)\dd t = \int_\alpha^\beta f(\varphi(u))\varphi'(u)du \]
\end{proposition}


\begin{remarque}
	Le résultat est valable si $\varphi$ est strictement décroissante, excepté que dans ce cas, les bornes $\alpha$ et $\beta$ sont inversées.
\end{remarque}

\begin{exemple}
	Soit $\ds{I=\int_0^{+\infty} \frac{1}{\sqrt{t}+t\sqrt{t}}\dd t}$. Calculer $I$ en effectuant le changement de variable $t=u^2$.
\end{exemple}

\solution[20]{Remarquons déjà que $f:t\mapsto \frac{1}{\sqrt{t}+t\sqrt{t}}$ est continue sur $]0;+\infty[$. De plus,
\[ f(t) \eq{+\infty} \frac{1}{t^{3/2}} \qeq f(t) \eq{0} \frac{1}{\sqrt{t}} \]
Les trois fonctions sont positives, la fonction $t\mapsto \frac{1}{t^{3/2}}$ est intégrable sur $\interfo{1 +\infty}$ (Intégrale de Riemann, $\frac{3}{2}>1$) et  la fonction $t\mapsto \frac{1}{t^{1/2}}$ est intégrable sur $\interof{0 1}$ (Intégrale de Riemann, $\frac{1}{2}<1$). Ainsi, $I$ est convergente.\\
Le changement de variable $t=u^2$ sur $[0;+\infty[$ est de classe $\CC^1$, strictement croissant sur $[0;+\infty[$. Par changement de variable, l'intégrale étant convergente :
\begin{align*}
	\int_0^{+\infty} \frac{1}{\sqrt{t}+t\sqrt{t}}\dd t &= \int_0^{+\infty} \frac{1}{u+u^2\times u} (2udu) \\
	 &= 2\int_0^{+\infty} \frac{1}{1+u^2}du
\end{align*}
Or,
\[ \int_0^a \frac{1}{1+u^2} = \left[ \arctan(u)\right]_0^{a} = \arctan(a) \tendversen{a\to +\infty}\frac{\pi}{2} \]
\textbf{Bilan} : $I=2\times \frac{\pi}{2}=\pi$.
}


\section{Convergence absolue}

    \subsection{Définition}

 \begin{definition}\vspace*{.2cm}
	Soit $f$ une fonction continue sur $\interfo{a b}$. On dit que l'intégrale $\ds{\int_a^{b} f(t)\dd t}$ \textbf{converge absolument} sur $\interfo{a b}$ si $\ds{\int_a^{b} |f(t)|\dd t}$ converge.
\end{definition}

 \begin{remarque}
	Rigoureusement, on dit que $f$ est \textbf{intégrable} sur $\interfo{a b}$ si $\ds{\int_a^{b} |f(t)|\dd t}$ converge.
\end{remarque}

\begin{theoreme}\vspace*{.2cm}
Soit $f$ une fonction continue sur $\interfo{a b}$. Si $\ds{\int_a^{b} f(t)\dd t}$ est absolument convergente, alors elle est convergente, et on a
   \[\left|\int_a^{b} f(t)\dd t\right| \leq \int_a^{b} |f(t)|\dd t\]
\end{theoreme}


\preuve{Admis.}

\begin{attention}
	La réciproque n'est pas vraie. Une intégrale peut être convergente, sans être absolument convergente. On dit alors que l'intégrale est \textbf{semi-convergente}. On pourra regarder l'exercice \ref{chap18exosin}.
\end{attention}

	\subsection{Intégrale convergeant absolument vers $0$}

\begin{proposition}
Soit $f$ une fonction continue sur $\interoo{a b}$. $f$ est nulle sur $\interoo{a b}$ si et seulement si \[\int_a^b |f(t)|\dd t = 0\]
\end{proposition}


 \preuve{Si $f$ est nulle, alors son intégrale existe et est nulle. Réciproquement, si $\ds{\int_a^b |f(t)|\D t=0}$, puisque $|f|$ est positive et continue sur $\interoo{a b}$, on en déduit que $|f|=0$ et donc $f=0$.}

\section{Un exemple complet}

Dans cette dernière partie, nous allons nous intéresser à une fonction, définie par une intégrale généralisée, qui est très importante en probabilité.

	\subsection{Intégrale de Gauss et fonction $\Phi$}

On va tout d'abord présenter un préliminaire qui utilise un résultat précédent :

\begin{proposition}[Intégrale de Gauss]\vspace*{.1cm}
	L'intégrale $\ds{\int_{-\infty}^{+\infty} \frac{1}{\sqrt{2\pi}}\eu{-t^2/2}\D t}$ converge et vaut $1$.
\end{proposition}

\preuve[10]{
On va partir du résultat annoncé plus haut : \[ \int_{-\infty}^{+\infty} \eu{-t^2}\D t=\sqrt{2\pi}. \]
On pose $t=\frac{u}{\sqrt{2}}$. Ce changement de variable est de classe $\C^1$ et est strictement croissante sur $\R$. Par changement de variable :
\begin{align*}
	\int_{-\infty}^{+\infty} \eu{-t^2}\D t &= \int_{-\infty}^{+\infty} \eu{-\left(\frac{u}{\sqrt{2}}\right)^2} \frac{\D u}{\sqrt{2}} \\
	&= \frac{1}{\sqrt{2}} \int_{-\infty}^{+\infty} \eu{-u^2/2}\D u
\end{align*}
Ainsi,
\begin{align*}
	\int_{-\infty}^{+\infty} \frac{1}{\sqrt{2\pi}}\eu{-u^2/2}\D u &= \frac{1}{\sqrt{2\pi}} \sqrt{2}\int_{-\infty}^{+\infty} \eu{-t^2}\D t\\
	&= \frac{1}{\sqrt{2\pi}} \sqrt{2} \sqrt{\pi} = 1.
\end{align*}
}

Cette intégrale étant convergente, on peut définir alors la fonction $\Phi$ :

\begin{definition}[Fonction $\Phi$]
On note $\Phi$ la fonction, définie sur $\R$, par
\[ \Phi : x\donne \int_{-\infty}^x \frac{1}{\sqrt{2\pi}}\eu{-t^2/2}\D t.\]
\end{definition}

\begin{remarque}
 Un résultat vu précédemment garantit la bonne définition de la fonction $\Phi$ sur $\R$.
\end{remarque}

	\subsection{Propriétés importantes}

On dispose d'une valeur particulière :

\begin{proposition}
	$\Phi\left(0\right)=\frac12$.
\end{proposition}

\preuve[4]{La fonction $\phi: t\mapsto \frac{1}{\sqrt{2\pi}}\eu{-t^2/2}$ est paire. Ainsi,
\[ \int_{-\infty}^{+\infty} \phi (t) = 2\int_{-\infty}^0 \phi(t)\D t \]
et donc
\[ 1 = 2 \Phi(0) \implies \Phi(0)=\frac12.\]
}

On en déduit une propriété nous ramenant à une intégrale sur un segment :

\begin{proposition}\label{prop:gauss_segment}
Pour tout $x\in \R$,
\[ \Phi(x) = \frac{1}{2} + \int_0^x \frac{1}{\sqrt{2\pi}}\eu{-t^2/2}\D t.\]
\end{proposition}

\preuve[3]{En effet, par la relation de Chasles (les intégrales étant convergentes)
\[ \Phi(x)=\int_{-\infty}^x \phi(t)\D t = \underbrace{\int_{-\infty}^0 \phi(t)\D t}_{=\frac12} + \int_0^x \phi(t)\D t.\]
}

La fonction $\Phi$ est régulière :

\begin{proposition}
La fonction $\Phi$ est de classe $\CC^1$ sur $\R$, et pour tout $x$, $\Phi'(x)=\phi(x)=\frac{1}{\sqrt{2\pi}}\eu{-x^2/2}$.

$\Phi$ est strictement croissante sur $\R$ et on a \[ \lim_{x\to -\infty}\Phi(x)=0 \qeq \lim_{x\to +\infty} \Phi(x)=1. \]
\end{proposition}

\preuve[10]{La fonction $\phi$ étant continue sur $\R$, et l'intégrale étant convergente, le théorème vu précédemment garantit que $\Phi$ est de classe $\CC^1$ sur $\R$, de dérivée $\phi$.

Puisque $\phi$ est positive sur $\R$, $\Phi$ est strictement croissante sur $\R$.

Enfin, soit $y\in \R$. On a :
\[ \Phi(x) = \int_{-\infty}^y \phi(t)\D t + \int_y^x \phi(t)\D t = \Phi(y)- \int_x^y \phi(t)\D t. \]
Puisque l'intégrale est convergente, \[ -\int_x^y \phi(t)\D t\tendversen{x\to -\infty} -\Phi(y) \]
et finalement $\Phi(x)\tendversen{x\to -\infty} 0$.

La deuxième limite découle de la valeur de l'intégrale.
}

Les éléments précédents permettent de garantir que la fonction $\Phi$ est bijective de $\R$ dans $\interoo{0 1}$.

	\subsection{Simulation avec Python}

On peut utiliser la proposition \ref{prop:gauss_segment} pour appliquer la méthode des rectangles.

Dans le chapitre 14, nous avons vu une majoration de la différence entre l'intégrale d'une fonction sur un segment et les sommes de Riemann :

\[ \forall n\in \N*, \quad \left| \int_a^b f(t)\D t-S_n(f)\right| \leq \frac{(b-a)^2}{2n}\max_{\interff{a b}}|f'|. \]
En étudiant $\phi$, on peut montrer que $\ds{\max_{\interff{a b}}|\phi'| \leq \frac14}$ et donc
\[ \forall n\in \N*,\, \forall x\in \R,\quad \left| \int_0^x f(t)\D t-S_n(f)\right| \leq \frac{x^2}{8n}. \]
Si on veut une approximation à $\eps$ près, il suffit de rendre $\frac{x^2}{8^n}$ plus petit que $\eps$.

On obtient la fonction suivante :

\begin{pythonfile}[0.9]{Phi.py}
\end{pythonfile}

On peut alors représenter la courbe représentative avec \textsf{matplotlib.pyplot} :
\vspace*{.3cm}
\begin{sanscadrepythonm}
import matplotlib.pyplot as plt
t = np.arange(-5.0, 5.0, 0.1)     # On la représente sur [-5, 5]
y = [Phi(u, 10**(-4)) for u in t] # Avec précision de $10^{-4}$
plt.plot(t, y, 'b')
plt.show()
\end{sanscadrepythonm}

\begin{center}
	\includegraphics{fonctionphi.eps}
\end{center}
%%% Fin du cours %%%
