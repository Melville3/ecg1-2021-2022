\chapter{Généralités sur les suites}
%!TeX root=../../encours.nouveau.tex

\objectifintro{Dans ce chapitre, on reprend les notions connues sur les suites, et on ajoute des suites de références : les suites arithmético-géométriques, et les suites récurrentes linéaires d'ordre $2$.}

\begin{extrait}{Charles Baudelaire (1821 -- 1867). \emph{Mon Coeur mis à nu}}
Une suite de petites volontés fait un gros résultat.
\end{extrait}


\begin{objectifs}
\begin{numerote}
        \item Concernant les généralités sur les suites :
                        \begin{itemize}[label=\textbullet]
                                \item \hyperref[objectif-05-1]{Connaître la méthode de représentation des suites}\dotfill $\Box$
                                \item \hyperref[objectif-05-2]{Savoir démontrer des variations par étude de fonctions}\dotfill $\Box$
                                \item \hyperref[objectif-05-3]{Savoir démontrer des variations par la méthode $u_{n+1}-u_n$}\dotfill $\Box$
                                \item \hyperref[objectif-05-4]{Savoir démontrer des variations par récurrence}\dotfill $\Box$
                                \item \hyperref[objectif-05-5]{Savoir démontrer une majoration par récurrence}\dotfill $\Box$
                        \end{itemize}
        \item Concernant les suites particulières :
                \begin{itemize}[label=\textbullet]
                        \item \hyperref[objectif-05-6]{Savoir étudier les suites arithmétiques et géométriques}\dotfill $\Box$
                        \item \hyperref[objectif-05-7]{Savoir étudier les suites arithmético-géométriques}\dotfill $\Box$
                        \item \hyperref[objectif-05-8]{Connaître la méthode d'étude des suites récurrentes linéaires d'ordre 2}\dotfill $\Box$
                \end{itemize}
\end{numerote}
\end{objectifs}


\section{Généralités}

	\subsection{Définition et notations}

\begin{definition}
	Une \textbf{suite numérique réelle} est une fonction de $\N$ (ou d'une partie de $\N$) dans $\R$.

  On note $\R^{\N}$ l'ensemble des suites réelles définies sur $\R$.
\end{definition}

\begin{notation}
Soit $u$ une suite. \begin{eqnarray*}u: \N & \rightarrow & \R\\n &\mapsto& u_n\end{eqnarray*}
La suite $u$ peut aussi se noter $(u_n)_{n\in \N}$ ou $(u_n)$. L'élément $u_n$ est appelé \textbf{terme de rang} $n$ de la suite $u$.
\end{notation}

	\subsection{Trois types de définition de suites}

On peut définir une suite de trois manières différentes.
		\subsubsection{Définition explicite}

La suite peut être définie explicitement :
\begin{itemize}
	\item[$\bullet$] $v$ est la suite définie par $v_n=\frac{3^n}{n}$ pour $n\in \N^*$.
	\item[$\bullet$] $u$ est la suite définie par $u_n$ est la $n^{\textrm{è}}$ décimale de $\pi$, pour $n\in \N^*$.
	\item[$\bullet$] $w$ est la suite définie par $$\left\{ \begin{aligned}w_n&=3n+2 \textrm{ si $n$ est pair} \\
	w_n&=\frac{1}{n}+4 \textrm{ si $n$ est impair}\end{aligned}\right.$$
\end{itemize}

		\subsubsection{Définition par récurrence}

Elle peut également être définie par récurrence :

\begin{itemize}
	\item[$\bullet$] $u$ est la suite définie par $u_0=-1$ et, pour $n\geq 0$, $u_{n+1}=2\times u_n-1$.
	\item[$\bullet$] $\displaystyle{\left\{
		\begin{aligned}
			u_0 &=& 2\\
			\forall~n,~u_{n+1}&=& \frac{1}{u_n}
		\end{aligned} \right.}$
\end{itemize}

\begin{remarque}
	Pour déterminer $u_n$ dans le cas d'une suite définie par récurrence, il est nécessaire de calculer $u_0, u_1, \hdots u_{n-1}$ pour déterminer ensuite $u_n$. Par exemple, dans le cas de la suite $u$ définie précédemment, pour calculer $u_2$ :
	\[ u_0=-1 \text{ donc } u_1=2\times u_0-1=-3 \text{ et donc } u_2=2\times u_1-1=-7 \]
\end{remarque}

        \subsubsection{Définition par équation}

Une suite peut également être définie par une equation. Par exemple, soit $(u_n)$ la suite définie pour tout $n\geq 3$ par $u_n$ est l'unique solution de l'équation $\ln(x)=nx$.

  \subsection{Opérations sur les suites}

Puisque les suites sont des fonctions particulières, on dispose des mêmes opérations générales :

\begin{definition}
  Soient $u=(u_n)_{n\in \N}$ et $v=(v_n)_{n\in \N}$ deux suites réelles, et $\alpha\in \R$. On note :
  \begin{itemize}
    \item La suite $u+v$ est la suite $w$ définie pour tout $n$ par $w_n=u_n+v_n$.
    \item La suite $u\times v$ est la suite $w$ définie pour tout $n$ par $w_n=u_n\times v_n$.
    \item La suite $\alpha u$ est la suite $w$ définie pour tout $n$ par $w_n=\alpha v_n$.
    \item Si la suite $(v_n)$ ne s'annule jamais, $\frac{u}{v}$ est la suite $w$ définie pour tout $n$ par $w_n=\frac{u_n}{v_n}$.
    \item La suite $|u|$ est la suite $w$ définie pour tout $n$ par $w_n=|u_n|$.
  \end{itemize}
\end{definition}

	\subsection{Représentation graphique d'une suite}

\label{objectif-05-1}
On représente une suite définie explicitement par l'ensemble des points $(n;u_n)$. On peut également représenter les suites définies par récurrence par une représentation ``en toile d'araignée''.

	\begin{center}
	\begin{tabular}{p{6cm}p{6cm}}
		\begin{center}
            \includegraphics[width=6cm]{suite_explicite}
        \end{center} &
\begin{center}
    \includegraphics[width=6cm]{suite_recurrente}
\end{center} \\
\begin{center} Définition $u_n=f(n)$\end{center} & \begin{center}Définition par récurrence, $u_0=1$\end{center}
	\end{tabular}
	\end{center}

%     \input{suite_recurrente1}

\section{Monotonie, majoration, minoration}

	\subsection{Suites monotones}

\begin{definition}[Monotonie]
Soit $(u_n)$ une suite. \\
On dit que $(u_n)$ est une suite \textbf{croissante} si $u_n \leq u_{n+1}$ pour tout $n$.\\
On dit que $(u_n)$ est une suite \textbf{décroissante} si $u_n \geq u_{n+1}$ pour tout $n$.\\
Une suite \textbf{monotone} est une suite soit croissante, soit décroissante.\\
On dit qu'une suite est \textbf{constante} s'il existe un réel $a$ tel que, pour tout $n$, $u_n=a$.
\end{definition}

\begin{remarque}
\begin{itemize}
	\item On a aussi des suites strictement croissantes ou strictement décroissantes.
	\item Une suite peut n'être monotone qu'à partir d'un certain rang $n_0$, c'est à dire $u_n\leq u_{n+1}$ pour tout $n\geq n_0$ (par exemple).
  \item Une suite est dite \textbf{stationnaire} si elle est constante à partir d'un certain rang.
\end{itemize}
\end{remarque}

\begin{definition}[Périodique]
  Soit $(u_n)$ une suite. On dit que $(u_n)$ est périodique s'il existe un entier $T\in \N*$ tel que, pour tout $n$, $u_{n+T}=u_n$.
\end{definition}

		\subsection{Méthodes de recherche des variations d'une suite}

			\subsubsection{Pour les suites $u_n=f(n)$}

\begin{theoreme}
\label{objectif-05-2}
Si $f$ est croissante sur $\R^+$, alors la suite $(u_n)$ est croissante.\\
Si $f$ est décroissante sur $\R^+$, alors la suite $(u_n)$ est décroissante.
\end{theoreme}

\ifprof
\begin{demonstration}
Si $f$ est croissante sur $\R^+$ :
$$n < n+1 \implies u_n=f(n) \leq f(n+1)=u_{n+1}$$
\end{demonstration}
\else
\lignes{3}
\fi

\begin{exemple}
\begin{itemize}
	\item[$\bullet$] Soit $u$ la suite définie par $u_n=n^2$ pour tout $n$. La fonction $x\mapsto x^2$ étant strictement croissante sur $\R^+$, la suite $(u_n)$ est strictement croissante.
	\item[$\bullet$] La suite $u$ définie par $u_n=\frac{1}{n}$ pour $n\geq 1$ est strictement décroissante, car la fonction inverse est strictement décroissante sur $\R^{+*}$.
\end{itemize}
\end{exemple}

\begin{methode}
Pour étudier la monotonie d'une suite $(u_n)$ définie pour tout entier $n$ par $u_n=f(n)$, on introduit la fonction $f$ associée, et on étudie ses variations. On en déduit alors la monotonie de $u$.
\end{methode}

\exercice{Soit $u$ la suite définie pour tout entier $n$ par $u_n=n^2-4n+2$. Déterminer la monotonie de $u$.}

\solution[5]{Introduisons la fonction $f$ définie sur $\R$ par $f(x)=x^2-4x+2$. $f$ étant un trinôme du second degré, on connait ses variations : ici, $a=1>0,~b=-4$ donc $-\frac{b}{2a}=2$. Ainsi, $f$ est décroissante sur $\left]-\infty;2\right]$ et est croissante sur $\left[2;+\infty\right[$.\\
La suite $(u_n)$ est donc croissante pour $n\geq 2$.

}

			\subsubsection{Termes consécutifs}

\begin{theoreme}[Etude de la différence de deux termes consécutifs]
\label{objectif-05-3}
Soit une suite $u$  donnée. On peut étudier le signe de la différence $u_{n+1}-u_n$.\\
Si $u_{n+1}-u_n \geq 0$ pour tout $n$, alors la suite $u$ est croissante.\\
Si $u_{n+1}-u_n \leq 0$ pour tout $n$, alors la suite $u$ est décroissante.
\end{theoreme}

\begin{theoreme}[Etude du quotient de deux termes consécutifs]
Soit une suite $u$  à termes \emph{strictement positifs}, on peut utiliser :\\
Si $\frac{u_{n+1}}{u_n} \geq 1$ (resp. $>1$) alors la suite $u$ est croissante (resp. strictement croissante).\\
Si $\frac{u_{n+1}}{u_n} \leq 1$ (resp. $<1$) alors la suite $u$ est décroissante (resp. strictement décroissante).
\end{theoreme}

\begin{remarque}Cette méthode est, en général, reservée aux suites définies par des puissances.
\end{remarque}

\begin{exemple}
\begin{itemize}
	\item[$\bullet$] Soit $(u_n)$ la suite définie par $u_0=1$ et $u_{n+1}=u_n+\frac{1}{n^2+1}$ pour tout $n$. Alors, pour tout $n$, on a $u_{n+1}-u_n=\frac{1}{n^2+1}>0$ : la suite $(u_n)$ est donc strictement croissante.
	\item[$\bullet$] La suite $u$ définie pour tout $n$ par $u_n=\frac{5^n}{2^{n+1}}$ est strictement croissante. En effet, elle est à termes strictement positifs, et on a
	$$\frac{u_{n+1}}{u_n} = \frac{\frac{5^{n+1}}{2^{n+2}}}{\frac{5^n}{2^{n+1}}}=\frac{5^{n+1}}{2^{n+2}}.\frac{2^{n+1}}{5^n}=\frac{5}{2} > 1$$
\end{itemize}
\end{exemple}

\begin{consequence}
\begin{itemize}
	\item[$\bullet$] Si $a > 1$, la suite $(a^n)$ est strictement croissante.
	\item[$\bullet$] Si $a=1$, la suite $(a^n)$ est constante.
	\item[$\bullet$] Si $0<a<1$, la suite $(a^n)$ est strictement décroissante.
\end{itemize}
\end{consequence}

\ifprof
\begin{demonstration}
Pour tout réel $a>0$, on constate que la suite $(a^n)$ est à termes strictement positifs. Enfin, en notant pour tout entier $n$, $u_n=a^n$, on a
$$\frac{u_{n+1}}{u_n}=\frac{a^{n+1}}{a^n}=a$$
Ainsi, la suite $(a^n)$ est strictement croissante si $a>1$, et strictement décroissante si $a<1$. Elle est enfin constante si $a=1$.
\end{demonstration}
\else
\lignes{6}
\fi

\afaire{Exercice \lienexo{01}.}

        \subsubsection{Par récurrence}

\begin{methode}
\label{objectif-05-4}
	Dans le cas d'une suite définie par récurrence, on peut étudier sa monotonie par récurrence, en prenant comme hypothèse de récurrence $P_n$: ``$u_{n+1}\geq u_n$'' pour montrer que la suite est croissante, ou $P_n$: ``$u_{n+1}\leq u_n$'' pour montrer qu'elle est décroissante.
\end{methode}

\begin{exemple}
	Soit $(u_n)$ la suite définie par $u_0=1$ et pour tout $n$, $u_{n+1}=\sqrt{u_n+1}$. Montrer que la suite $u$ est croissante.
\end{exemple}

\solution[10]{Soit $(P_n)$ la propriété définie pour tout $n$ par $P_n$:``$u_{n+1}\geq u_n$''.
\begin{itemize}
    \item[$\bullet$] \textbf{initialisation} : $u_0=1$ et $u_1=\sqrt{2}\geq u_0$. Donc $P_0$ est vraie.
    \item[$\bullet$] \textbf{hérédité} : supposons que la propriété $P_n$ est vraie pour un certain $n$. Alors
    $$u_{n+1}\geq u_n \Rightarrow u_{n+1}+1 \geq u_n+1 \Rightarrow
    \sqrt{u_{n+1}+1} \geq \sqrt{u_{n}+1} \textrm{ car la fonction racine est croissante sur }\R^+$$
    Donc $u_{n+2}\geq u_{n+1}$ : la propriété est héréditaire.
    \item[$\bullet$] \textbf{conclusion} : la propriété est donc vraie pour tout $n$ : $\forall~n, ~u_{n+1}\geq u_n$. La suite $u$ est donc croissante.
\end{itemize}
}

	\subsection{Suites majorées, minorées, bornées}

\begin{definition}
\begin{itemize}
	\item[$\bullet$] Une suite $(u_n)$ est dite \textbf{minorée} s'il existe $m\in\R$ tel que $m \leq u_n$ pour tout $n$. $m$ s'appelle un \textbf{minorant}.
	\item[$\bullet$] Une suite $(u_n)$ est dite \textbf{majorée} s'il existe $M\in\R$ tel que $M \geq u_n$ pour tout $n$. $M$ s'appelle un \textbf{majorant}.
	\item[$\bullet$] Une suite \textbf{bornée} est une suite à la fois majorée et minorée : il existe deux réels $m$ et $M$ tels que pour tout $n$ : $$m \leq u_n \leq M$$
\end{itemize}
\end{definition}

\begin{exemple}
	La suite $u$ définie par $u_n=\frac{1}{n+1}$ est bornée :
$$\forall~n,~0 \leq u_n \leq 1$$
\end{exemple}

\afaire{Exercice \lienexo{02}.}

\begin{methode}
\label{objectif-05-5}
	Dans le cas d'une suite définie par récurrence, on peut également montrer  qu'elle est bornée par récurrence.
\end{methode}

\begin{exemple}
	Soit $(u_n)$ la suite définie par $u_0=1$ et pour tout $n$, $u_{n+1}=\sqrt{u_n+2}$. Montrer que, pour tout $n$, $u_n\leq 2$.
\end{exemple}

\solution[10]{Soit $(P_n)$ la propriété définie pour tout $n$ par $P_n:``u_{n}\leq 2''$.
\begin{itemize}
    \item[$\bullet$] \textbf{initialisation} : $u_0=1 \leq 2$. $P_0$ est donc vraie.
    \item[$\bullet$] \textbf{hérédité} : supposons que la propriété $P_n$ est vraie pour un certain $n$. Alors
    $$u_{n}\leq 2 \Rightarrow u_{n}+2 \leq 4\Rightarrow
    \sqrt{u_{n}+2} \leq \sqrt{4} \textrm{ car la fonction racine est croissante sur }\R^+$$
    Donc $u_{n+1}\leq 2$ : la propriété est héréditaire.
    \item[$\bullet$] \textbf{conclusion} : la propriété est donc vraie pour tout $n$ : $\forall~n, ~u_{n}\leq 2$. La suite $u$ est donc majorée par $2$.
\end{itemize}
}

\afaire{Exercices \lienexo{03}, \lienexo{04} et \lienexo{05}.}

\section{Suites arithmétiques et géométriques}
\label{objectif-05-6}

    \subsection{Suites arithmétiques}

 \begin{definition}[Suite arithmétique]
	 Une \textbf{suite arithmétique} est une suite $(u_n)$ définie par une formule de récurrence de la forme
 $$\left \{ \begin{array}{l}
         u_0 \textrm{ est donné}\\
         \forall~n, u_{n+1} = u_n+a \textrm{ où } a \in \R
 \end{array}
 \right.$$
 Le réel $a$ est appelé \textbf{raison} de la suite $(u_n)$, et $u_0$ est le \textbf{premier terme}.
\end{definition}

\begin{exemple}
	La suite $u$ définie par $u_0=1$ et pour tout entier $n$, $u_{n+1}=u_n+2$ est une suite arithmétique de raison $2$ et de premier terme $u_0=1$.
\end{exemple}

\begin{theoreme}[Expression explicite]
	Soit $u$ une suite arithmétique, de premier terme $u_0$ et de raison $a$. Alors, pour tout entier $n$, $$u_n=u_0+na.$$
\end{theoreme}

\ifprof
\begin{demonstration}
Soit $P_n$ la proposition ``$u_n=u_0+na$'' définie pour tout entier $n$.
\begin{itemize}
    \item[$\bullet$] Initialisation : pour $n=0$ on a bien $u_0=u_0+0\times a$ : $P_0$ est donc vraie.
    \item[$\bullet$] Hérédité : supposons que la propriété $P_n$ est vraie pour un certain entier $n$. Alors, on a
    $$u_{n+1} \underbrace{=}_{\textrm{déf de $u$}} u_n + a \underbrace{=}_{\textrm{HR}} (u_0+na) +a = u_0+(n+1)a$$
    $P_{n+1}$ est donc vraie.
    \item[$\bullet$] La proposition $P_n$, d'après le principe de récurrence, est donc vraie pour tout $n$.
\end{itemize}
On peut également démontrer ce théorème par téléscopage.
\end{demonstration}
\else
\lignes{10}
\fi

\begin{remarque}
	Si $u$ est une suite arithmétique de premier terme $u_0$ et de raison $a$, on a également
$$\forall~(n,p)\in \N^2,~ u_n=u_p+(n-p)a$$
\end{remarque}

\begin{exemple}
	Soit $u$ une suite arithmétique de premier terme $u_0=2$ et de raison $-3$. Alors, pour tout entier $n$, $u_n=2-3n$.
\end{exemple}

On en déduit le résultat suivant concernant la monotonie des suites arithmétiques :

\begin{proposition}[Monotonie]
Soit $u$ une suite arithmétique, de premier terme $u_0$ et de raison $a$. Alors $u$ est strictement croissante si $a>0$, constante si $a=0$ et strictement décroissante si $a<0$.
\end{proposition}

\begin{theoreme}[Somme des termes]
	Soit $u$ une suite arithmétique, de raison $a$. Soient $n$ et $p$ deux entiers tels que $p\leq n$. Alors,
$$u_p+u_{p+1}+\cdots +u_n= (n-p+1)\frac{u_p+u_n}{2}=(\textrm{nb de terme}) \times \frac{\textrm{1er terme}+\textrm{dernier terme}}{2}$$
\end{theoreme}

\ifprof
\begin{demonstration}
Soit $S=u_p+u_{p+1}+\cdots+u_n$. D'une part
$$S=u_p+(u_p+a)+(u_p+2a)+\cdots + (u_p+(n-p)a)$$
et d'autre part
$$S=u_n+(u_n-a)+(u_n-2a)+\cdots + (u_n-(n-p)a)$$
En sommant les deux égalités,
$$2S=(u_p+u_n) + (u_p+u_n) + \cdots + (u_p+u_n)$$
Avec $n-p+1$ termes. On a donc $2S=(n-p+1)(u_p+u_n)$, ce qui donne le résultat.\\
On aurait pu également le démontrer par récurrence sur $n$.
\end{demonstration}
\else
\lignes{8}
\fi

\begin{exemple}[Classique]
	En s'intéressant à la suite arithmétique de premier terme $u_0=1$ et de raison $1$, on a
$$1+2+\cdots +n = n\frac{n+1}{2}=\frac{n(n+1)}{2}$$
\end{exemple}

    \subsection{Suites géométriques}

\begin{definition}[Suite géométrique]
	 Une \textbf{suite géométrique} est une suite $(u_n)$ définie par une formule de récurrence de la forme
 \[\left \{ \begin{array}{l}
         u_0 \textrm{ est donné}\\
         \forall~n, u_{n+1} = q\times u_n \textrm{ où } q \in \R
 \end{array}
 \right.\]
 Le réel $q$ est appelé \textbf{raison} de la suite $(u_n)$, et $u_0$ est le \textbf{premier terme}.
\end{definition}

\begin{exemple}
	La suite $u$ définie par $u_0=1$ et pour tout entier $n$, $u_{n+1}=2u_n$ est une suite géométrique de raison $2$ et de premier terme $u_0=1$.
\end{exemple}

\begin{theoreme}[Expression explicite]
	Soit $u$ une suite géométrique, de premier terme $u_0$ et de raison $q$. Alors, pour tout entier $n$, $$u_n=u_0\times q^n$$
\end{theoreme}

\ifprof
\begin{demonstration}
Soit $P_n$ la proposition ``$u_n=u_0\times q^n$'' définie pour tout entier $n$.
\begin{itemize}
    \item[$\bullet$] Initialisation : pour $n=0$ on a bien $u_0=u_0q^0$ : $P_0$ est donc vraie.
    \item[$\bullet$] Hérédité : supposons que la propriété $P_n$ est vraie pour un certain entier $n$. Alors, on a
    $$u_{n+1} \underbrace{=}_{\textrm{dèf de $u$}} q\times u_n \underbrace{=}_{\textrm{HR}} q\times (u_0\times q^n) = u_0\times q^{n+1}$$
    $P_{n+1}$ est donc vraie.
    \item[$\bullet$] La proposition $P_n$, d'après le principe de récurrence, est donc vraie pour tout $n$.
\end{itemize}
\end{demonstration}
\else
\lignes{8}
\fi

\begin{remarque}
	Si $u$ est une suite géométrique de premier terme $u_0$ et de raison $q$, on a également
$$\forall~(n,p)\in \N^2,~ u_n=u_p\times q^{n-p}$$
\end{remarque}

\begin{exemple}
	Soit $u$ une suite géométrique de premier terme $u_0=2$ et de raison $-3$. Alors, pour tout entier $n$, $u_n=2\times(-3)^n$.
\end{exemple}

On en déduit le résultat suivant concernant la monotonie des suites arithmétiques :

\begin{proposition}[Monotonie]
 Soit $u$ une suite géométrique, de premier terme $u_0$ et de raison $q$. Alors
 \begin{itemize}
   \item Si $u_0=0$, ou $q=1$, la suite est constante.
   \item Si $q>1$, la suite est strictement croissante si $u_0>0$, strictement décroissante si $u_0<0$.
   \item Si $q\in ]0,1[$, la suite est strictement décroissante si $u_0>0$, strictement croissante si $u_0<0$.
   \item Si $q=0$, la suite est stationnaire à $0$.
   \item Si $q<0$, la suite n'est pas monotone.
 \end{itemize}
\end{proposition}

\begin{theoreme}[Somme des termes]
Soit $u$ une suite géométrique, de raison $q\neq 1$. Soient $n$ et $p$ deux entiers tels que $p\leq n$. Alors,
$$u_p+u_{p+1}+\cdots +u_n= u_p\frac{1-q^{n-p+1}}{1-q}=\frac{u_p-qu_n}{1-q}=\frac{\textrm{1er terme}-\textrm{terme à suivre}}{1-\textrm{raison}}$$
\end{theoreme}

\ifprof
\begin{demonstration}
Soit $p$ un entier fixé. Soit $P_n$ la proposition ``$u_p+\cdots +u_n=\frac{u_p-qu_n}{1-q}$'' définie pour tout entier $n\geq p$.
\begin{itemize}
    \item[$\bullet$] Initialisation : $\frac{u_p-qu_p}{1-q}=u_p\frac{1-q}{1-q}=u_p$ : $P_p$ est donc vraie.
    \item[$\bullet$] Hérédité : supposons la propriété $P_n$ vraie pour un certain entier $n\geq p$. Alors
    $$u_p+\cdots +u_n+u_{n+1} \underbrace{=}_{\textrm{HR}} \frac{u_p-qu_n}{1-q}+u_{n+1}=\frac{u_p-qu_n+u_{n+1}-qu_{n+1}}{1-q}\underbrace{=}_{\textrm{dèf de $u$}} \frac{u_p-qu_{n+1}}{1-q}$$
    $P_{n+1}$ est donc vraie.
    \item[$\bullet$] D'après le principe de récurrence, la proposition $P_n$ est donc vraie pour tout $n\geq p$.
\end{itemize}
\end{demonstration}
\else
\lignes{10}
\fi

\begin{exemple}[Classique]
	En s'intéressant à la suite géométrique de premier terme $u_0=1$ et de raison $q$, on a
$$1+q+\cdots +q^n = \frac{1-qq^n}{1-q}=\frac{1-q^{n+1}}{1-q}$$
\end{exemple}

\afaire{Exercices \lienexo{06} et \lienexo{07}.}

\section{Suites arithmético-géométrique}
\label{objectif-05-7}

    \subsection{Définition}

 \begin{definition}
	 Une suite \textbf{arithmético-géométrique} est une suite $u$ définie par une formule de récurrence de la forme
 $$\left\{\begin{array}{l}
        u_0 \textrm { est donné} \\
        \forall~n,~u_{n+1}=au_n+b,~~ \textrm{ où } (a, b)\in \R^2
\end{array}
 \right.$$
\end{definition}

\begin{remarque}
	Si $b=0$, la suite $u$ est une suite géométrique de raison $a$. Si $a=1$, la suite $u$ est une suite arithmétique de raison $b$.
\end{remarque}

\begin{exemple}
	La suite $u$ définie par $u_0=1$ et pour tout entier $n$, $u_{n+1}=2u_n-1$ est une suite arithmético-géométrique.
\end{exemple}


    \subsection{Etude d'une suite arithmético-géométrique}

\begin{theoreme}
	Soit $u$ une suite arithmético-géométrique vérifiant, pour tout $n$, $u_{n+1}=au_n+b$ avec $a\neq 1$.\\
Soit $\ell$ l'unique réel vérifiant $\ell=a\ell+b$ (c'est-à-dire $\ell=\frac{b}{1-a}$).\\
Alors, la suite $(v_n)$, définie pour tout entier $n$ par $v_n=u_n-\ell$ est une suite géométrique, de raison $a$.
\end{theoreme}

\ifprof
\begin{demonstration}
En effet, soit $n\in \N$. On a, en soustrayant membre à membre :
$$\begin{array}{rclcl}
    u_{n+1}&=&au_n&+&b \\
    \ell&=& a\ell&+&b\\\hline
    u_{n+1}-\ell &=& a(u_n-\ell)&&\\
\end{array}$$
c'est-à-dire $v_{n+1}= av_n$ pour tout $n$.
\end{demonstration}
\else
\lignes{4}
\fi

\begin{methode}
	Pour étudier une suite arithmético-géométrique, on procédera ainsi :
\begin{itemize}
    \item[$\bullet$] On cherche le réel $\ell$ vérifiant $\ell=a\ell+b$
    \item[$\bullet$] On introduit la suite $v=u-\ell$ et on montre qu'elle est géométrique.
    \item[$\bullet$] On en déduit l'expression de $v_n$ puis de $u_n$ en fonction de $n$.
\end{itemize}
\end{methode}

\begin{exemple}
	Soit $u$ la suite définie par $u_0=1$ et pour tout entier $n$, $u_{n+1}=2u_n-3$. Déterminer l'expression de $u_n$ en fonction de $n$.
\end{exemple}

\solution[10]{\begin{itemize}
    \item[$\bullet$] On cherche le réel $\ell$ vérifiant $\ell=2\ell-3 \Leftrightarrow \ell=3$.
    \item[$\bullet$] On pose $v$ la suite définie pour tout entier $n$ par $v_n=u_n-3$. On a alors
    $$v_{n+1}=u_{n+1}-3=2u_n-3-3=2u_n-6=2(v_n+3)-6=2v_n$$
    La suite $v$ est donc géométrique, de raison $2$ et de premier terme $v_0=u_0-3=-2$.
    \item[$\bullet$] On a donc, pour tout entier $n$, $v_n=v_0\times 2^n=-2 \times 2^n=-2^{n+1}$. En revenant à la suite $u$ :
    $$\forall~n,~u_n=-2^{n+1}+3$$
\end{itemize}
Pour la deuxième étape, on peut également écrire que $u_{n+1}=2u_n-3$ et $l=2l-3$ et on soustrait : $u_{n+1}-l = 2(u_n-l)$ et donc $v_{n+1}=2v_n$.
}

\section{Suites récurrentes linéaires d'ordre 2}
\label{objectif-05-8}

    \subsection{Définition}

\begin{definition}
Une \textbf{suite récurrente linéaire d'ordre 2} est une suite $u$ définie par une formule de récurrence de la forme
 $$\left\{\begin{array}{l}
        u_0\textrm{ et } u_1 \textrm { sont donnés} \\
        \forall~n,~u_{n+2}=au_{n+1}+bu_n,~~ \textrm{ où } (a, b)\in \R^2
\end{array}
 \right.$$
\end{definition}

\begin{remarque}
	Si $b=0$, la suite $u$ est une suite géométrique de raison $a$.
\end{remarque}

\begin{exemple}
	La suite $u$ définie par $u_0=1,~u_1=2$ et pour tout entier $n$, $u_{n+2}=2u_{n+1}-u_n$ est une suite récurrente linéaire d'ordre 2.
\end{exemple}

 \subsection{Etude d'une suite récurrente linéaire d'ordre 2}

L'idée est de chercher des suites géométriques solutions de la récurrence.

\begin{theoreme}
	Soit $u$ une suite récurrente linéaire d'ordre $2$, vérifiant pour tout $n$, $u_{n+2}=au_{n+1}+bu_n$. Alors, la suite géométrique $(\ell^n)$ ($\ell\neq 0$) est solution de la récurrence si, et seulement si, $\ell^2=a\ell+b$.
\end{theoreme}

\ifprof
\begin{demonstration}
Si on a, pour tout $n$, $\ell^{n+2}=a\ell^{n+1}+b\ell^n$, puisque $\ell\neq 0$, en divisant par $\ell^n$ on obtient $\ell^2=a\ell=b$. La réciproque est également vraie.
\end{demonstration}
\else
\lignes{4}
\fi

\begin{theoreme}
	Soit $u$ une suite récurrente linéaire d'ordre $2$, vérifiant pour tout $n$, $u_{n+2}=au_{n+1}+bu_n$. On pose $(E)$ l'équation $X^2=aX+b$, appelée \textbf{équation caractéristique}.
\begin{itemize}
    \item[$\bullet$] Si l'équation $(E)$ possède deux solutions distinctes $q_1$ et $q_2$, alors il existe deux réels $\lambda$ et $\mu$ tels que, pour tout $n$,
    $$u_n=\lambda q_1^n+\mu q_2^n$$
    \item[$\bullet$] Si l'équation $(E)$ possède une racine double $q_0$, alors il existe deux réels $\lambda$ et $\mu$ tels que, pour tout $n$,
    $$u_n=(\lambda n +\mu)q_0^n$$
\end{itemize}
\end{theoreme}

\begin{remarque}
	On utilise les valeurs de $u_0$ et $u_1$ pour déterminer les deux nombres réels $\lambda$ et $\mu$.
\end{remarque}

\begin{methode}
Pour déterminer l'expression d'une suite récurrente linéaire d'ordre deux :
\begin{itemize}
    \item[$\bullet$] On pose l'équation caractéristique $(E)$.
    \item[$\bullet$] On détermine les solutions de l'équation caractéristique.
    \item[$\bullet$] Une fois la (ou les) solution(s) trouvée(s), on écrit $u_n$ sous la forme $\lambda q_1^n + \mu q_2^n$ ou $(\lambda n +\mu)q_0^n$, et on utilise les deux premiers termes pour obtenir un système nous donnant $\lambda$ et $\mu$
\end{itemize}
\end{methode}

\begin{exemple}
	Soit $u$ la suite définie par $u_0=0,~u_1=3$ et pour tout entier $n$, $u_{n+2}=-u_{n+1}+2u_n$. Déterminer l'expression de $u_n$ en fonction de $n$.
\end{exemple}

\solution[10]{
\begin{itemize}
    \item[$\bullet$] Soit $(E)$ l'équation $X^2=-X+2$. Les solutions sont $1$ et $-2$.
    \item[$\bullet$] Il existe donc deux réels $\lambda$ et $\mu$ vérifiant, pour tout $n$,
    $$u_n=\lambda \times 1^n +\mu (-2)^n = \lambda +\mu (-2)^n$$
    \item[$\bullet$] En utilisant $u_0$ et $u_1$, on a donc
    $$\left\{ \begin{array}{rclcl}
         0 &=& \lambda &+& \mu \\
         3 &=& \lambda &+& (-2\mu)
     \end{array}
    \right.$$
    ce qui donne $\mu=-1$ et $\lambda=1$.
    \item[$\bullet$] Conclusion : pour tout entier $n$, on a
    $$u_n=1-(-2)^n$$
\end{itemize}
}

\afaire{Exercices \lienexo{08}, \lienexo{09}, \lienexo{10}, \lienexo{11} et \lienexo{12}.}
