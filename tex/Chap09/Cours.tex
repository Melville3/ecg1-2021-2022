\chapter{Probabilités sur un ensemble fini}
%!TeX root=../../encours.nouveau.tex
%%% Début exercice %%%

\objectifintro{Nous allons dans ce chapitre revoir les notions de base sur les probabilités, vues depuis la classe de troisième, en les rendant plus rigoureuses. Nous revenons également sur les probabilités conditionnelles et la formule des probabilités totales.}

\begin{extrait}{Henri Poincaré (1854 -- 1912)}
	Le nom seul de calcul des probabilités est un paradoxe : la probabilité, opposée à la certitude, c'est ce qu'on ne sait pas, et comment peut-on calculer ce que l'on ne connaît pas ?
\end{extrait}

\begin{objectifs}
	\begin{numerote}
		\item Concernant les généralités sur les probabilités :
		\begin{itemize}
			\item \lienobj{01}{Déterminer des probabilités en situation d'équiprobabilité}
			\item \hyperref[objectif-06-2]{Utiliser la formule du crible de Poincaré}\dotfill $\Box$
			\item \hyperref[objectif-06-3]{Justifier qu'une famille est un système complet d'événements}\dotfill $\Box$
			\item \hyperref[objectif-06-4]{Utiliser les formules des probabilités composées et des probabilités totales}\dotfill $\Box$
			\item \hyperref[objectif-06-5]{Utiliser les formules de Bayes}\dotfill $\Box$
		\end{itemize}
		\item Concernant l'indépendance :
		\begin{itemize}
			\item \hyperref[objectif-06-6]{Démontrer que deux événements sont indépendants}\dotfill $\Box$
			\item \hyperref[objectif-06-7]{Utiliser la mutuelle indépendance d'une famille d'événements}\dotfill $\Box$
		\end{itemize}
	\end{numerote}
\end{objectifs}

\section{Espace probabilisé fini}

\subsection{Introduction}

L'objectif de la théorie des probabilités est d'étudier et modéliser les phénomènes dans lesquels intervient le hasard.

\begin{definition}
	On appelle \textbf{expérience aléatoire} une expérience que l'on peut répéter, dont le résultat ne peut être prévu à l'avance et tel que, lorsqu'on la répète dans des circonstances identiques, ne donne pas forcément le même résultat.
\end{definition}

\begin{exemple}
	Par exemple, le lancer d'un dé, le tirage d'une boule dans une urne, ou d'une carte dans un jeu de cartes sont des expériences aléatoires.
\end{exemple}

\begin{experiencehistorique}
	Historiquement, la théorie des probabilités est apparue pour étudier des jeux de hasard. Elle s'est développée ensuite pour étudier des phénomènes trop complexes pour être analysés de manière classique. On utilise alors le hasard pour simplifier ces problèmes.

	On utilise désormais le hasard pour étudier l'écoulement à travers un matériau poreux (la \textbf{percolation}), le comportant des molécules dans un aimant (le \textbf{modèle d'Ising}), l'évolution d'une population ou d'une espèce, ...
\end{experiencehistorique}

Mathématiquement, nous décrivons une expérience de probabilité à l'aide de trois éléments :
\begin{itemize}
	\item Un ensemble $\Omega$, que l'on appelle \textbf{univers}, qui contient tous les résultats possibles. Par exemple, pour un lancer de dé à $6$ faces, $\Omega=\interent{1 6}$.
	\item Un ensemble de phénomènes, appelés \textbf{événements}, qui peuvent ou non se produire lors de l'expérience. Ainsi, un événement est une partie de $\Omega$. Par exemple, si on lance un dé à $6$ faces, l'événement \og{}obtenir un chiffre impair\fg{} est l'événement $\{1,3,5\}$. Si le dé tombe sur $1$, $3$ ou $5$, on dit que cet événement est \textbf{réalisé}.
	\item Une fonction qui, à tout événement, associe la probabilité de celui-ci. Par exemple, si le dé est bien équilibré, on peut prendre la fonction qui à un événement $\{k\}$ (avec $k\in \interent{1 6}$) donne $\frac16$.
\end{itemize}

Nous allons définir rigoureusement les concepts précédents, pour l'instant dans le cadre d'un univers fini, c'est-à-dire des expériences aléatoires n'ayant qu'un nombre fini de résultats possibles.

\subsection{Espace probabilisable}

\begin{definition}
	Soit $\Omega$ un ensemble \underline{fini}, et $\mathcal{P}(\Omega)$ l'ensemble des parties de $\Omega$. On dit que $(\Omega, \mathcal{P}(\Omega) )$ est un \textbf{espace probabilisable fini}.

	$\Omega$ est appelé l'\textbf{univers}, les éléments de $\mathcal{P}(\Omega)$ sont appelés \textbf{événements}. On dit que l'événement $A\in \partie(\Omega)$ est \textbf{réalisé} si le résultat de l'expérience est un élément de $A$.

	Enfin les événements $\{\omega\}$ (pour $\omega \in \Omega$) sont appelés \textbf{événements élémentaires}.
\end{definition}

\begin{remarque}
	$\partie(\Omega)$ vérifie les propriétés suivantes, que nous reverrons plus tard :
	\begin{itemize}[label=\textbullet]
		\item $\Omega \in \partie(\Omega)$
		\item $\forall~A \in \partie(\Omega),\quad\overline{A} \in \partie(\Omega)$
		\item $\forall, A_1, \hdots, A_n \in \partie(\Omega), A_1\cup\hdots\cup A_n \in \partie(\Omega)$.
	\end{itemize}
\end{remarque}

\begin{remarque}
	L'événement $\vide$ est appelé \textbf{événement impossible}, et l'événement $\Omega$ l'\textbf{événement certain}.
\end{remarque}

\subsection{Opérations sur les événements}

\begin{definition}[Opérations usuelles]
	Soient $A$ et $B$ deux éléments de $\partie(\Omega)$.
	\begin{itemize}
		\item L'évènement $\overline{A}=\complement_{\Omega} A$ est réalisé si et seulement si $A$ n'est pas réalisé. On dit que $\overline{A}$ est \textbf{l'événement contraire} de $A$.
		\item $A\subset B$ signifie que la réalisation de $A$ implique celle de $B$.
		\item L'événement $A\cup B$ est réalisé si et seulement si l'un des événements au moins $A$ ou $B$ est réalisé.
		\item L'événement $A\cap B$ est réalisé si et seulement si les deux événements $A$ et $B$ sont réalisés.
		\item L'événement $A\setminus B$ est réalisé si et seulement si $A$ est réalisé mais pas $B$.
	\end{itemize}
\end{definition}

\begin{exemple}
	On lance un dé à $6$ faces. On note $A=\left \{1,2,3,4\right\}$ et $B=\left \{ 1,3,5\right \}$. $A$ est l'événement \og{}obtenir un nombre inférieur ou égal à $4$\fg{} et $B$ est l'événement \og{}obtenir un nombre impair\fg{}.

	Alors $A\cup B$ est l'événement $\left \{ 1,2,3,4,5\right \}$ à savoir l'événement \og{}obtenir un nombre inférieur ou égal à $5$\fg{}, $A\cap B=\left \{1, 3\right \}$ et $A\setminus B = \{2, 4\}$.

	Si on lance deux fois le dé en question, l'univers devient $\Omega = \interent{1 6}^2$. Si on note $A$ l'événement \og{}ne pas obtenir de $6$ sur les deux lancers\fg{}, l'événement $\overline{A}$ est l'événement \og{}obtenir au moins un $6$\fg{}.
\end{exemple}

\begin{exo}
	On lance successivement $n$ pièces de monnaies. On note $\Omega=\{P, F\}^n$ l'univers obtenu (où on note $P$ pour pile et $F$ pour face). Pour tout $k\in \interent{1 n}$, on note $A_k$ l'événement \og{}obtenir pile au $k$-ième lancer\fg{}.

	Exprimer les événements \og{}n'obtenir que des faces\fg{} et \og{}obtenir pile pour la première fois au $k$-ième lancer\fg{} (avec $k\in \interent{1 n}$) en fonction des $(A_i)_{i\in \interent{1 n}}$.
\end{exo}

\solution[4]{L'événement \og{}n'obtenir que des faces\fg{} est $\ds{\bigcap_{k=1}^n \overline{A_k} }$ et l'événement \og{}obtenir pile pour la première fois au $k$-ième lancer\fg{} (avec $k\in \interent{1 n}$) s'écrit \[ \left(\bigcap_{i=1}^{k-1} \overline{A_i}\right)\cap A_k \]
}

\begin{definition}[Incompatibilité]
	Deux événements $A$ et $B$ de l'espace probabilisable $(\Omega, \mathcal{P}(\Omega))$ sont dits \textbf{incompatibles} si \\$A\cap B=\vide$.
\end{definition}

Toutes les propriétés vues sur les ensembles sont bien évidemment valables, dont les lois de de Morgan et les distributivités.

\subsection{Loi de probabilité}

\begin{definition}
	Soit $(\Omega, \partie(\Omega))$ un espace probabilisable. On appelle \textbf{probabilité} sur $(\Omega, \partie(\Omega))$ toute application $\PP : \partie(\Omega) \rightarrow \R+$ vérifiant :
	\begin{itemize}
		\item $\PP(\Omega)=1$
		\item Pour tous événements incompatibles $A$ et $B$, $\PP(A\cup B)=\PP(A)+\PP(B)$.
	\end{itemize}
\end{definition}

\begin{remarque}
	Le triplet $(\Omega, \partie(\Omega), \PP)$ est alors appelé \textbf{espace probabilisé fini}, et pour tout $A \in \partie(\Omega)$, $\PP(A)$ s'appelle la \textbf{probabilité} de l'événement $A$.
\end{remarque}

\begin{attention}
	Si $\omega\in \Omega$, $\PP(\omega)$ n'a pas de sens, puisque $\PP$ est définie sur $\partie(\Omega)$. On devra écrire $\PP(\{\omega\})$.
\end{attention}

\begin{propriete}
	Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini, et $A, B$ deux événements. Alors, on a :
	\begin{itemize}
		\item $\PP(\overline{A})=1-\PP(A)$. Ainsi, $\PP(\vide)=1-\PP(\Omega)=0$.\vspace*{1mm}
		\item $0\leq \PP(A)\leq 1$.
		\item $\PP(B\setminus A) = \PP(B)- \PP(A\cap B)$.\vspace*{1mm}
		\item $\PP(B) = \PP(A\cap B) + \PP(\overline{A}\cap B)$.\vspace*{1mm}
		\item Si $A \subset B$ alors $\PP(A)\leq \PP(B)$.\vspace*{1mm}
		\item $\PP(A\cup B) = \PP(A)+\PP(B)-\PP(A\cap B)$.
	\end{itemize}
\end{propriete}

\ifprof
	\begin{demonstration}
		\begin{itemize}
			\item Par définition, $A$ et $\overline{A}$ sont incompatibles, et $A\cup \overline{A}= \Omega$. Donc par définition
			      \[1=\PP(\Omega)=P(A\cup \overline{A})=\PP(A)+\PP(\overline{A})\]
			\item Par définition de $\PP$, $\PP(A)\geq 0$. De plus, $\PP(\overline{A})\geq 0$ donc $\PP(A)=1-\PP(\overline{A}) \leq 1$.
			\item $B\setminus A$ et $A\cap B$ sont incompatibles, et de réunion $B$. Ainsi \[ \PP(B)=\PP(B\setminus A)+\PP(A\cap B). \]
			\item $A\cap B$ et $\overline{A}\cap B$ sont incompatibles, et $(A \cap B) \cup (\overline{A}\cap B) = B$. Par définition,
			      \[\PP(B) = \PP((A \cap B) \cup (\overline{A}\cap B) =\PP(A \cap B) + \PP(\overline{A}\cap B) \]
			\item Si $A\subset B$, on peut écrire $B = A \cup (B\cap \overline{A})$. Les événements $A$ et $B\cap \overline{A}$ étant incompatibles, par définition
			      \[\PP(B)=\PP(A \cup (B\cap \overline{A}))=\PP(A)+\PP(B\cap \overline{A})\geq \PP(A)\]
			\item On constate que $A\cup B = A \cup (B\cap \overline{A})$. Puisque $A$ et $B\cap \overline{A}$ sont incompatibles, par définition
			      \[\PP(A\cup B)= \PP(A \cup (B\cap \overline{A}))=\PP(A)+\PP(B\cap \overline{A})\underbrace{=}_{\textrm{d'après pt 2}} \PP(A)+\PP(B)-\PP(A\cap B)\]
		\end{itemize}
	\end{demonstration}
\else
	\lignes{10}
\fi

\begin{propriete}[Crible de Poincaré]
	\label{objectif-06-2}
	Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Si $A_1,\hdots,A_n$ sont des événements deux à deux incompatibles ($k\geq 1$), on a \[\PP\left(\bigcup_{k=1}^n A_k\right)=\sum_{k=1}^n \PP(A_k)\]
	Et si $A,B$ et $C$ sont trois événements quelconques, on a
	\[\PP(A\cup B\cup C) = \PP(A)+\PP(B)+\PP(C)-\PP(A\cap B) -\PP(A \cap C) -\PP(B\cap C)+\PP(A\cap B \cap C)\]
\end{propriete}

\begin{demonstration}
	Ce résultat se démontre par récurrence sur $n$.

	Concernant le cas plus général, le cas $n=2$ est vu dans la propriété précédente (point 6). Pour le cas $n=3$, prenons $3$ événements $A, B$ et $C$. On peut écrire $A\cup B \cup C = \left((A\cup B) \setminus C\right) \cup C$ et par incompatibilité :
	\[ \PP(A\cup B\cup C = \PP((A\cup B)\setminus C) + \PP(C) \]
	D'après les propriétés (point 3) :
	\[ \PP((A\cup B)\setminus C) = \PP(A\cup B) - \PP((A\cup B)\cap C).\]
	En utilisant les distributivités et le cas $n=2$ :
	\begin{align*}
		\PP(A\cup B\cup C) &= \PP(A\cup B) - \PP( (A\cap C)\cup(B\cap C)) + \PP(C)\\
		&= \PP(A)+\PP(B)-\PP(A\cap B)  - \left( \PP(A \cap C) + \PP(B\cap C) - \PP((A\cap C)\cap(B\cap C)) \right) +\PP(C)\\
		&= \PP(A)+\PP(B)-\PP(A\cap B)- \PP(A\cap C)- \PP(B\cap C) + \PP(A\cap B\cap C) +\PP(C)
	\end{align*}

\end{demonstration}

\begin{remarque}
	Il existe un crible de Poincaré plus général : pour tous événements $(A_1,\hdots, A_n$), on a
	\[\PP\left(\bigcup_{k=1}^n A_k\right)=\sum_{k=1}^n \PP(A_k) - \sum_{i<j} \PP(A_i\cap A_j) + \sum_{i< j < k} \PP(A_i\cap A_j \cap A_k) \hdots + (-1)^{n+1} \PP(A_1\cap A_2\cap \hdots \cap A_n)\]
\end{remarque}

\subsection{Loi équiprobable}

\begin{definition}
	\label{objectif-06-1}
	Dans le cas où toutes les issues ont la même probabilité, on dit qu'elles sont \textbf{équiprobables}, ou que la loi de probabilité $\PP$ est \textbf{uniforme}.\\
	Si $\Omega=\{x_1;\hdots;x_n\}$, alors la probabilité de chaque issue est \[p=\frac{1}{\card(\Omega)}=\frac{1}{n}\]
\end{definition}

\begin{theoreme}
	Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini en situation d'équiprobabilité. Alors la probabilité d'un événement $A$ est donnée par
	\[\PP(A)=\frac{\card(A)}{\card(\Omega)}=\frac{\textrm{nombre de cas favorables}}{\textrm{nombre de cas possibles}}\]
\end{theoreme}

\begin{exo}
	On lance un dé à 12 faces bien équilibré. On note $A$ l'événement ``obtenir un multiple de 3'' et $B$ l'événement ``obtenir un multiple de 4''. Déterminer $\PP(A)$ et $\PP(B)$.
\end{exo}

\ifprof
	\begin{soluce}
		Puisque le dé est bien équilibré, la loi de probabilité est uniforme. Ainsi, $\Omega=\llbracket 1;12\rrbracket$ et $\Card(\Omega)=12$. \\
		Puisque $A=\{3;6;9;12\}$ et $B=\{4;8;12\}$, on a
		\[\PP(A)=\frac{4}{12}=\frac{1}{3} \textrm{  et  } \PP(B)=\frac{3}{12}=\frac{1}{4}\]
	\end{soluce}
\else
	\lignes{4}
\fi

\afaire{Exercice \lienexo{01}.}

\section{Probabilités conditionnelles}

\subsection{Exemple}

Dans un sac, on possède $10$ jetons : $6$ jetons rouges, numérotés $1,1,1,2,2,4$ et quatre jetons verts, numérotés $2,2,4,4$.
On tire au hasard un jeton du sac.

On note $R$ l'événement ``obtenir un jeton rouge'', $V$ l'événement ``obtenir un jeton vert'', $1$ l'événement ``obtenir un jeton $1$'', ...

Cette expérience peut être représentée par un \textbf{arbre pondéré} :

\begin{center}
	\includegraphics{arbre.mps}
\end{center}
%\begin{center}
%	\includegraphics[width=10cm]{tex/Chap06/pic/arbres_pond}
%\end{center}

De cet arbre, on peut lire ainsi que $\PP(R)=\frac{6}{10}$, $\PP(V)=\frac{4}{10}$.

La branche $ - R - 2$ indique que l'on a obtenu un jeton rouge \textbf{et} numéroté $2$.

\begin{propriete}
	\begin{itemize}
		\item Loi des noeuds : La somme des probabilités inscrites sur les branches issues d'un même noeud est égale à $1$.
		\item La probabilité de l'événement représenté par un chemin est égale au produit des probabilités inscrites sur les branches de ce chemin.
	\end{itemize}
\end{propriete}
%

Ainsi, la probabilité de $R \cap 2$ est égale à \[\PP(R\cap 2)= \frac{6}{10} \times \frac{1}{3} = \PP(R) \times \PP_R(2)\]

\subsection{Probabilités conditionnelles}

\begin{definition}
	Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soit $A$ un événement de probabilité non nulle. Alors, l'application $\PP_A$ définie sur $\partie(\Omega)$ par \[\forall~B\in\partie(\Omega),~\PP_A(B)=\frac{\PP(A\cap B)}{\PP(A)}\] est une probabilité sur $(\Omega, \partie(\Omega))$ appelée \textbf{probabilité sachant} $A$.
\end{definition}

\preuve[10]{Il faut démontrer que $\PP_A$ est une probabilité
	\begin{itemize}
		\item Tout d'abord, pour tout $B\in \partie(\Omega)$, $\PP_A(B)\geq 0$ par définition, et puisque $A\cap B \subset A$, on a $\PP(A\cap B)\leq \PP(A)$ et finalement  \vspace*{3mm}$\ds{\PP_A(B)=\frac{\PP(A\cap B)}{\PP(A)} \leq \frac{\PP(A)}{\PP(A)} =1}$.
		\item $\ds{\PP_A(\Omega) = \frac{\PP(A\cap \Omega)}{\PP(A)} = \frac{\PP(A)}{\PP(A)}=1}$.
		\item Soient $B$ et $C$ deux événements incompatibles. Par distributivité :
		      \begin{align*}
			      \PP_A(B\cup C) & = \frac{\PP(A\cap(B\cup C))}{\PP(A)}        \\
			                     & = \frac{\PP((A\cap B)\cup(A\cap C)}{\PP(A)} \\
			                     & = \frac{\PP(A\cap B)+ \PP(A\cap C)}{\PP(A)}
		      \end{align*}
		      puisque $(A\cap B)\cap(A\cap C)=A\cap B\cap C=\vide$. Ainsi, $\ds{\PP_A(B\cup C)=\PP_A(B)+\PP_A(C)}$.
	\end{itemize}

}

\begin{remarque}
	$\PP_A(B)$ est également notée $\PP(B|A)$. Puisque $\PP_A$ est une probabilité sur $(\Omega, \partie(\Omega))$, toutes les propriétés des probabilités s'appliquent à $\PP_A$.
\end{remarque}

\begin{exemple}
	Dans l'exemple précédent, $\PP_V(2)=\frac{1}{2}$.
\end{exemple}

\begin{remarque}
	Dans la suite du cours, et plus généralement tout au long de l'année, on essaiera de se passer des arbres. D'une part, parce qu'il est plus important de comprendre les formules sous-jacentes; et d'autre part parce que, souvent, les expériences aléatoires que l'on fera ne se modéliseront pas facilement avec un arbre.
\end{remarque}

\subsection{Formule des probabilités composées}

\begin{theoreme}[Probabilités composées]
	On utilise souvent les probabilités conditionnelles pour déterminer la probabilité de l'intersection : $\PP(A\cap B)=\PP(A) \times \PP_A(B)$.

	On peut d'ailleurs généraliser cette formule : pour tous événements $A_1,\hdots,A_n$, tels que\\ $\PP(A_1\cap \hdots \cap A_n)\neq 0$, on a
	\[\PP(A_1\cap\hdots \cap A_n) = \PP(A_1) \PP_{A_1}(A_2) \PP_{A_1\cap A_2}(A_3)\hdots \PP_{A_1\cap \hdots \cap A_{n-1}}(A_n)\]
\end{theoreme}

\preuve[15]{Tout d'abord, on constate que pour tout $k\in \interent{1 n}$, on a $A_1\cap \hdots\cap A_n \subset A_1\cap \hdots \cap A_k$, donc \[ 0 < \PP(A_1\cap \hdots \cap A_n) \leq \PP(A_1\cap \hdots \cap A_k) \]
	et les probabilités conditionnelles intervenant dans la formule sont bien définies.

	On démontre celle-ci par récurrence sur $n\geq 2$.
	\begin{itemize}
		\item Pour $n=2$, on a bien $\PP_{A_1}(A_2)\PP(A_1)=\PP(A_1\cap A_2)$ par définition de la probabilité conditionnelle.
		\item Soit $n\geq 2$. On suppose que la formule est vraie au rang $n$. On se donne $A_1,\hdots, A_n, A_{n+1}$ dans $\partie(\Omega)$ tels que $\PP(A_1\cap\hdots\cap A_{n+1}) \neq 0$. Par définition des probabilités conditionnelles :
		      \[ \PP(A_1\cap \hdots \cap A_{n+1}) = \PP(A_1\cap \hdots \cap A_n) \PP_{A_1\cap \hdots \cap A_n}(A_{n+1}) \]
		      On applique alors l'hypothèse de récurrence à $\PP(A_1\cap \hdots \cap A_n)$ et on obtient \[ \PP(A_1\cap \hdots \cap A_{n+1}) = \PP(A_1)\PP_{A_1}(A_2)\times \hdots \times \PP_{A_1\cap \hdots \cap A_{n-1}}(A_n)\PP_{A_1\cap \hdots \cap A_n}(A_{n+1})\]
		      la formule est alors vraie au rang $n+1$.
	\end{itemize}
}

\begin{exemple}
	Toujours dans l'exemple précédent, $\PP(R\cap 4)=\PP(R)\times \PP_R(4)=\frac{6}{10}.\frac{1}{6}=\frac{1}{10}$.
\end{exemple}

\begin{remarque}
	Cette formule est très importante. C'est celle qu'on utilisera souvent pour déterminer la probabilité d'une intersection.
\end{remarque}

\begin{exo}
	On dispose d'une urne qui contient $10$ boules, dont $6$ rouges et $4$ bleues. On tire successivement $3$ boules avec les règles suivantes :
	\begin{itemize}
		\item Si on tire une boule rouge, on la retire.
		\item Si on tire une boule bleue, on la remplace par une boule rouge.
	\end{itemize}
	Quelle est la probabilité de tirer une boule bleue, une boule rouge puis une boule bleue dans cet ordre ?
\end{exo}

\solution[10]{Pour tout $n\in \interent{1 3}$, on note $B_n$ l'événement \og{}obtenir une boule bleue au $n$-ième tirage\fg{}. La probabilité que l'on souhaite calculer est $\PP(B_1\cap \overline{B_2}\cap B_3)$. On applique la formule des probabilités composées :
	\[ \PP(B_1\cap \overline{B_2}\cap B_3) = \PP(B_1)\PP_{B_1}(\overline{B_2})\PP_{B_1\cap\overline{B_2}}(B_3). \]
	On utilise les règles de l'énoncé : $\PP(B_1) = \frac{4}{10}$. Puis $\PP_{B_1}(\overline{B_2})=\frac{7}{10}$ : en effet, on a obtenu une bleue au premier tirage, on a donc remplacée celle-ci par une boule rouge et il y a donc $7$ rouges et $3$ bleue avant le tirage. Enfin, et de même, $\PP_{B_1\cap \overline{B_2}}(B_3) = \frac{3}{9}$ : en effet, on a obtenu une bleue puis une rouge, on a donc enlevé la rouge suite au deuxième tirage, et il y a donc $3$ bleues et $9$ boules au total.

	Finalement, $\PP(B_1\cap \overline{B_2}\cap B_3) = \frac{2}{5}\times \frac{7}{10}\times \frac{3}{9}=\frac{14}{150}$.
}

\subsection{Formule de Bayes}

\begin{theoreme}[Formule de Bayes]
	\label{objectif-06-5}
	Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A$ et $B$ deux événements de probabilité non nulle. Alors
	\[\PP_A(B)= \frac{\PP(B)}{\PP(A)}\PP_B(A) \textrm{ et }
		\PP(B)=\frac{\PP(A)\PP_A(B)}{\PP_B(A)}
	\]
\end{theoreme}

\ifprof
	\begin{demonstration}
		En effet, \[\frac{\PP(B)}{\PP(A)}\PP_B(A)=\frac{\PP(B)}{\PP(A)}\frac{\PP(B\cap A)}{\PP(B)} = \frac{\PP(B\cap A)}{\PP(A)}=\PP_A(B)\]
	\end{demonstration}
\else
	\lignes{3}
\fi

\begin{exemple}
	Dans l'exemple du début, la probabilité, sachant qu'on a eu un jeton $2$, que celui-ci soit vert, vaut
	\[\PP_2(V)= \frac{\PP(V)}{\PP(2)}\PP_V(2)=\frac{\frac{4}{10}}{\frac{4}{10}}\frac{1}{2}=\frac{1}{2}\]
\end{exemple}

\section{Probabilités totales}

\subsection{Système complet d'événements}

\begin{definition}
	\label{objectif-06-3}
	Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $(A_1,\hdots,A_n)$ une famille d'événements. On dit que cette famille est un \textbf{système complet d'événements} si
	\begin{itemize}
		\item les événements $A_1,\hdots,A_n$ sont deux à deux incompatibles ($\forall~i\neq j, A_i\cap A_j=\vide$).
		\item $\ds{\bigcup_{k=1}^n A_k=\Omega}$.
	\end{itemize}
\end{definition}

\begin{exemple}
	On lance un dé à $6$ faces, et on note $A:$``obtenir un nombre pair'', et $B:$``obtenir un nombre impair''. Alors $\Omega= \llbracket 1,6 \rrbracket$, $A\cup B=\Omega$ et $A\cap B=\vide$. Donc $(A,B)$ forme un système complet d'événements.
\end{exemple}

\begin{remarque}
	Si $A$ est un événement de $(\Omega, \partie(\Omega), \PP)$, alors $(A,\overline{A})$ est un système complet de deux événements.

	De même, si $\Omega=\left \{ \omega_1,\hdots,\omega_n\right\}$, alors la famille $\left(\left\{ \omega_i\right\}\right)_{i\in \interent{1 n}}$ est un système complet d'événements.
\end{remarque}

\subsection{Formules des probabilités totales}

\begin{theoreme}
	\label{objectif-06-4}
	Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soit $(A_1,\hdots,A_n)$ un système complet d'événements de $\Omega$, tel que pour tout $i\in \llbracket 1,n \rrbracket, \PP(A_i)\neq 0$. Alors, pour tout événement $B$, on a
	\[\PP(B)=\sum_{k=1}^n \PP(B\cap A_k)=\sum_{k=1}^n \PP(A_k) \PP_{A_k}(B)\]
\end{theoreme}

\ifprof
	\begin{demonstration}
		On peut écrire \[B=B\cap \bigcup_{k=1}^n A_k = \bigcup_{k=1}^n B\cap A_k\]
		Or les événements $B\cap A_k$ sont des événements deux à deux incompatibles, puisque les $A_k$ le sont. Donc
		\[\PP(B)= \PP\left(\bigcup_{k=1}^n B\cap A_k\right)=\sum_{k=1}^n \PP(B\cap A_k)\]
	\end{demonstration}
\else
	\lignes{6}
\fi

\begin{propriete}
	Dans un arbre pondéré, la probabilité d'un événement $E$ est la somme des probabilités des chemins qui aboutissent à $E$.
\end{propriete}

\begin{exemple}
	Dans l'exemple de début, $(R,V)$ est un système complet d'événements. La probabilité de l'événement $2$ vaut donc, d'après la formule des probabilités totales,
	\begin{align*}
		\PP(2) & =\PP(R\cap 2)+\PP(V\cap 2)                     \\
		       & =\PP_R(2)\times \PP(R) + \PP_V(2)\times \PP(V) \\&=\frac{1}{3}.\frac{6}{10}+\frac{1}{2}.\frac{4}{10}=\frac{2}{5}
	\end{align*}
\end{exemple}

\afaire{Exercices \lienexo{11} et \lienexo{12}.}

\begin{consequence}
	Si $(A_1, \hdots, A_n)$ est un système complet d'événements, alors \[ \sum_{k=1}^n \PP(A_k) = 1 \]
\end{consequence}

\begin{demonstration}
	Il suffit de prendre $B=\Omega$ dans la relation précédente.
\end{demonstration}

\begin{consequence}[Cas des événéments élémentaires]
	Soit $\Omega=\left \{ \omega_1,\hdots,\omega_n\right \}$ avec $n=\card(\Omega)$. Alors, pour tout $A\in \partie(A)$ :
	\[ \PP(A) = \sum_{k=1}^n \PP(A\cap \{\omega_k\}) = \sum_{\omega \in A} \PP(\{ \omega \}) \]
	En particulier, $\ds{\sum_{k=1}^n \PP(\{\omega_k\})=1}$.
\end{consequence}

\subsection{Construction de probabilités}

\begin{theoreme}
	Soit $\Omega=\left \{\omega_1,\hdots,\omega_n\right \}$ avec $n=\card(\Omega)$. Soient $p_1,\hdots,p_n$ des réels positifs tels que $\ds{\sum_{k=1}^n p_k=1}$. Alors il existe une unique probabilité sur $(\Omega, \partie(\Omega))$ telle que, pour tout $k\in \interent{1 n}$, $\PP(\{\omega_k\})=p_k$.
\end{theoreme}

\preuve[10]{On définie $\PP:\partie(\Omega)\to \R^+$ de la manière suivante : \[ \PP: A \mapsto \sum_{\substack{1\leq k\leq n\\\omega_k\in A}} p_k. \]
	Il s'agit bien d'une application de $\partie(\Omega)$ dans $\R+$, les réels étant positifs.

	On a, par définition de $\PP$, $\ds{\PP(\Omega)=\sum_{1\leq k\leq n} p_k=1}$ par hypothèse sur les $(p_k)$. Soient alors $A$ et $B$ deux événements disjoints. On a alors
	\begin{align*}
		\PP(A\cup B) & = \sum_{\substack{1\leq k\leq n \\\omega_k\in A\cup B}} pk \\
		             & = \sum_{\substack{1\leq k\leq n \\\omega_k\in A}} p_k + \sum_{\substack{1\leq k \leq n \\ \omega_k\in B}} \text{ sommation par paquet car ils sont disjoints}\\
		             & = \PP(A) + \PP(B)
	\end{align*}
	L'unicité est garantie par la conséquence précédente, qui nous garantit l'unicité de l'écriture $\PP(A)$ pour tout $A$ en fonction des $\{\omega_k\}$ et donc des $(p_k)$.
}

\subsection{Formule de Bayes et probabilités totales}

Si $A$ et $B$ sont deux événements, tels que $\PP(A)\in \interoo{0 1}$ et $\PP(B)\neq 0$, la formule de Bayes peut s'écrire également

\[ \PP_B(A) = \frac{\PP_A(B)\PP(A)}{\PP_A(B)\PP(A) + \PP_{\overline{A}}(B)\PP(\overline{A})} \]

De manière plus général, si $(A_1,\hdots, A_n)$ est un système complet d'événements tel que, pour tout $k\in \interent{1 n}$, $\PP(A_k)\neq 0$, alors pour tout  événement $B$ tel que $\PP(B)\neq 0$ alors

\[ \forall~i\in \interent{1 n},\quad \PP_B(A_i) = \frac{\PP_{A_i}(B)\PP(A_i)}{\sum\limits_{k=1}^n \PP_{A_k}(B)\PP(A_k)} \]

\begin{exemple}[Fiabilité d'un test]
	On réalise un test sanguin pour détecter une maladie dont la probabilité d'occurence est de $0,5\%$. Si le patient est malade, le test détecte la maladie dans $95\%$ des cas. De plus, ce test déclare malade (à tord) $1\%$ des personnes saines.

	Quelle est la probabilité qu'un patient soit malade sachant que le test est positif ?
\end{exemple}

\begin{remarque}
	Lorsqu'on dispose d'un test, on s'intéresse à deux éléments importants : la sensibilité, et la spécificité.
	\begin{itemize}
		\item La sensibilité d'un test est la probabilité que le test soit positif sachant que le patient est malade. Plus la sensibilité est proche de $1$, plus on a la garantie que le test détecte les patients malades; ici, la sensibilité est de $0,95$.
		\item La spécificité d'un test est la probabilité que le test soit négatif sachant que le patient est sain. Plus il est proche de $1$, plus on est sûr de ne pas détecter de faux positif. Ici, la spécificité vaut $0,99$.
	\end{itemize}
\end{remarque}

\solution[7]{
On introduit deux ensemble : $T$ l'événement \og{}le test est positif\fg{} et $M$ l'événement \og{}le patient est malade\fg{}.

On nous donne, dans l'énoncé, les valeurs suivante :
\[ \PP(M) = 0,005,\quad \PP_M(T)=0,95 \qeq \PP_{\overline{M}}(T)=0,01 \]
On cherche $\PP_T(M)$. On applique la formule de Bayes et la formule des probabilités totales :
\begin{align*}
 \PP_T(M) &= \frac{\PP_M(T)\PP(M)}{\PP(T)} \\
 &= \frac{\PP_M(T)\PP(M)}{\PP_M(T)\PP(M)+\PP_{\overline{M}}(T)\PP(\overline{M})}\\
 &= \frac{0,95 \times 0,005}{0,95\times 0,005 + 0,01\times (1-0,005)} \approx 0,323
\end{align*}
Ainsi, ce test n'est pas très fiable.
}

\section{Indépendance de deux évènements}

\subsection{Indépendance de deux événements}

\begin{definition}
	Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A$ et $B$ deux événements. On dit que $A$ et $B$ sont \textbf{indépendants} lorsque $\PP(A\cap B)=\PP(A)\times \PP(B)$.
\end{definition}

\begin{exemple}
	Dans l'exemple du II., les événements $V$ et $R$ sont indépendants.
\end{exemple}

\begin{propriete}
	\label{objectif-06-6}
	Si $A$ et $B$ sont des événements de probabilité non nulle, il y a équivalence entre les propositions suivantes :
	\begin{itemize}
		\item $A$ et $B$ sont indépendants.
		\item $\PP_A(B)=\PP(B)$
		\item $\PP_B(A)=\PP(A)$
	\end{itemize}
\end{propriete}

\begin{theoreme}
	Soient deux évènements  indépendants $A$ et $B$. Alors $\overline{A}$ et $B$ sont aussi indépendants.
\end{theoreme}

\ifprof
	\begin{demonstration}
		Puisque $A$ et $B$ sont indépendants, on a $\PP(A\cap B)=\PP(A)\PP(B)$.  Or, d'après la formule des probabilités totales :
		\[\PP(B)=\PP(A\cap B)+\PP(\overline{A}\cap B)=\PP(A)\PP(B)+\PP(\overline{A}\cap B)\]
		On a donc $\PP(\overline{A}\cap B)=\PP(B)-\PP(A)\PP(B)=(1-\PP(A))\PP(B)=\PP(\overline{A})\PP(B)$.
	\end{demonstration}
\else
	\lignes{5}
\fi

\subsection{Indépendance d'une famille d'événements}

\begin{definition}
	\label{objectif-06-7}
	Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A_1,\hdots, A_n$ des événements.
	\begin{itemize}
		\item On dit que $A_1,\hdots,A_n$ sont \textbf{deux à deux indépendants} pour la probabilité $\PP$ si
		      \[\forall~(i,j)\in \llbracket 1,n \rrbracket, i\neq j, ~\PP(A_i\cap A_j)=\PP(A_i)\PP(A_j)\]
		\item On dit que $A_1, \hdots, A_n$ sont \textbf{mutuellement indépendants} pour la probabilité $\PP$ si
		      \[\forall~I \subset \llbracket 1,n \rrbracket,~ \PP\left(\bigcap_{i\in I} A_i\right) = \prod_{i\in I} \PP(A_i)\]
		      En particulier, $\PP(A_1 \cap \hdots \cap A_n)=\PP(A_1)\times\hdots \times\PP(A_n)$.
	\end{itemize}
\end{definition}

\begin{proposition}
	Soient $A, B$ et $C$ trois événements mutuellement indépendants. Alors
\begin{itemize}
	\item $A$ et $B\cap C$ sont indépendants;
	\item $A$ et $B\cup C$ sont indépendants.
\end{itemize}
\end{proposition}

\preuve[7]{
\begin{itemize}
	\item $\PP(A\cap B\cap C) = \PP(A)\PP(B)\PP(C) = \PP(A)\PP(B\cap C)$ en utilisant la mutuelle indépendance. Ainsi, $A$ et $B\cap C$ sont indépendants.
	\item En utilisant la distributivité et la mutuelle indépendance :
	\begin{align*}
		\PP(A\cap(B\cup C)) &= \PP((A\cap B) \cup (A\cap C)) \\
		&= \PP(A\cap B) + \PP(A\cap C) - \PP( (A\cap B) \cap (A\cap C)) \text{ par la formule du crible}\\
		&= \PP(A\cap B) + \PP(A\cap C)- \PP(A\cap B\cap C)\\
		&= \PP(A)\PP(B) + \PP(A)\PP(C) - \PP(A)\PP(B)\PP(C) \\
		&= \PP(A)\left( \PP(B)+\PP(C)-\PP(B)\PP(C)\right)\\
		&= \PP(A)\left( \PP(B)+\PP(C)-\PP(B\cap C)\right)=\PP(A)\PP(B\cup C)
	\end{align*}
\end{itemize}
}

\begin{theoreme}[Théorème des coalitions]
	Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A_1,\hdots, A_n$ des événements mutuellement indépendants pour la probabilité $\PP$. Soit $B_1,\hdots, B_n$ des événements tels que, pour tout $i \in \llbracket 1,n \rrbracket$, $B_i=A_i \textrm{ ou } \overline{A_i}$. Alors les événements $B_1,\hdots, B_n$ sont encore mutuellement indépendants.
\end{theoreme}

\afaire{Exercices \lienexo{21} et \lienexo{22}.}
