\chapter{Applications linéaires}
%!TeX root=../../encours.nouveau.tex

\objectifintro{Ce chapitre est très important et tombe régulièrement au concours. Il est abstrait, mais pas difficile. Il sera approfondi l'année prochaine. \\Il doit être maitrisé dans son ensemble.}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Laurence Tardieu (1972 -- ). \emph{Le Jugement de Léa}}
	Durant des années le temps paraît linéaire, malgré les écueils, les soubresauts, les détours. Jusqu'au jour où une chute plus profonde fait voler en éclats les moindres repères.
\end{extrait}

\begin{objectifs}
	\begin{numerote}
		\item \lienobj{1}{Savoir montrer qu'une application est linéaire}
	%	\item \lienobj{2}{Savoir déterminer la matrice d'une application linéaire de $\MM_{n,1}(\R)$ dans $\MM_{p,1}(\R)$}
		\item \lienobj{3}{Savoir déterminer le noyau d'une application linéaire}
		\item \lienobj{4}{Savoir déterminer l'image d'une application linéaire}
		\item \lienobj{5}{Savoir démontrer qu'une application linéaire est injective, surjective, bijective}
	\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

\section{Applications linéaires}

\subsection{Définition}

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels. On appelle \textbf{application linéaire} de $E$ dans $F$ toute application $f:E\rightarrow F$ telle que
\begin{itemize}
  \item $\forall~(x,y)\in E^2, f(x+y)=f(x)+f(y)$
  \item $\forall~x\in E,\forall~\lambda\in \R, f(\lambda x)=\lambda f(x)$
\end{itemize}
\end{definition}

\begin{exemple}
	Soit $f:\R^2\to \R^2$ définie par
	\[ f: (x,y)\donne (x-y,x+y). \]
	Alors $f$ est une application linéaire.
\end{exemple}

\solution[7]{Soient $a=(x,y)$ et $b=(x',y')$ deux éléments de $\R^2$, et $\lambda \in \R$. Alors :
{\allowdisplaybreaks
\begin{align*}
	f(a+b) &= f(x+x', y+y') \\
	       &= ( (x+x')-(y+y'),(x+x')+(y+y'))\\
				 &= ( x-y + x'-y', x+y + x'+y') \\
				 &= (x-y, x+y) + (x'-y', x'+y') = f(a)+f(b)
\end{align*}
et
\begin{align*}
	f(\lambda a) &= f( \lambda x, \lambda y) \\
							 &= ( \lambda x - \lambda y, \lambda x +\lambda y)\\
							 &= \lambda (x-y, x+y) = \lambda f(a).
\end{align*}}
Ainsi, $f$ est linéaire.
}

\begin{notation}
L'ensemble des applications linéaires de $E$ dans $F$ est noté $\mathcal{L}(E,F)$.
\end{notation}

\begin{definition}
\begin{itemize}
  \item Si $F=E$, et si $f:E\rightarrow E$ est une application linéaire, on dit que $f$ est un \textbf{endomorphisme}.
  \item Si $F=\R$, et si $f\in \mathcal{L}(E,\R)$, on dit que $f$ est une \textbf{forme linéaire}. L'ensemble des formes linéaires sur $E$ est appelé le \textbf{dual} de $E$, et est noté $E^*=\mathcal{L}(E,\R)$.
\end{itemize}
\end{definition}

\subsection{Propriétés}

\begin{propriete}
Soient $E$ et $F$ deux espaces vectoriels, et $f:E\rightarrow F$ une application linéaire. Alors $f(0_E)=0_F$.
\end{propriete}

\preuve[3]{Si $f$ est linéaire, alors pour tous $x$ et $y$ dans $E$, $f(x+y)=f(x)+f(y)$. Prenons $x=y=0_E$. Alors $f(0_E+0_E)=f(0_E)+f(0_E)$ et donc $f(0_E)=0_F$.
}

\begin{proposition}
	Soient $E$ et $F$ deux espaces vectoriels, et $f:E\to F$ une application.

	Les trois propositions suivantes sont équivalentes :
\begin{enumerate}
		\item $f$ est linéaire de $E$ dans $F$.
		\item Pour tous $n \in \setminus\{0,1\}$, $\left(x_{1}, \hdots, x_{n}\right) \in E^{n}$ et $\left(\lambda_{1}, \hdots, \lambda_{n}\right) \in \R^n$
\[
f\left(\sum_{i=1}^{n} \lambda_{i} x_{i}\right)=\sum_{i=1}^{n} \lambda_{i} f\left(x_{i}\right)
\]
		\item Pour tous $(x, y) \in E^{2}$ et $\lambda \in \R$, $f(\lambda x+y)=\lambda f(x)+f(y)$.
\end{enumerate}
\end{proposition}

\preuve[10]{Nous allons montrer que $1\implies 2 \implies 3 \implies 1$ ce qui garantira l'équivalence.
\begin{itemize}
	\item L'implication $1 \Rightarrow 2$ se montre par récurrence sans difficultés.
	\item Supposons que le point $2$ est vrai. En prenant $n=2, x_{1}=x, x_{2}=y, \lambda_{1}=\lambda$ et $\lambda_{2}=1$, on obtient le point 3. Ainsi $2 \Rightarrow 3$.
	\item Supposons que le point $3$ est vrai. On montre les deux propriétés en deux temps :
		\begin{itemize}
				\item Si $(x, y) \in E^{2}$, on applique la propriété $3$ en prenant $\lambda=1$; on obtient $f(x+y)=f(x)+f(y)$.
				\item Si $x\in E$ et $\lambda\in \R$, en appliquant l'hypothèse avec $y=0_{E}$, on obtient $f(\lambda)=\lambda f(x)$.
			\end{itemize}
			Et donc $f$ est linéaire. Ainsi $3 \Rightarrow 1$.
		\end{itemize}
 }

\begin{methode}\labelobj{1}
Pour montrer qu'une application $f$ est une application linéaire, on prendra $x$ et $y$ dans $E$, $\lambda \in \R$, et on montre que $f(\lambda x + y) = \lambda f(x) + f(y)$.
\end{methode}

\begin{exo}\label{exo:chap25_suivi}
Soit $f:\MM_{2,1}(\R)\rightarrow \MM_{3,1}(\R)$ définie pour tout $(a, b)\in \R^2$ par
\[f\left( \left(\begin{array}{c} a\\b\end{array} \right) \right)=   \left(\begin{array}{c} b\\0\\a\end{array} \right)\]
Montrer que $f$ est une application linéaire.
\end{exo}

\solution[12]{
En effet, si $A=\left(\begin{array}{c} a_1\\a_2\end{array} \right)$ et $B=\left(\begin{array}{c} b_1\\b_2\end{array} \right)$, et si $\lambda \in \R$, alors
\[f(\lambda A+B)=f\left( \left( \begin{array}{c} \lambda a_1+b_1\\\lambda a_2+b_2 \end{array}\right) \right)=\left(\begin{array}{c} \lambda a_2+b_2\\0\\\lambda a_1+b_1\end{array} \right)\]
et
\[\lambda f(A)+f(B)=\lambda \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)+\left(\begin{array}{c} b_2\\0\\b_1\end{array} \right)=\left(\begin{array}{c} \lambda a_2+b_2\\0\\\lambda a_1+b_1\end{array} \right)\]
Donc $f(\lambda A + B)=\lambda f(A)+f(B)$ : $f$ est une application linéaire.
}

\afaire{Exercice \lienexo{1}.}

	\subsection{Exemples classiques}

On dispose d'applications linéaires classiques :
\begin{itemize}
 \item L'application \appli{\id_E}{E}{E}{x}{x} est un endomorphisme appelé \textbf{identité de $E$}.
	\item L'application \appli{0_{E,F}}{E}{F}{x}{0_F} est une application linéaire, appelée \textbf{application nulle} de $E$ dans $F$.
	\item Soit $\alpha \in \R$. L'application \appli{f_\alpha}{E}{E}{x}{\alpha x} est un endomorphisme, appelée \textbf{homothétie sur $E$ de rapport $\alpha$}.
	\item L'application transposée, définie par $A\donne A^{\perp}=\,^t A$, vue dans le chapitre 17, est une application linéaire de $\MM_{n,p}(\R)$ dans $\MM_{p,n}(\R)$, et est un endomorphisme de $\MM_n(\R)$.
\end{itemize}

\begin{exo}
Démontrer que  \appli{f}{\R[-n]}{\R[-n]}{P}{XP'} est un endomorphisme, et \appli{\Phi}{\CC^0(\interff{a b})}{\R}{f}{\int\limits_a^b f(t)\D t} est une forme linéaire sur $\CC^0(\interff{a b})$.
\end{exo}

\solution[10]{
%TODO: corrigé
}


\section{Operations sur les applications linéaires}

	\subsection{Linéarité}

\begin{proposition}
Soient $E$ et $F$ deux espaces vectoriels, et $f$ et $g$ deux applications linéaires de $\mathcal{L}(E,F)$. Soit $\lambda \in \R$.

Alors $\lambda f+g$ est également une application linéaire.
\end{proposition}

\preuve[6]{Soient $x$ et $y$ deux éléments de $E$, et $\alpha \in \R$. Alors
{\allowdisplaybreaks
\begin{align*}
	(\lambda f+g)(\alpha x+y) &= \lambda f(\alpha x+y) + g(\alpha x+y)\\
													  &= \lambda \left( \alpha f(x)+f(y)\right) + \alpha g(x)+g(y)&\text{car $f$ et $g$ sont linéaires}\\
													  &= \alpha\left(\lambda f(x)+g(x)\right) + \lambda f(y)+g(y)\\
														&= \alpha (\lambda f+g)(x) + (\lambda f+g)(y).
\end{align*}
Ainsi, $\lambda f+g$ est linéaire.
}
}

\begin{consequence}
	Si $E$ et $F$ sont deux espaces vectoriels, alors $\mathcal{L}(E,F)$ est un sous-espace vectoriel de $\mathcal{F}(E,F)$. Il en est de même pour $\mathcal{L}(E)$ et $E^*$.
\end{consequence}

\preuve[2]{La stabilité par somme et multiplication par un réel a été faite à la proposition précédente. Enfin, la fonction nulle est une application linéaire, donc $\mathcal{L}(E,F)$ est non vide.}

	\subsection{Restriction d'une application linéaire}

\begin{definition}
	Soient $E$ et $F$ deux espaces vectoriels. Soient $G$ un sous-espace vectoriel de $E$, et $H$ un sous-espace vectoriel de $F$. Soit $f \in \mathcal{L}(E, F)$ telle que $f(G) \subset H$.

On appelle \textbf{restriction de $f$} aux sous-espaces vectoriels $G$ et $H$ l’application \appli{\left.f\right|_{G} ^{H}}{G}{H}{x}{f(x)}.

	II s'agit d'une application linéaire de $G$ dans $H$.
\end{definition}

Considérer la restriction de $f$ à $G$ revient à ne regarder que les images des vecteurs de $G$ par $f$. Le fait que $f(G) \subset H$ permet de restreindre l'intervalle d'arrivée à $H$ au lieu de $F$.

\preuve[7]{
L'application $\left.f\right|_{G} ^{H}$ est bien définie puisque, pour tout $x \in G$, $\left.f\right|_{G} ^{H}(x)=f(x) \in H$ par hypothèse. Si $(x, y, \lambda) \in G^{2} \times \mathbb{K}$, alors $\lambda x+y \in G$ (puisque $G$ est un s.e.v de $E$ ). Comme $f$ est linéaire, on a
\[
\left.f\right|_{G} ^{H}(\lambda x+y)=f(\lambda x+y)=\lambda f(x)+f(y)=\left.\lambda f\right|_{G} ^{H}(x)+\left.f\right|_{G} ^{H}(y) .
\]
Ainsi $\left.f\right|_{G} ^{H}$ est linéaire de $G$ dans $H$.
}


	\subsection{Composition}

\begin{proposition}
Soient $E$, $F$ et $G$ trois espaces vectoriels. Soient $f\in \mathcal{L}(E,F)$ et $g \in \mathcal{L}(F,G)$.

Alors $g~\circ~f~\in~\mathcal{L}(E,G)$.
\end{proposition}

\preuve[4]{
Soient $x, y \in E^2$ et $\lambda \in \R$. Alors, en utilisant respectivement la linéarité de $f$ puis de $g$ :
\[g\circ f (\lambda x + y) = g( f(\lambda x + y)) = g(\lambda f(x)+f(y)) = \lambda g(f(x))+g(f(y))=\lambda g\circ f(x)+g \circ f(y)\]
}

Rappelons les propriétés de la composition :

\begin{propriete}
	Soient $E$, $F$ et $G$ trois espaces vectoriels.
Soient $(f,f_1, f_2) \in \mathcal{L}(E, F)^{3}$ et $(g,g_1, g_2) \in \mathcal{L}(F, G)^{3}$ et $\lambda \in \mathbb{R}$. Alors
\begin{itemize}
	\item $(g_1+g_2)\circ f = g_1\circ f + g_2\circ f$,
	\item $g \circ (f_1+ f_2) = g\circ f_1+g\circ f_2$,
	\item la composition n'est pas commutative : en général, $g\circ f \neq f\circ g$ (qui n'a parfois pas de sens),
	\item la composition est associative : si $h\in \mathcal{L}(H, I)$ (avec $I$ un espace vectoriel, alors $h\circ (g\circ f) = (h\circ g)\circ f$, noté en général $h\circ g\circ f$. Cette application est également linéaire.
\end{itemize}
\end{propriete}

Une dernière propriété utile quand les applications sont linéaires :
\begin{proposition}
	Soient $(f) \in \mathcal{L}(E, F)$ et $(g) \in \mathcal{L}(F, G)$ et $\lambda \in \mathbb{R}$. Alors
	\[ \lambda g\circ f = (\lambda g)\circ f = g\circ (\lambda f) = g\circ (f\circ(\lambda \id_E)) = (g\circ f)\circ(\lambda \id_E)\]
	c'est-à-dire
\[ \forall x\in E,\quad \lambda g(f(x)) = (\lambda g)(f(x)) = g(\lambda f(x))=g(f(\lambda x)). \]
\end{proposition}

	\subsection{Polynôme d'endomorphisme}

		\subsubsection{Puissances d'un endomorphisme}

Définissons la notion de puissance d'endomorphisme :

\begin{definition}
	Soit $E$ un espace vectoriel, et $f \in \mathcal{L}(E)$. On dispose $f^0 = \id_E$, $f^1 = f$ et, pour tout entier $p\in \N*$,
	\[ f^p = \underbrace{f\circ f\circ \hdots \circ f}_{p\text{ fois}}. \]
\end{definition}

\begin{attention}
Il ne faut pas confondre $f^p(x) = (f\circ \hdots \circ f)(x)$, et $(f(x))^p = (f(x))\times(f(x))\times \hdots \times f(x)$, qui n'a pas de sens en général dans un espace vectoriel.
\end{attention}

La puissance d'un endomorphisme vérifie les propriétés usuelles :
\begin{propriete}
	Soient $E$ un endomorphisme, et $f \in \mathcal{L}(E)$. Soit $(p, q)\in \N^2$. Alors
	\[ f^p \circ f^q = f^{p+q} = f^{q}\circ f^p. \]
	Si $g\in \mathcal{L}(E)$ commute avec $f$ (c'est-à-dire $f\circ g=g\circ f)$, alors $(g\circ f)^n = g^n\circ f^n$.
\end{propriete}

\begin{attention}
	La dernière propriété n'est valable que si $f$ et $g$ commutent.
\end{attention}

On dispose enfin de la formule du binôme de Newton :

\begin{proposition}[Formule du binôme de Newton]
	Soient $E$ un espace vectoriel, et $(f,g)\in \mathcal{L}(E)^2$ \textbf{tels que $f$ et $g$ commutent}. Alors, pour tout entier $n\in \N$
	\begin{align*}
		(f+g)^n &= \sum_{k=0}^n \binom{n}{k} f^k\circ g^{n-k} = \sum_{k=0}^n \binom{n}{k} f^{n-k}\circ g^k.
	\end{align*}
\end{proposition}

\preuve[2]{La preuve a déjà été faite dans le cas des matrices, le raisonnement est le même ici.}

\begin{exo}\logorefaire
	Soit $f:\R^3\to \R^3$ définie par $f(x,y,z)=(x+z,y+z,z)$. On note $g=f-\id_{\R^3}$. Montrer que $g^2=0_{\R^3}$, puis déterminer $f^n$ pour tout entier $n$.
\end{exo}

\solution[12]{Il faut tout d'abord montrer que $f$ est un endomorphisme. On démontre rapidement que $f$ est linéaire : soient $u=(x,y,z)\in \R^3$, $v=(x',y',z')\in \R^3$ et $\lambda \in \R$. Alors
{ \allowdisplaybreaks
\begin{align*}
	f(\lambda u+v) &= f\left( \lambda x+x', \lambda y+y', \lambda z+z'\right) \\
			&= ( \lambda x + x' + \lambda z+z', \lambda y+y' + \lambda z+z', \lambda z+z')\\
			&=  ( \lambda(x+z) + (x'+z'), \lambda (y+z)+(y'+z'), \lambda z+z')\\
			&= \lambda (x+z, y+z, z) + (x'+z', y'+z', z') = \lambda f(u)+f(v)
\end{align*}}
Ainsi, $f$ est linéaire de $\R^3$ dans $\R^3$ : c'est un endomorphisme de $\R^3$.

Pour tout $(x,y,z)\in \R^3$, \[ g(x,y,z)=f(x,y,z)-(x,y,z) = (z,z,0). \]
Remarquons alors que
\begin{align*}
	g^2(x,y,z) &= g(z,z,0) = (0, 0, 0).
\end{align*}
Ainsi, pour tout $k\geq 2$ et $(x,y,z)\in \R^3$, $g^k(x,y,z)=(0,0,0)$.

Appliquons alors la formule du binôme de Newton. On constate que $f=\id_{\R^3}+g$ et $g$ et $\id_{\R^3}$ commutent (car $\id_{\R^3}$ commute avec tout endomorphisme). Pour $n\geq 2$ :
\begin{align*}
	 f^n &= \sum_{k=0}^n \binom{n}{k} g^{k} \id_{\R^3}^{n-k} \\
	  &= \binom{n}{0}g^{0} + \binom{n}{1}g^1 + \underbrace{\sum_{k=2}^n \binom{n}{k} g^k}_{= 0_{\R^3}}\\
		&= \id_{\R^3} + n g
\end{align*}
Ainsi, \[ \forall n\geq 2,\, \forall (x,y,z)\in \R^3,\, f^n(x,y,z)=(x,y,z)+n(z,z,0) = (x+nz, y+nz,z). \]
On remarquera que ce résultat est valable pour $n=0$ et $n=1$.
}

	\subsubsection{Polynômes d'endomorphisme}

On peut désormais définir la notion de polynômes d'endomorphisme :

\begin{definition}
	Soit $E$ un espace vectoriel. On appelle \textbf{polynôme d'endomorphisme} tout endomorphisme de $E$ s'écrivant sous la forme $P(f)$, où $P$ est un polynôme, et $f\in \mathcal{L}(E)$.

 Si $P=\sum\limits_{k=0}^n a_kX^k$, avec $(a_0,\hdots,a_n)\in \R^{n+1}$, alors
 \[ P(f) = a_0\id_E + a_1 f + \hdots + a_n f^n = \sum_{k=0}^n a_k f^k. \]
 On dit alors que $P(f)$ est un polynôme en $f$.
\end{definition}

\begin{exemple}
	Si $P=X^2-1$, alors $P(f)=f^2-\id_E$.
\end{exemple}

Les polynômes d'endomorphisme vérifient les mêmes propriétés que les polynômes de matrices :

\begin{proposition}
	Soit $E$ un espace vectoriel. Soient $P$ et $Q$ deux polynômes, $\alpha \in \R$ et $f\in \mathcal{L}(E)$. Alors
	\[ (\alpha P+Q)(f) = \alpha P(f)+Q(f) \qeq (PQ)(f) = P(f)\circ Q(f)=Q(f)\circ P(f). \]

	Si $f$ et $g$ sont deux endomorphismes de $E$ qui \textbf{commutent}, alors tout polynôme en $f$ commute avec tout polynôme en $g$.
\end{proposition}

\begin{demonstration}
	Il suffit de calculer. La démonstration est laissée en exercice.
\end{demonstration}

	\subsubsection{Polynôme annulateur}

On dispose de la même définition que pour les matrices :

\begin{definition}
 Soient $E$ un espace vectoriel, et $f\in \mathcal{L}(E)$.  On dit que $P$ est un \textbf{polynôme annulateur} de $f$ si $P(f)$ est l'endomorphisme nul $0_E$.
\end{definition}

\begin{exemple}
	Soit $f:\R^2\to \R^2$ définie par $f(x,y)=(x,-y)$. On remarque que
	\[ \forall (x,y)\in \R^2,\, f^2(x,y)=(x,y). \]
	Ainsi, pour tout $(x,y)\in \R^2$, $(f^2-\id_{\R^2})(x,y)=(0,0)$ : le polynôme $X^2-1$ est donc annulateur de $f$.
\end{exemple}

Connaissant un polynôme annulateur, on peut en déduire l'expression de $f^n$ pour tout entier $n$, en utilisant la division euclidienne, comme nous l'avions fait sur les matrices.

	\subsection{Projecteur}

On va régulièrement rencontrer un certain type d'endomorphisme, très important : les projecteurs.

\begin{definition}
 Soient $E$ un espace vectoriel, et $p\in \mathcal{L}(E)$. On dit que $p$ est un \textbf{projecteur} si et seulement si $p\circ p=p$.
\end{definition}

\begin{methode}
  Pour montrer qu'une application $p$ est un projecteur, on démontre
	\begin{itemize}
		\item que $p$ est un endomorphisme,
		\item puis que $p\circ p=p$.
	\end{itemize}
\end{methode}

\begin{exemple}
 Montrer que $f:\R^2\to \R^2$ définie par $f(x,y)=(x,0)$ est un projecteur sur $\R^2$.
\end{exemple}

\solution[7]{
Tout d'abord, montrons que $f$ est un endomorphisme de $\R^2$. Soient $u=(x,y)$ et $v=(x',y')$ deux éléments de $\R^2$ et $\lambda \in \R$. Alors
{\allowdisplaybreaks
\begin{align*}
	f(\lambda u+v) &= f( \lambda x+x', \lambda y+y') \\
		&= (\lambda x+x', 0) = \lambda(x,0) + (x',0) = \lambda f(u)+f(v).
\end{align*}
}
Ainsi, $f$ est un endomorphisme de $\R^2$. On constate alors que, pour tout $(x,y)\in \R^2$ :
\[ f\circ f (x,y) = f( f(x,y)) = f(x,0) =(x,0) = f(x,y). \]
Ainsi, $f\circ f=f$ : $f$ est bien un projecteur de $\R^2$.
}

\begin{exo}
	Montrer que $p:\R^3\to \R^3$ définie par $p(x,y,z)=(2x+3y-z,-x-2y+z,-x-3y+2z)$ est un projecteur de $\R^3$.
\end{exo}

\solution[7]{
On montre rapidement que $p$ est un endomorphisme de $\R^3$. Enfin, pour tout $(x,y,z)\in \R^3$ :
\begin{align*}
	p\circ p (x,y,z) &= p( p(x,y,z)) \\
		&= p(2x+3y-z,-x-2y+z,-x-3y+2z)\\
		&= (2x+3y-z,-x-2y+z,-x-3y+2z)
\end{align*}
Ainsi, $p\circ p =p$ et $p$ est un projecteur.
}

\begin{remarque}
Puisque $p\circ p =p$, on en déduit que $p^2-p=0_E$, c'est-à-dire que $X^2-X$ est annulateur de $p$.
\end{remarque}

\section{Noyau, image}

\subsection{Noyau d'une application linéaire}

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. On appelle \textbf{noyau} de $f$, et on note $\Ker f$, le sous-ensemble de $E$ défini par
     \[\Ker f = \left \{ x\in E,~f(x)=0_F \right\}\]
\end{definition}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $\Ker f$ est un sous-espace vectoriel de $E$.
\end{theoreme}

\preuve[6]{
Puisque $f(0_E)=0_F$, on a déjà $0_E \in \Ker f$. Ensuite, si $x$ et $y$ sont deux vecteurs de $\Ker f$, et si $\lambda \in \R$, alors
\[f(\lambda x + y) = \lambda f(x) +f(y) \underbrace{=}_{x\textrm{ et }y \textrm{ dans } \ker f} \lambda 0_F+0_F=0_F\]
donc $\lambda x + y \in \Ker f$ : $\Ker f$ est bien un sous-espace vectoriel de $E$.}

\begin{methode}\labelobj{3}
Pour déterminer le noyau d'une application linéaire, on résout l'équation $f(x)=0_F$ d'inconnue $x$. On se ramène en général à un système.
\end{methode}

\begin{exo}
Déterminer le noyau de l'application linéaire de l'exercice \thechapter.\ref{exo:chap25_suivi}.
\end{exo}

\solution[8]{
Si $f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=   \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)$, alors $X=\left( \begin{array}{c} x_1\\x_2 \end{array} \right) \in \Ker f$ si et seulement si
\[f\left( X \right)=   \left(\begin{array}{c} x_2\\0\\x_1\end{array} \right)=\left( \begin{array}{c} 0\\0\\0 \end{array} \right)\]
ce qui donne le système
\[\left \{ \begin{array}{ccc} x_2 &=& 0 \\ 0 &=& 0 \\ x_1 &=& 0 \end{array} \right. \Leftrightarrow \left \{ \begin{array}{ccc} x_2 &=& 0 \\x_1 &=& 0 \end{array} \right. \Leftrightarrow X=0_E\]
Donc, $\Ker f = \{ 0_E \}$.}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. $f$ est injective si et seulement si $\Ker f = \{ 0_E \}$.
\end{theoreme}

\preuve[7]{
Supposons $f$ injective. Soit $x$ tel que $f(x)=0_F$ alors $f(x)=f(0_E)$. Par injectivité de $f$, $x=0_E$. Donc $\Ker f = \{ 0_E \}$. \\
Réciproquement, supposons que $\Ker f = \{ 0_E \}$. Soient $x$ et $x'$ tels que $f(x)=f(x')$. Par linéarité de $f$, $f(x-x')=0_F$. Puisque $\Ker f = \{ 0_E \}$, alors $x-x'=0_E$ c'est-à-dire $x=x'$ : $f$ est injective.}

\subsection{Image d'une application linéaire}

\subsubsection{Image d'un sous-espace vectoriel}

\begin{proposition}[Image d'un s.e.v par une application linéaire]
Soient $E$ et $F$ deux espaces vectoriels. Soient $f\in \mathcal{L}(E,F)$ et $E'$ un sous-espace vectoriel de $E$. Alors $f(E') = \left \{ f(x),\, x\in E'\right \}$, l'image de $E'$ par $f$, est un sous-espace vectoriel de $F$.
\end{proposition}

\preuve[8]{Puisque $E'$ est un s.e.v. de $E$, $0_E\in E'$. Ainsi, $f(0_E)\in f(E')$, c'est-à-dire $0_F \in f(E')$.

Soient $u$ et $v$ deux éléments de $f(E')$, et $\lambda$ un réel.
\begin{itemize}
	\item Puisque $u\in f(E')$, il existe $x\in E'$ tel que $u=f(x)$.
	\item De même, il existe $y\in E'$ tel que $v=f(y)$.
\end{itemize}
Mais alors, par linéarité de $f$ :
\[ \lambda u+v = \lambda f(x)+f(y) = f(\lambda x + y) \]
et puisque $E'$ est un sous-espace vectoriel de $E$, $\lambda x+y\in E'$, et finalement $\lambda u+v \in f(E')$.
}

On dispose d'un résultat plus intéressant si $E'$ est un sous-espace vectoriel engendré par une famille :

\begin{proposition}\label{prop:vect_famille}
	Soient $E$ et $F$ deux espaces vectoriels. Soient $f\in \mathcal{L}(E,F)$ et $(e_1,\hdots,e_n)$ une famille de $E$.

Alors \[ f\left( \Vect(e_1,\hdots,e_n)\right) = \Vect\left( f(e_1),\hdots, f(e_n)\right).\]
\end{proposition}

\preuve[12]{Soit $y \in f\left(\Vect\left(e_1,\hdots,e_n\right)\right)$. Il existe $x \in \Vect\left(e_1,\hdots,e_n\right)$ tel que $y=f(x)$. Par définition, il existe $\left(\lambda_{1}, \hdots, \lambda_{n}\right) \in \R^{n}$ tel que $x=\sum\limits_{i=1}^{n} \lambda_{i} e_i$.
Ainsi, par linéarité,
\[
y=f(x)=f\left(\sum_{i=1}^{n} \lambda_{i} e_i\right)=\sum_{i=1}^{n} \lambda_{i} f\left(e_i\right) \in \Vect\left(f\left(e_1\right), \hdots, f\left(e_n\right)\right)
\]
Ainsi $f\left(\Vect\left(e_1,\hdots,e_n\right)\right) \subset \Vect\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$.

Réciproquement, soit $y \in \Vect\left(f\left(e_1\right), \hdots, f\left(e_n\right)\right)$. II existe alors $\left(\mu_{1}, \hdots, \mu_{n}\right) \in \R^{n}$ tel que $y=\sum\limits_{i=1}^{n} \mu_{i} f\left(e_i\right)$ et donc, puisque $f$ est linéaire, on a \[ y=f\left(\sum_{i=1}^{n} \mu_{i} e_i\right).\]
Or $\sum\limits_{i=1}^{n} \mu_{i} e_i \in \Vect\left(e_1,\hdots,e_n\right)$ donc $y \in f\left(\Vect\left(e_1,\hdots,e_n\right)\right)$.

Par conséquent  $\Vect\left(f\left(e_1\right), \hdots, f\left(e_n\right)\right) \subset f\left(\Vect\left(e_1,\hdots,e_n\right)\right)$. D'où l'égalité.
}

\subsubsection{Image d'une application linéaire}

\begin{definition}[Image d'une application linéaire]
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. On appelle \textbf{image} de $f$, et on note $\Img f$, le sous-ensemble de $F$ défini par
     \[\Img f = \left \{ y \in F, ~\exists~x\in E,~y=f(x) \right \}\]
\end{definition}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $\Img f$ est un sous-espace vectoriel de $F$. Si $\mathcal{B}=(e_1,\cdots, e_n)$ est une famille génératrice de $E$, alors
\[\Img f = \Vect(f(e_1),\cdots, f(e_n))\]
\end{theoreme}

\preuve[8]{
Puisque $O_F=f(0_E)$, on a $O_F \in \Img f$. Soient $a$ et $b$ deux éléments de $\Img f$, et $\lambda \in \R$. Puisque $a$ et $b$ sont dans $\Img f$, il existe $x$ et $y$ dans $E$ tels que
\[a=f(x) \qeq  b=f(y)\]
Mais alors
\[\lambda a + b = \lambda f(x) + f(y)  \underbrace{=}_{\textrm{linéarité de }f} f(\lambda x + y)\]
Donc $\lambda a + b$ s'écrit $f(z)$ pour un certain $z\in E$ : $\lambda a + b \in \Img f$. $\Img f$ est donc bien un sous-espace vectoriel de $F$.

Le deuxième point découle de la proposition \ref{prop:vect_famille}}

\begin{methode}
	Pour déterminer $\Image f$, si on dispose d'une famille génératrice, on utilise le point précédent. Sinon, on résout $f(x)=y$ d'inconnue $x$ et on détermine à quelles conditions l'équation admet au moins une solution.
\end{methode}

\begin{exo}
 Soit $f:\R^3\to \R^3$ définie par $f(x,y,z)=(2x - y + 3z, x - 2y, -x - 2y - 4z)$. Déterminer $\Image f$.
\end{exo}

\solution[15]{On démontre rapidement que $f$ est un endomorphisme de $\R^3$. Déterminons $\Image f$.

\textbf{Première méthode} :
$(a,b,c)\in \Image f$ si et seulement s'il existe $(x,y,z)\in \R^3$ tel que $f(x,y,z)=(a,b,c)$. Alors

\begin{align*}
	\systeme{2x-y+3z=a, x-2y=b,-x-2y-4z=c} &\iff \systeme{ 2x-y+3z=a@L_1, -3y-3z=2b-a@L_2\leftarrow 2L_2-L_1,-5y-5z=2c+a@L_3\leftarrow 2L_3+L_1}\\
	&\Leftrightarrow \systeme{2x-y+3z=a, -3y-3z=2b-a, 0=8a-10b+6c @L_3\leftarrow 3L_3-5L_2}
\end{align*}
Ce système admet au moins une solution si la dernière condition est vérifiée.

Ainsi, $(a,b,c)\in \Image f$ si et seulement si $8a-10b+6c=0$, c'est-à-dire $c=-\frac{4}{3}a+\frac{5}{3}b$.

Finalement,
\begin{align*}
  \Image f &= \left \{ (a,b,c)\in \R^3,\,8a-10b+6c=0  \right \}\\
	&= \left \{ \left(a,b,-\frac{4}{3}a+\frac{5}{3}b\right),\,(a,b)\in \R^2 \right \}\\
	&= \Vect \left( \left(1,0,-\frac43\right), \left(0,1,\frac53\right) \right) = \Vect \left( (3,0,-4), (0,3,5) \right)
\end{align*}

\textbf{Deuxième méthode} : $(1,0,0), (0,1,0), (0,0,1)$  est une famille génératrice de $\R^3$. Ainsi
\begin{align*}
 \Image f &= \Vect\left( f(1,0,0), f(0,1,0), f(0,0,1)\right) \\
 &= \Vect \left( (2,1,-1), (-1,-2,-2), (3,0,-4) \right)
\end{align*}
La famille est liée, puisque $2(2,1,-1)+(-1,-2,-2) = (3,0,-4)$. Ainsi,
\[ \Image f = \Vect \left( (2,1,-1), (-1,-2,-2) \right) \]
}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $f$ est surjective si et seulement si $\Img f = F$.
\end{theoreme}

\preuve[3]{
Par définition, $f$ est surjective si et seulement si $f(E)=F$. Or, par définition, $f(E)=\Img f$. Donc $f$ est surjective si et seulement si $\Img f = F$.}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $f$ est bijective si et seulement si $\Ker f = \{0_E \}$ et $\Img f = F$.
\end{theoreme}

\begin{methode}\labelobj{5}
Pour montrer qu'une application linéaire $f:E\rightarrow F$ est bijective, on montrera que $\Ker f=\{0_E\}$ et que $\Img f = F$.
\end{methode}

\begin{exo}
Soit $f:\MM_{2,1}(\R)\rightarrow \MM_{2,1}(\R)$ l'application définie par
\[f\left( \left(\begin{array}{c}x_1\\x_2
\end{array}\right) \right) = \left( \begin{array}{c} 2x_2\\x_1 \end{array}\right)\]
est bijective.
\end{exo}

\solution[15]{
$f$ est bien linéaire (c'est un endomorphisme de $\MM_{2,1}(\R)$. Déterminons noyau et image.
\begin{itemize}[label=\textbullet]
	\item Soit $X=\left(\begin{array}{c}x\\y\end{array}\right) \in \Ker f$. Alors
		\[f(X)=\left(\begin{array}{c}0\\0\end{array}\right) \Leftrightarrow \left(\begin{array}{c}2y\\x\end{array}\right) = \left(\begin{array}{c}0\\0\end{array}\right) \Leftrightarrow x=y=0\]
		ainsi, $\Ker f =\left\{ \left(\begin{array}{c}0\\0\end{array}\right) \right\}$ et $f$ est injective.
	\item Soit $e_1=\left(\begin{array}{c}1\\0\end{array}\right)$ et $e_2=\left(\begin{array}{c}0\\1\end{array}\right)$ la base canonique de $\MM_{2,1}(\R)$. Alors,
		\[f(e_1)=\left(\begin{array}{c}0\\1\end{array}\right)=e_2 \qeq f(e_2)=\left(\begin{array}{c}2\\0\end{array}\right)=2e_1\]
		La famille $(2e_1,e_2)$ est libre (car les vecteurs ne sont pas colinéaires) donc forme une base de $\Img f$ :
		\[\Img f = \Vect(2e_1,e_2) = \Vect(e_1,e_2) = \MM_{2,1}(\R)\]
		Ainsi, $f$ est surjective.
\end{itemize}
\textbf{Bilan} : $f$ est bien bijective.}

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels.
\begin{itemize}
  \item Si $f \in \mathcal{L}(E,F)$ est bijective, on dit que $f$ est un \textbf{isomorphisme} de $E$ sur $F$.
  \item Si $f \in \mathcal{L}(E)$ est bijective, on dit que $f$ est un \textbf{automorphisme}.
  \item On note $\mathrm{Isom}(E,F)$ l'ensemble des isomorphismes de $E$ dans $F$, et $\mathrm{Aut}(E)$ ou $GL(E)$ l'ensemble des automorphismes de $E$.
\end{itemize}
\end{definition}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathrm{Isom}(E,F)$. Alors $f^{-1}$ est également une application linéaire et $f^{-1}\in \mathrm{Isom}(F,E)$.
\end{theoreme}

\preuve[6]{
Soient $x,y$ deux éléments de $F$ et $\lambda$ un réel. Puisque $f$ est bijective, il existe un unique $a\in E$ tel que $f(a)=x$ et un unique $b\in E$ tel que $f(b)=y$. Donc $f(\lambda a + b)=\lambda f(a)+f(b) = \lambda x + y$ donc, par unicité de bijectivité de $f$, $\lambda a + b = f^{-1}(\lambda f(a)+f(b))$, c'est-à-dire
\[\lambda f^{-1}(x)+f^{-1}(y)=f^{-1}(\lambda x + y)\]
$f^{-1}$ est bien linéaire.}

\begin{remarque}
	\`A partir d'un polynôme annulateur, on peut déterminer rapidement l'application réciproque si elle est bijective.

	Par exemple, si $f\in \mathcal{L}(E)$ vérifie $f^2-2f+\id_E=0_E$, alors
	\[ f\circ (f-2\id_E) = -\id_E \iff f\circ (-f+2\id_E) = \id_E. \]
	Ainsi, $f$ est bijective et $f^{-1} = -f +2\id_E$.
\end{remarque}

On définit alors la notion d'espaces vectoriels isomorphes :

\begin{definition}
On dit que deux espaces vectoriels E et F sont isomorphes s'il existe un isomorphisme de $E$ sur $F$.
\end{definition}

\afaire{Exercices \lienexo{3}, \lienexo{4}, \lienexo{5}, \lienexo{6} et \lienexo{7}.}

	\subsection{Cas des projecteurs}

Dans le cas des projecteurs, on dispose de propriétés importantes :

\begin{proposition}
	Soient $E$ un espace vectoriel, et $p$ un projecteur de $E$. Alors
	\begin{itemize}
		\item $\Ker(p)\cap \Image p = \{0_E\}$,
		\item $\id_E-p$ est également un projecteur de $E$,
		\item $\Image(p) = \Ker(\id_E-p)$ et $\Ker(p)=\Image(\id_E-p)$.
	\end{itemize}
\end{proposition}

\preuve[10]{
\begin{itemize}
	\item Soit $x\in \Ker(p)\cap \Image(p)$. Alors, il existe $y\in E$ tel que $x=p(y)$, et alors $p(x)=p^2(y)=p(y)$ car $p$ est un projecteur. Or, $p(x)=0_E$ puisque $x$ est dans le noyau de $p$, donc $p(y)=0_E$, c'est-à-dire $x=0_E$.
	\item En calculant :
	\[ (\id_E-p)\circ(\id_E-p) = \id_E^2 - \id_E\circ p-p\circ \id_E + p^2 = \id_E - p - p + p = \id_E-p. \]
	\item Montrons le premier cas. Le deuxième s'en déduit en appliquant le premier au projecteur $\id_E-p$.
	\begin{itemize}
		\item Soit $x\in \Image(p)$. Il existe $y\in E$ tel que $x=p(y)$. Mais alors
		\[ (\id_E-p)(x) = \id_E(x)-p(x) = x-p^2(y)=x-p(y)=x-x=0_E. \]
		Ainsi, $x\in \Ker(\id_E-p)$.
		\item Réciproquement, soit $x\in \Ker(\id_E-p)$. Alors $(\id_E-p)(x) = x-p(x)=0_E$, c'est-à-dire $x = p(x)$ : $x\in \Image(p)$.
	\end{itemize}
	Par double inclusion, $\Image(p)=\Ker(\id_E-p)$.
\end{itemize}
}

\section{Applications linéaires et bases}

On dispose de résultats importants liant application linéaire et base, dont un résultat important : une application est uniquement déterminée par l'image d'une base de l'espace de départ.

\begin{lemme}
Soient $f \in \mathcal{L}(E, F)$ et $\left(e_{1}, \hdots, e_{n}\right)$ une famille de $E$.
\begin{itemize}
	\item Si la famille est génératrice et si $f$ est surjective, alors $\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$ est une famille génératrice de $F$.
 	\item Si la famille  est libre et si $f$ est injective, alors $\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$ est une famille libre de $F$.
 	\item Si la famille  une base et si $f$ est un isomorphisme de $E$ dans $F$, alors $\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$ est une base de $F$.
\end{itemize}
\end{lemme}

\preuve[10]{
\begin{itemize}
\item Supposons que $\left(e_{1}, \hdots, e_{n}\right)$ est génératrice et que $f$ est surjective. Le résultat sur l'image de $f$ garantit que $\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$ engendre $\Image(f)$, et $\Image(f)=F$ par surjectivité.
\item Supposons que $\left(e_{1}, \hdots, e_{n}\right)$ est une famille libre de $E$ et que $f$ est injective.

Soit $\left(\lambda_{1}, \hdots, \lambda_{n}\right) \in \R^{n}$ tel que $\sum\limits_{i=1}^{n} \lambda_{i} f\left(e_{i}\right)=0$.
Comme $f$ est linéaire, on a $f\left(\sum\limits_{i=1}^{n} \lambda_{i} e_{i}\right)=0$, c'est-à-dire $\sum\limits_{i=1}^{n} \lambda_{i} e_{i} \in \Ker(f)$.

Puisque $f$ est injective, on a $\Ker(f)=\{0\}$ et donc $\sum\limits_{i=1}^{n} \lambda_{i} e_{i}=0$. Enfin, puisque $\left(e_{1}, \hdots, e_{n}\right)$ est libre, on en déduit que $\lambda_{1}=\hdots=\lambda_{n}=0$ : ainsi $\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$ est une famille libre de $F$.
\item Le dernier point est une implication des deux premiers points.
\end{itemize}}

On peut alors en déduire le résultat fondamental, sur lequel nous reviendrons plus tard :

\begin{theoreme}[Caractérisation par l'image d'une base] Soit $E$ un espace vectoriel, admettant une base $\left(e_{1}, \hdots, e_{n}\right)$. Soient $F$ un espace vectoriel, et  $\left(v_{1}, \hdots, v_{n}\right)$ une famille de vecteurs de $F$.

Il existe une \textbf{unique} application linéaire $f$ de $E$ dans $F$ telle que, pour tout $i \in \interent{1 n}$, $f\left(e_{i}\right)=v_{i}$ :
\[ \appli{f}{E}{F}{x=\sum\limits_{i=1}^n x_ie_i}{\sum\limits_{i=1}^n x_iv_i}. \]
\end{theoreme}

Ainsi, une application linéaire de $E$ dans $F$ est entièrement définie par la donnée des images des vecteurs d'une base de $E$.

\preuve[15]{
Démontrons l'existence. On introduit, comme proposé dans le théorème, l'application
\[ \appli{f}{E}{F}{x=\sum\limits_{i=1}^n x_ie_i}{\sum\limits_{i=1}^n x_iv_i}. \]
Il s'agit d'une application de $E$ dans $F$ puisque $F$ est un espace vectoriel. Montrons qu'elle est linéaire.

Soit $\lambda \in \R$, $x=\sum\limits_{i=1}^{n} x_{i} e_{i} \in E$ et $y=\sum\limits_{i=1}^{n} y_{i} e_{i} \in E$. On a $\lambda x+y=\sum\limits_{i=1}^{n}\left(\lambda x_{i}+y_{i}\right) e_{i} \in E$ donc
\[
f(\lambda x+y)=\sum_{i=1}^{n}\left(\lambda x_{i}+y_{i}\right) v_{i}=\lambda \sum_{i=1}^{n} x_{i} v_{i}+\sum_{i=1}^{n} y_{i} v_{i}=\lambda f(x)+f(y)
\]
$f$ est donc linéaire.

Enfin, si $i \in \interent{1 n}$, alors on peut écrire \[e_{i}=0 \cdot e_{1}+\hdots+0 \cdot e_{i-1}+1 \cdot e_{i}+0 \cdot e_{i+1}+\hdots+0 \cdot e_{n}\]
 et donc \[f\left(e_{i}\right)=0 \cdot v_{1}+\hdots+0 \cdot v_{i-1}+1 \cdot v_{i}+0 \cdot v_{i+1}+\hdots+0 \cdot v_{n}=v_{i}\]
L'application $f$ existe donc bien.

\vspace*{.5cm}
Montrons l'unicité. Supposons qu'il existe deux applications linéaires $f$ et $g$ de $E$ dans $F$ telles que, pour tout $i \in \interent{1 n}$, $f\left(e_{i}\right)=v_{i}=g\left(e_{i}\right)$.

Soit $x \in E$. Puisque $\left(e_{1}, \hdots, e_{n}\right)$ est une base de $E$, il existe $\left(x_{1}, \hdots, x_{n}\right) \in \R^{n}$ tel que $x=\sum\limits_{i=1}^{n} x_{i} e_{i}$. On a donc, par linéarité de $f$ et de $g$,
\[
f(x)=\sum_{i=1}^{n} x_{i} f\left(e_{i}\right)=\sum_{i=1}^{n} x_{i} v_{i}=\sum_{i=1}^{n} x_{i} g\left(e_{i}\right)=g(x)
\]
Ainsi $f$ et $g$ coïncident sur $E$. D'où l'unicité.
}

On dispose d'un résultat complémentaire suivant de la famille $(v_i,\hdots,v_n)$

\begin{proposition}
On reprend les notations du théorème précédent.
\begin{itemize}
\item $f$ est injective si et seulement si $\left(v_{1}, \ldots, v_{n}\right)$ est une famille libre de $F$.
\item $f$ est surjective si et seulement si $\left(v_{1}, \ldots, v_{n}\right)$ est une famille génératrice de $F$.
\item $f$ est un isomorphisme si et seulement si $\left(v_{1}, \ldots, v_{n}\right)$ est une base de $F$.
\end{itemize}
\end{proposition}

\preuve[10]{
\begin{itemize}
\item Si $f$ est injective, alors le lemme précédent entraîne que $\left(v_{1}, \hdots, v_{n}\right)=\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$ est libre.

Réciproquement, supposons la famille $\left(v_{1}, \hdots, v_{n}\right)$ libre et montrons que $f$ est injective. Soit $x=\sum\limits_{i=1}^{n} x_{i} e_{i} \in \Ker(f)$. On a alors $0=f(x)=\sum\limits_{i=1}^{n} x_{i} f\left(e_{i}\right)=\sum\limits_{i=1}^{n} x_{i} v_{i}$.

Comme $\left(v_{1}, \hdots, v_{n}\right)$ est libre, on en déduit que $x_{1}=\hdots=x_{n}=0$. Ainsi $x=0$ et finalement $\Ker(f)=\{0\}$ : $f$ est injective.
\item Si $f$ est surjective, alors le lemme précédent entraîne que $\left(v_{1}, \hdots, v_{n}\right)=\left(f\left(e_{1}\right), \hdots, f\left(e_{n}\right)\right)$ est génératrice.

Réciproquement, supposons $\left(v_{1}, \hdots, v_{n}\right)$ est génératrice et montrons que $f$ est surjective.

Soit $y \in F=\Vect\left(v_{1}, \hdots, v_{n}\right)$ II existe alors $y_{1}, \hdots, y_{n}$ tel que $y=\sum\limits_{i=1}^{n} y_{i} v_{i}$. On a donc, par linéarité, $y=\sum\limits_{i=1}^{n} y_{i} f\left(e_{i}\right)=f\left(\sum_{i=1}^{n} y_{i} e_{i}\right) \in \Image(f)$. Ainsi, $F=\Image(f)$ et $f$ est surjective.
\item Le troisième point découle immédiatement des deux points précédents.
\end{itemize}
}

On en déduit enfin un théorème important :
\begin{consequence}
	Soient $E$ et $F$ deux espaces vectoriels, tels que $E$ admette une base.

	Si $f$ et $g$ sont deux applications linéaires de $E$ dans $F$ qui coïncident sur cette base, alors elles sont égales.
\end{consequence}

%%% Fin du cours %%%
