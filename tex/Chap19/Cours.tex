\chapter{Analyse asymptotique}
%!TeX root=../../encours.nouveau.tex


\objectifintro{
Ce chapitre est une base utilisée dans de nombreuses situations : calcul de limites, et plus tard, pour des développements limités et des comportements de fonctions.
Il est à connaître rapidement.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Jean d'Ormesson (1925--). \emph{L'amour est un plaisir}}
Ce qu'on fait de sa seule vie n'est jamais négligeable. Mais rien ne la gâche aussi sûrement que trop d'efforts pour la sauver.
\end{extrait}

\begin{objectifs}
\begin{numerote}
  \item Concernant la négligeabilité de suites et de fonctions :
  \begin{itemize}
      \item connaître la définition\dotfill $\Box$
      \item connaître les différentes propriétés usuelles\dotfill $\Box$
      \item connaître les négligeabilités usuelles\dotfill $\Box$
\end{itemize}
\item Concernant l'équivalence de suites et de fonctions :
\begin{itemize}
    \item connaître la définition\dotfill $\Box$
    \item connaître les différentes propriétés usuelles\dotfill $\Box$
    \item connaître le résultat $f(x)-f(0) \underset{0}{\sim} f'(0)x$ si $f$ est dérivable\dotfill $\Box$
    \item connaître les équivalences usuelles\dotfill $\Box$
\end{itemize}
\end{numerote}
\end{objectifs}





%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%
\section{Négligeabilité}
%%%%%%%%%%%%%%%%%%%%%

Dans toute cette partie, $x_0$ désignera ou bien un nombre réel, ou bien $+\infty$, ou bien $-\infty$. Rappelons tout d'abord la notion de voisinage :

\begin{definition}
On appelle \textbf{voisinage} de $x_0$ :
\begin{itemize}[label=\textbullet]
	\item un intervalle de la forme $]x_0-\eps, x_0+\eps[$, ou un intervalle contenant $x_0$ dont $x_0$ n'est pas une borne.
	\item $\interoo{A +\infty}$ ou $\interfo{A +\infty}$, avec $A$ réel, si $x_0=+\infty$.
	\item $\interoo{-\infty{} A}$ ou $\interof{-\infty{} A}$, avec $A$ réel, si $x_0=-\infty$.
\end{itemize}
\end{definition}

	%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Définition}
	%%%%%%%%%%%%%%%%%%%%%%%%

\begin{definition}[Cas des fonctions]
Soient $f$ et $g$ définies au voisinage de $x_0$, avec $g$ ne s'annulant pas au voisinage de $x_0$. Alors, on dit que $f$ est \textbf{négligeable} devant $g$ au voisinage de $x_0$, et on note $f=o_{x_0}(g)$, si
\[ \lim_{x\rightarrow x_0} \frac{f(x)}{g(x)} = 0 \]
$f=o_{x_0}(g)$ peut se noter également $f(x)=o_{x_0}(g(x))$ ou encore $f(x)\underset{x_0}{=} o(g(x))$ et se lit ``$f$ est un petit $o$ de $g$ au voisinage de $x_0$''.
\end{definition}

\begin{exemple}
  Par exemple, $x\underset{+\infty}{=}o(x^2)$ puisque \[ \lim_{x\rightarrow +\infty} \frac{x}{x^2}=\lim_{x\rightarrow +\infty} \frac{1}{x}=0 \]
\end{exemple}

\begin{exo}
  Montrer que $\sin(x) \underset{0}{=} o\left(\sqrt{x}\right)$
\end{exo}

\solution[4]{En effet, pour tout $x > 0$ :
\begin{align*}
  \frac{\sin(x)}{\sqrt{x}} &= \frac{\sin(x)  \sqrt{x} }{ \sqrt{x}  \sqrt{x}  }\\
  &=  \sqrt{x}  \frac{\sin(x)}{x}
\end{align*}
Puisque $\frac{\sin(x)}{x}\tendversen{x\to 0} 1$ et $\sqrt{x}\tendversen{x\to 0} 0$, par produit \[ \lim_{x\to 0} \frac{\sin(x)}{\sqrt{x}} = 0\]
et donc $\sin(x) \underset{0}{=} o\left(\sqrt{x}\right)$.
}

Rappelons la définition déjà vue pour les suites :

\begin{definition}[Cas des suites]
Soient $(u_n)$ et $(v_n)$ deux suites, telles que $(v_n)$ ne s'annule pas à partir d'un certain rang. On dit que $(u_n)$ est négligeable devant $(v_n)$, et on note $u_n = o_{+\infty}\left(v_n\right)$, voire $u_n=o\left(v_n\right)$ si \[ \lim_{n \to +\infty} \frac{u_n}{v_n} =0. \]
\end{definition}

%% Définition : nom Landau

\begin{proposition}
  $f\underset{x_0}{=}o(1)$ si et seulement si $\ds{\lim_{x\rightarrow x_0} f(x)=0}$.

  $f -\ell \underset{x_0}{=} o(1)$ si et seulement si $f\tendversen{x\to x_0} \ell$.
\end{proposition}

\preuve[4]{En effet, la fonction constante égale à $1$ ne s'annule pas au voisinage de $x_0$ et on a $f=o_{x_0}(1)$ par définition si et seulement si
\[ \lim_{x\rightarrow x_0} \frac{f(x)}{1}=0 \Longleftrightarrow \lim_{x\rightarrow x_0} f(x)=0 \]
Pour le deuxième point, $f-\ell\underset{x_0}{=} o(1)$ et donc $f-\ell \tendversen{x\to x_0} 0$ soit $f\tendversen{x\to x_0} \ell$.
}

    \subsection{Négligeabilités usuelles}

On dispose d'un ensemble de négligeabilités à connaitre.

\begin{theoreme}[Négligeabilités usuelles -- fonctions]
  On a
\begin{itemize}[label=\textbullet]
	\item Si $\alpha < \beta$, on a
	\[ x^\alpha \underset{+\infty}{=} o\left( x^\beta\right) \qeq x^\beta \underset{0}{=} o\left(x^\alpha\right) \]
  \item Si $\alpha < \beta$,
  \[ (\ln(x))^{\alpha} \underset{+\infty}{=} o\left((\ln(x))^{\beta}\right) \qeq |\ln(x)|^{\alpha} \underset{0^+}{=} o\left(|\ln(x)|^{\beta}\right). \]
  \item Si $0 < p < q$, alors $p^x \underset{+\infty}{=} o(q^x)$.
\end{itemize}
\end{theoreme}

\preuve[7]{On prend $\alpha<\beta$ et $0<p<q$.
\begin{align*}
  \frac{x^{\alpha}}{x^{\beta}} &= \frac{1}{x^{\beta-\alpha}} \tendversen{x\to +\infty} 0 &&&
  \frac{x^{\beta}}{x^{\alpha}} &= x^{\beta-\alpha}\tendversen{x\to 0} 0 \\
  \frac{\ln(x)^{\alpha}}{\ln(x)^{\beta}} &= \frac{1}{\ln(x)^{\beta-\alpha}} \tendversen{x\to +\infty} 0 &&&
  \frac{|\ln(x)|^{\alpha}}{|\ln(x)|^{\beta}} &=\frac{1}{|\ln(x)|^{\beta-\alpha}} \tendversen{x\to 0^+} 0\\
  \frac{p^x}{q^x} &= \eu{x\left(\ln(p)-\ln(q)\right)} \tendversen{x\to +\infty} 0
\end{align*}

}

\begin{theoreme}[Croissances comparées -- fonctions]Soient $\alpha, \beta$ des réels \textbf{strictement positifs}.
  \begin{itemize}
	\item Si $q>1$, on a \[ x^\alpha\underset{+\infty}{=} o\left(q^{x}\right) \]
	et en particulier $x^{\alpha}\underset{+\infty}{=}o(\E^x)$.
  \item Si  $q>1$ alors \[ |x|^{\alpha} \underset{-\infty}{=} o\left(\frac{1}{q^x}\right). \]
	\item On a \[ \left(\ln(x)\right)^\alpha = o_{+\infty}\left(x^\beta\right) \qeq  \left|\ln(x)\right|^\alpha\underset{0}{=}o \left(\frac{1}{x^\beta}\right) \]
  \item Si $0<q<1$, alors \[ q^x \underset{+\infty}{=} o\left(\frac{1}{x^\alpha}\right).\]
\end{itemize}
\end{theoreme}

\begin{demonstration}
  Cela découle des croissances comparées. Par exemple
  \begin{align*}
  \frac{(\ln(x))^{\alpha}}{x^\beta} \tendversen{x\to +\infty} 0
  \end{align*}
  donc $(\ln(x))^\alpha \underset{+\infty}{=} o\left(x^\beta\right)$.
\end{demonstration}

On rappelle les négligeabilités vues dans le cas des suites :

\begin{theoreme}[Négligeabilités usuelles -- cas des suites]
  Soient $\alpha>0$, $\beta>0$ et $q \in \mathbb{R}$.
  \begin{itemize}
\item  On a $(\ln (n))^{\beta} \underset{+\infty}{=} o\left(n^{\alpha}\right)$.
\item Si $q>1$, alors $n^{\alpha} \underset{+\infty}{=} o\left(q^{n}\right)$.
\item Si $0<q<1$, alors $q^{n} \underset{+\infty}{=} o\left(\frac{1}{n^{\alpha}}\right)$.
\item Si $q>1$, alors $q^{n} \underset{+\infty}{=} o(n !)$.
\item  On a $n ! \underset{+\infty}{=} o\left(n^{n}\right)$.
\end{itemize}
\end{theoreme}

  \subsection{Propriétés}

\begin{propriete}[Transitivité et linéarité]
  Soient $f, g$ et $h$ des fonctions définies au voisinage de $x_0$ :
\begin{itemize}
	\item \petittitre{Transitivité} : si $f\underset{x_0}{=}o(g)$ et $g\underset{x_0}{=}o(h)$, alors $f\underset{x_0}{=}o(h)$.
	\item \petittitre{Linéarité} : si $f\underset{x_0}{=}o(h)$ et $g\underset{x_0}{=}o(h)$, alors pour tous réels $a$ et $b$, on a $af+bg\underset{x_0}{=}o(h)$
\end{itemize}
\end{propriete}

\preuve[4]{Si $f=o_{x_0}(g)$ et $g=o_{x_0}(h)$ alors $g$ et $h$ ne s'annulent pas au voisinage de $x_0$, et on peut écrire
\[ \frac{f}{h} = \frac{f}{g}\times \frac{g}{h} \]
et ainsi $\ds{\lim_{x\rightarrow x_0} \frac{f(x)}{h(x)} = 0}$ par produit des limites.\\
De même, si $f=o_{x_0}(h)$ et $g=o_{x_0}(h)$ alors $h$ ne s'annule pas au voisinage de $x_0$ et on a
\[ \frac{af+bg}{h} = a\frac{f}{h}+b\frac{g}{h} \]
Par somme et produit des limites, on en déduit donc que $\ds{\lim_{x\rightarrow x_0} \frac{(af+bg)(x)}{h(x)} = 0}$.
}

\begin{propriete}[Produits]
  Soient $f$, $g$, $h$ et $\phi$ des fonctions définies au voisinage de $x_0$.
\begin{itemize}
  \item Si $f\underset{x_0}{=}o(g)$ et $h \underset{x_0}{=} o(\varphi)$, alors $f\times h\underset{x_0}{=} o(g \times\varphi)$.
  \item Si $f \underset{x_0}{=} o(g)$, alors $f \times \varphi \underset{x_0}{=} o(g \times \varphi)$.
\end{itemize}
\end{propriete}

\preuve[5]{Toujours de la même manière :
\begin{align*}
 \frac{f\times h(x)}{g\times \phi(x)} &= \frac{f(x)}{g(x)}\times \frac{h(x)}{\phi(x)} \tendversen{x\to x_0} 0\\
 \frac{f\times \phi(x)}{g\times \phi(x)} &= \frac{f(x)}{g(x)}\tendversen{x\to x_0} 0.
\end{align*}
}

\begin{remarque}
  On peut dans également diviser, si les fonctions sont bien définies. Le plus classique : si $f(x) = o_0(x^n)$, alors pour tout $p\leq n$, $\frac{f(x)}{x^p} = o_0(x^{n-p})$. En effet
\[ \frac{\frac{f(x)}{x^p}}{x^{n-p}} = \frac{f(x)}{x^n} \underset{x\rightarrow 0}{\longrightarrow} 0 \]
\end{remarque}

\begin{attention}
  On a vu que si $f$ et $g$ sont des petits o de $h$, alors $f+g$ également. En revanche, dans le cas général, on ne peut pas ajouter les petits o : si $f = o(h)$ et $g=o(\phi)$ alors $f+g$ n'est pas égal à $o(h+\phi)$.

  L'idée est que c'est le plus \og{}gros\fg{} des petits o qui l'emporte. Par exemple, si
  \[ f(x) \underset{0}{=} x-x^2+x^3+o(x^3) \qeq g(x)=\underset{0}{=} 1-x^2 + o(x^4)\]
alors \[ f(x)+g(x) = x-x^2+x^3+o(x^3) + 1 - x^2 + o(x^4) = 1+x-2x^2+x^3+o(x^3) \]
puisque $x^4\underset{0}{=} o(x^3)$ : c'est $x^3$ le \og{}plus gros\fg{}.
\end{attention}

\begin{exo}
  Soient $f$ et $g$ deux fonctions telles que
  \[ f(x) \underset{+\infty}{=} 1-\frac{1}{x}+o\left(\frac{1}{x^2}\right) \qeq g(x) \underset{+\infty}{=} x-\frac{1}{x^2}+\frac{1}{x^3}+o\left(\frac{1}{x^3}\right). \]
  Déterminer une expression de $f+g$ au voisinage de $+\infty$.
\end{exo}

\solution[4]{
On ajoute :
\begin{align*}
  f(x)+g(x) &\underset{+\infty}{=} 1-\frac{1}{x}+o\left(\frac{1}{x^2}\right)+x-\frac{1}{x^2}+\frac{1}{x^3}+o\left(\frac{1}{x^3}\right) \\
  &= 1-\frac{1}{x} +x - \frac{1}{x^2} + o\left(\frac{1}{x^2}\right)
\end{align*}
car $\frac{1}{x^3}\underset{+\infty}{=} o\left(\frac{1}{x^2}\right)$.
}

\begin{propriete}[Substitution]
Soient $f$ et $g$ deux fonctions définies au voisinage de $x_0$ telles que $f \underset{x_0}{=} o\left(g\right)$.
Soit $\phi$ une fonction définie sur intervalle $J$ de $\R$ et à valeurs dans $I$. Soit $t_0$ un point ou une extrémité (éventuellement infinie) de $J$. Si $\phi(t) \underset{t \rightarrow t_{0}}{\longrightarrow} x_{0}$, alors $f(\phi(t)) \underset{t_{0}}{=} o(g(\phi(t)))$.
\end{propriete}

\preuve[4]{
Si \[ \frac{f(x)}{g(x)}\tendversen{x\to x_0} 0 \qeq \phi(t)\tendversen{t\to t_0} x_0\]
alors par composée des limites
\[ \frac{f(\phi(t))}{g(\phi(t))}\tendversen{t\to t_0} 0. \]
}

\begin{propriete}[Cas des suites]
Les propriétés valables pour la négligeabilité des fonctions est valable pour les suites. Ainsi :
\begin{itemize}
  \item Si $u_n = o(v_n)$ et $v_n=o(w_n)$ alors $u_n=o(w_n)$.
  \item Si $u_n=o(v_n)$ et $w_n=o(v_n)$ alors pour tous réels $(a, b)\in \R$, $au_n+bw_n=o(v_n)$.
  \item Si $u_n=o(v_n)$ et $w_n=o(x_n)$ alors $u_nw_n=o(v_nx_n)$.
  \item Si $u_n=o(v_n)$ alors pour toute suite $w$, $u_nw_n=o(v_nw_n)$.
  \item Si $f\underset{x_0}{=}o(g)$ et si $u_n\tendversen{n\to +\infty} x_0$ alors $f(u_n) =o(g(u_n))$.
\end{itemize}
\end{propriete}

Pour la somme, le résultat est identique : si $u_n=o(v_n)$ et $w_n=o(x_n)$, on n'a pas forcément $u_n+w_n=o(v_n+x_n)$. A nouveau, c'est le plus \og{}fort\fg{} qui l'emporte.

\section{Equivalence}

  \subsection{Définitions}

\begin{definition}[\'Equivalence de fonctions]
Soient $f$ et $g$ deux fonctions définies au voisinage de $x_0$. On dit que $f$ est \textbf{équivalente} à $g$ en $x_0$ si, au voisinage de $x_0$, on a
\[ f-g \underset{x_0}{=} o(g) \]
ce qu'on note $f \underset{x_0}{\sim} g$. Ainsi, puisque $g$ ne s'annule pas au voisinage de $x_0$, on a
\[ f\underset{x_0}{\sim} g \quad\text{si et seulement si}\quad \lim_{x\rightarrow x_0} \frac{f(x)}{g(x)} = 1 \]
\end{definition}

\begin{remarque}
  Il y a bien équivalence. En effet, puisque $g$ ne s'annule pas au voisinage de $x_0$
  \[ f-g \underset{x_0}{=} o(g) \Longleftrightarrow \frac{f(x)-g(x)}{g(x)} \tendversen{x\to x_0} 0 \Longleftrightarrow \frac{f(x)}{g(x)}\tendversen{x\to x_0} 1. \]
\end{remarque}

\begin{exemple}
  On a les équivalents suivants :
  \[ \sin(x) \underset{0}{\sim} x \qeq x^2-x^3 \underset{+\infty}{\sim} -x^3. \]
  En effet
  \[ \frac{\sin(x)}{x}\tendversen{x\to 0} 1 \qeq \frac{x^2-x^3}{-x^3}=-\frac{1}{x}+1\tendversen{x\to +\infty} 1. \]
\end{exemple}

Rappelons la définition déjà vue pour les suites :

\begin{definition}[\'Equivalence de suites]
Soient $u$ et $v$ deux suites telles que $v$ ne s'annule pas à partir d'un certain rang. On dit que $u$ et $v$ sont équivalentes, et on note $u_n \underset{+\infty}{\sim} v_n$ ou plus simplement $u_n \sim v_n$ si $\dfrac{u_n}{v_n} \tendversen{n\to +\infty} 1$. De manière équivalente, $u$ et $v$ sont équivalentes si $u_n-v_n=o(v_n)$.
\end{definition}

\begin{proposition}
  Soit $f$ une fonction définie au voisinage de $x_0$ et $\ell$ un réel \textbf{non nul}. Alors $f \underset{x_0}{\sim} \ell$ si et seulement si $f(x)\tendversen{x\to x_0} \ell$.

  De manière identique, $u_n\sim \ell$ si et seulement si $u_n\tendversen{n\to +\infty} \ell$.
\end{proposition}

\preuve[3]{En effet, puisque $\ell\neq 0$,
\[ f\underset{x_0}{\sim} \ell \Longleftrightarrow \frac{f(x)}{\ell}\tendversen{x\to x_0} 1 \Longleftrightarrow f(x)\tendversen{x\to x_0} \ell. \]
}

\begin{attention}
  On n'écrira jamais $f(x)\sim 0$, ce qui n'a aucun sens.
\end{attention}

  \subsection{Propriétés}

\begin{propriete}[Relation d'équivalence]
  Soient $f, g$ et $h$ trois fonctions définies au voisinage de $x_0$, et ne s'annulant pas au voisinage de $x_0$.

L'équivalence de fonctions est une relation d'équivalence :
\begin{itemize}
  \item \petittitre{Réflexivité} : $f \underset{x_0}{\sim} f$.
  \item \petittitre{Transitivité} : si $f \underset{x_0}{\sim} g$ et $g \underset{x_0}{\sim} h$ alors $f \underset{x_0}{\sim} h$.
  \item \petittitre{Symétrie} : si $f \underset{x_0}{\sim} g$ alors $g\underset{x_0}{\sim} f$.
\end{itemize}
\end{propriete}

\preuve[5]{Par calcul rapide :
\begin{align*}
  \frac{f(x)}{f(x)} &= 1 \tendversen{x\to x_0} 1\\
  \frac{f(x)}{h(x)} &= \frac{f(x)}{g(x)}\frac{g(x)}{h(x)} \tendversen{x\to x_0} 1\text{ par produit}\\
  \frac{g(x)}{f(x)} &= \frac{1}{\frac{f(x)}{g(x)}} \tendversen{x\to x_0} 1 \text{ par quotient.}
\end{align*}
}

\begin{propriete}[Limites]
  Soient $f$ et $g$ deux fonctions telles que $f\underset{x_0}{\sim} g$. Si $f$ admet une limite $\ell$ (finie, ou infinie) en $x_0$, alors $g$ admet également $\ell$ comme limite en $x_0$.
\end{propriete}

\preuve[3]{
En effet, \[ g(x) = \frac{g(x)}{f(x)} f(x) \tendversen{x\to x_0} 1\times \ell. \]
}

\begin{propriete}[Equivalent et petit o]
Soient $f, g$ et $h$ trois fonctions définies et ne s'annulant pas au voisinage de $x_0$. Alors
\begin{itemize}
  \item Si $f \underset{x_0}{\sim} g$ et $g \underset{x_0}{=} o(h)$, alors $f \underset{x_0}{=} o(h)$.
  \item Si $f \underset{x_0}{=} o(g)$ et $g \underset{x_0}{\sim} h$ alors $f \underset{x_0}{=} o(h)$.
\end{itemize}
\end{propriete}

\preuve[4]{
Les deux résultats reposent sur l'écriture \[ \frac{f(x)}{h(x)}= \frac{f(x)}{g(x)}\times \frac{g(x)}{h(x)}. \]
Par produit, dans les deux cas, ces limites sont nulles.
}

\begin{propriete}[Compatibilité avec produit, quotient et puissances]
  Soient $f, g, h$ et $\phi$ des fonctions définies et ne s'annulant pas au voisinage de $x_0$. On suppose que $f \underset{x_0}{\sim} g$. Alors
  \begin{itemize}
    \item $f\times \phi \underset{x_0}{\sim} g\times \phi$.
    \item $\frac{1}{f} \underset{x_0}{\sim} \frac{1}{g}$.
    \item Si $\lambda \in \R*$, $\lambda f \underset{x_0}{\sim} \lambda g$.
    \item Si $h \underset{x_0}{\sim} \phi$, alors $f\times h \underset{x_0}{\sim} g\times \phi$.
    \item Si $\alpha\in \R$ est tel que $f^\alpha$ et $g^\alpha$ sont bien définies au voisinage de $x_0$, on a $f^\alpha \underset{x_0}{\sim} g^\alpha$.
  \end{itemize}
\end{propriete}

\begin{attention}
  Pour la puissance, $\alpha$ ne doit pas dépendre de $x$ et doit être une constante. Par exemple, $1+x \underset{0}{\sim} 1$ et pourtant $(1+x)^{\frac{1}{x}} \underset{0}{\not\sim} 1$ puisque \[ (1+x)^{\frac{1}{x} }= \eu{\frac{1}{x}\ln(1+x)} \tendversen{x\to 0} \eu{1}=\E \text{ par composée et taux d'accroissement.}\]

  Enfin, l'équivalence n'est pas compatible avec l'addition. En effet, $1+x \underset{0}{\sim} 1$, $x-1\underset{0}{\sim} -1$ mais pourtant $2x \underset{x_0}{\not\sim} 0$ (qui n'a aucun sens).
\end{attention}

\preuve[8]{Il suffit d'écrire :
\begin{align*}
  \frac{f\times \phi}{g\times \phi} &= \frac{f}{g} \tendversen{x\to x_0} 1\\
  \frac{\frac{1}{f}}{\frac{1}{g}} &= \frac{g}{f} \tendversen{x\to x_0} 1\\
  \frac{\lambda f}{\lambda g} &= \frac{f}{g} \tendversen{x\to x_0} 1\\
  \frac{f\times h}{g\times \phi} &= \frac{f}{g} \frac{h}{\phi} \tendversen{x\to x_0} 1 \text{ par produit}\\
  \frac{f^\alpha}{g^\alpha} & =\left(\frac{f}{g}\right)^\alpha \tendversen{x\to x_0} 1 \text{ par composée}.
\end{align*}
}

\begin{propriete}[Substitution]
  Soient $f$ et $g$ deux fonctions définies au voisinage de $x_0$ telles que $f \underset{x_0}{\sim} g$.
  Soit $\phi$ une fonction définie sur intervalle $J$ de $\R$ et à valeurs dans $I$. Soit $t_0$ un point ou une extrémité (éventuellement infinie) de $J$. Si $\phi(t) \tendversen{t\to t_0} x_{0}$, alors $f(\phi(t)) \underset{t_{0}}{\sim} g(\phi(t))$.
\end{propriete}

\preuve[4]{
Simplement par composée
\[ \frac{f(x)}{g(x)}\tendversen{x\to x_0} 1 \qeq \phi(t)\tendversen{t\to t_0} x_0.\]
Par composée, \[ \frac{f(\phi(t))}{g(\phi(t))} \tendversen{t\to t_0} 1.\]
}

\begin{attention}
  On ne peut pas composer à gauche. Dans ce cas, il faut étudier le quotient en détail.

  Par exemple, si $f$ et $g$ sont strictement positives sur $I$, si $f \underset{x\to x_0}{\sim} g$ et si $g$ admet une limite en $x_0$ différente de $1$, alors $\ln(f) \underset{x\to x_0}{\sim} \ln(g)$. En effet
  \[ \frac{\ln(f)}{\ln(g)} = \frac{ \ln\left(g\times \frac{f}{g}\right)}{\ln(g)} = \frac{\ln(g)+\ln\left(\frac{f}{g}\right)}{\ln(g)}=1+ \frac{ \ln\left(\frac{f}{g}\right)}{\ln(g)} \tendversen{x\to x_0} 1. \]
\end{attention}

L'ensemble des propriétés vues précédemment sont valables pour les suites :

\begin{propriete}[Cas des suites]
  On considère des suites $u$, $v$, $w$, $x$ ne s'annulant pas à partir d'un certain rang.
  \begin{itemize}
    \item $u_n \sim u_n$. Si $u_n \sim v_n$ alors $v_n\sim u_n$. Enfin, si $u_n\sim v_n$ et $v_n\sim w_n$ alors $u_n \sim w_n$.
    \item Si $u_n\sim v_n$ et si $v$ admet une limite finie, alors $u$ admet la même limite.
    \item Si $u_n \sim v_n$ et $v_n=o(w_n)$ alors $u_n=o(w_n)$. De même, si $u_n=o(v_n)$ et $v_n\sim w_n$ alors $u_n=o(w_n)$.
    \item Si $u_n\sim v_n$, $w_n\sim x_n$ et $\alpha \in \R$ tel que $(u_n^\alpha)$ et $(v_n^\alpha)$ aient un sens, alors
    \[ u_nw_n\sim v_nw_n,\quad \frac{1}{u_n}\sim \frac{1}{v_n},\quad  u_nw_n\sim v_nx_n \qeq u_n^\alpha \sim v_n^\alpha. \]
    \item Si $f \underset{x_0}{\sim} g$ et si $u_n\tendversen{n\to +\infty} x_0$ alors $f(u_n)\underset{x_0}{\sim}g(u_n)$.
  \end{itemize}
\end{propriete}

  \subsection{\'Equivalences usuelles}

  On commence par des équivalences importantes :

  \begin{theoreme}[Polynômes et puissances]
  Soient $\alpha$ et $\beta$ deux réels tels que $0\leq \alpha < \beta$. Alors
  \[ \left(x^\alpha + x^\beta\right) \underset{+\infty}{\sim} x^\beta \qeq \left(x^\alpha + x^\beta \right) \underset{0}{\sim} x^\alpha. \]
  De plus, si $\alpha$ et $\beta$ sont des entiers, alors
  \[ \left(x^\alpha + x^\beta\right) \underset{-\infty}{\sim} x^\beta \]
  Ainsi, si $p<q$ et si $(a_p,\hdots, a_q)$ sont des réels tels que $a_p\neq 0$ et $a_q\neq 0$, alors
\[ \sum_{k=p}^q a_kx^k \underset{+\infty}{\sim} a_q x^q,\quad \sum_{k=p}^q a_kx^k \underset{0}{\sim} a_p x^p,\qeq \sum_{k=p}^q \frac{a_k}{x^k} \underset{+\infty}{\sim} \frac{a_p}{x^p}.\]
  \end{theoreme}

  \preuve[5]{En effet, puisque $\alpha-\beta<0$ et $\beta-\alpha>0$ :
  \[ \frac{x^\alpha+x^\beta}{x^\beta}= x^{\alpha-\beta} + 1 \underset{x\rightarrow +\infty}{\longrightarrow} 0+1 =1 \]
  et
  \[ \frac{x^\alpha+x^\beta}{x^\alpha}=1+x^{\beta-\alpha} \underset{x\rightarrow 0}{\longrightarrow} 1+0=1 \]
  Le reste s'en déduit des propriétés précédentes.
  }

Avant d'obtenir d'autres équivalents importants, montrons un lemme :

\begin{lemme}[Equivalent d'ordre $1$]
Soit $f$ une fonction définie sur un voisinage de $0$, qu'on suppose dérivable sur ce voisinage. On suppose que $f'(0)\neq 0$. Alors \[ f(x)-f(0) \underset{0}{\sim} f'(0)x. \]
\end{lemme}

\preuve[4]{
En effet, $x\mapsto f'(0)x$ ne s'annule pas sur un voisinage de $0$ (sauf en $0$) et
\[ \frac{f(x)-f(0)}{f'(0)x} = \frac{1}{f'(0)} \frac{f(x)-f(0)}{x}\tendversen{x\to x_0} \frac{1}{f'(0)} f'(0) = 1 \]
puisque $f$ est dérivable en $0$.
}

\begin{theoreme}[Equivalences usuelles]
On dispose des équivalents suivants :
\begin{multicols}{2}
  \begin{itemize}
    \item $\ln(1+x)\underset{0}{\sim} x$,
    \item $\eu{x}-1\ \underset{0}{\sim} x$,
    \item Si $\alpha \in \R*$, $(1+x)^\alpha -1 \underset{0}{\sim} \alpha x$,
    \item $\sin(x) \underset{0}{\sim} x$,
    \item $\tan(x) \underset{0}{\sim} x$,
    \item $\arctan(x) \underset{0}{\sim} x$,
    \item $\cos(x)-1 \underset{0}{\sim} -\frac{x^2}{2}$.
  \end{itemize}
\end{multicols}
\end{theoreme}

\preuve[6]{Les six premiers découlent du lemme précédent. Pour la dernière, on utilise la trigonométrie :
\begin{align*}
  \cos(x)-1 &= \cos\left(\frac{x}{2} +\frac{x}{2}\right)-1\\
  &= 1-2\sin^2\left(\frac{x}{2}\right)-1 \\
  &= -2 \sin^2\left(\frac{x}{2}\right)
\end{align*}
Pusique $\sin(x)\underset{0}{\sim} x$, par substitution, $\sin\left(\frac{x}{2}\right) \underset{0}{\sim} \frac{x}{2}$. Par exponentiation, \[ \left(\sin\left(\frac{x}{2}\right)\right)^2 \underset{0}{\sim} \left(\frac{x}{2}\right)^2 \]
et finalement
\[ \cos(x)-1 \underset{0}{\sim} -2\frac{x^2}{4}=-\frac{x^2}{2}. \]
}

En appliquant les résultats précédents, et la substitution, on en déduit les équivalents suivants sur les fonctions et les suites :

\begin{theoreme}
Soit $f$ une fonction définie au voisinage de $x_0$ telle que $u(x)\tendversen{x\to x_0} 0$. Soit $u$ une suite de limite $0$. Alors :
\begin{itemize}
  \item $\ln(1+f(x)) \underset{x_0}{\sim} f(x)$ et $\ln(1+u_n) \sim u_n$.
  \item $\eu{f(x)}-1  \underset{x_0}{\sim}  f(x)$ et $\eu{u_n}-1\sim u_n$.
  \item Si $\alpha\in \R*$, $(1+f(x))^\alpha -1  \underset{x_0}{\sim}  \alpha f(x)$ et $(1+u_n)^\alpha -1 \sim \alpha u_n$.
  \item $\sin(f(x)) \underset{x_0}{\sim}  f(x)$ et $\sin(u_n)\sim u_n$.
  \item $\tan(f(x))  \underset{x_0}{\sim} f(x)$ et $\tan(u_n)\sim u_n$.
  \item $\arctan(f(x))  \underset{x_0}{\sim}  f(x)$ et $\arctan(u_n)\sim u_n$.
  \item $\cos(f(x))-1 \underset{x_0}{\sim} -\frac{(f(x))^2}{2}$ et $\cos(u_n)-1\sim -\frac{u_n^2}{2}$.
\end{itemize}
\end{theoreme}

Enfin, on dispose de deux résultats, concernant $\ln$ et $n!$ :

\begin{corollaire}
 Soit $f$ une fonction définie au voisinage de $x_0$ telle que $f(x)\tendversen{x\to x_0} 1$. Soit $u$ une suite de limite $1$. Alors \[ \ln(f(x))  \underset{x_0}{\sim}  f(x)-1 \qeq \ln(u_n) \sim u_n-1. \]
\end{corollaire}

\preuve[3]{ En effet, $f(x)-1\tendversen{x\to x_0} 0$. D'après ce qui précède :
\[  \ln(f(x)) = \ln( 1+ (f(x)-1))  \underset{x_0}{\sim}  f(x)-1. \]
}

\begin{proposition}[Formule de Stirling]
  On a \[ n! \sim \left(\frac{n}{e}\right)^n \sqrt{2\pi n}. \]
\end{proposition}

  \subsection{Exemples}

\begin{exo}
Déterminer un équivalent de $\sin\left(\arctan\left(\frac{1}{n}\right)\right)$.
\end{exo}

\solution[8]{Puisque $\frac{1}{n}\tendversen{n\to +\infty} 0$, on a \[ \arctan\left(\frac{1}{n}\right) \sim \frac{1}{n} \]
mais on ne peut pas composer à gauche par $\sin$. On le traite donc dans l'autre sens.

On note $u_n=\arctan\left(\frac1n\right)$. Par composée, $u_n\tendversen{n\to +\infty} 0$. Par composée, on a alors \[ \sin(u_n)\sim u_n = \arctan\left(\frac1n\right). \]
Enfin, on a vu que $u_n\sim \frac{1}{n}$.

Par transitivité \[ \sin\left(\arctan\left(\frac1n\right)\right) \sim \frac{1}{n}.\]

}

\begin{exo}
  Déterminer un équivalent en $0$ de $\sin^4\left(\sqrt{x}\right)$.
\end{exo}

\solution[6]{Puisque $\sqrt{x}\tendversen{x\to 0} 0$, par composée \[ \sin\left(\sqrt{x}\right)  \underset{0}{\sim} \sqrt{x}. \]
Par exponentiation
\[ \sin^4\left(\sqrt{x}\right)  \underset{0}{\sim} \left(\sqrt{x}\right)^4 = x^2. \]

}

\begin{exo}
  Déterminer un équivalent de $\cos^4(x)-1$ en $0$.
\end{exo}

\solution[6]{Le seul équivalent qu'on a repose sur $\cos(x)-1$. On essaie de s'y ramener :
\begin{align*}
  \cos^4(x)-1 &= ( 1 + \underbrace{\cos(x)-1}_{\tendversen{x\to 0} 0})^4 - 1 \\
              & \underset{0}{\sim}  4\left(\cos(x)-1\right) \text{ car } (1+u)^4-1 \underset{0}{\sim}  4u \\
              &  \underset{0}{\sim}  4 \left(-\frac{x^2}{2}\right) = -2x^2.
\end{align*}
}

\begin{exo}
  Déterminer un équivalent simple de $\dfrac{x^x-1}{x}$ en $0^+$.
\end{exo}

\solution[6]{
Pour $x>0$ :
\begin{align*}
 \frac{x^x-1}{x} &= \frac{\eu{x\ln(x)}-1}{x}
\end{align*}
Par substitution, puisque $x\ln(x)\tendversen{x\to 0^+} 0$,
\[ \eu{x\ln(x)}-1  \underset{0}{\sim}  x\ln(x) \]
puis, par quotient
\[ \frac{\eu{x\ln(x)}-1}{x}  \underset{0}{\sim}  \ln(x). \]
}

%%% Fin du cours %%%
