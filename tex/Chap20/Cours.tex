\chapter{Formules de Taylor}
%!TeX root=../../encours.nouveau.tex


\objectifintro{Nous revenons sur les dérivées multiples, on s'intéressant à l'espace des fonctions plusieurs fois dérivables.
Nous introduisons ensuite deux résultats importants sur les fonctions dérivables : la formule de Taylor avec reste intégral, et l'inégalité de Taylor-Lagrange, qui nous servira régulièrement, ainsi que dans le chapitre suivant.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Frederick Taylor (1856--1015). \emph{Collaboration in the cloud}}
Je vous emploie pour votre force et vos capacités physiques. On ne vous demande pas de penser ; il y a des gens payés pour cela.
\end{extrait}

\begin{objectifs}
\begin{numerote}
\item Concernant les dérivées successives :
\begin{itemize}
  \item \lienobj{1}{Connaître la structure d'espace vectoriel des fonctions $D^n$, $\CC^n$ et $\CC^{\infty}$}
  \item \lienobj{2}{Connaître les dérivées successives des fonctions usuelles}
  \item \lienobj{3}{Connaître la formule de Leibniz}
\end{itemize}
\item \lienobj{4}{La formule de Taylor avec reste intégral}
\item \lienobj{5}{L'inégalité de Taylor-Lagrange}
\item \lienobj{6}{Savoir appliquer l'inégalité de Taylor-Lagrange à l'étude de limites}
\item Concernant l'étude des extrema :
\begin{itemize}
\item \lienobj{7}{Connaître la définition de point critique}
\item \lienobj{8}{Savoir utiliser la condition suffisante d'existence d'extrema dans le cas $\CC^2$}
\end{itemize}
\end{numerote}
\end{objectifs}





%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

Dans l'ensemble de ce chapitre, $I$ désignera un intervalle non vide, et non réduit à un point.

\section{Dérivées successives}

On va tout d'abord revenir sur la notion de dérivée successive, et ajouter diverses propriétés sur celle-ci.

  \subsection{Définition}

\begin{definition}[Dérivées successives]
Soit $f:I\to \R$ une fonction dérivable sur $I$.

Si $f'$ est également dérivable sur $I$, on dit que $I$ est deux fois dérivable et on appelle dérivée seconde de $f$, notée $f''$ ou $f^{(2)}$, la dérivée de $f'$.

Par récurrence, si $n$ est un entier supérieur ou égal à $2$, on dit que $f$ est $n$ fois dérivable sur $I$ si :
\begin{itemize}
  \item $f$ est $n-1$ fois dérivable sur $I$,
  \item et si $f^{(n-1)}$ est dérivable sur $I$.
\end{itemize}
La dérivée $(f^{(n-1)})'$ est appelée dérivée d'ordre $n$ (ou dérivée $n$-ième) et est notée $f^{(n)}$.

Par convention, on note $f^{(1)}=f'$ et $f^{(0)}=f$.
\end{definition}

\begin{notation}
  On note, pour tout entier $n\in \N*$, $D^n(I, \R)$, ou plus simplement $D^n(I)$, l'ensemble des fonctions $n$ fois dérivable sur $I$.
\end{notation}

Lorsqu'une fonction est dérivable, on peut s'intéresser à la continuité de sa dérivée.

\begin{definition}
Soit $n$ un entier. On dit que $f$ est de classe $\CC^n$ sur $I$ si et seulement si $f$ est $n$ fois dérivable sur $I$ et si $f^{(n)}$ est continue sur $I$.
\end{definition}

\begin{notation}
On note $\CC^n(I,\R)$, ou plus simplement $\CC^n(I)$ l'ensemble des fonctions de classe $\CC^n$ sur $I$.
\end{notation}

D'après le théorème d'analyse classique, toute fonction dérivable sur $I$ est continue sur $I$. On obtient alors les inclusions
\[ \CC^n(I)\subset D^n(I) \subset \CC^{n-1}(I) \subset \hdots \subset \CC^1(I) \subset D^1(I) \subset \CC^0(I). \]

Enfin, on dispose d'un ensemble important : les fonctions qui sont dérivables une infinité de fois :

\begin{definition}
On dit que $f$ est de classe $\CC^{\infty}$ sur $I$ si, pour tout entier $n$, $f$ est de classe $\CC^n$ sur $I$.

Ainsi, $f$ est infiniment dérivable sur $I$.
\end{definition}

\begin{notation}
On note $\CC^{\infty}(I,R)$, ou plus simplement $\CC^{\infty}(I)$ l'ensemble des fonctions de classe $\CC^{\infty}$ sur $I$.
\end{notation}

\begin{remarque}
Par définition, et toujours en utilisant le théorème d'analyse classique :
\[ \CC^{\infty}(I) = \bigcap_{n\in \N} \CC^n(I) = \bigcap_{n\in \N} D^n(I). \]
\end{remarque}

  \subsection{Exemples classiques}

Dans cette partie, on revoit les fonctions usuelles. Leurs dérivées successives doivent être retrouvées rapidement.

\begin{theoreme}[$\cos$ et $\sin$]
Les fonctions cosinus et sinus sont de classe $\CC^{\infty}$ sur $\R$, et on a :
\[ \forall n\in \N,\, \cos^{(n)} = \left \{ \begin{array}{ccc}\cos &\text{si} & n\equiv 0[4]\\-\sin &\text{si} & n\equiv 1[4]\\-\cos &\text{si} & n\equiv 2[4]\\\sin &\text{si} & n\equiv 3[4]\end{array}\right. \qeq \sin^{(n)} = \left \{ \begin{array}{ccc}\sin &\text{si} & n\equiv 0[4]\\\cos &\text{si} & n\equiv 1[4]\\-\sin &\text{si} & n\equiv 2[4]\\-\cos &\text{si} & n\equiv 3[4]\end{array}\right. \]
\end{theoreme}

\preuve[3]{Cela se fait par récurrence, en posant $P$ la proposition $P_n$ : \og{}$\cos$ est de classe $\CC^{4n+3}$ et \[ \cos^{(4n)}=\cos,\quad \cos^{(4n+1)}=-\sin,\quad \cos^{(4n+2)} = -\cos\qeq \cos^{(4n+3)}=\sin. \fg\]
et de même pour $\sin$.
}

\begin{theoreme}[Monôme]
Pour tout entier $n$, notons $f_n:x\mapsto x^n$. Alors $f_n$ est de classe $\CC^{\infty}$ sur $\R$ et pour tout entier $k$, on a :
\begin{align*}
  f_n^{(k)}:x\mapsto &\left \{\begin{array}{lll}n(n-1)\hdots (n-k+1)x^{n-k} & \text{si} & k\leq n\\0 & \text{sinon}& \end{array}\right.\\
  & \left\{ \begin{array}{ccc} A_n^k x^{n-k} & \text{si} & n< k \\
  n! & \text{si} & n=k\\
  0 & \text{si} & n>k\end{array}\right..
\end{align*}
\end{theoreme}

\preuve[10]{Fixons $n$ et raisonnons par récurrence sur $k$ : on note $P_k$ la proposition \og{}$f_n$ est $k$ fois dérivable et sa dérivée est donnée par la proposition\fg{}.
\begin{itemize}
  \item $k=0$, $f_n^{(0)} = f_n = x\mapsto x^n$ et la propriété est bien vérifiée.
  \item Supposons la proposition vraie pour un certain entier $k$. Ainsi, $f_k$ est $k$ fois dérivable.
  \begin{itemize}
    \item si $k\geq n$, alors $f_n^{(k)}$ est constante ou nulle, donc est dérivable et sa dérivée est nulle.
    \item si $k<n$, alors \[ f_n^{(k)}:x \mapsto n(n-1)\hdots (n-k+1)x^{n-k} \]
    et est donc dérivable (polynôme), de dérivée
    \[ \left(f_n^{(k)}\right)' = x\mapsto n(n-1)\hdots (n-k+1) (n-k)x^{n-k-1}.\]
  \end{itemize}
  Dans les deux cas, la propriété est héréditaire.
\end{itemize}
Le principe de récurrence nous permet alors de conclure.
}

\begin{theoreme}[Exponentielle et logarithme]
$\exp$ est de classe $\CC^{\infty}$ sur $\R$, et $\ln$ est de classe $\CC^{\infty}$ sur $\R>$, et on a
\[ \forall n\in \N,\quad \exp^{(n)}=\exp \qeq \ln^{(n)} = x\mapsto \frac{(-1)^{n-1} (n-1)!}{x^n}. \]
\end{theoreme}

\preuve[12]{Par récurrence à nouveau. Pour $\exp$ c'est rapide, puisque $\exp'=\exp$. Soit $P$ la proposition définie pour tout entier $n\geq 1$ par \og{}$\ln$ est $k$ fois dérivable et sa dérivée est $x\mapsto \frac{(-1)^{n-1}(n-1)!}{x^n}$\fg{}.
\begin{itemize}
  \item Pour $n=1$, $\ln$ est dérivable et sa dérivée est $x\mapsto \frac1x = \frac{(-1)^{1-1}(1-1)!}{x^1}$. Ainsi, $P_1$ est vraie.
  \item Supposons la proposition $P_n$ vraie pour un certain entier $n\geq 1$. Ainsi, $\ln$ est $n$ fois dérivable et sa dérivée est \[ x\mapsto \frac{(-1)^{n-1}(n-1)!}{x^n}. \]
  Mais alors, par quotient, cette fonction est elle-même dérivable, donc $\ln$ est $n+1$ fois dérivable, et en dérivant :
  \[ \ln^{(n+1)} = x\mapsto (-1)^{n-1}(n-1)!\frac{-n}{x^{n+1}} = \frac{(-1)^{n}n!}{x^{n+1}}. \]
  Ainsi, $P_{n+1}$ est vérifiée.
\end{itemize}
Le principe de récurrence nous permet alors de conclure.
}

\begin{attention}
  Il existe des fonctions dérivable sur $\R$ mais pas de classe $\CC^1$ en un point. On peut s'intéresser par exemple à
  \[ x\mapsto \left \{ \begin{array}{lll} x^2\sin\left(\frac{1}{x}\right)&\text{si} x\neq 0\\0&\text{sinon}&\end{array}\right. \]
  Cette fonction est continue et dérivable sur $\R$, mais pas de classe $\CC^1$ en $0$.

  En prenant des primitives successives de cette fonction, on a donc des fonctions de classe $\CC^n$ qui ne sont pas de classe $\CC^{n+1}$.
\end{attention}

  \subsection{Opérations sur les dérivées}

    \subsubsection{Addition, multiplication}

On dispose d'un résultat fondamental :
\begin{proposition}[Structure d'espace vectoriel]
Pour tout entier $n\in \N*$, $D^n(I, \R)$ est un $\R$-espace vectoriel.

Si $f$ et $g$ sont deux fonctions de $D^n(I, \R)$, et $\lambda \in \R$, alors $\lambda f +g \in D^n(I,\R)$ et on a
\[ \left(\lambda f + g\right)^{(n)} = \lambda f^{(n)}+g^{(n)}. \]

Pour tout entier $n$, $\CC^n(I, \R)$ ainsi que $\CC^{\infty}(I, \R)$ sont également des espaces vectoriels.
\end{proposition}

\begin{demonstration}
Cela se fait par récurrence, laissée au lecteur.
\end{demonstration}

D'après le résultat sur les monômes vu plus haut, on peut rappeler le résultat sur les polynômes :

\begin{proposition}
Si $P$ est un polynôme de degré $n$, alors $P$ est de classe $\CC^{\infty}$ et pour tout $k>n$, $P^{(k)}$ est le polynôme nul.
\end{proposition}

\preuve[4]{En effet, un polynôme est une combinaison linéaire de monômes. Puisque $\CC^{\infty}(\R, \R)$ est un espace vectoriel, $P$ est de classe $\CC^{\infty}$ et le résultat découle de ce qui précède.}

Pour le produit de deux fonctions, le résultat est plus compliqué :

\begin{theoreme}[Formule de Leibniz]
Soit $n\in \N*$. Soient $f$ et $g$ deux fonctions de $D^n(I, \R)$. Alors $fg$ est également de classe $D^n$ et on a
\[ (fg)^{(n)} = \sum_{k=0}^n \binom{n}{k} f^{(k)}g^{(n-k)}. \]
\end{theoreme}

\preuve[20]{Démontrons le par récurrence sur $n$. Soit $P$ la proposition définie pour tout entier $n\geq 1$ par \og{}Pour toutes fonctions $f$ et $g$ de $D^n(I,\R)$, $fg$ est de classe $D^n$ et $\ds{\left(fg\right)^{(n)}=\sum_{k=0}^n \binom{n}{k}f^{(k)}g^{(n-k)}}$.
\begin{itemize}
  \item pour $n=1$, si $f$ et $g$ sont de classe $D^1$, alors $fg$ est dérivable et \[ (fg)' = f'g+fg' = \binom{1}{1}f^{(1)}g^{(0)}+\binom{1}{0}f^{(0)}g^{(1)}. \]
  $P_1$ est donc vérifiée.
  \item Supposons la proposition $P_n$ vérifiée. Soient $f$ et $g$ deux fonctions de classe $D^{n+1}$. Elles sont donc au moins de classe $D^n$. Par hypothèse de récurrence, $fg$ est de classe $D^n$ et \[ (fg)^{(n)} = \sum_{k=0}^n \binom{n}{k}f^{(k)}g^{(n-k)}. \]
  Constatons alors que $(fg)^{(n)}$ est dérivable comme somme et produit de fonctions dérivables (puisque $f$ et $g$ sont $n+1$ fois dérivables). On dérive :
  {\allowdisplaybreaks
  \begin{align*}
    (fg)^{(n+1)} &= \sum_{k=0}^n \binom{n}{k}\left( f^{(k+1)}g^{(n-k)}+ f^{(k)}g^{(n-k+1)}\right) \\
    &= \sum_{k=0}^n \binom{n}{k}f^{(k+1)}g^{(n-k)} + \sum_{k=0}^n \binom{n}{k} f^{(k)}g^{(n-k+1)}\\
    &=  \sum_{k=0}^n \binom{n}{k}f^{(k+1)}g^{(n+1-(k+1))} + \sum_{k=0}^n \binom{n}{k} f^{(k)}g^{(n-k+1)}\\
    &= \sum_{i=1}^{n+1} \binom{n}{i-1} f^{(i)} g^{(n+1-i)} + \sum_{k=0}^n \binom{n}{k} f^{(k)}g^{(n+1-k)} \text{ en posant }i=k+1\\
    &= \sum_{i=1}^{n} \left(\binom{n}{i-1}+\binom{n}{i}\right) f^{(i)}g^{(n+1-i)} +   \binom{n}{n}f^{(n)}g^{(0)} + \binom{n}{0}f^{(0)} g^{(n+1)} \\
    &= \sum_{i=1}^{n} \binom{n+1}{i} f^{(i)}g^{(n+1-i)} + \binom{n+1}{n+1}f^{(n)}g^{(0)} + \binom{n+1}{0}f^{(0)} g^{(n+1)} \text{ par la formule de Pascal}\\
    &= \sum_{i=0}^{n+1} \binom{n+1}{i} f^{(i)}g^{(n+1-i)}
  \end{align*}}
  Ainsi, $P_{n+1}$ est vérifiée, et le principe de récurrence nous garantit qu'elle est vraie pour tout $n\geq 1$.
\end{itemize}
}

Par produit de fonctions continues, on en déduit également le résultat suivant

\begin{proposition}
  Soit $n\in \N \cup \{+\infty\}$.
  Soient $f$ et $g$ deux fonctions de $\CC^n(I,\R)$. Alors $fg$ est également de classe $\CC^n$.
\end{proposition}

\begin{exo}
Soit $f$ la fonction définie sur $\R$ par $f:x\mapsto x^2\eu{x}$. Justifier que $f$ est de classe $\CC^{\infty}$ et déterminer, pour tout entier $n$, $f^{(n)}$.
\end{exo}

\solution[7]{$f$ est le produit des fonctions carrées et exponentielle, fonctions de classe $\CC^{\infty}$. $f$ est donc de classe $\CC^{\infty}$. Notons $u:x\mapsto x^2$ et $v:x\mapsto \eu{x}$. Alors, pour tout $n\geq 3$, $u^{(n)}=0$.
\begin{itemize}
  \item On a $f':x\mapsto (x^2+2x)\eu{x}$ et $f^{(2)}:x\mapsto (x^2+4x+2)\eu{x}$.
  \item Pour tout $n\geq 3$, par la formule de Leibniz :
  \begin{align*}
\forall x\in \R,\quad f^{(n)}(x) &= \sum_{k=0}^n \binom{n}{k} u^{(k)}(x)v^{(n-k)}(x) \\
&= \sum_{k=0}^2 \binom{n}{k} u^{(k)}(x) v^{(n-k)}(x) + \underbrace{\sum_{k=3}^n \binom{n}{k}u^{(k)}(x)v^{(n-k)}(x)}_{=0} \\
&= \binom{n}{0} x^2 \eu{x} + \binom{n}{1} 2x\eu{x}+ \binom{n}{2} 2\eu{x}\\
&= \left( x^2 + 2nx + n(n-1)\right)\eu{x}
  \end{align*}
Remarquons que cette formule est valable pour $n=0, 1$ et $2$.
\end{itemize}
Ainsi \[ \boxed{\forall n\in \N,\, \quad f^{(n)}:x\mapsto \left(x^2+2nx+n(n-1)\right)\eu{x}.}\]
}

Une dernière proposition pour le quotient :

\begin{proposition}[Quotient]
Soient $f$ et $g$ deux fonctions de classe $D^n$ (respectivement $\CC^n$, respectivement $\CC^{\infty}$) sur $I$ telles que $g$ ne s'annule pas sur $I$. Alors $\frac{1}{g}$ et $\frac{f}{g}$ sont de classe $D^n$ (resp. $\CC^n$, resp. $\CC^{\infty}$) sur $I$.
\end{proposition}

    \subsubsection{Composition}

Les espaces $D^n(I, \R)$, $\CC^n(I, \R)$ et $\CC^{\infty}(I, \R)$ sont stables par composition :

\begin{proposition}[Composition]
Soient $f: I \to \R$ et $g: J \to \R$ avec $J$ un intervalle de $\R$ tel que $f(I) \subset J$.
\begin{itemize}
\item Soit $n \in \N*$. Si $f \in D^{n}(I, \R)$ et $g \in D^{n}(J, \R)$, alors $g \circ f \in D^{n}(I, \R)$.
\item Soit $n \in \N \cup\{+\infty\}$. Si $f \in \CC^{n}(I,\R)$ et $g \in \CC^{n}(J, \R)$, alors $g \circ f \in \CC^{n}(I, \R)$.
\end{itemize}
\end{proposition}

\preuve[10]{Prouvons le cas de $D^n$, les autres se montrant de la même manière. Soit $P$ la proposition définie pour tout entier $n\geq 1$ par $P_n$: \og{}pour toutes fonctions $f\in D^n(I, \R)$ et $g\in D^n(J,\R)$, $g\circ f$ est de classe $D^n$ sur $I$.
\begin{itemize}
  \item Pour $n=1$, le résultat vu dans le chapitre sur la dérivabilité garanti que $g\circ f$ est dérivable, de dérivée $f'\times g'\circ f$.
  \item Supposons la proposition $P_n$ vraie pour un certain entier $n$. Soient $f$ et $g$ deux fonctions de classe $D^{n+1}$ respectivement sur $I$ et $J$. Alors, entre autre, $f$ et $g$ sont de classe $D^1$. Ainsi, $g\circ f$ est dérivable, et sa dérivée est \[ (g\circ f)' = f' \times g'\circ f. \]
  Or, $f', g'$ et $f$ sont de classe $D^n$ (puisque $f$ et $g$ sont $D^{n+1}$). Par hypothèse de récurrence, $g'\circ f$ est de classe $D^n$, et par produit, $f'\times g'\circ f$ l'est également. Donc $g\circ f$ est de classe $D^{n+1}$ et la proposition est héréditaire.
\end{itemize}
Par le principe de récurrence, on en déduit le résultat.
}
  \subsection{Fonctions usuelles}

Toutes les fontions usuelles sont de classe $\CC^{\infty}$ sur leur domaine de définition :

\begin{proposition}
Les fonctions polynôme, $\exp$, $\ln$, $\sin$, $\cos$, $\tan$, $\arctan$ et les fractions rationnelles sont de classe $\CC^{\infty}$ sur leur domaine de définition.
\end{proposition}

\preuve[4]{On a vu précédemment le cas des fonctions polynômes, $\exp$, $\ln$, $\sin$ et $\cos$. $\tan=\frac{\sin}{\cos}$ et les fractions rationnelles sont de classe $\CC^{\infty}$ sur leur domaine de définition comme quotient de fonctions de classe $\CC^{\infty}$ dont le dénominateur ne s'annule pas. Enfin, $\arctan$ est dérivable et $\arctan':x\mapsto \frac{1}{1+x^2}$. Sa dérivée est de classe $\CC^{\infty}$ (fraction rationnelle) sur $\R$, donc $\arctan$ également.
}

\section{Formules de Taylor}

Nous allons, dans cette partie, exprimer les fonctions, quand cela est possible, en fonction de ses dérivées.

Dans toute la suite, $n$ désigne un entier naturel. Par convention, si $a>b$, l'intervalle $\interff{a b}$ désigne l'intervalle $\interff{b a}$.

  \subsection{Formule de Taylor avec reste intégral}

La formule de Taylor avec reste intégral donne une formule exacte de $f$ en fonction de la valeur de ses dérivées en un point.

\begin{theoreme}[Formule de Taylor avec reste intégral]
  Soit $f$ une fonction de classe $\CC^{n+1}$ sur $I$. Alors
  \[ \forall (a, b)\in I^2,\quad f(b) = \sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k + \int_a^b \frac{(b-t)^n}{n!}f^{(n+1)}(t)\dd t. \]
Cette formule est appelée \textbf{formule de taylor avec reste intégral d'ordre $n$}.
\end{theoreme}

\preuve[20]{On va le démontrer par récurrence sur $n$. Soit $P$ la proposition définie, pour tout entier $n$, par $P_n$: \og{}pour toute fonction $f$ de classe $\CC^{n+1}$ sur $I$, et tout $(a,b)\in I^2$, on a $\ds{f(b) = \sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k + \int_a^b \frac{(b-t)^n}{n!}f^{(n+1)}(t)\dd t}.$\fg{}.
\begin{itemize}
  \item Pour $n=0$, si $f$ est de classe $\CC^1$ sur $I$, l'intégrale a un sens (car $t\mapsto \frac{(b-t)^n}{n!}f'(t)$ est continue sur $I$) et on a
\begin{align*}
\sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k + \int_a^b \frac{(b-t)^n}{n!}f^{(n+1)}(t)\dd t &= \frac{f(a)}{0!}(b-a)^0 + \int_a^b \frac{(b-t)^0}{0!}f'(t)\dd t \\
&= f(a) + \int_a^b f'(t)\dd t = f(a)+\left[f(t)\right]_a^b = f(b)
\end{align*}
La proposition est donc vraie pour $n=0$.
\item Supposons la proposition $P_n$ vraie pour un certain entier $n$. Soient $f$ de classe $\CC^{n+2}$ et $(a,b)\in I^2$. Par hypothèse de récurrence, $f$ est de classe $\CC^{n+1}$ et donc
\begin{align*}
 f(b) = \sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k + \int_a^b \frac{(b-t)^n}{n!}f^{(n+1)}(t)\dd t.
\end{align*}
Procédons à une intégration par parties dans l'intégrale. On pose $u=f^{(n+1)}$ et $v':t\mapsto \frac{(b-t)^n}{n!}$, soit $v:t\mapsto \frac{-(b-t)^{n+1}}{(n+1)!}$. $u$ et $v$ sont de classe $\CC^1$ (car $f$ est de classe $\CC^{n+2})$ sur $\interff{a b}$. Par intégration par parties :
\begin{align*}
f(b) &= \sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k + \int_a^b \frac{(b-t)^n}{n!}f^{(n+1)}(t)\dd t\\
&= \sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k + \left[ f^{(n+1)}(t)\frac{-(b-t)^{n+1}}{(n+1)!} \right]_a^b-\int_a^b \frac{-(b-t)^{n+1)}}{(n+1)!}f^{(n+2)}(t) \dd t\\
&=  \sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k + \left(0 - f^{(n+1)}(a)\frac{-(b-a)^{n+1}}{(n+1)!}\right)-\int_a^b \frac{-(b-t)^{n+1}}{(n+1)!}f^{(n+2)}(t) \dd t\\
&= \sum_{k=0}^{n+1} \frac{f^{(k)}(a)}{k!}(b-a)^k + \int_a^b \frac{(b-t)^{n+1}}{(n+1)!} f^{(n+2)}(t)\dd t.
\end{align*}
Ainsi $P_{n+1}$ est vérifiée, et d'après le principe de récurrence, la proposition est vraie pour tout entier $n$.
\end{itemize}
}

\begin{remarque}
Si $f$ est un polynôme de degré $p$, pour $n>p$, $f^{(n)}=0$. L'intégrale est alors nulle, et on retrouve la formule de Taylor pour les polynômes.
\end{remarque}

Cette remarque admet une réciproque :

\begin{proposition}
Soit $n\in \N*$ et $f$ une fonction de classe $\CC^n$ sur $\R$ telle que $f^{(n)}$ est la fonction nulle. Alors $f$ est une fonction polynomiale, de degré au plus $n-1$.
\end{proposition}

\preuve[5]{Il suffit d'appliquer la formule de Taylor. $f^{(n)}$ étant nulle, la formule de Taylor d'ordre $n-1$, appliquée entre $0$ et $x\in \R$ donne alors :
\[ \forall x\in \R, \quad f(x)=\sum_{k=0}^{n-1} \frac{f^{(k)}(0)}{k!}(x-0)^k + \underbrace{\int_0^x \frac{(x-0)^{n-1}}{(n-1)!} f^{(n)}(t)\dd t}_{=0} = \sum_{k=0}^{n-1} \frac{f^{(k)}(0)}{k!}x^k . \]
Donc $f\in \R_{n-1}[X]$.
}
  \subsection{Inégalité de Taylor-Lagrange}

Souvent, le terme intégral n'est pas utile, et on peut se contenter de le majorer : c'est l'inégalité de Taylor-Lagrange.

\begin{theoreme}[Inégalité de Taylor-Lagrange]
Soit $f$ une fonction de classe $\CC^{n+1}$ sur $I$. Alors
\[ \forall (a, b)\in I^2,\quad \left | f(b)-\sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k\right| \leq \frac{|b-a|^{n+1}}{(n+1)!} \max_{\interff{a b}}\left |f^{(n+1)}\right|. \]
\end{theoreme}

\preuve[15]{
Tout d'abord, remarquons que si $f$ est de classe $\CC^{n+1}$, $f^{(n+1)}$ est continue sur $I$, et est donc bornée et atteint ses bornes. Le $\max$ a donc un sens. Notons, pour simplifier $M=\max\limits_{\interff{a b}} |f^{(n+1)}|$ (en rappelant que $\interff{a b}=\interff{b a}$ si $a>b$).

\begin{itemize}
  \item Si $a\leq b$, on part de la formule de Taylor avec reste intégral et on applique l'inégalité triangulaire :
  \begin{align*}
\left |  f(b)-\sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k\right | &= \left | \int_a^b \frac{(b-t)^n}{n!} f^{(n+1)}(t)\dd t\right | \\
&\leq \int_a^b \left| \frac{(b-t)^n}{n!} f^{(n+1)}(t)\right |\dd t \\
&\leq \int_a^b \frac{(b-t)^n}{n!} |f^{(n+1)}(t)|\dd t \\
&\leq M \int_a^b \frac{(b-t)^n}{n!}\dd t = M \left[ -\frac{(b-t)^{n+1}}{(n+1)!}\right]_a^b = M \frac{(b-a)^{n+1}}{(n+1)!}
  \end{align*}
  \item Si $a>b$, le principe est le même en échangeant les bornes en passant à l'inégalité triangulaire :
  \begin{align*}
\left |  f(b)-\sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k\right | &= \left | \int_a^b \frac{(b-t)^n}{n!} f^{(n+1)}(t)\dd t\right | \\
&\leq \int_b^a \left| \frac{(b-t)^n}{n!} f^{(n+1)}(t)\right |\dd t \\
&\leq \int_b^a \frac{(t-b)^n}{n!} |f^{(n+1)}(t)|\dd t \\
&\leq M \int_b^a \frac{(t-b)^n}{n!}\dd t = M \left[ \frac{(t-b)^{n+1}}{(n+1)!}\right]_b^a = M \frac{(a-b)^{n+1}}{(n+1)!}
  \end{align*}
\end{itemize}
Dans tous les cas
\[ \left |  f(b)-\sum_{k=0}^n \frac{f^{(k)}(a)}{k!}(b-a)^k\right | \leq M \frac{|b-a|^{n+1}}{(n+1)!}. \]
}

  \subsection{Applications}

Les formules précédentes permettent de déterminer des résultats sur des limites de sommes que nous étudierons prochainement. Montrons-en un :

\begin{proposition}[Série exponentielle]
  Pour tout réel $x$, on a \[ \lim_{n\to +\infty} \sum_{k=0}^n \frac{x^k}{k!} = \eu{x}. \]
\end{proposition}

\begin{remarque}
  On notera prochainement $\ds{\sum_{n=0}^{+\infty} \frac{x^n}{n!} = \eu{x}}$. Remarquons que le résultat permet de montrer l'existence de l'exponentielle.
\end{remarque}

\preuve[10]{Soit $n\in \N*$. La fonction $\exp$ est de classe $\CC^{n+1}$ (puisqu'elle est $\CC^{\infty})$ sur $\R$. Appliquons l'inégalité de Taylor-Lagrange entre $0$ et $x\in \R$ :
\begin{align*}
  \left |\, \eu{x} - \sum_{k=0}^n \frac{\exp^{(k)}(0)}{k!}(x-0)^k\,\right| &\leq \frac{(x-0)^{n+1}}{(n+1)!} \max_{\interff{0 x}} |\exp^{(n+1)}|
\end{align*}
Or, pour tout entier $k$, $\exp^{(k)}=\exp$. De plus, par croissance de la fonction $\exp$, \[ \max_{\interff{0 x}} |\exp^{(n+1)}| = \left \{ \begin{array}{ccc} \eu{x}&\text{si}& x\geq 0\\1 & \text{si} &x<0\end{array}\right. = \max(1, \eu{x})  \]
et ne dépend pas de $n$.

Ainsi :
\begin{align*}
  \left|\, \eu{x}-\sum_{k=0}^n \frac{x^k}{k!}\,\right|&\leq \frac{x^{n+1}}{(n+1)!} \max(1, \eu{x}) \tendversen{n\to +\infty} 0 \text{ par croissances comparées.}
\end{align*}
Ce qui montre, d'après le théorème d'encadrement, que \[ \sum_{k=0}^n \frac{x^k}{k!} \tendversen{n\to +\infty} \eu{x}.\]
}

\section{Applications à l'étude d'extrema locaux}

Dans cette section, on se fixe un intervalle $I$ non vide, et non réduit à un point.

  \subsection{Point critique}

Rappelons la définition d'extremum local, vue précédemment.

\begin{definition}[Extremum local]
Soient $f: I \to \R$ et $x_0 \in I$.

On dit que $f$ admet un \textbf{maximum} (respectivement un \textbf{minimum}) \textbf{local} en $x_0$ s'il existe $\alpha>0$ tel que, pour tout $x \in I \cap\interff{{x_0-\alpha} {x_0+\alpha}}, f(x) \leq f\left(x_0\right)$ (resp. $f(x) \geq f\left(x_0\right)$ ).

On dit que $f$ admet un \textbf{extremum local} en $x_0$ si $f$ admet un maximum ou un minimum local en $x_0$.
\end{definition}

On a vu dans le chapitre sur la dérivabilité une condition nécessaire d'existence d'extremum quand la fonction est dérivable.

\begin{definition}[Point critique]
Soient $f$ une fonction dérivable sur $I$ et $x_0\in I$. On dit que $x_0$ est un \textbf{point critique} de $f$ sur $I$ si $f'(x_0)=0$.
\end{definition}

On a alors vu :

\begin{proposition}
Soit $f$ une fonction dérivable sur $I$ et $x_0\in I$ un point qui n'est pas à la frontière de $I$. Si $f$ admet un extremum local en $x_0$, alors $x_0$ est un point critique de $f$.
\end{proposition}

\begin{attention}
  Nous avons déjà vu que la réciproque est fausse, par exemple en s'intéressant à la fonction cube.

  De même, si $x_0$ est un point de la frontière, le résultat n'est pas vabale. Par exemple, si $f:x\mapsto x$ sur $\interff{1 2}$, alors $2$ est un extremum local de $f$, et pourtant $f'(2)\neq 0$.
\end{attention}

  \subsection{Condition suffisante d'existence d'extrema}

On va pouvoir énoncer une condition suffisante dans le cas où la fonction est de classe $\CC^2$.

\begin{theoreme}[Condition suffisante dans le cas $\CC^2$]
\labelobj{8}
Soit $f$ une fonction de classe $\CC^2$ sur $I$, et $x_0$ un élément de $I$ qui n'est pas  à la frontière de $I$.

Si $x_0$ est un point critique de $f$ alors :
\begin{itemize}
  \item Si $f''(x_0)>0$, $f$ admet un minimum local en $x_0$.
  \item Si $f''(x_0)<0$, $f$ admet un maximum local en $x_0$.
\end{itemize}
\end{theoreme}

\begin{attention}
  Si $f''(x_0)=0$, on ne peut pas conclure.
\end{attention}

\preuve[15]{On va démontrer le cas $f''(x_0)>0$, l'autre cas se traitant de la même manière.

$f$ étant de classe $\CC^2$, on peut écrire la formule de Taylor avec reste intégral à l'ordre $1$, entre $x_0$ et $x\in I$ :
\begin{align*}
  f(x) &= f(x_0)+x\underbrace{f'(x_0)}_{=0}+ \int_{x_0}^x \frac{(x-t)^1}{1!}f^{(2)}(t)\dd t \\
  &= f(x_0) + \int_{x_0}^x (x-t)f''(t)\dd t.
\end{align*}
Puisque $f''(x_0)>0$, il existe un voisinage de $x_0$, de la forme $I\cap \interff{{x_0-\alpha} {x_0+\alpha}}$ tel que, \[ \forall x\in  I\cap \interff{{x_0-\alpha} {x_0+\alpha}},\quad f''(x)\geq 0. \]
Mais alors, soit $x\in  I\cap \interff{{x_0-\alpha} {x_0+\alpha}}$.
\begin{itemize}
  \item Si $x\geq x_0$, $(x-t)\geq 0$ pour tout $t\in \interff{x_0 x}$, et par positivité de l'intégrale
  \[ \int_{x_0}^x (x-t)f''(t)\dd t \geq 0. \]
  \item Si $x\leq x_0$, $(x-t)\leq 0$ pour tout $t\in \interff{x_0 x}$, et par positivité de l'intégrale (attention : les bornes sont dans le mauvais sens)
  \[ \int_{x_0}^x (x_0-t)f''(t)\dd t \geq 0. \]
\end{itemize}
Dans tous les cas, on en déduit :
\[\forall x \in I\cap \interff{{x_0-\alpha} {x_0+\alpha}},\quad f(x)-f(x_0)\geq 0 \implies f(x)\geq f(x_0). \]
Ainsi, $f$ admet un minimum local en $x_0$.
}

\begin{exo}
  Soit $f$ la fonction définie sur $\R*$ par $f:x\mapsto x^2+4x-6\ln(|x|)$. \'Etudier les extrema locaux de $f$.
\end{exo}

\solution[10]{$f$ est de classe $\CC^2$ sur $\R>$ et sur $\R<$. On détermine tout d'abord ses points critiques. Remarquons que sur $\R*$, $f$ se dérive en \[f':x\mapsto 2x+4-\frac{6}{x}=\frac{2x^2+4x-6}{x}\]
Les racines de $2x^2+4x-6$ sont $1$ et $-3$. Il y a donc deux points critiques.

Calculons $f''$: \[ f'' : x\mapsto 2+\frac{6}{x^2} \]
et \[ f''(1)>0 \qeq f''(-3)>0. \]
Ainsi, $f$ admet deux minima locaux, atteints en $x=1$ et $x=-3$, et valant \[ f(1) = 5 \qeq f(-3) = -3-6\ln(3). \]
}

%%% Fin du cours %%%
