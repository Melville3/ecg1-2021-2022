\chapter{Sommes et produits de réels}
%!TeX root=../../encours.nouveau.tex

%%%%%%%%%%%%%%%%%%%
%%%% Objectif  %%%%
%%%%%%%%%%%%%%%%%%%
\objectifintro{Le but de ce chapitre est d'introduire les symboles $\Sigma$ et $\Pi$ que l'on utilisera régulièrement au cours de l'année. Nous en profitons pour définir rigoureusement factorielle et coefficients binomiaux.}


%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Confucius (-551 -- -479)}
Le tout est plus grand que la somme des parties.
\end{extrait}

%%%%%%%%%%%%%%%%%%%
%%%% Objectif  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{objectifs}
\begin{numerote}
  \item connaître les manipulations sur les sommes et produits :
  	\begin{itemize}
  		\item \lienobj{1}{connaître la définition de la somme sur une partie finie}
  		\item \lienobj{2}{savoir passer d'une notation en extension à une notation avec le symbole $\sum$}
  		\item \lienobj{3}{connaître la définition de produit}
  		\item \lienobj{4}{connaître les sommes usuelles}
  		\item \lienobj{5}{savoir utiliser la linéarité, la relation de Chasles, la sommation par paquets}
      \item \lienobj{6}{connaître les différentes propriétés des sommes et produits}
  	\end{itemize}
  \item savoir utiliser les méthodes de calcul de sommes et produits :
  \begin{itemize}
    \item \lienobj{7}{le changement de variable}
    \item \lienobj{8}{les sommes et produits télescopiques}
  \end{itemize}
  \item concernant les notions factorielles et coefficients binomiaux :
  \begin{itemize}
    \item \lienobj{9}{connaître définitions et propriétés de la factorielle}
    \item \lienobj{10}{connaître définitions et propriétés des coefficients binomiaux}
    \item \lienobj{11}{savoir utiliser la formule du binôme de Newton}
  \end{itemize}
  \item concernant les sommes doubles :
  \begin{itemize}
    \item \lienobj{12}{connaître la définition d'une somme double}
    \item \lienobj{13}{savoir utiliser le théorème de Fubini suivant les cas}
  \end{itemize}
\end{numerote}
\end{objectifs}

%%%%%%%%%%%%%%%%%%%
%%%% Programme %%%%
%%%%%%%%%%%%%%%%%%%

\ifversion
\begin{programme}{}
L'objectif est d'acquérir le vocabulaire élémentaire des raisonnements mathématiques, mais tout exposé théorique est exclu. Les notions de ce paragraphe pourront être présentées en contexte au cours du semestre, évitant ainsi une présentation trop formelle.
\begin{center}
\hspace*{-1cm}\begin{tabular}{|p{7cm}|p{10cm}|}\hline
Connecteurs : et, ou, non, implication, réciproque, contraposée. \newline Quantificateurs : $\forall$, $\exists$.& On présentera des exemples de phrases mathématiques utilisant les connecteurs et les quantificateurs, et on expliquera comment écrire leurs négations. \\
Emploi du raisonnement par récurrence. & Tout exposé théorique sur le raisonnement par récurrence est exclu.\\
\hline
\end{tabular}
\end{center}

\end{programme}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Début du chapitre %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Définitions et propriétés}

    \subsection{Familles finies d'entiers}

\begin{definition}
Soient $n$ et $p$ deux entiers tels que $p<n$. On note $\interent{p n}  = \{p;p+1;\cdots;n\}$.
\end{definition}

\begin{exemple}
Par exemple, $\interent{2 5} = \accol{2,3,4,5}$.
\end{exemple}


\begin{remarque}
Il y a $n$ entiers dans $\interent{1 n}$, et $n+1$ entiers dans $\interent{0 n}$. Il y a $n-p+1$ entiers dans $\interent{p n}$.
\end{remarque}


\begin{exemple}
Il y a $7$ entiers dans $\interent{2 8}$.
\end{exemple}

Plus généralement, on peut définir des familles d'entiers indexées par des parties finies de $\N$ :

\begin{definition}[Familles finies de réels]
Soit $I$ une partie \textbf{finie} de $\N$. On appelle \textbf{famille de nombres réels indexée par $I$} la donnée, pour chaque entier $i$ de $I$, d'un unique réel $x_i$. On la note $(x_i)_{i\in I}$.

L'ensemble	$I$ est appelé \textbf{ensemble des indices} de la famille.
\end{definition}

\begin{exemple}
	La famille $(i^2)_{i\in \interent{1 6}}$ est la famille constituée des réels $1, 4, 9, 16, 25, 36$.

	Si $I=\{1,3,5\}$, la famille $(2i)_{i\in I}$ est la famille constituée des réels $2, 6, 10$.
\end{exemple}

\begin{remarque}
	\begin{itemize}
 	 \item Si $I=\interent{p n}$, avec $p\leq n$, la famille $(x_i)_{i\in \interent{p n}}$ est également notée $(x_i)_{p\leq i\leq n}$.
	 \item Si $A=\left \{x_i,\,i\in I \right \}$ où $I$ est finie non vide, $A$ admet un maximum et un minimum, et on les notera $\ds{\max_{i\in I} x_i}$ et $\ds{\min_{i\in I} x_i}$ plutôt que $\max~A$ et $\min~A$. Si $I=\interent{p n}$, on les notera également $\ds{\max_{p\leq i\leq n} x_i}$ et $\ds{\min_{p\leq i\leq n} x_i}$.
	\end{itemize}
\end{remarque}
	\subsection{Somme et produit sur des familles finies}

\begin{definition}
\begin{itemize}
    \item Soient $a_0,a_1,\cdots, a_n$ des réels. On note \[\sum_{k=0}^n a_k=a_0+a_1+\cdots a_n\]
et on lit ``somme de $k=0$ à $n$ des $a_k$''. $0$ et $n$ sont appelées les \textbf{bornes} de la somme.
    \item Soient $a_p, \cdots, a_n$ des réels ($p\leq n$). On note
    \[\sum_{k=p}^n a_k = a_p+\cdots +a_n\]
et on lit ``somme de $k=p$ à $n$ des $a_k$''. $p$ et $n$ sont appelées les \textbf{bornes} de la somme.
	\item Plus généralement, on noter $\ds{\sum_{i\in I} x_i}$ la somme de tous les nombres de la famille $(x_i)_{i\in I}$ indexée par une partie finie $I$ de $\N$.
\end{itemize}
\end{definition}

\begin{remarque}
	Par convention, si la somme est vide, elle vaut $0$. Si elle n'est composée que d'un terme, elle vaut ce terme :
	\[ \sum_{k=n}^n x_k = x_n \]
\end{remarque}

\begin{exemple}
Par exemple, $\displaystyle{\ln(2)+\ln(3)+\cdots + \ln(n) = \sum_{k=2}^n \ln(k)}$.
\end{exemple}

\begin{exo}
Ecrire la notation en extension de $\ds{\sum_{k=2}^{30} k}$ et $\ds{\sum_{n=1}^p \sqrt{n}}$.\\Ecrire à l'aide du symbole $\sum$ l'expression $\ds{1+\frac{1}{2}+\cdots +\frac{1}{n}}$   et $\ds{2+4+6+\cdots + 18}$.
\end{exo}

\solution[4]{On a, rapidement :
\begin{itemize}
	\item $\ds{\sum_{k=2}^{30} k = 2 + 3 + \cdots  + 30}$,
	\item $\ds{\sum_{n=1}^p \sqrt{n} = 1 + \sqrt{2}+\cdots + \sqrt{p}}$,
	\item $\ds{1+\frac{1}{2}+\cdots +\frac{1}{n} = \sum_{k=1}^n \frac{1}{k}}$,
	\item $\ds{2+4+\cdots + 18 = \sum_{k=1}^9 2k}$.
\end{itemize}
}

\begin{remarque}
L'ordre de la sommation n'a pas d'importance. Ainsi
$\displaystyle{\sum_{k=1}^n k^2}$ représente la même somme que $\displaystyle{\sum_{k=n}^1 k^2}$. En effet, la même partie finie de $\N$ est parcourue : il s'agit de $\interent{1 n}$.
\end{remarque}

\begin{definition}
On définit de la même manière $\displaystyle{\prod_{k=p}^n a_k} = a_p\times a_{p+1}\times \cdots \times a_n$, et plus généralement, $\ds{\prod_{i\in I} x_i}$ représente le produit de tous les termes de la famille $(x_i)_{i\in I}$, où $I$ est une partie finie de $\N$.
\end{definition}

\begin{remarque}
	Par convention, si le produit est vide, celui-ci vaut $1$.
\end{remarque}

\begin{attention}
	Lorsqu'on somme ou qu'on fait un produit sur une famille $(x_i)_{i\in I}$, il faut \textbf{absolument} que $I$ soit fini. Sinon, la somme et le produit ne sont pas définis dans le cas général. Nous verrons, au second semestre, une définition lorsque $I=\N$, qui nécessitera des conditions supplémentaires sur les $(x_i)_{i\in I}$.
\end{attention}

\begin{remarque}[Variable muette]
Lorsqu'on écrit $\displaystyle{\sum_{i \in I} x_i}$, la variable $i$ est appelée \textbf{variable muette} : on peut la remplacer par n'importe quelle autre lettre non utilisée :
\[\sum_{i\in I} x_i = \sum_{k\in I} x_k = \sum_{z\in I} x_z\]
\end{remarque}

\subsection{Sommes usuelles}

\begin{proposition}\logoparcoeur
On dispose des résultats suivants :
\[ \sum_{k=0}^n k = \frac{n(n+1)}{2},  \hspace*{1cm}
 \sum_{k=0}^n k^2 = \frac{n(n+1)(2n+1)}{6}, \hspace*{1cm}
 \sum_{k=0}^n k^3 = \left(\frac{n(n+1)}{2}\right)^2
\]
\end{proposition}


\preuve[15]{
La première et la dernière ont été vues dans le chapitre $1$. Montrons la deuxième par récurrence. On note $P$ la proposition définie pour tout entier $n$ par $P_n$ : \og{} $\ds{\sum_{k=0}^n k^2 = \frac{n(n+1)(2n+1)}{6}}$ \fg{}.
\begin{itemize}
	\item \textbf{Initialisation} : pour $n=0$, la somme est composée d'un élément : $0$. D'autre part, $\dfrac{0(0+1)(2\times 0+1)}{6} = 0$. Ainsi, $P_0$ est vraie.
	\item \textbf{Hérédité} : supposons la proposition $P_n$ vraie pour un certain entier $n$. On a alors, par hypothèse de récurrence :
	\begin{align*}
		\sum_{k=0}^{n} &= \frac{n(n+1)(2n+1)}{6}
	\end{align*}
	Mais alors
	\begin{align*}
		\sum_{k=0}^{n+1} k^2 = \sum_{k=0}^n k^2+(n+1)^2 &= \frac{n(n+1)(2n+1)}{6} + (n+1)^2 \\
		&= (n+1)\left(\frac{n(2n+1)}{6} + (n+1)\right) \\
		&= (n+1) \frac{2n^2+n+6n+6}{6} \\
		&= (n+1)\frac{2n^2+7n+6}{6} \\ &=(n+1)\frac{(2n+3)(n+2)}{6}=\frac{(n+1)((n+1)+1)(2(n+1)+1)}{6}
	\end{align*}
\end{itemize}
On a ainsi montré par récurrence le résultat.
}

\begin{proposition}[Somme géométrique]
Pour tout entier naturel $n$ et tout réel $q$, on a :
\[	 \sum_{k=0}^n q^k =
	\left \{ \begin{array}{rcl} \dfrac{1-q^{n+1}}{1-q} &\textrm{si }& q\neq 1\\
	n+1 & \textrm{si } & q=1 \end{array}\right.
\]
\end{proposition}

\preuve[5]{Le cas général a été vu dans le chapitre $1$. Pour le cas $q=1$, on a rapidement que \[ \sum_{k=0}^n q = \sum_{k=0}^n 1 = n+1 \]
}

\subsection{Première propriétés}

Les propositions suivantes se montrent toutes par récurrence sur le nombre d'éléments de l'ensemble des indices de la sommes.

Soient $(x_i)_{i\in I}$ et $(y_i)_{i\in I}$ deux familles finies de réels.

\begin{proposition}[Propriétés de la somme]
	\begin{itemize}
		\item \petittitre{Linéarité} : pour tout $\lambda \in \R$, \[ \sum_{i\in I} (x_i+y_i) = \sum_{i\in I} x_i + \sum_{i\in I} y_i \qeq \sum_{i\in I} (\lambda x_i) = \lambda \sum_{i\in I} x_i \]
		\item \petittitre{Relation de Chasles} : pour tous entiers $p, m$ et $n$, tels que $p\leq m \leq n$, on a \[ \sum_{i=p}^n x_i = \sum_{i=p}^m x_i + \sum_{i=m+1}^n x_i \]
		\item \petittitre{Sommation par paquets} : soient deux parties finies $J$ et $K$ de $\N$, tels que $J\cap K = \vide$, et $I=J\cup K$. Alors
		\[ \sum_{i\in I} x_i = \sum_{i\in J} x_i + \sum_{i\in K} x_i \]
	\end{itemize}
\end{proposition}

\begin{remarque}
	On verifiera, lors de l'utilisation de la linéarité, que les bornes des deux sommes sont les mêmes. Si ce n'est pas le cas, on commencera par se ramener aux mêmes bornes, quitte à sortir de la somme quelques termes.

	Par exemple :
	\begin{align*}
		\sum_{i=0}^{n-1} x_i + \sum_{i=1}^n y_i &= x_0 + \sum_{i=1}^{n-1} x_i + \sum_{i=1}^{n-1} y_i + y_n \\
		&= x_0 + y_n + \sum_{i=1}^{n-1} (x_i+y_i)
	\end{align*}
\end{remarque}

\begin{remarque}
	L'exemple classique d'utilisation de la sommation par parquet est de séparer les termes pairs et les termes impairs d'une somme.
\end{remarque}

\begin{propriete}
Si pour tout $i\in I$, $x_i\leq y_i$, alors \[ \sum_{i\in I} x_i \leq \sum_{i\in I} y_i \]
 Si de plus il existe $i_0\in I$ tel que $x_{i_0}< y_{i_0}$, alors \[ \sum_{i\in I} x_i < \sum_{i\in I} y_i \]
\end{propriete}

\begin{propriete}[Inégalité triangulaire]
 On a \[ \left | \sum_{i\in I} x_i \right| \leq \sum_{i\in I} |x_i| \]
\end{propriete}

\begin{proposition}[Propriétés du produit]
	\begin{itemize}
		\item Si $n\in \N$, alors \[ \prod_{i\in I} (x_i)^n = \left(\prod_{i\in I} x_i\right)^n \]
		Ce résultat est valable si $n\in \Z\setminus \N$ et si les $x_i$ sont tous non nuls.
		\item Pour tout réel $\lambda$, on a \[ \prod_{i\in I} \left(\lambda x_i\right) = \lambda^q \prod_{i\in I} x_i \] où $q$ désigne le nombre d'éléments de $I$.
		\item \petittitre{Séparation} : on a \[ \prod_{i\in I} \left(x_iy_i\right) = \left( \prod_{i\in I} x_i\right) \left(\prod_{i\in I} y_i\right). \]
		\item $\ds{\left| \prod_{i\in I} x_i\right| = \prod_{i\in I} |x_i|}$.
	\end{itemize}
\end{proposition}

\begin{propriete}
	Si, pour tout $i\in I$, on a $0\leq x_i \leq y_i$, alors \[ \prod_{i\in I} x_i \leq \prod_{i\in I} y_i \]
\end{propriete}

Enfin, nous avons le résultat suivant, sur lequel nous reviendrons plus tard :

\begin{propriete}[Exponentielle et logarithme]
    \begin{itemize}
        \item On a
        \[\exp\left( \sum_{i\in I} x_i \right) = \prod_{i\in I} \eu{x_i}\]
        \item Si les $(x_i)_{i\in I}$ sont tous strictement positifs :
        \[\ln \left( \prod_{i\in I} x_i \right) = \sum_{i\in I} \ln(x_i)\]
    \end{itemize}
\end{propriete}

\section{Calculs de sommes et produits}

    \subsection{Changement de variable}


Puisque la variable d'une somme est muette, on peut faire un changement de variable, qui consiste à ré-écrire la somme différemment.

\begin{proposition}
  Soient $p\leq n$ deux entiers, $l$ un entier, et $a_{p+l},\cdots a_{n+l}$ des réels. Alors
$$\sum_{k=p+l}^{n+l} a_k = \sum_{j=p}^n a_{j+l}$$
On a effectué le changement de variable $j=k-l$ : ainsi, si $k=p+l$, alors $j=p$. De même, $k=n+l$ amène $j=n$.
\end{proposition}

\begin{methode}
  Pour faire un changement de variable $j=f(k)$, on procède en remplaçant toutes les occurrences de $k$ par son expression en fonction de $j$, mais on n'oublie pas de changer les bornes en conséquence !
\end{methode}

\begin{exemple}
Calculer $\displaystyle{S=\sum_{k=0}^n (n-k)}$ en posant $j=n-k$.
\end{exemple}


\solution[4]{Posons $j=n-k$. Alors
\[S=\sum_{j=n}^0 j = \sum_{j=0}^n j\]
car l'ordre de la somme des termes n'importe pas. On a donc
\[S=\frac{n(n+1)}{2}\]}

\begin{remarque}
	On pourrait vouloir faire d'autres changements de variables, mais tous ne sont pas autorisés. Par exemple, poser $k=2i$ est interdit, car $2i$ ne parcourt que les nombres pairs, alors que $k$ parcourt des valeurs y compris impaires.
\end{remarque}

	\subsection{Sommes et produits télescopiques}

	\subsubsection{Définition}

\begin{definition} Soit $p\leq n$. Soient $a_p,\cdots, a_{n+1}$ des réels. On appelle \textbf{somme télescopique} une somme de la forme
\[\sum_{k=p}^n a_{k+1}-a_k\]
\end{definition}

\begin{exemple}
La somme $\displaystyle{\sum_{k=1}^n \frac{1}{k+1}-\frac{1}{k}}$ est une somme télescopique.
\end{exemple}

	\subsubsection{Simplification}

\begin{proposition}
Soit $\displaystyle{S_n=\sum_{k=p}^n a_{k+1}-a_k}$. Alors
\[S_n=a_{n+1}-a_p\]
\end{proposition}

\begin{demonstration}
En effet,
\[S_n=(a_{p+1}-a_p)+(a_{p+2}-a_{p+1})+(a_{p+3}-a_{p+2})+\cdots + (a_n-a_{n-1})+(a_{n+1}-a_n) = -a_p+a_{n+1}\]
\end{demonstration}

\begin{exemple}
La somme $S_n=\displaystyle{\sum_{k=1}^n \frac{1}{k+1}-\frac{1}{k}}$ se simplifie en
\[S_n=\frac{1}{n+1}-1\]
\end{exemple}

\exercice{Soit $S_n=\displaystyle{\sum_{k=1}^n \ln\left( \frac{k+1}{k} \right)}$. Simplifier $S_n$.}

\solution[4]{On constate en effet, en utilisant les propriétés du logarithme, que
\[S_n=\sum_{k=1}^n \ln(k+1)-\ln(k)\]
La somme $S_n$ est donc télescopique. On a donc
\[S_n=\ln(n+1)-\ln(1)=\ln(n+1)\]}

	\subsubsection{Produits télescopiques}

On peut définir également les produits télescopiques, avec un résultat assez similaire à celui des sommes télescopiques.

\begin{definition}
	Soit $p\leq n$. Soient $a_p,\cdots a_{n+1}$ des réels tous non nuls. On appelle \textbf{produit télescopique} un produit de la forme
\[\prod_{k=p}^n \frac{a_{k+1}}{a_k}\]
\end{definition}

\begin{exemple}
Le produit
\[\frac{2}{1}\times\frac{3}{2}\cdots \frac{n+1}{n} = \prod_{k=1}^{n} \frac{k+1}{k}\] est un produit télescopique.
\end{exemple}

\begin{proposition}
Soit $\displaystyle{P_n=\prod_{k=p}^n \frac{a_{k+1}}{a_k}}$ avec $a_p,\cdots a_{n+1}$ tous non nuls. Alors
$\displaystyle{P_n=\frac{a_{n+1}}{a_p}}$
\end{proposition}

\section{Factorielles et coefficients binomiaux}

	\subsection{Factorielle}

\begin{definition}[Factorielle]
Soit $n$ un entier non nul. On appelle \textbf{factorielle} de $n$, et on note $n!$, le nombre $\displaystyle{n! = \prod_{k=1}^n k}$. \\Par convention, $0!=1$.
\end{definition}

\begin{remarque}
On a ainsi $1!=1$, $2!=1\times 2 = 2$ et $3!=1\times 2 \times 3 = 6$.
\end{remarque}

\begin{proposition}
\label{objectif-02-4}
  Pour tout entier $n\geq 1$, on a \[ (n+1)! = (n+1) \times n! \]
\end{proposition}

\ifprof
\begin{demonstration}
En effet, par définition, \[ (n+1)! = \underbrace{1 \times 2 \times \cdots \times n}_{=n!} \times (n+1) = n! \times (n+1) \]
\end{demonstration}
\else
\lignes{2}
\fi

\begin{exo}
Pour $n\pgq 1$, simplifier $\ds{\frac{(n+2)!}{n!}}$.
\end{exo}

\solution[3]{On a
\[ \frac{(n+2)!}{n!} = \frac{(n+2)(n+1)n!}{n!}=(n+2)(n+1) \]}

\begin{exo}[Produit des nombres pairs et nombres impairs]
	\'Ecrire, à l'aide des factorielles, les produits \[ \prod_{k=1}^n (2k) \qeq \prod_{k=1}^n (2k+1).\]
\end{exo}

\solution[10]{L'astuce est de factoriser chaque terme par $2$ pour la première :
\begin{align*}
	2\times 4\times \hdots \times (2n) = \prod_{k=1}^n (2k) &= 2^n \prod_{k=1}^n k = 2^n n!
\end{align*}
Pour la deuxième, nous allons ré-écrire le produit en ajoutant les termes manquants :
\begin{align*}
	1\times 3 \times \hdots \times (2n+1) = \prod_{k=1}^n (2k+1) &= \frac{1\times 2 \times 3 \times 4 \times 5 \times \hdots \times (2n) \times (2n+1)}{2\times 4 \times 6\times \hdots \times (2n)} \\
	&= \frac{(2n+1)!}{2^n n!}
\end{align*}
en utilisant le résultat précédent.
}

	\subsection{Coefficients binomiaux}

Nous reviendrons sur une autre définition des coefficients binomiaux plus tard. Nous allons les définir de manière analytique :

\begin{definition}[Coefficients binomiaux]
	Soient $n\in \N$, et $p\in \interent{0 n}$. On note \[ \binom{n}{p}=\frac{n!}{p! (n-p)!} \]
et on lit \og $p$ parmi $n$\fg{}.
\end{definition}

\begin{remarque}
	Si $n$ et $p$ sont deux entiers tels que $2\leq p\leq n$, on a
	\begin{align*}
		\binom{n}{p} &= \frac{n\times (n-1)\times \hdots \times (n-p+1)\times (n-p)\times (n-p-1)\times \hdots\times 1}{p! \times  (n-p)\times (n-p-1)\times \hdots \times 1}\\
		&= \frac{n(n-1)(n-2)\times (n-p+1)}{p!}
	\end{align*}
	C'est cette formule que nous utiliserons, en pratique, pour déterminer $\ds{\binom{n}{p}}$.
\end{remarque}

\begin{exemple}
	Par exemple, \[ \binom{11}{4} = \frac{11\times 10\times 9\times 8}{4\times 3 \times 2 \times 1} = 11\times 10 \times 3 = 330 \]
\end{exemple}

\begin{proposition}[Propriétés des coefficients binomiaux]
 Soient $n$ et $p$ deux entiers naturels tels que $p\leq n$.
 \begin{itemize}
	 \item $\ds{\binom{n}{0}=\binom{n}{n}=1}$.
	 \item Si $n\geq 1$, alors $\ds{\binom{n}{1} = \binom{n}{n-1} = n}$.
	 \item Si $n\geq 2$, alors $\ds{\binom{n}{2}=\frac{n(n-1)}{2}}$.
	 \item \petittitre{Symétrie} : $\ds{\binom{n}{p}=\binom{n}{n-p}}$.
	 \item Si $n\geq 1$ et $p\geq 1$, alors \[ p\binom{n}{p}=n\binom{n-1}{p-1}.\]
	 \item \petittitre{Formule de Pascal} : si $1<p<n$, alors \[ \binom{n}{p} = \binom{n-1}{p-1}+\binom{n-1}{p}. \]
 \end{itemize}
\end{proposition}

\preuve[15]{
A faire.
}

\begin{remarque}
	Par convention, on étend la définition en posant $\ds{\binom{n}{p}=0}$ pour tous $n\in \N$ et $p\not \in \interent{0 n}$. Dans ce cas, la formule de Pascal est valable pour tous $n$ et $p$.
\end{remarque}

La formule de Pascal permet deux choses : d'une part, calculer algorithmiquement les coefficients binomiaux; d'autre part, montrer le résultat (qui n'est pas évident a priori) :

\begin{consequence}
	Pour tous entier $n\in \N$ et $0\leq p \leq n$, alors $\ds{\binom{n}{p}\in \N}$.
\end{consequence}

\begin{demonstration}
	Elle se fait par récurrence sur $n$, en utilisant la formule de Pascal.
\end{demonstration}

	\subsection{Formule du binôme de Newton}

Une application importante des coefficients binomiaux est la formule du binôme de Newton :

\begin{theoreme}[Formule du binôme de Newton]
	Soient $a$ et $b$ deux nombres réels, et $n$ un entier naturel. Alors
	\[ (a+b)^n = \sum_{k=0}^n \binom{n}{k}a^kb^{n-k}=\sum_{k=0}^n \binom{n}{k} a^{n-k}b^k \]
\end{theoreme}

\begin{demonstration}
	Même si on dispose de tous les outils pour la démontrer, on repousse cette preuve à un prochain chapitre.
\end{demonstration}

Pour $n=2$, on retrouve les identités remarquables classiques \[ (a+b)^2 = a^2+2ab+b^2 \qeq (a-b)^2 = a^2-2ab+b^2 \]

Pour $n=3$ et $n=4$, on obtient ces identités qu'il peut être judicieux de retenir :

\begin{align*}
	(a+b)^3 &= a^3+3a^2b+3ab^2+b^3&&& (a-b)^3&= a^3-3a^2b+3ab^2-b^3\\
	(a+b)^4 &= a^4+4a^3b+6a^2b^2+4ab^3+b^4 &&& (a-b)^4 &= a^4-4a^3b+6a^2b^2-4ab^3+b^4
\end{align*}

\begin{exo}
	Déterminer, pour tout $n\in \N$, \[ \sum_{k=0}^n \binom{n}{k} \qeq \sum_{k=0}^n \binom{n}{k}(-1)^k. \]
\end{exo}

\preuve[5]{Il suffit d'appliquer la formule du binôme de Newton, en prenant $a=b=1$ pour la première, et $a=1$ et $b=-1$ pour la seconde. On obtient \[
\sum_{k=0}^n \binom{n}{k} = 2^n \qeq \sum_{k=0}^n \binom{n}{k}(-1)^k=0 \text{ si } n\geq 1,\quad 1 \text{ sinon}. \]
}

\section{Sommes doubles}

La somme $\ds{\sum_{i\in I} x_i}$, où $(x_i)_{i\in I}$ est une famille de réels indexée par une partie finie $I$, est appelée \textbf{somme simple}.

  \subsection{Notion de somme double}

\begin{definition}[Couple d'entiers]
  On appelle \textbf{couple} d'entiers naturels la donnée de deux entiers naturels $x$ et $y$, dans cet ordre, noté $(x,y)$.

  L'ensemble des couples d'entiers naturels est noté $\N^2$.
\end{definition}

\begin{remarque}
  Graphiquement, l'ensemble $\N^2$ est représenté par un quadrillage infini, où un couple $(a,b)$ est représenté par un point à l'intersection de la droite d'équation $x=a$ et $y=b$.
\end{remarque}

\begin{definition}[Somme double]
  Soit $A$ une partie finie de $\N^2$. On appelle \textbf{famille de nombres réels} indexée par $A$ la donnée, pour chaque couple d'entiers naturels $(i,j)$ de $A$, d'un unique nombre réel $x_{i,j}$. On la note $\ds{(x_{i,j})_{(i,j)\in A}}$.

  On note $\ds{\sum_{(i,j)\in A} x_{i,j}}$ la somme des éléments de la famille. On dit qu'il s'agit d'une \textbf{somme double}.
\end{definition}

\begin{exemple}
  Par exemple, si $n\in \N*$ et $A=\left \{ (i,j),\, i\in \ll 1,n \rr, j\in \ll 1,n \rr \right \}$, la famille $\left(3ij^2\right)_{(i,j)\in A}$ est une famille de nombres réels indexée par $A$.
\end{exemple}

En général, le calcul d'une somme double consiste en le calcul successif de sommes simples.

  \subsection{Le cas d'un domaine rectangulaire}

Un domaine de $\N^2$ est rectangulaire s'il peut s'écrire \[ A=\left \{ (i,j)\in \N^2,\, m\leq i\leq n,\, p\leq j \leq q \right \} \]
avec $m,n,p,q$ des entiers naturels tels que $m\leq n$ et $p\leq q$.

On note alors, en général, $\ds{(x_{i,j})_{\substack{m\leq i\leq n\\p\leq j\leq q}}}$ au lieu de $(x_{i,j})_{(i,j)\in A}$, et la somme est notée
\[ \sum_{\substack{m\leq i\leq n\\p\leq j\leq q}} x_{i,j} \quad \text{au lieu de}\quad \sum_{(i,j)\in A} x_{i,j}. \]

Si $m=p$ et $n=q$, on note encore plus simplement $\ds{(x_{i,j})_{p\leq i,j\leq q}}$ la famille et $\ds{\sum_{p\leq i,j \leq q} x_{i,j}}$.

Le domaine est dit rectangulaire car les éléments de la famille peuvent être rangés dans un tableau :

\[ \renewcommand{\arraystretch}{1.8}{\begin{array}{|c||c|c|c|c|c|}\hline
\hbox{\diagbox{$i$}{$j$}} & p & p+1 & \hdots & q-1 & q \\\hline\hline
m & x_{m,p} & x_{m,p+1} & \hdots & x_{m,q-1} & x_{m,q} \\\hline
m+1 & x_{m+1,p} & x_{m+1,p+1} & \hdots & x_{m+1,q-1} & x_{m+1,q} \\\hline
\vdots & \vdots& \vdots & \ddots & \vdots & \vdots \\\hline
n-1 & x_{n-1,p} & x_{n-1,p+1} & \hdots & x_{n-1,q-1} & x_{n-1,q} \\\hline
n & x_{n,p} & x_{n,p+1} & \hdots & x_{n,q-1} & x_{n,q}\\\hline
\end{array}}\]

Pour sommer les éléments de la famille, on peut le faire de deux manières différentes :
\begin{itemize}
  \item ajouter d'abord chaque élément d'une ligne, c'est-à-dire calculer $\ds{\sum_{j=p}^q x_{i,j}}$ pour tout entier $i\in \interent{m n}$, puis de sommer tous les résultats, pour finalement calculer \[ \sum_{i=m}^n \left (\sum_{j=p}^q x_{i,j}\right) \]
  \item ajouter d'abord chaque élément d'une colonne, c'est-à-dire calculer $\ds{\sum_{i=m}^n x_{i,j}}$ pour tout entier $j\in \interent{p q}$, puis de sommer tous les résultats, pour finalement calculer \[ \sum_{j=p}^q \left (\sum_{i=m}^n x_{i,j}\right) \]
\end{itemize}

Ces deux calculs sont valides, et on obtient le théorème suivant :

\begin{theoreme}[Théorème de Fubini]
  Soit $\ds{A=\left \{ (i,j)\in \N^2,\, m\leq i\leq n,\, p\leq j \leq q \right \} }$, et $(x_{i,j})_{(i,j)\in A}$ une famille indexée par $A$. Alors
  \[ \sum_{\substack{m\leq i\leq n\\p\leq j\leq q}} x_{i,j} = \sum_{i=m}^n \left( \sum_{j=p}^q x_{i,j}\right)=\sum_{j=p}^q \left(\sum_{i=m}^n x_{i,j}\right) \]
\end{theoreme}

\begin{remarque}
  Dans cette somme, il y a autant de termes qu'il y a de termes dans le tableau, c'est-à-dire $(n-m+1)(q-p+1)$.
\end{remarque}

\begin{exemple}
  Soit $n\in \N*$, et pour tous $i$ et $j$ dans $\interent{1 n}$, on pose $x_{i,j}=ij^2$. On souhaite calculer $\ds{\sum_{1\leq i,j\leq n} x_{i,j}}$.
\ifprof
\begin{itemize}
  \item Première méthode. On fixe $i\in \interent{1 n}$. On calcule alors
  \begin{align*}
    \sum_{j=1}^n x_{i,j} &= \sum_{j=1}^n ij^2 \\ &= i\sum_{j=1}^n j^2 = i\frac{n(n+1)(2n+1)}{6}
  \end{align*}
   On en déduit alors que :
   \begin{align*}
    \sum_{1\leq i,j\leq n} x_{i,j} & = \sum_{i=1}^n \left( \sum_{j=1}^n x_{i,j}\right) \\
    &= \sum_{i=1}^n \left( i\frac{n(n+1)(2n+1)}{6}\right) \\
    &= \frac{n(n+1)(2n+1)}{6}\sum_{i=1}^n i = \frac{n^2(n+1)^2(2n+1)}{12}
   \end{align*}
   \item Deuxième méthode. On fixe $j\in \interent{1 n}$. On calcule alors
   \begin{align*}
     \sum_{i=1}^n x_{i,j} &= \sum_{i=1}^n ij^2 \\ &= j^2\sum_{i=1}^n i = j^2\frac{n(n+1)}{2}
   \end{align*}
    On en déduit alors que :
    \begin{align*}
     \sum_{1\leq i,j\leq n} x_{i,j} & = \sum_{j=1}^n \left( \sum_{i=1}^n x_{i,j}\right) \\
     &= \sum_{j=1}^n \left( j^2\frac{n(n+1)}{2}\right) \\
     &= \frac{n(n+1)}{2}\sum_{j=1}^n j^2 = \frac{n^2(n+1)^2(2n+1)}{12}
    \end{align*}
\end{itemize}
\else
\lignes{15}
\fi
\end{exemple}

\begin{remarque}
  L'exemple précédent est un cas particulier où on peut factoriser les termes. De manière plus générale :
  \[ \sum_{\substack{m\leq i\leq n\\p\leq j\leq q}} x_{i}y_{j} = \left(\sum_{i=m}^n x_i\right) \left( \sum_{j=p}^q y_j\right) \]
\end{remarque}

\preuve[4]{
En effet, en utilisant le théorème de Fubini :
\begin{align*}
 \sum_{\substack{m\leq i\leq n\\p\leq j\leq q}} x_{i}y_{j} &= \sum_{i=m}^n \left( \sum_{j=p}^q x_i y_j\right)\\
 &= \sum_{i=m}^n \left(x_i \left (\sum_{j=p}^q y_j \right)\right)\\
 &= \left(\sum_{j=p}^q y_j\right) \left( \sum_{i=m}^n x_i)\right)
\end{align*}
}

  \subsection{Le cas d'un domaine triangulaire}

  Un domaine de $\N^2$ est triangulaire s'il peut s'écrire \[ A=\left \{ (i,j)\in \N^2,\, p\leq i\leq j\leq n  \right \} \]
  avec $n$ et $p$ des entiers naturels tels que $p\leq n$.

  On note alors, en général, $\ds{(x_{i,j})_{\substack{p\leq i\leq j \leq n}}}$ au lieu de $(x_{i,j})_{(i,j)\in A}$, et la somme est notée
  \[ \sum_{p\leq i\leq j\leq n} x_{i,j} \quad \text{au lieu de}\quad \sum_{(i,j)\in A} x_{i,j}. \]

  Le domaine est dit triangulaire car les éléments de la famille peuvent être rangés dans un tableau de cette manière  :


  \[ {\renewcommand{\arraystretch}{1.4}\begin{array}{|c||c|c|c|c|c|}\hline
  \hbox{\diagbox{$i$}{$j$}} & p & p+1 & \hdots & n-1 & n \\\hline\hline
  p & x_{p,p} & x_{p,p+1} & \hdots & x_{p,n-1} & x_{p,n} \\\hline
  p+1 &   & x_{p+1,p+1} & \hdots & x_{p+1,n-1} & x_{p+1,n} \\\hline
  \vdots & &  & \ddots & \vdots & \vdots \\\hline
  n-1 &   &  &  & x_{n-1,n-1} & x_{n-1,n} \\\hline
  n &  &  &  & & x_{n,n}\\\hline
  \end{array}}\]

  Pour sommer les éléments de la famille, on peut le faire de deux manières différentes :
  \begin{itemize}
    \item ajouter d'abord chaque élément d'une ligne, c'est-à-dire calculer $\ds{\sum_{j=i}^n x_{i,j}}$ pour tout entier $i\in \interent{p n}$, puis de sommer tous les résultats, pour finalement calculer \[ \sum_{i=p}^n \left (\sum_{j=i}^n x_{i,j}\right) \]
    \item ajouter d'abord chaque élément d'une colonne, c'est-à-dire calculer $\ds{\sum_{i=p}^j x_{i,j}}$ pour tout entier $j\in \interent{p n}$, puis de sommer tous les résultats, pour finalement calculer \[ \sum_{j=p}^n \left (\sum_{i=p}^j x_{i,j}\right) \]
  \end{itemize}

  Ces deux calculs sont valides, et on obtient le théorème suivant :

  \begin{theoreme}[Théorème de Fubini]
    Soit $\ds{A=\left \{ (i,j)\in \N^2,\, p\leq i\leq j\leq n \right \} }$, et $(x_{i,j})_{(i,j)\in A}$ une famille indexée par $A$. Alors
    \[ \sum_{p\leq i\leq j\leq n} x_{i,j} = \sum_{i=p}^n \left( \sum_{j=i}^n x_{i,j}\right)=\sum_{j=p}^n \left(\sum_{i=p}^j x_{i,j}\right) \]
  \end{theoreme}

  \begin{remarque}
    Dans cette somme, il y a autant de termes qu'il y a de termes dans le tableau, c'est-à-dire $\dfrac{(n-p+1)(n-p+2)}{2}$.
  \end{remarque}

On dipose d'un autre cas particulier : le cas de la somme des termes sur-diagonaux stricts. Dans ce cas :
 \[ A=\left \{ (i,j)\in \N^2,\, p\leq i < j\leq n  \right \} \]
avec $n$ et $p$ des entiers naturels tels que $p\leq n$.

On note alors, en général, $\ds{(x_{i,j})_{\substack{p\leq i< j \leq n}}}$ au lieu de $(x_{i,j})_{(i,j)\in A}$, et la somme est notée
\[ \sum_{p\leq i <  j\leq n} x_{i,j} \quad \text{au lieu de}\quad \sum_{(i,j)\in A} x_{i,j}. \]

Cela donne le tableau suivant :
\[ {\renewcommand{\arraystretch}{1.4}\begin{array}{|c||c|c|c|c|c|c|}\hline
\hbox{\diagbox{$i$}{$j$}} & p & p+1 & p+2 & \hdots & n-1 & n \\\hline\hline
p &  & x_{p,p+1} & x_{p,p+2}& \hdots & x_{p,n-1} & x_{p,n} \\\hline
p+1 &   &  & x_{p+1,p+2} &\hdots & x_{p+1,n-1} & x_{p+1,n} \\\hline
\vdots & &  & &  \ddots & \vdots & \vdots \\\hline
n-2 &   &  &  & & x_{n-2,n} & x_{n-2,n} \\\hline
n-1 &   &  &  & & & x_{n-1,n} \\\hline
n &  &  &  & &&  \\\hline
\end{array}}\]

Le théorème de Fubini s'applique à nouveau :

\begin{theoreme}[Théorème de Fubini]
    Soit $\ds{A=\left \{ (i,j)\in \N^2,\, p\leq i < j\leq n \right \} }$, et $(x_{i,j})_{(i,j)\in A}$ une famille indexée par $A$. Alors
    \[ \sum_{p\leq i < j\leq n} x_{i,j} = \sum_{i=p}^{n-1} \left( \sum_{j=i+1}^n x_{i,j}\right)=\sum_{j=p+1}^n \left(\sum_{i=p}^{j-1} x_{i,j}\right) \]
\end{theoreme}

  \begin{remarque}
    Dans cette somme, il y a autant de termes qu'il y a de termes dans le tableau, c'est-à-dire $\dfrac{(n-p+1)(n-p)}{2}$.
  \end{remarque}

\begin{exemple}
  On souhaite calculer $\ds{\sum_{1\leq i<j\leq n} ij}$.
\ifprof
  \begin{itemize}
    \item Première méthode.
    \begin{align*}
      \sum_{1\leq i<j\leq n} ij &=\sum_{i=1}^{n-1} \left( \sum_{j=i+1}^n ij \right)\\
      &= \sum_{i=1}^{n-1} i \left(\sum_{j=i+1}^n j\right)\\
      &= \sum_{i=1}^{n-1} i \left( \sum_{j=0}^n j - \sum_{j=0}^i j\right)\\
      &= \sum_{i=1}^{n-1} i \left( \frac{n(n+1)}{2}-\frac{i(i+1)}{2}\right)\\
      &= \frac{n(n+1)}{2}\sum_{i=1}^{n-1} i - \frac{1}{2}\sum_{i=1}^{n-1} \left(i^3+i^2\right)\\
      &= \frac{n^2(n+1)(n-1)}{4} - \frac{n^2(n-1)^2}{8}- \frac{n(n-1)(2n-1)}{12} = \frac{n(n+1)(n-1)(3n+2)}{24}
    \end{align*}
    \item Deuxième méthode.
    \begin{align*}
      \sum_{1\leq i<j\leq n} ij &=\sum_{j=2}^{n} \left( \sum_{i=1}^{j-1} ij \right)\\
      &= \sum_{j=2}^n j \sum_{i=1}^{j-1} i \\
      &= \sum_{j=2}^n j \frac{(j-1)j}{2} \\
      &= \sum_{j=2}^n \frac{j^2-j}{2} \\
      &= \frac{1}{2}\left( \sum_{j=2}^n j^3 - \sum_{j=2}^n j^2\right) \\
      &= \frac{1}{2}\left( \sum_{j=1}^n j^3 -1 - \left(\sum_{j=1}^n j^2 -1 \right) \right)\\
      &= \frac{1}{2}\left( \frac{n^2(n+1)^2}{4}-\frac{n(n+1)(2n+1)}{6} \right)= \frac{n(n+1)(n-1)(3n+2)}{24}
    \end{align*}
  \end{itemize}
  \else
  \lignes{20}
  \fi
\end{exemple}

\begin{exo}[Carré d'une somme]
  Démontrer que, pour tous réels $x_1, \hdots, x_n$, on a
  \[ \left( \sum_{i=1}^n x_i\right)^2 = \sum_{i=1}^n x_i^2 + 2\sum_{1\leq i < j \leq n} x_ix_j \]
\end{exo}

\solution[6]{On part du théorème de Fubini et on réécrit :
\begin{align*}
  \left(\sum_{i=1}^n x_i\right)^2 &= \left ( \sum_{i=1}^n x_i\right)\left(\sum_{j=1}^n x_j\right) \\
  &= \sum_{1\leq i, j \leq n} x_ix_j \\
  &= \sum_{i=1}^n x_ix_i + \sum_{1\leq i < j \leq n} x_ix_j + \sum_{1\leq j < i \leq n} x_ix_j \\
  &= \sum_{i=1}^n x_i^2 + 2 \sum_{1\leq i < j \leq n} x_ix_j.
\end{align*}
}
