import numpy as np

# Définition de la fonction phi
def phi(x): return 1/np.sqrt(2*np.pi)*np.exp((-x**2)/2)

def Phi(x,eps):
   ''' Fonction Phi prend 2 arguments
        - x, la valeur en laquelle on la calcule
        - eps, la précision demandée
      Elle renvoie une valeur approchée de Phi(x) '''
   inf=1/2 # On commence à 1/2
   n = int( np.floor(x**2/(8*eps))+1) # Pour avoir la bonne précision
   for i in range(n):
      inf = inf+x/n*phi(i*x/n)
   return inf

# Exemple : Phi(1) à une précision de 10^(-4)
print(Phi(1, 10**(-4)))
# Renvoie 0.8414074716156686
