import matplotlib.pyplot as plt # Pour représenter l'histogramme
import pylab                    # Pour améliorer la présentation
import numpy as np              # Pour l'aléatoire

# On simule une loi uniforme avec np.random.binomial
# np.random.binomial(n,p, nb) simule nb tirages suivant la loi binomiale de paramètres n et p.
data=np.random.binomial(10,0.3,10000)
# On compte les apparitions de chaque valeur
counts = np.bincount(data)/10000
# On dresse l'histogramme
plt.bar(range(11), counts, width=0.5, align='center')
# On rend l'histogramme plus propre
plt.xlim(-1,11)
pylab.xticks(range(0,11))
# On enregistre dans un fichier
plt.savefig('loi_binomiale.png')
# On affiche
plt.show()
