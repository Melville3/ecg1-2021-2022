import math

def f(x): return math.sqrt(1+x**2)

def rectangle(a,b,n):
   ''' Fonction rectangle prend 3 arguments 
        - [a b] désigne le segment sur lequel on calcule l'intégrale
        - n représente le nombre de subdivision 
      Elle renvoie une valeur approchée de l'intégrale '''
   inf=0
   for i in range(n):
      inf = inf+(b-a)/n*f(a+i*(b-a)/n)
   return inf

# Exemples pour a=1, b=2 et n=100
print(rectangle(1,2,100))
# Cela  renvoie 1.805984429314782
