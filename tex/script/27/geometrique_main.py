import matplotlib.pyplot as plt # Pour représenter l'histogramme   
import pylab                    # Pour améliorer la présentation
import numpy as np              # Pour l'aléatoire

# Fonction qui simule une expérience de loi géométrique
def geometrique(p):
    n = 0
    while np.random.random()>p: # tant qu'on a un échec
        n = n+1                 # on continue
    return n

# On simule une loi geometrique avec np.random.geometric
# np.random.geometric(p, nb) simule nb tirages suivant la loi géométrique de paramètre p.
data=[geometrique(0.3) for i in range(10000)]
# On compte les apparitions de chaque valeur
counts = np.bincount(data)/10000
# On dresse l'histogramme
plt.bar(range(max(data)+1), counts, width=0.5, align='center')
# On rend l'histogramme plus propre
plt.xlim(-1,max(data))
pylab.xticks(range(0,max(data)-1))
# On enregistre dans un fichier
plt.savefig('loi_geometrique_main.eps') 
# On affiche
plt.show()
