%!PS
%%BoundingBox: -29 -29 71 66 
%%HiResBoundingBox: -28.62991 -28.62991 70.4911 65.6007 
%%Creator: MetaPost 2.01
%%CreationDate: 2021.11.24:1108
%%Pages: 1
%*Font: cmmi10 9.96265 9.96265 12:8000000000010004
%*Font: cmmi9 8.96637 8.96637 42:c
%*Font: cmmi6 5.97758 5.97758 12:8
%*Font: cmsy10 9.96265 9.96265 43:8
%*Font: cmsy9 8.96637 8.96637 44:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0.8 0.8 0.8 setrgbcolor
newpath 0 0 moveto
56.6929 0 lineto
56.6929 13.3288 51.99661 26.23141 43.42886 36.44173 curveto
 closepath fill
 0 0 0 setrgbcolor 0 0.56693 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 49.0959 -28.34645 moveto
54.24672 -19.80142 56.88008 -9.97565 56.6929 0 curveto
56.11002 31.06598 31.06598 56.11 0 56.6929 curveto
-9.97565 56.88008 -19.80142 54.24672 -28.34645 49.0959 curveto stroke
newpath 17.00804 0 moveto
17.00804 3.99867 15.59914 7.8695 13.0288 10.93263 curveto stroke
newpath 16.33511 8.6813 moveto
15.3322 9.55331 14.22385 10.31056 13.0288 10.93263 curveto
13.43393 9.64772 13.68217 8.32855 13.77472 7.00278 curveto
 closepath
gsave fill grestore stroke
newpath -28.34645 0 moveto
56.6929 0 lineto stroke
 0.56693 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -28.34645 moveto
0 56.6929 lineto stroke
 0 1 dtransform truncate idtransform setlinewidth pop
newpath 0 0 moveto
54.99219 0 lineto stroke
newpath 51.29662 -1.53079 moveto
54.99219 0 lineto
51.29662 1.53079 lineto
 closepath
gsave fill grestore stroke
 1 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 0 moveto
0 54.99219 lineto stroke
 0 1 dtransform truncate idtransform setlinewidth pop
newpath 1.53079 51.29662 moveto
0 54.99219 lineto
-1.53079 51.29662 lineto
 closepath
gsave fill grestore stroke
 0.56693 0 dtransform exch truncate exch idtransform pop setlinewidth
 [1.5 1.5 ] 0 setdash
newpath 56.6929 0 moveto
56.6929 47.5656 lineto stroke
 0 0.2 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 0 0 moveto
43.42886 36.44173 lineto
56.6929 47.5656 lineto stroke
-9.976 -8.90779 moveto
(O) cmmi10 9.96265 fshow
 0 3 dtransform truncate idtransform setlinewidth pop
newpath 0 0 moveto 0 0 rlineto stroke
29.23526 33.90956 moveto
(B) cmmi9 8.96637 fshow
36.20856 32.84686 moveto
(\022) cmmi6 5.97758 fshow
newpath 43.42886 36.44173 moveto 0 0 rlineto stroke
59.6929 45.03345 moveto
(C) cmmi9 8.96637 fshow
66.2708 43.97075 moveto
(\022) cmmi6 5.97758 fshow
newpath 56.6929 47.5656 moveto 0 0 rlineto stroke
59.6929 -3.4039 moveto
(A) cmmi10 9.96265 fshow
-7.92679 58.7929 moveto
(C) cmsy10 9.96265 fshow
18.9824 2.35793 moveto
(\022) cmmi10 9.96265 fshow
37.01608 8.80626 moveto
(D) cmsy9 8.96637 fshow
44.13318 7.74356 moveto
(\022) cmmi6 5.97758 fshow
showpage
%%EOF
