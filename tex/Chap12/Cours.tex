\chapter{Continuité}
%!TeX root=../../encours.nouveau.tex
%%% Début exercice %%%

\objectifintro{On définit rigoureusement la notion déjà vue l'année dernière de continuité. C'est l'occasion de revoir le théorème des valeurs intermédiaires, et un corollaire important - le théorème de la bijection.}

\begin{extrait}{Albert Camus (1913 -- 1960). \emph{Noces}}
Une certaine continuité dans le désespoir peut engendrer la joie.
\end{extrait}

\begin{objectifs}
\begin{numerote}
        \item Concernant la continuité :
                        \begin{itemize}[label=\textbullet]
                        			\item \lienobj{1}{Savoir montrer qu'une fonction est continue en un point}
                                \item \lienobj{2}{Savoir prolonger de manière continue une fonction en un point}
                                \item \lienobj{3}{Savoir utiliser la continuité pour déterminer la limite d'une suite récurrente}\
                        \end{itemize}
        \item \lienobj{4}{Savoir utiliser le théorème des valeurs intermédiaires pour déterminer l'existence d'une solution à une équation  du type $f(x)=a$}
        \item \lienobj{5}{Savoir utiliser le théorème des bornes atteintes pour prouver l'existence d'un maximum ou d'un minimum}
        \item \lienobj{6}{Savoir utiliser le théorème de la bijection pour montrer qu'une fonction est bijective, et étudier le sens de variations d'une fonction réciproque}
        \item \lienobj{7}{Connaître la définition et les propriétés de la fonction $\arctan$}
\end{numerote}
\end{objectifs}

 \section{Généralités}

 	\subsection{Continuité en un point, sur un intervalle}

\begin{definition}[Continuité en un point]
\labelobj{1}
	Soit $f$ une fonction, et $I$ un \textbf{intervalle} inclus dans l'ensemble de définition de $f$.
 \begin{itemize}
 	\item On dit que la fonction $f$ est \textbf{continue à droite en un point $a$} de $I$ si $f$ admet une limite à droite en $a$, qui est égale à $f(a)$ : \[\lim_{x\rightarrow a^+} f(x)=f(a)\]
 	\item On dit que la fonction $f$ est \textbf{continue à gauche en un point $a$} de $I$ si $f$ admet une limite à gauche en $a$, qui est égale à $f(a)$ : \[\lim_{x\rightarrow a^-} f(x)=f(a)\]
 	\item On dit que la fonction $f$ est \textbf{continue en un point $a$} de $I$ si $f$ admet une limite en $a$, qui est égale à $f(a)$ : \[\lim_{x\rightarrow a} f(x)=f(a)\]
 \end{itemize}
\end{definition}

%% Figure
\begin{remarque}
Ainsi, $f$ est continue en $a$ si et seulement si
 \[\forall~\eps>0,~\exists~\eta >0,~\forall~x\in I,~|x-a|<\eta \Rightarrow |f(x)-f(a)|<\eps\]
\end{remarque}

\begin{definition}[Continuité sur un intervalle]
 Soit $f$ une fonction, et $I$ un \textbf{intervalle} inclus dans l'ensemble de définition de $f$. \\On dit que $f$ est \textbf{continue sur l'intervalle I} si elle est continue en tout point de $I$.
\end{definition}


 \begin{remarque}[Somme, produit, quotient, composée]
 Il résulte des théorèmes sur les limites que la \textbf{somme}, le \textbf{produit}, le \textbf{quotient} (là où il est défini) et la \textbf{composée} (là où elle est définie) de deux fonctions continues est continue. De même, la \textbf{valeur absolue} d'une fonction continue est également continue.
 \end{remarque}


 \begin{exemple}
 La fonction carré est une fonction continue sur $\R$.
 \begin{center}\includegraphics[width=5cm]{carree}\end{center}
\end{exemple}

 \begin{exemple}
La fonction \textbf{partie entière} n'est pas continue en $0,1,2,\ldots$ :
 \begin{center}\includegraphics[width=5cm]{partie_ent}\end{center}
 \end{exemple}

	\subsection{Continuité à droite et à gauche, et continuité}

Un résultat vu sur les limites nous permet d'en déduire une propriété importante :

\begin{propriete}
Soit $f$ une fonction définie sur un intervalle $I$, et $a$ un point intérieur à $I$ (c'est-à-dire que $a$ n'est pas une borne de $I$). \\
$f$ est continue en $a$ si et seulement si $f$ est continue à droite et à gauche en $a$.
\end{propriete}

\begin{methode}
Pour montrer qu'une fonction définie par morceaux est continue en un réel $a$, on calcule les limites à droite et à gauche en ce réel, et on montre que les deux limites valent $f(a)$.
\end{methode}


\begin{exemple}
Soit $f$ la fonction définie sur $\R$ par \[f(x)= \left \{ \begin{array}{cc} x+1 & \textrm{ si }x\geq 0 \\ \eu{-x} & \textrm{sinon}\end{array}\right. \]
 Montrer que $f$ est continue en $0$.
\end{exemple}

\solution[5]{On a $f(0)=1$. Constatons alors que
 \[\lim_{x\rightarrow 0^+} f(x)=\lim_{x\rightarrow 0^+} x+1 = 1 = f(0)\]
 \[\lim_{x\rightarrow 0^-} f(x)=\lim_{x\rightarrow 0^-} \eu{-x}=\eu{-0}=1=f(0)\]
 Ainsi, $f$ est continue à droite et à gauche en $0$. Elle est donc continue en $0$.
 }

\begin{experiencehistorique}
\textbf{Karl Weierstrass} (encore lui) donna vers 1850 la première définition de la continuité d'une fonction. C'est également lui qui donna les définitions rigoureuses de la dérivée, que l'on verra plus tard.
 \end{experiencehistorique}

 	\subsection{Continuité des fonctions usuelles}

 \begin{proposition}
 \begin{itemize}
 \item Les fonctions polynômes sont continues sur $\R$.
 \item Les fonctions rationnelles sont continues sur tout intervalle contenu dans leur domaine de définition.
 \item La fonction valeur absolue est continue sur $\R$.
 \item La fonction racine carrée est continue sur $[0;+\infty[$.
 \item La fonction exponentielle est continue sur $\R$, et la fonction $\ln$ sur $\R>$.
 \item Les fonctions puissances $x\mapsto a^x$ ($a>0$) sont continues sur $\R$. Les fonctions puissances $x \mapsto x^a$ ($a \in \R$) sont continues sur $\R>$.
 \item Les fonctions trigonométriques $\cos$, $\sin$, et $\tan$ sont continues sur leurs domaines de définition.
 \end{itemize}
 \end{proposition}

 \begin{demonstration}
Admis. On verra dans un chapitre ultérieure qu'une fonction dérivable est continue, ce qui nous permet de conclure rapidement.
 \end{demonstration}

 \begin{proposition}[Restriction d'une fonction continue]
   Soit $f:I\to \R$ une fonction continue, et $J\subset I$ un intervalle non vide et non réduit à un point. Alors $\restric{f}{J}$ est également continue.
 \end{proposition}

 Cela amène une remarque importante : la notion de continuité est une notion \textbf{locale}. Ainsi, si $f:\interoo{a b}\to \R$ est une fonction telle que $f$ est continue sur $\interof{a c}$ et sur $\interfo{c b}$, alors $f$ est continue sur $\interoo{a b}$. En effet, la continuité étant une notion locale, le seul soucis est en $c$. Or, étant continue sur $\interof{a c}$, $f$ est continue à gauche en $c$ et $\ds{\lim_{x\to c^-} f(x)=f(c)}$; de même, par continuité sur $\interfo{c b}$, $\ds{\lim_{x\to c^+} f(x)=f(c)}$ et donc $f$ est bien continue en $c$.

\begin{attention}
  En revanche, si $f$ est continue sur $\interoo{a c}$ et sur $\interfo{c b}$, on ne peut pas conclure, car la fonction n'est pas \textit{a priori} continue à gauche en $c$.

  Il suffit de prendre la fonction partie entière pour s'en convaincre.
\end{attention}

 	\subsection{Prolongement par continuité}

 \begin{definition}
 \labelobj{2}
Soit $f$ une fonction définie sur un intervalle $I$ sauf en un réel $a$. On suppose que $f$ est continue sur $I\setminus \{a\}$, et qu'il existe un réel $l$ tel que $\displaystyle{\lim_{x\rightarrow a} f(x)=\ell}$. \\On dit qu'on peut \textbf{prolonger par continuité} la fonction $f$ en posant $f(a)=\ell$. On définit ainsi une nouvelle fonction, définie sur $I$, qui est continue sur $I$ et coïncide avec $f$ sur $I\setminus \{a\}$.
 \end{definition}

 \begin{exemple}
Soit $f$ la fonction définie sur $\R>$ par $f(x)=x\ln(x)$. Montrer que l'on peut prolonger par continuité $f$ en $0$.
 \end{exemple}

 \solution[3]{On constate que $f$ est continue sur $\R>$. De plus, \[\lim_{x\rightarrow 0^+} f(x)=0 \textrm{ par croissance comparée}\]
 Ainsi, on peut prolonger par continuité $f$ en $0$ en posant $f(0)=0$.
 }

\begin{remarque}
Rigoureusement, on doit définir une nouvelle fonction $\tilde{f}$ qui est égale à $f$ sur $I\setminus \{a\}$, et telle que $\tilde{f}(a)=\ell$. En pratique, on confondra toujours $\tilde{f}$ et $f$.
\end{remarque}

\afaire{Exercices \lienexo{01}, \lienexo{02}, \lienexo{03}, \lienexo{04} et \lienexo{08}.}

 	\subsection{Tableau de variations et convention}

 \begin{remarque}
Dans un tableau de variation, on convient que les flèches obliques indiquent que la fonction est \textbf{continue et strictement monotone}.
 \end{remarque}

     \subsection{Continuité par morceaux}

\begin{definition}
Une fonction $f$ est dite \textbf{continue par morceaux} sur le segment $[a,b]$ s’il existe une \textbf{subdivision} $a_0=a<a_1 <\cdots<a_n=b$ (c'est-à-dire un découpage du segment $[a;b]$) telle que les restrictions de $f$ à chaque intervalle ouvert $]a_i , a_{i+1} [$ admettent un prolongement continu à l’intervalle fermé $[a_i, a_{i+1}]$.
\end{definition}

   \begin{center}
       \includegraphics[width=7cm]{continuemorceau}
   \end{center}

\begin{exemple}
La fonction partie entière est continue par morceaux sur $\R$. En effet, sur tout segment de la forme $]n;n+1[$ (où $n \in \Z$) le fonction est continue (car constante), et prolongeable par continuité en $n$ et $n+1$.
\end{exemple}

 \section{Théorème des valeurs intermédiaires}

 \subsection{Théorème des valeurs intermédiaires sur un segment}


\begin{theoreme}[Théorème des valeurs intermédiaires I]
Soit $f$ une fonction continue sur un intervalle $\interff{a b}$. Alors, pour tout réel $k$ pris entre $f(a)$ et $f(b)$, il existe \textbf{au moins} un réel $c$ de l'intervalle $\interff{a b}$, tel que $f(c)=k$.
\end{theoreme}

 \begin{center}
 \includegraphics[width=7cm]{val_int}
 \end{center}

 \preuve[20]{Si $k=f(a)$ ou $k=f(b)$, le résultat est trivial (il suffit de prendre respectivement $c=a$ et $c=b$). De même, si $f(a)=f(b)$, il suffit de prendre $c=a$.

 Supposons alors $f(a)\neq f(b)$ et, quitte à remplacer $f$ par $-f$, que $f(a)<f(b)$.

 Soit $k\in \interoo{f(a) f(b)}$. Nous allons introduire deux suites par un découpage dit \textbf{par dichotomie}. On pose $a_0=a$, $b_0=b$ et pour tout entier $n$ :
 \[ (a_{n+1}, b_{n+1}) = \left \{ {\renewcommand{\arraystretch}{2}\begin{array}{ccc} \left(a_n, \dfrac{a_n+b_n}{2}\right) & \text{si} & f\left(\dfrac{a_n+b_n}{2}\right)>k \\
\left(\dfrac{a_n+b_n}{2}, b_n\right) & \text{si} & f\left(\dfrac{a_n+b_n}{2}\right)\leq k\end{array}}\right..
\]
Par définition des suites, et qu'on peut montrer par récurrence sur $n$, on a les résultats suivants :
\begin{itemize}
  \item Pour tout $n$, $f(a_n)\leq k\leq f(b_n)$,
  \item $(a_n)$ est croissante,
  \item $(b_n)$ est décroissante.
\end{itemize}
De plus, par définition toujours, \[ b_{n+1}-a_{n+1}=\frac{b_n-a_n}{2} \]
et donc la suite $(b_n-a_n)$ est une suite géométrique, de raison $\frac12$. Elle converge donc vers $0$.

Ainsi, les suites $(a_n)$ et $(b_n)$ sont adjacentes. Par théorème, elles convergent donc toutes les deux vers un même réel, que l'on note $c$. D'une part, $a_0\leq c \leq b_0$ donc $c\in \interff{a b}$. D'autre part, par continuité de $f$ sur $\interff{a b}$, on a
\[ f(a_n)\tendversen{n\to +\infty} f(c) \qeq f(b_n)\tendversen{n\to +\infty} f(c). \]
Or, pour tout $n$, $f(a_n)\geq k$ et $f(b_n)<k$. Par passage à la limite, $f(c)\geq k$ et $f(c)\leq k$, c'est-à-dire $f(c)=k$.
}

\begin{remarque}
  La méthode utilisée ici pour démontrer l'existence sera utilisé un peu plus tard pour écrire une fonction \textsc{Python} déterminant une valeur approchée d'une solution d'une équation du type $f(x)=0$.
\end{remarque}

\begin{experiencehistorique}
\textbf{Bernard Bolzano} donna en 1817 la première démonstration analytique de ce théorème. \textbf{Weierstrass} en donna une autre plus tard, vers 1850.
\end{experiencehistorique}

\begin{consequence}
  Soit $f$ une fonction continue sur $I$. Si $f$ ne s'annule pas sur $I$, alors $f$ est strictement positive ou strictement négative.
\end{consequence}

\preuve[5]{On raisonne par l'absurde et on suppose qu'elle change de signe. Il existe donc deux réels $x$ et $y$ de $I$ tels que $f(x)>0$ et $f(y)<0$. Mais alors, puisque $0\in \interff{f(y) f(x)}$, le théorème des valeurs intermédiaires nous assure l'existence d'un réel $c$ entre $x$ et $y$ tel que $f(c)=0$, ce qui est absurde.
}

  \subsection{Théorème des valeurs intermédiaires sur un intervalle}

Le théorème précédent n'est valable que sur un segment $\interff{a b}$. On va le généraliser à un intervalle $I$ tout entier.

\begin{definition}[Bornes supérieures et inférieures d'une fonction]
  Soit $f$ une fonction définie sur un intervalle $I$ non vide.

  Si $f$ est majorée, on appelle \textbf{borne supérieure} de $f$, le nombre
  \[ \sup_{x\in I} f(x) = \sup \left \{ f(x),\, x\in I \right\}. \]

  Si $f$ est minorée, on appelle \textbf{borne inférieure} de $f$, le nombre
  \[ \inf_{x\in I} f(x) = \inf \left \{ f(x),\, x\in I \right \}. \]
\end{definition}

Remarquons que le théorème de la borne supérieure nous garantit l'existence de ces valeurs.

On peut alors énoncer une version plus large du théorème des valeurs intermédiaires :

\begin{theoreme}[Théorème des valeurs intermédiaires II]
 Soit $f$ une fonction continue sur $I$, à valeurs réelles. Notons
 \[ M = \left \{ \begin{array}{ll}\sup\limits_{x\in I} f(x) & \text{si $f$ est majorée}\\ +\infty & \text{sinon.}\end{array}\right. \qeq m = \left \{ \begin{array}{ll}\inf\limits_{x\in I} f(x) & \text{si $f$ est minorée}\\ -\infty & \text{sinon.}\end{array}\right.. \]
 Alors, pour tout $k\in \interoo{m M}$, il existe $c\in I$ tel que $f(c)=k$. Autrement dit, $\interoo{m M}\subset f(I)$.
\end{theoreme}

\preuve[5]{Soit $k\in \interoo{m M}$. Par caractérisation de la borne supérieure (et inférieure), il existe $a\in I$ tel que $f(a)<k$ et il existe $b\in I$ tel que $f(b)>k$. Ainsi, $k\in \interff{f(a), f(b)}$. D'après le TVI sur un segment, il existe $c$ entre $a$ et $b$ tel que $f(c)=k$. Comme $c\in I$, on en déduit que $k\in f(I)$ et donc $\interoo{m M} \subset f(I)$.
}

On a un exemple classique:

\begin{exemple}
 Soit $P$ une fonction polynomiale à coefficients réels de degré impair. Alors $P$ admet au moins une racine.
 \end{exemple}

 \solution[4]{En effet, puisque le degré est impair, les limites de $P$ en $+\infty$ et $-\infty$ sont infinies et opposées l'une de l'autre. Ainsi, $P$ n'est ni majorée, ni minorée et le résultat précédent assure, puisque $0\in \interoo{-\infty{} +\infty}$, qu'il existe au moins un réel $c$ tel que $f(c)=0$.

 }


  \subsection{Théorème des valeurs intermédiaires général}

On dispose d'un énoncé plus général :

\begin{theoreme}[Théorème des valeurs intermédiaires II]
\labelobj{4}
Soit $f$ une fonction définie et continue sur un intervalle $I$ et à valeurs réelles. Alors l'image $f(I)$ est un intervalle.
\end{theoreme}

\preuve[5]{
En utilisant les notations du théorème précédent, pour tout $x\in I$, $m\leq f(x)\leq M$, et donc $f(I)$ est inclus dans $\interoo{m M}, \interof{m M}, \interfo{m M}$ ou bien $\interff{m M}$ (si ces intervalles ont un sens).

Or, le TVI précédent nous assure que $\interoo{m M} \subset f(I)$. Par conséquent, $f(I)$ est égal à $\interoo{m M}$, $\interof{m M}$, $\interfo{m M}$ ou bien $\interff{m M}$ et est donc un intervalle.
}

\begin{attention}
  L'image d'un intervalle par une fonction continue n'est pas forcément un intervalle de même nature (caractère ouvert/fermé/semi-fermé).
\end{attention}

  \subsection{Théorème des bornes atteintes}

Les fonctions continues sur un segment vérifient une propriété fondamentale : les bornes supérieures et inférieures sont nécéssairement atteintes.

\begin{definition}
  On dit que $f:I\to \R$ atteint ses bornes s'il existe $s\in I$ et $t\in I$ tels que \[ f(s)=\inf_{x\in I} f(x) \qeq f(t)=\sup_{x\in I} f(x). \]
  Dans ce cas, on note plutôt
  \[ f(s)=\min_{x\in I} f(x) \qeq f(t)=\max_{x\in I} f(x). \]
\end{definition}

On a alors le théorème suivant :

\begin{theoreme}
  Soient $a$ et $b$ deux réels tels que $a<b$. Soit $f$ une fonction continue sur $\interff{a b}$. Alors $f$ est bornée et atteint ses bornes.
\end{theoreme}

\preuve[15]{Soit $f$ une fonction continue sur $\interff{a b}$. Montrons tout d'abord que $f$ est bornée.

On suppose par l'absurde qu'elle ne l'est pas. On construit deux suites par dichotomie de telle sorte que, pour tout $n$, $a_n<b_n$ et $f$ n'est pas bornée sur $\interff{a_n b_n}$.
\begin{itemize}
  \item On pose $a_0=a$ et $b_0=b$.
  \item Soit $n\in \N$. On suppose qu'on a construit $a_n$et $b_n$ tels que $a_n<b_n$ et $f$ n'est pas bornée sur $\interff{a_n b_n}$. On pose $c_n=\dfrac{a_n+b_n}{2}\in \interff{a_n b_n}$. Puisque $f$ n'est pas bornée sur $\interff{a_n b_n}$, elle n'est pas bornée sur $\interff{a_n c_n}$ ou bien sur $\interff{c_n b_n}$. Si elle n'est pas bornée sur $\interff{a_n c_n}$, on pose $a_{n+1}=a_n$ et $b_{n+1}=c_n$. Sinon, on pose $a_{n+1}=c_n$ et $b_{n+1}=b_n$.
\end{itemize}
Par construction, $(a_n)$ et $(b_n)$ sont adjacentes (même raisonnement que pour le TVI) et convergent donc toutes les deux vers un réel $c\in \interff{a b}$. Par continuité de $f$ en $c$, $f$ est bornée au voisinage de $c$ : il existe $\alpha>0$ et $M>0$ tels que \[ \forall~x\in I \cap \interff{{c-\alpha} {c+\alpha}},\, |f(x)|\leq M. \]
Mais alors, puisque $(a_n)$ et $(b_n)$ convergent vers $c$, il existe deux rangs $n_0$ et $n'_0$ tels que
\[ \forall~n\geq n_0,\, a_n\in I \cap \interff{{c-\alpha} {c+\alpha}} \qeq \forall~n\geq n'_0,\, b_n\in I \cap \interff{{c-\alpha} {c+\alpha}}. \]
Mais alors, posons $N=\max(n_0, n'_0)$. D'après ce qui précède, $a_N\in I \cap \interff{{c-\alpha} {c+\alpha}}$ et $b_N\in I \cap \interff{{c-\alpha} {c+\alpha}}$, donc $\interff{a_N b_N} \subset I \cap \interff{{c-\alpha} {c+\alpha}}$. Et finalement
\[ \forall~x\in \interff{a_N b_N},\quad |f(x)|\leq M. \]
$f$ est donc bornée sur $\interff{a_N b_N}$ ce qui est absurde.

\vspace*{0.5cm}
Il reste enfin à prouver que les bornes sont atteintes. On suppose par l'absurce que ce n'est pas le cas. On note $m$ la borne inférieure de $f$ sur $\interff{a b}$ qu'on suppose non atteinte. On a donc, pour tout $x\in \interff{a b}$, $f(x)>m$. On pose alors $g:x\mapsto \dfrac{1}{f(x)-m}$. Elle est bien définie, positive et continue sur $\interff{a b}$. D'après ce qui précède, $g$ est bornée, donc majorée. On note $K>0$ un majorant de $g$. On a alors
\[ \forall~x\in \interff{a b},\quad \frac{1}{f(x)-m} \leq K \]
soit
\[ \forall~x\in \interff{a b},\quad f(x)\geq m+\frac{1}{K} \]
ce qui contredit le fait que $m$ soit le plus grand des minorants.
}

On peut ré-écrire ce théorème ainsi :

\begin{theoreme}[Image d'un segment par une fonction continue]
 L'image d'un segment par une fonction continue est un segment.

 Si $f:\interff{a b}\to \R$ est continue, avec $a<b$, alors \[ m=\min_{x\in \interff{a b}}f(x) \qeq M=\max_{x\in \interff{a b}} f(x) \]
 existent et on a $f\left(\interff{a b}\right)=\interff{m M}$.
\end{theoreme}

 	\section{Théorème de la bijection}

    \subsection{Enoncé}

 Dans le cas d'une fonction continue strictement monotone sur un intervalle, on dispose d'un théorème plus intéressant encore :

\begin{theoreme}[Théorème de la bijection]
  Soit $f:I\to \R$. Si $f$ est continue et strictement monotone sur $I$ alors
  \begin{itemize}
    \item $f$ est une bijection de $I$ sur l'intervalle $f(I)$,
    \item la bijection réciproque $f^{-1}$ est strictement monotone sur $f(I)$, de même sens de variation que $f$,
    \item la bijection réciproque $f^{-1}$ est continue sur $f(I)$.
  \end{itemize}
\end{theoreme}

\begin{theoreme}[Théorème de la bijection]
\labelobj{5}
  Soit $f$ une fonction \textbf{continue strictement monotone} sur l'intervalle $I=[a;b]$. Alors pour tout réel $k$ compris entre $f(a)$ et $f(b)$, l'équation $f(x)=k$ possède une \textbf{unique solution} dans $[a;b]$.
\end{theoreme}

\preuve[15]{
A faire
}


\begin{proposition}
  Dans un repère orthonormé $\repere$ du plan, les courbes représentatives de $f$ et de sa bijection réciproque sont symétriques par rapport à la droite d'équation $y=x$.
\end{proposition}


\preuve[7]{A faire.}

\begin{methode}
  On utilise le théorème de la bijection pour démontrer l'existence et l'unicité d'une solution à une équation, ou bien pour démontrer qu'une fonction est une bijection.

Pour démontrer que $f$ est une bijection de $I$ dans un intervalle $J$ ainsi :
\begin{itemize}
  \item on montre que $f$ est continue et strictement monotone sur $I$,
  \item on écrit \og{}d'après le théorème de la bijection, $f$ réalise une bijection de $I$ dans $f(I)$\fg{},
  \item et on déterminer $f(I)$ à l'aide du tableau suivant :
\[ {\renewcommand{\arraystretch}{2}\begin{array}{!{\vrule width 2pt}c||c|c|c|c!{\vrule width 2pt}}\noalign{\hrule height 2pt} f\quad \setminus \quad  I & \interff{a b} & \interof{a b} & \interfo{a b} & \interoo{a b} \\\noalign{\hrule height 1.5pt}
\text{st. }\nearrow & \interff{f(a) f(b)}& \left]{\lim\limits_a f},\, f(b)\right] & \left[ f(a),\, {\lim\limits_b f}\right[ & \left]{\lim\limits_a f},\, \lim\limits_b f\right[ \\\hline
\text{st. }\searrow & \interff{f(b) f(a)}& \left[f(b),\,{\lim\limits_a f}\right[ & \left]  {\lim\limits_b f},\, f(a)\right] & \left]{\lim\limits_b f},\, \lim\limits_a f\right[
\\\noalign{\hrule height 2pt}
\end{array}}
\]
\end{itemize}

Pour démontrer que l'équation $f(x)=k$ admet une unique solution ainsi :
\begin{itemize}
  \item on montre que $f$ est continue et strictement monotone sur $I$,
  \item on écrit \og{}d'après le théorème de la bijection, $f$ réalise une bijection de $I$ dans $f(I)$\fg{},
  \item on détermine $f(I)$ par le tableau précédent,
  \item et on montre que $k\in f(I)$.
\end{itemize}
\end{methode}

\begin{exemple}
  On suppose que la fonction $f$ est continue, et que ses variations sont décrites dans le tableau suivant :

\begin{center}
\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $f(x)$ / 1.5}{$0$, $5$}
   \tkzTabVar{-/ $-3$, +/ $4$}
\end{tikzpicture}
\end{center}

 Montrer que l'équation $f(x)=0$ possède une unique solution sur $[0;5]$.
\end{exemple}


 \solution[5]{
 \begin{itemize}
 	\item $f$ est continue sur $\interff{0 5}$;
 	\item $f$ est strictement croissante sur $\interff{0 5}$.
\end{itemize}
D'après le théorème de la bijection, $f$ établit une bijection de $\interff{0 5}$ sur $f(\interff{0 5}) = \interff{-3 4}$. Or $0 \in \interff{-3 4}$; doncl'équation $f(x)=0$ possède une unique solution sur l'intervalle $[0;5]$.
 }

\begin{exo}
   Prouver que l'équation $(E): x\sqrt{x}=1-x$ admet une unique solution sur $\R^{+*}$.
\end{exo}


 \solution[10]{On se ramène à une écriture $f(x)=k$ : $x+x\sqrt{x}=1$. Soit $f$ la fonction définie sur $\R^{+*}$ par $f(x)=x+x\sqrt{x}$. $f$  est dérivable sur $\R^{+*}$ et sa dérivée vaut
 \[f'(x)=1+\frac{3}{2}\sqrt{x} > 0\]
 La fonction $f$ est donc strictement croissante, et continue sur $]0;+\infty[$. D'après le théorème de la bijection, elle établit donc une bijection de $\interoo{0 +\infty}$ sur \[f\left(\interoo{0 +\infty}\right)=\interoo{{\lim_{x\to 0} f(x)} {\lim_{x\to+\infty} f(x)}} = \interoo{0 +\infty} \] qui contient $1$.\\
 L'équation $f(x)=1$ possède donc une unique solution sur $]0;+\infty[$.
 }


 \subsection{Une application : la fonction arctangente}

 \begin{proposition}
   La fonction tangente est une bijection de $\left] -\frac{\pi}{2},\, \frac{\pi}{2}\right[$ sur $\R$. Sa fonction réciproque est appelée \textbf{arctangente}, notée $\arctan$.

   Cette fonction est impaire, continue, strictement croissante sur $\R$ et \[ \lim_{x\to -\infty} \arctan(x)=-\frac{\pi}{2}\qeq \lim_{x\to +\infty} \arctan(x)=+\frac{\pi}{2}. \]

   On a également $\arctan(0)=0$ et $\arctan(1)=\frac{\pi}{4}$.
\end{proposition}

\preuve[8]{La fonction $\tan$ est continue et strictement croissante sur $I=\left] -\frac{\pi}{2},\, \frac{\pi}{2}\right[$. Le théorème de la bijection nous assure qu'elle réalise une bijection de $I$ sur $f(I)$ et que sa réciproque $\arctan$ est continue et strictement croissante sur $f(I)$. Puisque
\[ \lim_{x\to \left(\frac{\pi}{2}\right)^-} \tan(x)=+\infty \qeq \lim_{x\to\left(-\frac{\pi}{2}\right)^+} \tan(x)=-\infty, \]
la proposition précédente entraîne que $f(I)=\interoo{-\infty{} \infty{}}$.

$\tan$ est impaire donc, pour tout $x\in \R$ :
\[ \tan(\arctan(-x)) = -x = -\tan(\arctan(x)) = \tan(-\arctan(x)) \]
et donc, puisque $\tan$ est injective sur $\left] -\frac{\pi}{2},\, \frac{\pi}{2}\right[$, $\arctan(-x)=\arctan(x)$.

Enfin, $\tan(0)=0$ donc $\arctan(0)=0$, et $\tan\left(\frac{\pi}{4}\right)=1$ donc $\arctan(1)=\frac{\pi}{4}$.
}

\begin{center}
  \includegraphics{arctan.mps}
\end{center}


\section{Méthode d'encadrement d'une solution par dichotomie}

 Dans le cas d'une fonction continue, et strictement monotone sur un intervalle $[a;b]$, avec $f(a)$ et $f(b)$ de signe contraire, on peut déterminer une valeur approchée de la solution de l'équation $f(x)=0$ par \textbf{dichotomie} : on découpe au fur et à mesure l'intervalle en $2$ pour pouvoir cibler la solution, en utilisant les idées de la démonstratoin du théorème des valeurs intermédiaires.

 \begin{center}
 \includegraphics[width=7cm]{dichotomie}\\
 \textit{Image disponible sur Wikipédia}
 \end{center}


\begin{culturel}
Le mot dichotomie vient du grec \textit{dikha} (\textit{en deux}), et \textit{tomein} (\textit{couper}), c'est à dire ``couper en deux''.
\end{culturel}

 \begin{algorithme}
 	Dans cet algorithme, $e$ représente la précision de la valeur approchée.

 \begin{algorithm}[H]
 \DontPrintSemicolon
 \SetAlgoLined
 \SetKwInOut{Initialisation}{Initialisation}
 \SetKwFor{Tq}{Tant que}{}{FinTantque}
 \Entree{Saisir $a$, $b$ et $e$}
 \Tq{$b-a \geq e$}{
 	$m \leftarrow \frac{a+b}{2}$\;
 	\eSi{$f(a)\times f(m) \leq 0$}{$b\leftarrow m$}{$a\leftarrow m$}\; }
 \Sortie{Afficher $a$ et $b$}
 \caption{DICHOTOMIE}
 \end{algorithm}
 \end{algorithme}


En \textsc{Python}, cela donne (à condition que la fonction $f$ ait été définie, dans le cas $f$ croissante par exemple) la fonction suivante, où $[x;y]$ désigne l'intervalle de recherche de départ, et \verb-eps- la précision :

\begin{pythonfile}{dicho.py}
\end{pythonfile}



\section{Suites récurrentes et continuité}

La continuité va nous permettre de simplifier l'étude des suites récurrentes, mais également de pouvoir déterminer les limites des suites récurrentes convergentes.

		\subsection{Intervalle stable}

\begin{rappel}
  Soit $f:I\to \R$ et $J \subset I$. On dit que $J$ est un intervalle \textbf{stable} de $f$ si $f(J)\subset J$, c'est-à-dire si
\[ \forall~x\in J,\quad f(x) \in J \]
\end{rappel}


\begin{exemple}
  Par exemple, l'intervalle $[0,1]$ est un intervalle stable de la fonction carrée. En effet, pour tout $x\in [0,1]$, on a $x^2\in [0,1]$.
\end{exemple}


\begin{methode}
  Pour démontrer qu'un intervalle $I$ est stable, on dispose de deux méthodes classique :
\begin{enumerate}
	\item Si $I$ est un intervalle du type $I=[a;b]$, on part de $a\leq x\leq b$ et on essaie de raisonner par implication pour démontrer que $a\leq f(x)\leq f(b)$.
	\item Sinon, on étudie les variations de $f$, et en utilisant les monotonies de $f$ et l'éventuelle continuité, on conclut.
\end{enumerate}
\end{methode}


\begin{exo}
  \label{exo:intervalle_stable} Soit $f:\R\to \R$ définie pour tout $x$ par $f(x)=\frac{1}{3}x^2+\frac{2}{3}x-\frac{2}{3}$. Montrer que $[-1;2]$ est un intervalle stable de $f$.
\end{exo}


\solution[10]{$f$ est un trinôme du second degré. On obtient alors le tableau de variation suivant :

\begin{center}
\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $f(x)$ / 1.5}{$-\infty$, $-1$, $+\infty$}
   \tkzTabVar{+/ $+\infty$, -/ $-1$, +/ $+\infty$}
\end{tikzpicture}
\end{center}
Sur $[-1;2]$, $f$ est croissante. On a $f(-1)=-1$ et $f(2)=2$. Par croissance et continuité de $f$, on en déduit que $f([-1;2])=[-1;2]$.

Sans la continuité, mais uniquement avec la monotonie, on en déduit que $f([-1;2])\subset [-1;2]$, ce qui est souvent suffisant.
}

Les intervalles stables sont très pratiques pour l'étude des suites récurrentes :

\begin{theoreme}[Rappel]
  Soit $f:I\to \R$ une fonction, $J\subset I$ et soit $u$ la suite définie par $u_0 \in I$ et pour tout $n$, $u_{n+1}=f(u_n)$.

Si $u_0\in J$ et $J$ est un intervalle stable de $f$, alors pour tout $n$, $u_n\in J$.
\end{theoreme}


\ifprof
\begin{demonstration}
  Cela se montre rapidement par récurrence. Soit $P$ la proposition définie pour tout $n$ par $P_n:u_n\in J$.
\begin{itemize}
\item Pour $n=0$, par hypothèse, $u_0\in J$ donc $P_0$ est vraie.
\item Supposons la proposition $P_n$ vraie pour un certain $n$, et montrons que $P_{n+1}$ est vraie.\\Par hypothèse de récurrence, $u_n\in J$. Or $J$ est un intervalle stable de $f$, donc $f(u_n) \in J$. Or, par définition, $u_{n+1}=f(u_n)$ et donc $u_{n+1}\in J$ et donc $P_{n+1}$ est vraie.
\end{itemize}
D'après le principe de récurrence, la proposition $P_n$ est vraie pour tout $n$, et donc \[ \forall~n\in \N,\quad u_n\in J \]
\end{demonstration}

\else
\lignes{10}
\fi

\begin{remarque}
  Cela permet ainsi de montrer qu'une suite récurrente est bornée, en cherchant un intervalle stable de la fonction sous-jacente.
\end{remarque}

\begin{exo}
  Soit $u$ la suite définie par $u_0=1$ et pour tout entier $n$, $u_{n+1}=f(u_n)$, avec $f:x\mapsto \frac{1}{3}x^2+\frac{2}{3}x-\frac{2}{3}$. Montrer que pour tout $n$, $u_n\in [-1;2]$.
\end{exo}


\solution[5]{On a montré dans l'exercice \thechapter.\ref{exo:intervalle_stable} que l'intervalle $[-1;2]$ est stable par $f$. Puisque $u_0=1\in [-1;2]$, d'après le théorème précédent, on a bien que pour tout $n$, $u_n\in [-1;2]$}

\begin{remarque}
  En pratique, cette dernière récurrence sera systématiquement à rédiger, mais appliquée au contexte de l'exercice.
\end{remarque}


		\subsection{Théorème du point fixe}

La continuité va permettre de déterminer la limite d'une suite définie sous la forme $u_{n+1}=f(u_n)$, lorsque $f$ est continue.

\begin{theoreme}
\labelobj{3}
   Soit $f$ une fonction continue, et $\ell$ un réel. Soit $(u_n)$ une suite convergente, de limite $\ell$. Alors la suite $(f(u_n))$ converge également, et a pour limite $f(\ell)$.
\end{theoreme}


\begin{remarque}
  Si $f$ n'est pas continue en $x_0$ mais si la fonction $f$ a pour limite $\ell$ en $x_0$, alors quelle que soit la suite $(u_n)$ de limite $x_0$, $(f(u_n))$ converge vers $\ell$.
\end{remarque}


 \begin{theoreme}[Théorème du point fixe]
 Soit $f$ une fonction continue, et $u$ une suite définie par $u_0$ donné, et pour tout $n$, $u_{n+1}=f(u_n)$.
 \\ Si la suite $u$ est convergente, alors par passage à la limite, en notant $\ell$ la limite de $u$, on en déduit que la limite $\ell$ vérifie $\ell=f(\ell)$ : c'est donc un \textbf{point fixe} de la fonction $f$.
\end{theoreme}


\begin{methode}
   Le théorème précédent permet souvent de déterminer la limite d'une suite définie par récurrence lorsqu'elle est convergente.
\end{methode}


\begin{exo}
  Soit $u$ la suite définie par $u_0=\frac{1}{2}$ et, pour tout $n$, $u_{n+1}=\sqrt{u_n}$.
 \begin{enumerate}
     \item Montrer que la suite $u$ est croissante, et que pour tout $n$, $0\leq u_n \leq 1$.
     \item En déduire que la suite $u$ converge, et déterminer sa limite.
 \end{enumerate}
\end{exo}


 \solution[15]{
 \begin{enumerate}
     \item Méthode classique : on montre par récurrence que pour tout $n$, $0\leq u_n\leq 1$ et que $u_n\leq u_{n+1}$. ~\\Pour tout entier $n$, soit $P_n$ la proposition définie par ``$0\leq u_n \leq 1$''.
     \begin{itemize}
         \item[$\bullet$] Initialisation : pour $n=0$, on a $u_0=\frac{1}{2}$ et on a bien $0\leq u_0 \leq 1$ : $P_0$ est vraie.
         \item[$\bullet$] Hérédité : supposons que la propriété $P_n$ est vraie pour un certain entier $n$, et montrons que $P_{n+1}$ est vraie. \\On a $0\leq u_n \leq 1$. La fonction racine étant croissante sur $\R^+$, on a donc
         \[\sqrt{0} \leq \sqrt{u_n} \leq 1\]
         et donc
         $0\leq u_{n+1} \leq 1$ : $P_{n+1}$ est donc vraie : la propriété est héréditaire.
         \item[$\bullet$] Conclusion : d'après le principe de récurrence, la proposition $P_n$ est vraie pour tout entier $n$ :
         \[\forall~n,~0\leq u_n \leq 1\]
     \end{itemize}
     ~\\Pour tout entier $n$, soit $Q_n$ la proposition définie par ``$u_n \leq u_{n+1}$''.\\
     \textbf{Remarque} : on aurait pu, aussi, montrer que $[0;1]$ est un intervalle stable de la fonction racine, puis de démontrer rapidement par récurrence que pour tout $n$, $u_n\in [0;1]$.
     \begin{itemize}
         \item[$\bullet$] Initialisation : pour $n=0$, on a $u_0=\frac{1}{2}$ et $u_1=\sqrt{\frac{1}{2}} = \frac{\sqrt{2}}{2} > u_0$. $u_0 \leq u_1$ : $Q_0$ est vraie.
         \item[$\bullet$] Hérédité : supposons que la propriété $Q_n$ est vraie pour un certain entier $n$, et montrons que $Q_{n+1}$ est vraie. \\On a $ u_n \leq u_{n+1}$. La fonction racine étant croissante sur $\R^+$, on a donc
         \[\sqrt{u_n} \leq \sqrt{u_{n+1}}\]
         et donc
         $u_{n+1} \leq u_{n+2}$ : $Q_{n+1}$ est donc vraie : la propriété est héréditaire.
         \item[$\bullet$] Conclusion : d'après le principe de récurrence, la proposition $Q_n$ est vraie pour tout entier $n$ :
         \[\forall~n,~u_n \leq u_{n+1}\]
         La suite $u$ est donc croissante.
     \end{itemize}
 	\textbf{Remarque} : on aurait également pu démontrer, par récurrence, les deux propositions en une seule, en démontrant ``$0\leq u_n \leq u_{n+1}\leq 1$''.
     \item La suite $u$ est donc croissante et majorée. D'après le théorème de convergence monotone, celle-ci converge. Notons $\ell$ sa limite.
 Puisque la fonction racine est continue sur $]0;+\infty[$, on en déduit par passage à la limite que que $\ell=\sqrt{\ell}$, c'est à dire $\ell=0$ ou $\ell=1$. Or, puisqu'elle est croissante, \[\textrm{Pour tout }~n, u_n\geq u_0=\frac{1}{2}\]
  La limite est donc $1$.
 \end{enumerate}
 }

 \afaire{Exercice \lienexo{07}}
