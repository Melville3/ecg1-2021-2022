\chapter{Ensembles et applications}
%!TeX root=../../encours.nouveau.tex

\objectifintro{Nous allons reprendre les notions d'ensembels et d'applications vues dans les chapitres précédents, en approfondissant. Nous verrons ainsi la notion de produit cartésien d'ensemble, et d'applications injectives, surjectives et bijectives.}

\begin{extrait}{Gustave Flaubert (1821--1880) -- \emph{Correspondance}}
Les honneurs déshonorent; Le titre dégrade ; La fonction abrutit.
\end{extrait}

\begin{objectifs}
\begin{numerote}
	\item Concernant les ensembles :
		\begin{itemize}
			\item \lienobj{01}{Savoir décrire par extension et par compréhension un ensemble}
			\item \lienobj{02}{Connaître la définition et les propriétés du produit cartésien}
			\item \lienobj{03}{Connaître la définition et les propriétés de l'union et de l'intersection}
			\item \lienobj{04}{Savoir déterminer l'ensemble des parties d'un ensemble et son cardinal}
			\item \lienobj{05}{Savoir déterminer le complémentaire d'un ensemble et ses propriétés}
		\end{itemize}
		\item Concernant les ensembles :
			\begin{itemize}
				\item \lienobj{06}{Savoir les éléments de base d'une fonction (courbe représentative, domaine de définition)}
				\item \lienobj{07}{Connaître les différentes opérations sur les fonctions}
				\item \lienobj{08}{Savoir démontrer qu'une fonction est injective, n'est pas injective}
				\item \lienobj{09}{Savoir démontrer qu'une fonction est surjective, n'est pas surjective}
				\item \lienobj{10}{Savoir démontrer qu'une fonction est bijective, n'est pas bijective}
				\item \lienobj{11}{Savoir déterminer l'application réciproque d'une application bijective}
			\end{itemize}
\end{numerote}

\end{objectifs}

\section{Ensembles}

Nous avons, dans le chapitre 1, introduit la notion d'ensemble. Rappelons quelques définitions :

			 \subsection{Ensembles et éléments}

\begin{definition}
\label{objectif-01-5}
On appelle \textbf{ensemble} toute \textit{collection} d'objets, appelés \textbf{éléments} de cet ensemble. \\Pour signifier que l'élément $x$ appartient à un ensemble $E$, on note $x \in E$. Si $x$ n'appartient pas à $E$, on écrit $x \not \in E$.
\end{definition}

\begin{definition}
L'ensemble constitué d'aucun élément est appelé \textbf{ensemble vide}, et est noté $\vide$.

Un ensemble ayant un seul élément $x$ est appelé \textbf{singleton}, et est noté $\{x\}$.
\end{definition}


\begin{attention}
 On note $\vide$ et non $\{\vide\}$. $\{\vide\}$ représente l'ensemble contenant l'ensemble vide.
\end{attention}

Un ensemble peut être défini de deux manières :
\begin{itemize}
	\item par \textit{extension}, en listant tous ses éléments entre accolades. L'orde dans lequel les éléments sont listés n'a pas d'importance, et chaque élément figure une seule fois dans la liste.
	\item par \textit{compréhension} : si $P$ est une propriété portant sur les éléments d'un ensemble $E$, alors on note $\{ x\in E,\, P(x)\}$ l'ensemble des éléments $x$ de $E$ tels que $P(x)$ est vraie.
\end{itemize}

\begin{exemple}[Exemples importants]
	On note $\CC(\R, \R)$ l'ensemble des fonctions  à valeurs réelles qui sont continues, et $\DD(\R, \R)$ l'ensemble des fonctions à valeurs réelles qui sont dérivables. Par exemple, $\sin \in \DD(\R, \R)$, mais $\mathrm{abs}:x\mapsto |x| \not \in \DD(\R, \R)$.

	On note $\R^{\N}$ l'ensemble des suites réelles.
\end{exemple}



			 \subsection{Inclusion, sous-ensemble}

\begin{definition}
Soit $E$ un ensemble. On dit que $F$ est \textbf{inclus} dans $E$, et on note $F \subset E$, si tous les éléments de $F$ sont aussi des éléments de $E$. On dit alors que $F$ est une \textbf{partie} (ou un \textbf{sous-ensemble}) de $E$.

Si $E$ n'est pas inclus dans $F$, on note $E \not\subset F$. Si $E\subset F$ et $E\neq F$, on dit que l'inclusion est stricte et on note $E\subsetneq F$.
\end{definition}

\begin{proposition}
	Soient $E$, $F$ et $G$ trois ensembles.
	\begin{itemize}
		\item \petittitre{Reflexivité} : un ensemble est inclus dans lui-même : $E\subset E$.
		\item \petittitre{Transitivité} : Si $E\subset F$ et $F\subset G$ alors $E\subset G$.
	\end{itemize}
\end{proposition}

\begin{exemple}
$\N \subset \R$, $\{0,1\} \subset \{0,1,2\}$.
\end{exemple}

\begin{methode}[Inclusion]
 Pour montrer que $E$ est inclus dans $F$, on se donne $x$ de $E$, et on montre que $x$ est un élément de $F$.

 \quad\logoredaction On écrira \og{} Soit $x\in E$. Montrons que $x\in F$.\fg{}.
\end{methode}

\begin{methode}[Double inclusion]
 Soient $E$ et $F$ deux ensembles. On a :
 \[ E=F \quad \Longleftrightarrow (E \subset F \et F\subset E). \]
\end{methode}

\subsection{Produit cartésien}

\begin{definition}
\label{objectif-01-9}
Soient $E$ et $F$ deux ensembles. On appelle \textbf{produit cartésien} de $E$ et $F$, noté $E \times F$, l'ensemble formé des couples $(a,b)$ avec $a \in E$ et $b\in F$.
\end{definition}


\begin{exemple}
Si $E=\{a,b\}$ et $F=\{-1,1\}$ alors
$E\times F = \{ (a,-1), (a,1), (b,-1), (b,1)   \}$.
\end{exemple}


\begin{remarque}
\begin{itemize}[label=\textbullet]
\item Si $E=F$, on note en général $E\times E = E^2$.
\item On peut généraliser à un produit fini d'ensemble $\ds{E_1\times \hdots \times E_n = \prod_{k=1}^n E_k}$. Si $E_1=\hdots=E_n$, notera $E^n = \prod\limits_{k=1}^n E_k$.
\end{itemize}
\end{remarque}

\begin{exemple}
Les deux exemples classiques sont $\R^2=\{(x,y), x \in \R, y \in \R\}$ et \[\R^n = \{(x_1,\hdots,x_n),~ \ x_1\in \R,\hdots, x_n \in \R\}\]
Ainsi, par exemple, $(1,\,-4\sqrt{2}) \in \R^2$ et $(0,\,0,\,1)\in \R^3$.
\end{exemple}

On peut généraliser la notion de produit cartésien avec la notion de famille d'éléments :

\begin{definition}[Famille d'éléments d'un ensemble]
Soit $I$ un ensemble non vide (appelé ensemble d'indices), et $E$ un ensemble non vide. On appelle \textbf{famille d'éléments de $E$ indexée par $I$} la donnée, pour tout $i\in I$, d'un unique élément $x_i$ de $E$. On la note $(x_i)_{i\in I}$.
\end{definition}

\subsection{Union, intersection}


\begin{definition}
\label{objectif-01-6}
Soient $A$ et $B$ deux ensembles.
\begin{itemize}[label=\textbullet]
\item On appelle \textbf{intersection} de $A$ et de $B$, noté $A\cap B$, l'ensemble constitué des éléments qui sont à la fois dans $A$ et dans $B$.
\item On appelle \textbf{réunion} (ou \textbf{union}) de $A$ et de $B$, noté $A\cup B$, l'ensemble constitué des éléments qui sont dans $A$ ou dans $B$ (voire dans les deux).
\end{itemize}
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=5cm]{intersection.mps} & \includegraphics[width=5cm]{reunion.mps}
\end{tabular}
\end{center}
\end{definition}

\begin{remarque}
On a donc \[ x\in A\cup B \Longleftrightarrow x\in A \ou x\in B,\qeq x\in A\cap B \Longleftrightarrow x\in A \et x\in B. \]
\end{remarque}

\begin{exemple}
Si $A=\{ 1;2;4\}$ et $B=\{ 2;4;5 \}$ alors
\[A\cup B = \{1;2;4;5 \} \qeq A\cap B = \{2;4\}\]
\end{exemple}

\begin{definition}
	Deux parties $A$ et $B$ sont dits \textbf{disjoints} si et seulement si $A\cap B=\vide$. Dans ce cas, on dit que l'union $A\cup B$ est une \textbf{union disjointe}.
\end{definition}

\begin{propriete}
Soient $A, B$ et $C$ trois partie d'un ensemble $E$.
\begin{itemize}
\item \petittitre{Commutativité} : $A\cup B = B \cup A$ et $A\cap B=B\cap A$.
\item \petittitre{Associativité} : $A\cup (B\cup C)=(A\cup B)\cup C$ et $A\cap (B\cap C)=(A\cap B)\cap C$.
\item \petittitre{Distributivité} : $A\cup(B\cap C) = (A\cup B)\cap(A\cup C)$ et $A\cap(B\cup C) = (A\cap B)\cup(A\cap C)$.
\item \petittitre{Idempotence} : $A\cap A = A$ et $A\cup A = A$.
\item $\vide \cap A = \vide$ et $\vide \cup A = A$.
\item $E\cap A = A$ et $E\cup A = E$.
\item On a les inclusions $(A\cap B) \subset A \subset (A\cup B)$.
\end{itemize}
\end{propriete}

On peut, plus généralement, définir l'union et l'intersection d'une famille de partie de $E$.

\begin{definition}
Soit $(A_i)_{i\in I}$ une famille de parties de $E$ indexée par un ensemble non vide $I$. On définit
\begin{itemize}
\item $\ds{\bigcup_{i\in I} A_i} = \left \{ x\in E,\, \exists~i\in I,~x\in A_i \right \}$ l'union de la famille $(A_i)_{i\in I}$.
\item $\ds{\bigcap_{i\in I} A_i} = \left \{ x\in E,\, \forall~i\in I,~x\in A_i \right \}$ l'intersection de la famille $(A_i)_{i\in I}$.
\end{itemize}
Si $I=\interent{p n}$, avec $p$ et $n$ deux entiers naturels tels que $p\leq n$, on notera plutôt $\ds{\bigcup_{i=p}^n}$ au lieu de $\ds{\bigcup_{i\in I}}$, et $\ds{\bigcap_{i=p}^n}$ au lieu de $\ds{\bigcap_{i\in I}}$.
\end{definition}

\exercice{Montrer que \[ \bigcup_{k\in \N*} \interff{\frac{1}{k+1} \frac{1}{k}} = \interof{0 1} \]}

\solution[10]{On raisonne par double inclusion.
\begin{itemize}
	\item Soit $\ds{x\in  \bigcup_{k\in \N*} \left [ \dfrac{1}{k+1}, \dfrac{1}{k}\right]}$. Alors il existe un entier $k\in \N*$ tel que $\ds{x\in \left[ \frac{1}{k+1}, \frac{1}{k}\right]}$. Ainsi, $\ds{0<\frac{1}{k+1}\leq x \leq \frac{1}{k} \leq 1}$ et donc $x\in \interof{0 1}$.
	On a ainsi montré que \[\bigcup_{k\in \N*} \interff{\frac{1}{k+1} \frac{1}{k}} \subset \interof{0 1}.\]
	\item Réciproquement, soit $x\in \interof{0 1}$. Alors $\dfrac{1}{x}\geq 1$, et donc $\ell = \left [\dfrac{1}{x}\right] \in \N*$. Comme $\ell \leq \dfrac{1}{x} < \ell+1$, on en déduit que $\dfrac{1}{\ell+1} < x \leq \dfrac{1}{\ell}$. Ainsi, \[ x\in \interff{\frac{1}{\ell+1} \frac{1}{\ell}} \subset  \bigcup_{k\in \N*} \interff{\frac{1}{k+1} \frac{1}{k}}. \]
	Ainsi, \[\interof{0 1} \subset \bigcup_{k\in \N*} \interff{\frac{1}{k+1} \frac{1}{k}} .\]
\end{itemize}
On peut conclure quant à l'égalité des deux ensembles.
}

\begin{propriete}[Distributivité]
	Soit $B$ une partie de $E$, et $(A_i)_{i\in I}$ une famille de parties de $E$. Alors :
	\begin{itemize}
		\item \petittitre{Distributivité de $\cap$ sur $\cup$} : \[ \left( \bigcup_{i\in I} A_i\right) \cap B = \bigcup_{i\in I} (A_i\cap B). \]
		\item \petittitre{Distributivité de $\cup$ sur $\cap$} : \[ \left( \bigcap_{i\in I} A_i\right) \cup B = \bigcap_{i\in I} (A_i\cup B) \]
	\end{itemize}
\end{propriete}

\preuve[5]{
Montrons le premier point (le deuxième se démontrant de la même manière).
\begin{align*}
	x\in \left(\bigcup_{i\in I} A_i\right) \cap B &\Longleftrightarrow (\exists~i \in I,\, x\in A_i )\et (x\in B) \\&\Longleftrightarrow \exists~i\in I,\, (x\in A_i \et x\in B) \\
	&\Longleftrightarrow \exists~i\in I,\, x\in A_i \cap B \\
	&\Longleftrightarrow x\in \bigcup_{i\in I} (A_i\cap B)
\end{align*}
}

	\subsection{Ensemble des parties}

\begin{definition}
\label{objectif-01-8}
Soit $E$ un ensemble. On appelle \textbf{ensemble des parties} de $E$, et on note $\partie(E)$, l'ensemble formé des sous-ensembles de $E$.

Ainsi, $A\in \partie(E) \Longleftrightarrow A\subset E$.
\end{definition}

\begin{remarque}
	L'ensemble vide possède un seul sous-ensemble : lui-même.

	Pour tout ensemble $E$, $\vide \in \partie(E)$ et $E \in \partie(E)$.
\end{remarque}

\begin{exemple}
On a   $\ds{\partie(\{a,b\}) = \{ \vide, \{a\}, \{b\}, \{a,b\} \}}$.
\end{exemple}


\exercice{Déterminer $\partie(\{a,b,c\})$.}

\solution[4]{Si $E=\{a,b,c\}$, alors \[\partie(E)=\left \{ \vide, \{a\}, \{b\}, \{c\}, \{a,b\}, \{a,c\}, \{b,c\}, \{a,b,c\}  \right \}  \]
}

\begin{remarque}[En probabilités]
Lorsqu'on lance un dé à $6$ faces, le résultat est un élément de l'ensemble $\Omega=\interent{1 6}$, appelé univers associé à l'expérience aléatoire.

On appellera toute partie de $\Omega$ un événement. Par exemple, $\{1,2,3,4\}$ correspond à l'événement \og{} obtenir un chiffre inférieur ou égal à $4$\og{}. Ainsi, $\partie(\Omega)$ est l'ensemble des événements.
\end{remarque}

\exercice{Montrer que $\{x\} \in \partie(E) \Longleftrightarrow x \in E$.}

\solution[2]{En effet, $\{x\} \in \partie(E) \Longleftrightarrow \{x\}\subset E \Longleftrightarrow x \in E$. }

\begin{theoreme}
Soit $E$ un ensemble possédant $n$ éléments (avec $n\geq 1$). Alors $\partie(E)$ possède $2^n$ éléments.
\end{theoreme}

\ifprof
\begin{demonstration}
Pour construire un sous-ensemble de $E$, il faut prendre certains éléments de $E$ et pas d'autres. Si on note $x_1,\hdots, x_n$ les éléments de $E$, alors pour chaque élément $x_k$, on peut soit le prendre, soit ne pas le prendre ; il y a donc 2 possibilités pour chaque élément. Puisqu'il y a  $n$ éléments, et que le choix se fait de manière indépendante, il y a donc \[\underbrace{2\times \hdots \times 2}_{n \textrm{ fois}} =2^n \textrm{ sous-parties}\]
\end{demonstration}
\else
\lignes{5}
\fi

\afaire{Exercice \lienexo{04}.}

			 \subsection{Complémentaire}

\begin{definition}
\label{objectif-01-7}
Soit $E$ un ensemble et $A$ un sous-ensemble de $E$. On appelle \textbf{complémentaire} de $A$, et on note $\overline{A}$ ou $\complement_{\textsc{\tiny E}} A$, l'ensemble des éléments de $E$ qui ne sont pas dans $A$ : $\overline{A} = E \backslash A$.
\begin{center}
 ~\includegraphics[width=4cm]{complementaire.mps}~
\end{center}
\end{definition}

\begin{remarque}
	Ainsi, pour tout $x$ de $E$, $x\in \overline{A}$ si et seulement si $x\not \in A$.
\end{remarque}

\begin{exemple}
Si $E=\{ 1;2;3;4;5\}$ et $A=\{1;2;4\}$ alors $\overline{A}=\{3;5\}$.
\end{exemple}


\begin{theoreme}[Lois de de Morgan]
 Soit $E$ un ensemble, et soient $A,B$ deux sous-ensembles de $E$.
\begin{itemize}[label=\textbullet]
	 \item $\overline{A\cup B} = \overline{A} \cap \overline{B}$
	 \item $\overline{A\cap B} = \overline{A} \cup \overline{B}$
\end{itemize}
\end{theoreme}

\ifprof
\begin{demonstration}On raisonne par double inclusion.
\begin{itemize}[label=\textbullet]
	 \item  Soit $x \in \overline{A\cup B}$. Cela veut dire qu'il n'est pas dans $A\cup B$. Donc il n'est ni dans $A$, ni dans $B$ : il est donc dans $\overline{A}\cap \overline{B}$.
	 \item Soit $ \in \overline{A}\cap \overline{B}$. Il n'est donc pas dans $A$, et il n'est pas dans $B$ : il n'est donc pas dans $A \cup B$. Ainsi, $x \in \overline{A\cup B}$.
\end{itemize}
L'autre égalité se montre de la même manière.
\end{demonstration}
\else
\lignes{5}
\fi

\begin{propriete}
Soient $A$ et $B$ deux parties de $E$.
\[ \overline{\vide} = E,\quad\quad \overline{E}=\vide,\quad\quad \overline{\overline{A}}=A,\quad\quad A\subset B \Longleftrightarrow \overline{B}\subset \overline{A}. \]
\end{propriete}

\begin{definition}[Différence de deux ensembles]
	Soient $A$ et $B$ deux parties de $E$. On appelle \textbf{différence} de $A$ par $B$, ou encore \og{}$A$ privé de $B$\fg{}, l'ensemble \[A\setminus B = A\cap \overline{B} = \left \{ x \in E,\, x\in A \et x\not \in B \right \}. \]
\end{definition}

\begin{attention}
 $A\setminus B \neq B \setminus A$ (on a même mieux : $A\setminus B$ et $B\setminus A$ sont disjoints).
 \end{attention}

\afaire{Exercice \lienexo{03}.}


\section{Applications}

			 \subsection{Définition}

\begin{definition}
 Soient $E$ et $F$ deux ensembles. Une \textbf{application} (ou \textbf{fonction}) $f$ de $E$ vers $F$ est une transformation qui, à chaque élément $x$ de $E$, associe un \underline{unique} élément $y$ de $F$. L'élément $y$ de $F$ est alors noté $f(x)$ et est appelé \textbf{image} de $x$ par $f$. $x$ est alors un \textbf{antécédent} de $y=f(x)$ par $f$.\\
L'ensemble $E$ est appelé \textbf{ensemble (ou domaine) de définition}.
\end{definition}

\begin{notation}
On note $f:E\rightarrow F$ pour signifier que $f$ est une application de $E$ vers $F$.\\L'ensemble des fonctions de $E$ vers $F$ est noté $\mathcal{F}(E,F)$ ou encore $F^E$.
\end{notation}

\begin{exemple}
$\left \{ \begin{array}{ccc} \R &\rightarrow& \R\\x &\mapsto& \eu{-x^2} \end{array}\right.$ et
$\left \{ \begin{array}{ccc} \R^2 &\rightarrow& \R\\(x,y) &\mapsto& \frac{x+y}{1+x^2+y^2} \end{array}\right.$ sont des applications.
\end{exemple}


\begin{remarque}
Il y a, en réalité, une différence entre application et fonction. On peut parler généralement de la fonction logarithme népérien, sans indiquer qu'elle n'a pas de sens sur $\R^-$. En revanche, on parlera de l'application logarithme népérien définie sur $\R^+_*$. Dans la pratique, on utilisera quasi-systématiquement le mot fonction.
\end{remarque}


\begin{definition}
L'ensemble des points du plan cartésien de coordonnées $(x, f(x))$ où $x$ est un élément du domaine de définition de $f$ est appelé \textbf{courbe représentative} de la fonction $f$.
\begin{center}
 \includegraphics[width=7cm]{courbe_fonction.mps}
\end{center}
\end{definition}

\begin{remarque}
Si l'ensemble de définition d'une fonction n'est pas indiqué, il est convenu que cet ensemble de définition est le \textbf{plus grand ensemble} sur lequel $f(x)$ existe.
\end{remarque}


\exercice{Déterminer le domaine de définition de la fonction $f:x \mapsto \sqrt{x^2-4}$.}

\solution[6]{La fonction $f$ est définie si et seulement si $x^2-4\geq 0$. Puisque $x^2-4=(x-2)(x+2)$, et après tableau de signe rapide, on obtient $\DD_f=]-\infty;-2]\cup[2;+\infty[$.
}

			 \subsection{Opérations sur les fonctions}

\begin{definition}[\'Egalité de fonctions]
	Deux applications $f$ et $g$ sont égales si et seulement si elles ont le même espace de départ, le même ensemble d'arrivée, et si, pour tout $x$ de l'espace de départ, $f(x)=g(x)$.
\end{definition}

\begin{attention}
	Il faut que les fonctions aient le même espace de départ. Par exemple, $f:\R\to \R$ définie par $f(x)=x^2$ et $g:\R^+\to \R$ définie par $g(x)=x^2$ ne sont pas égales.
\end{attention}

\begin{definition}
Soient $f$ et $g$ deux fonctions définies sur le \underline{même ensemble} $E$. Soit $\lambda$ un réel.
\begin{itemize}[label=\textbullet]
 \item On appelle $f+g$ la fonction définie sur $E$ par $(f+g)(x)=f(x)+g(x)$.
 \item On appelle $f\times g$ la fonction définie sur $E$ par $(f\times g)(x)=f(x)\times g(x)$.
 \item On appelle $\lambda f$ la fonction définie sur $E$ par $(\lambda f)(x)=\lambda f(x)$.
 \item On appelle $f+\lambda$ la fonction définie sur $E$ par $(f+\lambda)(x)=f(x)+\lambda$.
	\item Si $g$  \textbf{ne s'annule pas} sur $E$, on appelle $\frac{f}{g}$ la fonction définie sur $E$ par $\left(\frac{f}{g}\right )(x)=\frac{f(x)}{g(x)}$.
\end{itemize}
\end{definition}


\begin{exemple}
Soient $f$ est la fonction définie sur $\R$ par $f(x)=x^2$ et $g$ la fonction définie sur $\R$ par $g(x)=\E^x+1$. Déterminer $f+g$ et $\frac{f}{g}$.
\end{exemple}

\solution[3]{
Par définition, $f+g$ est la fonction définie sur $\R$ par $(f+g)(x)=x^2+\E^x+1$ et $\frac{f}{g}$ est la fonction définie sur $\R$ par $\frac{f}{g}(x)=\frac{x^2}{\E^x+1}$.
}

\begin{definition}
Soit $f$ une fonction définie sur $E$ et prenant ses valeurs dans $F$.\\
 Soit $g$ une fonction définie sur $F$ et à valeur dans $G$.\\
 La fonction qui, à tout réel $x$ de $E$, fait correspondre le réel $g(f(x))$ est appelée \textbf{fonction composée} de $f$ suivie de $g$. On a ainsi
	\begin{eqnarray*}
				 E \rightarrow& F &\rightarrow G \\
				 x \mapsto& f(x) &\mapsto g(f(x))
				 \end{eqnarray*}
Cette fonction est notée $g\circ f$.
\end{definition}


\begin{exemple}
Soit $f: \left \{ \begin{array}{ccc} \R &\rightarrow& \R\\x &\mapsto& x^2 \end{array}\right.$ et $g: \left \{ \begin{array}{ccc} \R &\rightarrow& \R\\x &\mapsto& x-1 \end{array}\right.$. Déterminer $f\circ g$ et $g\circ f$.
\end{exemple}

\solution[3]{On a, pour tout réel $x$, \[g\circ f(x) = x^2-1 \textrm{  et  } f\circ g(x) = (x-1)^2\]
}

\begin{remarque}
Dans le résultat précédent, on remarque que $g\circ f \neq f\circ g$. On dit que la composée n'est pas \textbf{commutative}.
\end{remarque}

\begin{methode}
Pour déterminer la composée de deux fonctions, on étudiera d'abord les domaines de définition pour déterminer le domaine de définition de la fonction composée.
\end{methode}


\begin{exemple}
Soit $f:x\mapsto x^2-1$ et $g:x\mapsto \sqrt{x}$. Déterminer $g\circ f$.
\end{exemple}


\solution[6]{$g\circ f(x)$ n'est définie que si $f(x)\geq 0$ (car $g$ est la fonction racine, définie sur $\R^+$). Or \[f(x)\geq 0 \Leftrightarrow x\in ]-\infty;-1]\cup [1;+\infty[\] Ainsi, $g\circ f$ est définie sur $]-\infty;-1]\cup [1;+\infty[$ par $\ds{g\circ f(x)=\sqrt{x^2-1}}$
}

\begin{proposition}[associativité]
Si $f:E\rightarrow F, ~g:F\rightarrow G,~ h:G\rightarrow H$ sont trois fonctions, alors
\[h\circ (g\circ f) = (h \circ g) \circ f = h \circ g \circ f\]
\end{proposition}

			 \subsection{Injection, surjection}

					 \subsubsection{Injection}

\begin{definition}
\label{objectif-01-10}
On dit qu'une fonction $f:E\rightarrow F$ est \textbf{injective} si $f$ ne prend jamais deux fois la même valeur : \[\forall (x,x') \in E^2, x \neq x' \Rightarrow f(x) \neq f(x')\]
\end{definition}


\begin{remarque}
On dispose également de la formulation équivalente suivante (il s'agit de la contraposée de la précédente) :
\[\forall (x, x') \in E^2, f(x)=f(x') \Rightarrow x=x'\]
\end{remarque}


\begin{methode}
Pour montrer qu'une fonction est injective, on prend deux éléments $x$ et $x'$ de $E$ vérifiant $f(x)=f(x')$. On montre alors que nécessairement $x=x'$.
\end{methode}


\begin{exemple}
Montrer que la fonction $f$ définie sur $\R$ par $f(x)=\eu{x+1}$ est injective.
\end{exemple}

\solution[3]{
En effet, soient $x, x' \in \R^2$ tels que $f(x)=f(x')$. On a donc $\eu{x+1}=\eu{x'+1}$, c'est-à-dire $x+1=x'+1$ en composant par la fonction logarithme népérien. On a donc bien $x=x'$.
}

\begin{remarque}
$f:E\rightarrow F$ n'est pas injective si et seulement si \[\exists~(x, x') \in E^2, f(x)=f(x') \textrm{ et } x\neq x'\]
\end{remarque}

\begin{methode}
Pour montrer qu'une fonction n'est pas injective, on cherche un contre-exemple.
\end{methode}


\begin{exemple}
Montrer que la fonction $f$ définie sur $\R$ par $f(x)=x^2$ n'est pas injective.
\end{exemple}


\solution[2]{En effet, $1\neq -1$ et pourtant $1^2=(-1)^2=1$.}


\begin{theoreme}
Si $f:E\rightarrow F$ et $g:F\rightarrow G$ sont deux applications injectives, alors $g\circ f:E\rightarrow G$ est aussi injective.
\end{theoreme}

\ifprof
\begin{demonstration}
Soient $x$ et $x'$ deux éléments de $E$ tels que $g\circ f (x)=g \circ f(x')$. Alors, puisque $g(f(x))=g(f(x'))$, et comme $g$ est injective, on a nécessairement $f(x)=f(x')$. Or, $f$ est aussi injective: on a donc $x=x'$. $g\circ f$ est bien injective.
\end{demonstration}
\else
\lignes{4}
\fi

\begin{proposition}
	Toute fonction $f:I\to \R$ (avec $I$ intervalle non vide de $\R$) strictement monotone sur $I$ est injective.
\end{proposition}

\preuve[5]{Soient $x$ et $y$ dans $I$ tels que $x\neq y$. Alors on a $x<y$ ou $x>y$, mais puisque $f$ est strictement monotone, on en déduit que $f(x)<f(y)$ ou $f(x)>f(y)$. Dans tous les cas $f(x)\neq f(y)$ et $f$ est injective.
}

			 \subsubsection{Surjection}

\begin{definition}
\label{objectif-01-11}
On dit qu'une fonction $f:E \rightarrow F$ est \textbf{surjective} si tout élément de $F$ possède au moins un antécédent par $f$ dans $E$ :
\[\forall~y \in F, \exists~x \in E, y=f(x)\]
\end{definition}

\begin{methode}
Pour prouver qu'une fonction $f$ est surjective, il suffit donc de trouver une solution $x$ à l'équation $f(x)=y$ pour tout élément $y$ de $F$.
\end{methode}

\begin{exemple}
Montrer que la fonction $f:\R \rightarrow \R$, définie pour tout $x$ par $f(x)=2x-1$ est surjective.
\end{exemple}


\solution[4]{En effet, pour tout $y\in \R$, $y=f(x) \Leftrightarrow x=\frac{y+1}{2}$. Il existe donc au moins un antécédent à $y$ par la fonction $f$.}

\begin{methode}
Pour montrer qu'une fonction n'est pas surjective, on exhibe une valeur qui ne peut être atteinte par la fonction.
\end{methode}


\begin{exemple}
Montrer que la fonction $f:\R \rightarrow \R$, définie pour tout réel $x$ par $f(x)=\E^x$ n'est pas surjective.
\end{exemple}


\solution[3]{Puisque, pour tout réel $x$, $\E^x>0$, la valeur $0$ n'est jamais atteinte par $f$. Ainsi, $f$ n'est pas surjective.
}

\begin{remarque}
On constate cependant que la fonction précédente est surjective sur $\R^*_+$.
\end{remarque}


\begin{theoreme}
Si $f:E\rightarrow F$ et $g:F\rightarrow G$ sont deux applications surjectives, alors $g\circ f:E\rightarrow G$ est aussi surjective.
\end{theoreme}


\ifprof
\begin{demonstration}
 Soit $z$ un élément de $G$. Puisque $g$ est surjective, il existe un élément $y \in F$ tel que $g(y)=z$. De plus, $f$ est surjective, donc il existe un élément $x \in E$ tel que $f(x)=y$. Mais alors
\[g(f(x))=g(y)=z\]
Donc $x$ est un antécédent de $z$ par la fonction $g\circ f$.
\end{demonstration}
\else
\lignes{4}
\fi

			 \subsubsection{Bijection}

\begin{definition}
\label{objectif-01-12}
On dit qu'une fonction $f:E\rightarrow F$ est \textbf{bijective} si elle est à la fois injective et surjective. Ainsi, $f$ est bijective si, et seulement si, chaque élément de $F$ possède un \textbf{unique antécédent} par $f$ dans $E$ :
\[\forall~y\in F, \exists!~x\in E, y=f(x)\]
\end{definition}


\begin{methode}
Pour démontrer qu'une fonction $f:E\rightarrow F$ est bijective, il faut montrer que, pour tout $y \in F$, l'équation $f(x)=y$ possède une \textbf{unique} solution dans $E$.
\end{methode}


\begin{exemple}
Montrer que la fonction $f$ définie sur $\R$ par $f(x)=x-4$ est bijective.
\end{exemple}


\solution[3]{En effet, pour tout $y\in \R$, on a
\[f(x)=y \Leftrightarrow x-4=y \Leftrightarrow x=y+4\]
Il existe donc bien un unique antécédent à $y$ par $f$. }

\begin{definition}
	On dit que deux ensembles $E$ et $F$ sont \textbf{en bijection} s'il existe une bijection de $E$ dans $F$ ou une bijection de $F$ dans $E$.
\end{definition}

\begin{exemple}
Par exemple, $\R^+$ et $\R^-$ sont en bijection, par exemple par l'application $x\mapsto -x$.
\end{exemple}

	\subsubsection{Application réciproque}

\begin{exemple}
L'application $\id_E:E \rightarrow E$ définie pour tout $x$ de $E$ par $\id_E(x)=x$ est une bijection de $E$ dans $E$.
\end{exemple}

\begin{proposition}
La fonction $\id_E$ est le neutre pour la composition :
\[ \forall~f\in \mathcal{F}(E,F),~\forall~g\in \mathcal{F}(F,E),\quad f\circ \id_E=f \qeq \id_E\circ g = g \]
\end{proposition}

\ifprof
\begin{demonstration}
En effet, pour tout $x\in E$ :
\[ f\circ \id_E(x) = f(\id_E(x)) = f(x) \text{ car } \id_E(x)=x\]
et pour tout $x\in F$ :
\[ \id_E\circ g(x) = \id_E(g(x)) = g(x) \text{ car }\id_E(u)=u\text{ avec ici }u=g(x)\]
\end{demonstration}
\else
\lignes{5}
\fi

\begin{theoreme}
Soit $f:E\rightarrow F$ une fonction. $f$ est bijective si, et seulement si, il existe une unique fonction $g:F\rightarrow E$, telle que $f\circ g = \id_F$ et $g\circ f=\id_E$.\\La fonction $g$ est appelée \textbf{fonction réciproque} de $f$, et est notée $g=f^{-1}$.
\end{theoreme}



\begin{exemple}
Soit $f:\R\rightarrow \R$ définie par $f:x\mapsto 2x-1$. Soit $g:\R \rightarrow \R$ définie par $g:x\mapsto \frac{x+1}{2}$. Montrer que $f$ et $g$ sont bijectives, et réciproques l'une de l'autre.
\end{exemple}


\solution[4]{On constate que, pour tout $x \in \R$,
\[f\circ g (x)=2\frac{x+1}{2}-1=x \textrm{ et } g\circ f(x)=\frac{(2x-1)+1}{2}=x\]
Ainsi, $f$ est bijective, et sa bijection réciproque est la fonction $g$.
}

\begin{methode}
Pour déterminer la fonction réciproque d'une fonction $f$, on résout l'équation $f(x)=y$ d'inconnue $x$. On obtiendra $x=g(y)$ et $g$ représente alors la fonction réciproque.
\end{methode}


\begin{exemple}
Soit $f: \R \rightarrow \R^*_+$ la fonction définie pour tout réel $x$ par $f(x)=\eu{x+1}$. Déterminer $f^{-1}$.
\end{exemple}



\solution[4]{Soit $x \in \R$ et $y\in \R^*_+$. Alors
\[f(x)=y \Leftrightarrow \eu{x+1}=y \Leftrightarrow x=\ln(y) -1\]
qui a un sens car $y>0$. Ainsi $x=g(y)$ avec $g:\R^*_+ \rightarrow \R$ définie pour tout réel $x>0$ par $g(x)=\ln(x)-1$. Donc $f^{-1}=g$.}


\begin{methode}
Lorsqu'on donne $f$ et $g$ et qu'on demande de montrer que $g=f^{-1}$, il suffit de montrer que $f\circ g = \id$ et $g\circ f = \id$.
\end{methode}


\begin{exemple}
Soit $f$ la fonction définie sur $\R$ par $f(x)= 4x-2$. Montrer que $f^{-1}$ est la fonction définie sur $\R$ par $f^{-1}(y)=\frac{y+2}{4}$.
\end{exemple}

\solution[4]{Notons $g:y\mapsto \frac{y+2}{4}$. Alors, pour tous réels $x$ et $y$, on a
\[f\circ g(y)= f\left(\frac{y+2}{4}\right) = 4\left(\frac{y+2}{4}\right)-2=y\]
\[g\circ f(x)=g(4x-2) = \frac{(4x-2)+2}{4}=x\]
Ainsi, on a bien $f\circ g = Id_{\R}$ et $g\circ f=Id_{\R}$ : $g=f^{-1}$.}

\begin{theoreme}
Soient $f:E\rightarrow F$ et $g: F \rightarrow G$ deux fonctions bijectives. Alors $g\circ f : E \rightarrow G$ est bijective, et on a \[(g \circ f)^{-1}=f^{-1} \circ g^{-1}\]
\end{theoreme}

\ifprof
\begin{demonstration}
En effet, \[(f^{-1} \circ g^{-1})\circ(g\circ f) = f^{-1} \circ g^{-1}\circ g \circ f = f^{-1} \circ \id_F \circ f=f^{-1} \circ f = \id_E\]
et
\[(g\circ f)\circ (f^{-1}\circ g^{-1}) = g \circ f \circ f^{-1} \circ g^{-1}=g\circ \id_F \circ g^{-1}=g\circ g^{-1} = \id_G\]
D'après le théorème précédent, $g\circ f$ est bijective, d'application réciproque $f^{-1}\circ g^{-1}$.
\end{demonstration}
\else
\lignes{5}
\fi

\afaire{Exercice \lienexo{07}.}

% 			\subsection{Image directe, image réciproque}
%
% \begin{definition}
% Soit $f:E\rightarrow F$ une fonction.
% \begin{itemize}[label=\textbullet]
% 	 \item \label{objectif-01-13} Si $A \subset E$, on appelle \textbf{image directe} de $A$ par $f$, et on note $f(A)$, l'ensemble composé par les images par $f$ des éléments de $A$ :
% 	 \[f(A)=\left \{ y \in F, ~\exists x \in A, f(x)=y \right \}\]
% 	 \item \label{objectif-01-14} Si $B \subset F$, on appelle \textbf{image réciproque} de $B$ par $f$, et on note $f^{-1}(B)$, l'ensemble composé par les antécédents par $f$ des éléments de $B$ :
% 	 \[f^{-1}(B)= \left \{ x \in E,~f(x) \in B \right \}\]
% \end{itemize}
% \end{definition}
%
%
% \begin{exemple}
% Soit $f:x\mapsto x+2$. Déterminer $f\left([0;1]\right)$ et $f^{-1}\left([0;1]\right)$.
% \end{exemple}
%
%
% \solution[2]{On a :
% \[f\left( [0;1] \right) = [2;3] \qeq f^{-1}(\left([0;1] \right) = [-2;-1]\]
% }
%
% \begin{attention}
% On ne confondra pas l'image réciproque d'un ensemble $f^{-1}(B)$ et l'application réciproque d'une application $f^{-1}$ lorsque celle-ci existe.
% \end{attention}
%
% \begin{proposition}
% Soit $f:E\rightarrow F$ une application. $f$ est surjective si et seulement si $f(E)=F$.
% \end{proposition}
%
% \ifprof
% \begin{demonstration}
% Si $f$ est surjective, tout élément de $F$ admet au moins un antécédent dans $E$. Donc $f(E) = F$.\\
% Réciproquement, si $f(E)=F$, alors pour tout élément $y\in F$, il existe un $x\in E$ tel que $f(x)=y$ : par définition, $f$ est surjective.
% \end{demonstration}
% \else
% \lignes{3}
% \fi
%
% \afaire{Exercice \lienexo{8}.}
%
%  \subsection{Restriction}
%
% \begin{definition}
% \label{objectif-01-15}
% Soit $f:E\rightarrow F$ une fonction, et $G \subset E$. On appelle \textbf{restriction} de $f$ à $G$, et on note $f\restreinta_{G}$ l'application définie sur $G$ par \[ \forall~x\in G,~f\restreinta_{G}(x)=f(x) \]
% \end{definition}
%
% \begin{exemple}
% Soit $f:\R \rightarrow \R$ définie pour tout réel $x$ par $f(x)=x^2$. $f$ n'est pas injective. En revanche, $f\restreinta_{\R[+]}$ est injective.
% \end{exemple}
