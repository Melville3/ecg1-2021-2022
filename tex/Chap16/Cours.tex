\chapter{Systèmes linéaires}
%!TeX root=../../encours.nouveau.tex

\objectifintro{L'objectif de ce chapitre est d'introduire rigoureusement la notion de système linéaire, déjà vue lors des années antérieures. On y voit, entre autre, la méthode de résolution du pivot de Gauss.}

\begin{extrait}{Bernard Werber (1961 -- ). \emph{L'empire des anges}}
Pour comprendre un système, il faut... s'en extraire.
\end{extrait}

\begin{objectifs}
\begin{numerote}
                                \item \hyperref[objectif-09-1]{Savoir résoudre un système simple par substitution}\dotfill $\Box$
                                \item \hyperref[objectif-09-2]{Savoir appliquer la méthode du pivot de Gauss-Jordan pour transformer un système en un système triangulaire}\dotfill $\Box$
                                \item \hyperref[objectif-09-3]{Résoudre un système ayant une infinité de solutions avec un (ou plusieurs) paramètres}\dotfill $\Box$
                                \item \hyperref[objectif-09-4]{Savoir déterminer le rang d'un système}\dotfill $\Box$
                                \item \hyperref[objectif-09-5]{Savoir résoudre un système ayant un paramètre}\dotfill $\Box$
\end{numerote}

\end{objectifs}

%% Pour l'espacement des systemes
\syssignspace{6pt}\syseqspace{6pt}
%%%

\section{Définitions et propriétés}

	    \subsection{Définitions}

\begin{definition}
Soient $n$ et $p$ deux nombres entiers non nuls. On appelle \textbf{système d'équations linéaires} de $n$ équations à $p$ inconnus (ou système $n$ fois $p$, $n\times p$) un système de la forme

\[(S)\quad \left \{
\begin{array}{cccccccccr}
a_{11}x_1 &+& a_{12}x_2&+&\hdots&+&a_{1p}x_p &=& b_1&(L_1)\\
a_{21}x_1 &+& a_{22}x_2&+&\hdots&+&a_{2p}x_p &=& b_2&(L_2)\\
&&&&\vdots&&&&& \\
a_{n1}x_1 &+& a_{n2}x_2&+&\hdots&+&a_{np}x_p &=& b_n&(L_n)\\
\end{array}
\right.
\]

où les $(a_{ij})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ et les $(b_i)_{1\leq i \leq n}$ sont des nombres réels, et $x_1, x_2,\hdots, x_n$ sont des inconnues.

Le nombres $a_{ij}$ est le \textbf{coefficient} de la $j^{\textrm{ème}}$ inconnue $x_j$ dans la $i^{\textrm{ème}}$ équations $(L_i)$.
\end{definition}

\begin{remarque}
Si $n=p$ on dit que le système $(S)$ est \textbf{carré d'ordre} $n$.
\end{remarque}

\begin{exemple}
Le système \[ (S_1) \quad\systeme{2x_1-3x_2=1@(L_1),3x_1+x_2=-2@(L_2)} \] est un système linéaire de $2$ équations à $2$ inconnues. C'est ainsi un système carré d'ordre $2$.

Le système \[ (S_2)\quad\systeme{x_1-2x_2+2x_3=4@(L_1),2x_1+4x_2-2x_3=3@(L_2)} \] est un système linéaire de $2$ équations à $3$ inconnues.
\end{exemple}

   \subsection{Propriétés}

\begin{definition}
Soit \[(S)\quad \left \{
\begin{array}{cccccccccr}
a_{11}x_1 &+& a_{12}x_2&+&\hdots&+&a_{1p}x_p &=& b_1&(L_1)\\
a_{21}x_1 &+& a_{22}x_2&+&\hdots&+&a_{2p}x_p &=& b_2&(L_2)\\
&&&&\vdots&&&&& \\
a_{n1}x_1 &+& a_{n2}x_2&+&\hdots&+&a_{np}x_p &=& b_n&(L_n)\\
\end{array}
\right.
\]

\begin{itemize}
   \item \textbf{Résoudre} le système $(S)$, c'est trouver toutes les $p$-listes $(x_1,\hdots,x_p)$ de réels vérifiant les $n$ équations $(L_1,\hdots, L_n)$.
   \item On dit qu'un système est \textbf{incompatible} s'il n'admet pas de solution.
\end{itemize}
\end{definition}

\begin{remarque}
Dans le cas où $p<n$, il y a plus d'équations que d'inconnues. Soit certaines équations sont redondantes (et on peut donc les supprimer), soit le système est incompatible.\\
Dans la suite, on ne s'intéressera qu'au cas $n\leq p$.
\end{remarque}


\begin{definition}
	Deux systèmes $(S)$ et $(S')$ sont dits \textbf{équivalents} s'ils ont les mêmes solutions. On notera $(S) \sim (S')$ pour signifier que $(S)$ et $(S')$ sont équivalents, ou bien $(S)\Leftrightarrow (S')$.
\end{definition}

\begin{exemple}
Les systèmes \systeme{x+y=1,3x-2y=2}	 et \systeme{2x+2y=2,3x-2y=2} sont équivalents.
\end{exemple}

\begin{definition}
Soit \[(S)\quad \left \{
\begin{array}{cccccccccr}
a_{11}x_1 &+& a_{12}x_2&+&\hdots&+&a_{1p}x_p &=& b_1&(L_1)\\
a_{21}x_1 &+& a_{22}x_2&+&\hdots&+&a_{2p}x_p &=& b_2&(L_2)\\
&&&&\vdots&&&&& \\
a_{n1}x_1 &+& a_{n2}x_2&+&\hdots&+&a_{np}x_p &=& b_n&(L_n)\\
\end{array}
\right.
\]

Le système $(S)$ est dit \textbf{homogène} (ou \textbf{sans second membre}) si $b_1=\hdots=b_n=0$. Dans ce cas, la $p$-liste $(0,\hdots,0)$ est toujours solution de $(S)$.

On appelle \textbf{système homogène associé} à $(S)$ le système obtenu à partir de $(S)$ en remplaçant tous les nombres $b_i$ par $0$.
\end{definition}

\begin{exemple}
Le système homogène associé à $(S)~\systeme{x+y=1,2x-3y=2}$ est $(S_0)~\systeme{x+y=0,2x-3y=0}$.
\end{exemple}


   \subsection{Résolution par substitution}

\begin{methode}
\label{objectif-09-1}
La méthode par résolution consiste à écrire une des inconnues (par exemple $x_1$) en fonction des autres ($x_2, x_3,\hdots$), puis à remplacer cette inconnue $x_1$ dans toutes les autres équations en fonction de $x_2, x_3,\hdots$. Cette méthode est efficace lorsqu'il y a peu d'inconnues ou d'équations.
\end{methode}

\begin{exemple}
Résoudre le système suivant : \[ (S_1)\quad \systeme{2x_1-3x_2=1@(L_1),-3x_1+x_2=2@(L_2)} \]
\end{exemple}

\solution[6]{En utilisant $(L_2)$, on peut exprimer $x_2$ en fonction de $x_1$ : $x_2=3x_1+2$. On remplace alors cette égalité dans $(L_1)$ pour en déduire la valeur de $x_1$. On obtiendra enfin la valeur de $x_2$ :

\begin{align*}
(S_1) \quad&\sim \systeme{2x_1-3x_2=1,x_2=3x_1+2} \\
 &\sim \left \{ \begin{array}{ccccc}
    2x_1 &-& 3(3x_1+2) & = & 1  \\
    &  & x_2 &=& 3x_1+2
\end{array}
\right. \\
 &\sim \systeme{-7x_1=7,x_2=3x_1+2}\\
 &\sim \systeme*{x_1=-1, x_2=-1}
\end{align*}
Ainsi, le système admet une unique solution : $\{(-1;-1)\}$.}

   \subsection{Systèmes triangulaires}

Les système triangulaires sont les plus simples  des systèmes, puisqu'ils se résolvent très facilement.

\begin{definition}
On dit qu'un système $(S)$ $n\times p$ est \textbf{triangulaire} si
\[\forall i\in \interent{1 n},\,\forall j \in \interent{1 p}, \, i>j \implies a_{i,j}=0\]
Ainsi, si $n<p$, le système est de la forme
\[(S) \left \{
\begin{array}{cccccccccccccr}
 a_{11}x_1 &+& a_{12}x_2 &+& a_{13}x_3&+& \hdots & + &\hdots &+& a_{1p}x_p &=& b_1 & (L_1)\\
 && a_{22}x_2 &+& a_{23}x_3 &+& \hdots &+ & \hdots & +&a_{2p}x_p &=& b_2& (L_2) \\
 &&&&&&&\vdots&&&&&\\
 &&&&&&a_{nn}x_n&+&\hdots&+& a_{np}x_p &=& b_n & (L_n)
\end{array}
\right.\]
Si $n=p$, on a alors le système suivant :
\[(S) \left \{
\begin{array}{cccccccccccccr}
 a_{11}x_1 &+& a_{12}x_2 &+& a_{13}x_3&+& \hdots & + &\hdots &+& a_{1n}x_n &=& b_1 & (L_1)\\
 && a_{22}x_2 &+& a_{23}x_3 &+& \hdots &+ & \hdots & +&a_{2n}x_n &=& b_n& (L_2) \\
 &&&&&&&&&&\vdots&&\\
 &&&&&&&&&& a_{nn}x_n &=& b_n & (L_n)
\end{array}
\right.\]
Les coefficients diagonaux $a_{11},\hdots,a_{nn}$ sont appelés les \textbf{pivots} du système.
\end{definition}

\begin{remarque}
Lorsque $n=p$ et que tous les pivots $a_{ii}$ (pour $i\in \{1;\hdots;n\}$) sont non nuls, le système se résout par substitutions successives, de $(L_n)$ à $(L_1)$. Il y a alors une \textbf{unique $n$-liste solution}.
\end{remarque}

\begin{methode}
\label{objectif-09-3}
Dans le cas $n<p$, il y a une (ou plusieurs) inconnue(s) en trop. On choisit alors ces inconnues comme inconnues auxiliaire, et on résout comme pour la cas $n=p$.
\end{methode}

\begin{exemple}
Résoudre le système suivant : \[ (S)\quad \systeme{2x-y+3z=-1,2y-4z=2} \]
\end{exemple}

\solution[6]{Il y a $3$ inconnues, pour deux équations. Exprimons $x$ et $y$ en fonction de $z$ :
\begin{align*}
(S)\quad &\sim \systeme{2x-y=-1-3z,2y=2+4z} \quad z\in \R\\
&\sim \systeme*{x=-\frac12z, y=1+2z}	 \quad z \in \R
\end{align*}
L'ensemble des solutions est donc $\displaystyle{\mathcal{S}=\left\{\left(-\frac{1}{2}z;1+2z;z\right), ~z\in \R\right \}}$.
}

\begin{remarque}
Bien évidemment, si on choisit une autre inconnue auxiliaire, le résultat ne sera pas sous la même forme, mais désignera bien le même ensemble de solutions.
\end{remarque}

\section{Pivot de Gauss-Jordan}

   \subsection{Exemple}

On souhaite résoudre le système suivant
\[ (S)\quad \systeme{x+2y+2z=1@L_1,2x-2y+2z=2@L_2,-x+y+3z=1@L_3}\]

Pour faire cela, on va utiliser différentes opérations dites élémentaires, qui transforment le système $(S)$ en un système équivalent, mais triangulaire cette fois-ci. Il ne restera alors plus qu'à résoudre le système triangulaire associé.

Ici :
\begin{align*}
(S)\quad &\sim \systeme{x+2y+2z=1@L_1\text{ ligne pivot}, -6y-2z=0@L_2\leftarrow L_2-2L_1, 3y+5z=2@L_3\leftarrow L_3+L_1}\\
&\sim 	\systeme{x+2y+2z=1@L_1,-6y-2z=0@L_2\text{ ligne pivot},8z=4@L_3\leftarrow 2L_3+L_2}
\end{align*}

On obtient ainsi un système triangulaire, qu'on résout :

\[ (S) \quad \sim \systeme*{x=\frac13, y=-\frac16, z=\frac12}\]

Ainsi, la solution de $(S)$ est $\mathcal{S}= \left \{ \left( \frac{1}{3}; -\frac{1}{6}; \frac{1}{2} \right) \right \}$.

   \subsection{Opérations élémentaires}

\begin{definition}
Soit $(S)$ un système $n\times p$. On appelle \textbf{opération élémentaire} l'une des trois opérations suivantes :
 \begin{itemize}
     \item $L_i \leftrightarrow L_j$ : \textbf{échange} de la $i^\textrm{ième}$ ligne $L_i$ et  de la $j^\textrm{ième}$ ligne $L_j$.
     \item $L_i \leftarrow aL_i$ où $a\neq 0$ : on \textbf{remplace} la $i^\textrm{ième}$ ligne par elle même \textbf{multipliée} par un nombre non nul $a$.
     \\\textit{Utilité} : lorsqu'on  a des fractions dans la ligne $L_i$, cela permet d'enlever les dénominateurs.
     \item $L_i \leftarrow L_i+bL_j$ où $b$ est quelconque : on \textbf{remplace} la $i^\textrm{ième}$ ligne par la \textbf{somme} d'elle même et d'un multiple d'une autre ligne.\\
     \textit{Utilité} : permet d'éliminer une inconnue.
 \end{itemize}
\end{definition}

\begin{remarque}
En combinant la deuxième et la troisième opérations élémentaires, on obtient l'opération \\$L_i \leftarrow aL_i+bL_j$ où $a$ est non nul, et $b$ est quelconque.
\end{remarque}

\begin{theoreme}
Tout système obtenu à partir d'un système $(S)$ en transformant l'une de ses équations par une opération élémentaire est équivalent à $(S)$, et a donc le même ensemble de solutions.
\end{theoreme}

\begin{demonstration}
  Il faut démontrer, pour chacune des opérations, qu'une solution de l'un est solution de l'autre, ce qui se fait en traitant les 3 cas.
\end{demonstration}

\begin{remarque}
Ainsi, en combinant différentes opérations élémentaires, on ne change pas l'ensemble de solutions du système.
\end{remarque}

On a enfin une propriété qui en découle :

\begin{consequence}
Si un système possède deux lignes identiques, il est équivalent au système où on enlève une de ces deux lignes.
\end{consequence}


   \subsection{Pivot de Gauss-Jordan}

\begin{methode}
\label{objectif-09-2}
En utilisant les opérations élémentaires comme dans l'exemple, on va résoudre un système $(S)$ par la méthode du pivot de Gauss-Jordan :
\begin{enumerate}
   \item On élimine successivement des inconnues via les opérations élémentaires, pour transformer le système initial en un système triangulaire;
   \item On résout le système triangulaire par substitutions.
\end{enumerate}
\end{methode}

\begin{exemple}
\label{ex-chap9-multi}
Résoudre les systèmes suivants par la méthode du pivot de Gauss. Le nombre de solutions est indiqué.
\begin{itemize}
   \item (Une unique solution)
   \[ (S_1)\quad \systeme{x+2y+3z=2@(L_1),3x+y+2z=1@(L_2),2x+3y+z=0@(L_3)} \]
   \item (Une infinité de solution)
   \[ (S_2)\quad \systeme[xyzt]{-y+2z+3t=0@(L_1),2x+2y-z=0@(L_2),3x-y+2z-2t=0@(L_3),5x+y+z-2t=0@(L_4)} \]
   \item (Aucune solution)
   \[ (S_3)\quad \systeme[xyzt]{-y+2z+3t=0@(L_1),2x+2y-z=0@(L_2),3x-y+2z-2t=0@(L_3),5x+y+z-2t=1@(L_4)} \]
\end{itemize}
\end{exemple}


\solution[42]{On utilise la méthode du Pivot de Gauss, en n'oubliant pas d'indiquer les opérations effectuées.
	\begin{itemize}
		\item On applique les opérations élémentaires pour obtenir un système triangulaire :
	\begin{align*}
	(S_1)\quad &\sim \systeme{x+2y+3z=2@L_1\text{ ligne pivot}	,-5y-7z=-5@L_2\leftarrow L_2-3L_1,-y-5z=-4@L_3\leftarrow L_3-2L_1}\\
	&\sim \systeme{x+2y+3z=2@L_1,-5y-7z=-5@L_2\text{ ligne pivot},-18z=-15@L_3\leftarrow 5L_3-L_2}
	\end{align*}
	Le système est triangulaire avec tous ses pivots non nuls : on remonte celui-ci pour trouver une unique solution.
	\[ (S_1)\quad \sim \systeme*{x=-\frac16, y=-\frac16,z=\frac56}\]
	Ainsi, \[\boxed{\mathcal{S}=\left \{ \left(-\frac{1}{6}  ;-\frac{1}{6}  ;\frac{5}{6}  \right) \right\}}\]
	\item On n'hésite pas à échanger des lignes pour avoir une ligne pivot pratique.
	\begin{align*}
	(S_2)\quad &\sim \systeme[xyzt]{2x+2y-z=0@L_1\leftrightarrow L_2,-y+2z+3t=0,3x-y+2z-2t=0,5x+y+z-2t=0} \\
	&\sim \systeme[xyzt]{2x+2y-z=0@L_1\text{ ligne pivot}, -y+2z+3t=0@L_2,-8y+7z-4t=0@L_3\leftarrow 2L_3-3L_1,-8y+7z-4t=0@L_4\leftarrow 2L_4-5L_1}
	\end{align*}
	Les deux dernières lignes sont les mêmes. On en élimine une, et on continue la méthode du pivot de Gauss. On peut ajouter une ligne pour rappeler l'inconnue auxiliaire (ici, la ligne $L_4$), mais ce n'est pas nécessaire :
	\begin{align*}
	(S_2) \quad &\sim \systeme[xyzt]{2x+2y-z=0@L_1,-y+2z+3t=0@L_2\text{ ligne pivot},-9z-28t=0@L_3\leftarrow L_3-8L_2,t=t@L_4}
	\end{align*}
On obtient un système triangulaire, qu'on résout en utilisant une variable auxiliaire (par exemple ici, $t$) : \[ (S_2)\quad \sim \systeme*{x=\frac{15}{9}t, y=-\frac{29}{9}t,z=-\frac{28}{9}t, t=t}, \quad t\in \R\]
	Ainsi, \[\boxed{\mathcal{S}=\left \{ \left(\frac{5}{3}t  ;-\frac{29}{9}t  ;-\frac{28}{9}t; t  \right),~t \in \R \right\}}\]
	\item Le système est le même que précédemment, excepté la dernière ligne. Par les deux mêmes opérations, on obtient :
	\begin{align*}
	(S_3) \quad &\sim \systeme[xyzt]{2x+2y-z=0@L_1\leftrightarrow L_2, -y+2z+3t=0,3x-y+2z-2t=0,5x+y+z-2t=1} \\
	&\sim \systeme[xyzt]{2x+2y-z=0@L_1\text{ ligne pivot},-y+2z+3t=0@L_2, -8y+7z-4t=0@L_3\leftarrow 2L_3-3L_1,-8y+7z-4t=2@L_4\leftarrow 2L_4-5L_1}
	\end{align*}
Les deux dernières lignes étant incompatibles, le système est incompatible. Ainsi, \[\boxed{\mathcal{S}=\vide }\]
	\end{itemize}
}

\begin{remarque}
Un système linéaire admet :
\begin{itemize}
   \item soit aucune solution (il est donc incompatible);
   \item soit une unique solution;
   \item soit une infinité de solutions (quand il y a une (ou des) inconnues auxiliaires)
\end{itemize}
\end{remarque}

	\subsection{Rang d'un système}

\begin{definition}
\label{objectif-09-4}
Soit $(S)$ un système. On appelle \textbf{rang} d'un système, que l'on note $\rg(S)$, le nombre de pivot non nul qu'on obtient après avoir appliqué la méthode du pivot de Gauss, ou encore le nombre d'équations non nulles.
\end{definition}

\begin{exemple}
Soit $(S)$ le système \systeme{x+2y+z=0, 2x-y-z=0, -x+3y+2z=0}. Appliquons la méthode du pivot :
\begin{align*}
(S) \quad &\sim \systeme{x+2y+z=0 @ L_1\text{ ligne pivot}, -5y-3z=0@L_2\leftarrow L_2-2L_1,5y+3z=0@L_3\leftarrow L_3+L_1}	\\
&\sim \systeme{x+2y+z=0@L_1, -5y-3z=0@L_2\text{ ligne pivot}, 0z=0@L_3\leftarrow L_3+L_2}
\end{align*}
Le système est triangulaire, et il y a $2$ pivots non nuls (ou $2$ équations non nulles). Ainsi, le rang de $(S)$ est de $2$.
\end{exemple}


\section{Systèmes de Cramer}

   \subsection{Définition}

\begin{definition}
Un système carré d'ordre $n$ est dit \textbf{de Cramer} s'il possède une unique $n$-liste solution.
\end{definition}

\begin{consequence}
Un système homogène $(S)$ de $n$ équations linéaires à $n$ inconnues est un système de Cramer si son unique solution est la $n$-liste $(0,0,\hdots, 0)$.
\end{consequence}

   \subsection{Systèmes de Cramer et pivot de Gauss}

\begin{theoreme}
Un système $(S)$ carré d'ordre $n$ est de Cramer si et seulement si la méthode du pivot de Gauss fait apparaitre $n$ pivots successifs non-nuls.
\end{theoreme}

\begin{exemple}
Montrer que le système suivant est de Cramer
\[ (S)\quad \systeme{x+y-2z=0,2x-y+z=0,2x+y-2z=0} \]
\end{exemple}


\solution[8]{En appliquant la méthode du pivot de Gauss :
\begin{align*}
(S)\quad &\sim \systeme{x+y-2z=0,-3y+5z=0@L_2\leftarrow L_2-2L_1,-y+2z=0@L_3\leftarrow L_3-2L_1} \\
 &\sim \systeme{x+y-2z=0,-y+2z=0@L_3\leftrightarrow L_2,-3y+5z=0}\\
 &\sim \systeme{x+y-2z=0,-y+2z=0,-z=0@L_3\leftarrow L_3-3L_2}
\end{align*}
On a ainsi fait apparaitre les pivots $1, -1$ et $-1$ qui sont tous les trois non nuls : le système $(S)$ est bien de Cramer.
}

On peut ré-écrire le théorème précédent avec la notion de rang :

\begin{theoreme}
Un système carré d'ordre $n$ (avec $n\geq 1$) est de Cramer si et seulement si $\rg(n)=n$.
\end{theoreme}

   \subsection{Système de Cramer et système homogène associé}

\begin{theoreme}
Un système $(S)$ est de Cramer si et seulement si son système homogène associé est aussi de Cramer.
\end{theoreme}

\begin{demonstration}
En effet, le choix des pivots dans la méthode des pivots de Gauss ne dépend pas du second membre.
\end{demonstration}

\begin{methode}
Pour montrer qu'un système quelconque est de Cramer, il suffit donc de montrer que son système homogène associé l'est, ce qui est plus simple.
\end{methode}
