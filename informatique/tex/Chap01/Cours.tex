\chapter{Algorithmique : instructions et structures}
%!TeX root=../../encours.nouveau.tex

%%%%%%%%%%%%%%%%%%%
%%%% Objectif  %%%%
%%%%%%%%%%%%%%%%%%%
\objectifintro{Dans ce chapitre, on introduit les concepts essentiels d'algorithmique, pour certains déjà vus dans les années précédentes, et que nous appliquerons en TP cette année.
}


%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Donald Knuth (1938 -- )}
An algorithm must be seen to be believed.
\end{extrait}

\newpage



\section{Programme et complexité}

	\subsection{Définition de l'algorithmique}

\begin{definition}
 On appelle \emph{algorithme} une suite finie d'opérations élémentaires permettant d'effectuer un calcul ou de résoudre un problème de manière automatique.

 On appelle \emph{algorithmique} l'ensemble des règles et techniques régissant et amenant à la conception d'algorithme.
 \end{definition}

 \begin{remarque}
 Cette définition est très générale et ne s'applique pas qu'au domaine mathématique ou scientifique, ainsi on peut donner comme exemples d'algorithmes :
 \begin{itemize}
 	\item la méthode de résolution d'une équation du second degré;
 	\item la méthode de calcul de l'écriture en base $2$ d'un entier naturel;
 	\item la méthode de tracé d'un hexagone à l'aide d'un compas;
 \end{itemize}
 mais aussi
 \begin{itemize}
 	\item toute recette de cuisine;
 	\item l'indication d'un chemin par un GPS...
 \end{itemize}
 \end{remarque}

 \begin{exo}
 	Décrire toutes les opérations élémentaires possibles à l'aide d'une règle non graduée, d'un compas et d'un crayon.

 	Décrire un algorithme permettant de couper un segment $[AB]$ en deux segments de longueur égale.
 \end{exo}

 \lignes{8}

 	\subsection{Conception et programmation d'un algorithme}

 		\subsubsection{Structure d'un algorithme et conception}

 	On peut découper la conception d'un algorithme comme suit :
 	\begin{itemize}
 		\item \textbf{Déterminer les entrées de l'algorithme}, autrement dit, lister les données (ou ingrédients !) de l'algorithme;
 		\item \textbf{Préciser les sorties de l'algorithme},  c'est-à-dire la nature et la forme du ou des résultats de l'algorithme;
 		\item \textbf{Détailler la séquence d'instructions constituant l'algorithme}. C'est évidemment le coeur du travail. On pratiquera pour ce faire l'\emph{analyse descendante} du problème, c'est-à-dire que l'on décomposera la tâche à effectuer en sous-tâches, que l'on découpera à nouveau et ainsi de suite, jusqu'à aboutir à des opérations élémentaires.
 	\end{itemize}

 \begin{exemple}
 	Détaillons la méthode de résolution d'une équation du second degré sur $\mathbb{R}$.
 	\begin{itemize}
 		\item \emph{Données d'entrée de l'algorithme} : on prendra en entrée les coefficients $a, b, c$ réels de l'équation $ax^2+bx+c=0$ à résoudre.
 		\item \emph{Sorties de l'algorithme} : on affichera la ou les solutions. On affichera un message s'il n'y a pas de solution réelle.
 		\item \emph{Séquence d'instructions} : on a à résoudre les sous-tâches suivantes :
 		\begin{itemize}
 			\item Tester si $a=0$;
 			\item Calculer le discriminant $\Delta$;
 			\item Calculer la ou les solutions et les afficher, suivant la valeur du discriminant. Cette tâche peut se décomposer ainsi :
 			\begin{itemize}
 				\item Si $\Delta<0$ : afficher ``pas de solution'';
 				\item Si $\Delta=0$ : afficher $-\frac{b}{2a}$;
 				\item Si $\Delta>0$ : afficher $\frac{-b-\sqrt{\Delta}}{2a}$ et $\frac{-b+\sqrt{\Delta}}{2a}$.
 			\end{itemize}
 		\end{itemize}
 	\end{itemize}
 	On obtient alors l'algorithme suivant :
 	\begin{figure}[H]
 	  \centering
 	\fbox{
 	\begin{minipage}{10cm}
 	\begin{algorithm}[H]
 	  \SetKwInput{KwData}{Entrée} \SetKwInput{KwResult}{Sortie}
 		\SetKwIF{If}{ElseIf}{Else}{Si}{alors }{sinon si }{sinon }{fin~si}%
 	 \KwData{$a,b,c$ : réels}
 	 \KwResult{Affiche le résultat}
 	 \uIf{$a=0$}{Afficher "L'équation n'est pas du second degré}
 	 \Else{ $\Delta \leftarrow b^2-4ac$\\
 	 \uIf{$\Delta=0$}{Afficher "Unique solution : $-\frac{b}{2a}$"}
 	 \uElseIf{$\Delta<0$}{Afficher "Aucune solution réelle"}
 	 \Else{Afficher "Deux solutions :  $\frac{-b-\sqrt{\Delta}}{2a}$ et $\frac{-b+\sqrt{\Delta}}{2a}$ "}
 	 }
 	\end{algorithm}
 	\end{minipage}}
 	\caption{Résolution d'une équation du second degré}\label{algo:resolution_equation_second_degre}
 	\end{figure}
 \end{exemple}

 \begin{remarque}
 	On dit que l'on écrit un \emph{pseudo-code}, ou en \emph{pseudo-langage}, quand on écrit sous cette forme.
 \end{remarque}

 		\subsubsection{Programmer un algorithme}

 \begin{definition}
 	On appelle \textbf{programmation} d'un algorithme le fait de traduire la description de l'algorithme en pseudo-code dans un langage de programmation.
 \end{definition}

 \begin{exemple}
 	On peut programmer la résolution de l'équation du second degré en python comme suit :
 \begin{sanscadrepython}
 print("Entrer a, b et c")
 a = float(input())
 b = float(input())
 c = float(input())
 if a==0:
   print("L'équation n'est pas du second degré")
 else:
   Delta = b**2-4*a*c
   if Delta<0:
     print("Pas de solutions")
   if Delta==0:
     print("Une unique solution : " + str(-b/(2*a)))
   if Delta>0:
     print("Deux solutions : " + str((-b-sqrt(Delta))/(2*a))
              + "et" + str((-b+sqrt(Delta))/(2*a)))
 \end{sanscadrepython}
 \end{exemple}

 	\subsection{Efficacité et complexité d'un algorithme}

 On dira qu'un algorithme est efficace si
 \begin{itemize}
 	\item il effectue peu de calculs et d'opérations lors de son exécution,
 	\item il utilise peu de place mémoire lors de son exécution.
 \end{itemize}

 Les deux points sont importants. La difficulté est bien souvenir de parvenir à optimiser les deux aspects à la fois.

 Pour étudier l'efficacité d'un algorithme, on introduit la notion de complexité :

 \begin{definition}
 	On appelle \textbf{complexités} d'un algorithme le nombre d'opérations (comparaisons, sommes, produits) élémentaires effectuées par un algorithme, ainsi que la taille mémoire utilisée par l'algorithme, données en fonction des variables d'entrées de l'algorithme.
 \end{definition}

 \begin{notation}
 	On note $C(n)=O(f(n))$ si la suite $\left(\frac{C(n)}{f(n)}\right)$ est bornée. Globalement, la complexité est en $O(f(n))$ si elle est majorée par (une constante fois) $f(n)$, pour $n$ assez grand. Cette notation mathématique vous sera présentée plus tard dans l'année.\\Vous verrez l'année prochaine que l'on peut trier une liste de $n$ nombres avec une complexité en $O(n\ln(n))$
 \end{notation}
%
 \begin{exemple}
 	Prenons l'algorithme de division euclidienne naïf :
 	\begin{figure}[H]
 	  \centering
 	\fbox{
 	\begin{minipage}{10cm}
 	\begin{algorithm}[H]
 	  \SetKwInput{KwData}{Entrées} \SetKwInput{KwResult}{Sortie}
 		\SetKwFor{While}{Tant que}{faire}{Fin~tant~que}
 	 \KwData{$a,b$ : entiers, $b\neq 0$}
 	 \KwResult{$(q,r)$ : $q$ le quotient, et $r$ le reste}
 	 $r \leftarrow a$\\$q\leftarrow 0$\\
 	 \While{$r\geq b$}{$r\leftarrow r-b$\\$q\leftarrow q+1$}
 	\end{algorithm}
 	\end{minipage}}
 	\caption{Division euclidienne naïve}\label{algo:division_euclidienne_naive}
 	\end{figure}
 \ifprof
 On effectue $2$ affectations puis $2 \times$ le nombre de boucles affectations, et $2\times$ le nombre de boucles sommes. Le nombre de passage dans boucle \emph{Tant que} est inconnu a priori, mais est majorée par $a/b$. Ainsi, on a une majoration par $2+4\times \frac{a}{b}$ opérations élementaires, c'est-à-dire une complexité en $O\left(\frac{a}{b}\right)$.\\Pour ce qui est de la mémoire, on n'a besoin que de stocker deux variables $r$ et $q$, tout deux majorés par $\max(a,b)$. Ainsi, on a une complexité en $O(\max(a,b))$.
 \else
 \lignes{8}
 \fi
 \end{exemple}

 \begin{remarque}
 	L'un des buts en algorithmique est généralement de déterminer des algorithmes en complexité polynomiale ou quasi polynomiale, ce qui n'est pas toujours possible, même pour des outils d'utilisation courante.  L'exemple le plus connu actuellement est la recherche d'un facteur premier d'un nombre entier.

 \end{remarque}

 On peut donner le tableau suivant pour fixer les idées :

 \vspace{0.25cm}
 \hspace{-1.25cm}
 \begin{tabular}{|c|c|p{3.2cm}|c|}
 \hline
 Complexité & Nom Courant & Temps pour $n = 10^6$ & Remarques\\
 \hline
 $O(1)$ & Temps Constant & 1 ns & Indépendant de $n$ :\\&&&rarissime\\\hline
 $O(\ln n)$ & Logarithmique & 10 ns & Très souvent en $\log_2(n)$ \\\hline
 $O(n)$ & Linéaire & 1 ms & Dans ce cas, le contrôle de la mémoire sera privilégié\\\hline
 $O(n^2)$ & Quadratique & 1/4 heure & Reste encore acceptable\\\hline
 $O(n^k)$ & Polynomiale & 10 ans si $k = 3$... & On voit souvent des complexité en $O(n^3)$ ou $O(n^4)$\\\hline
 $O(2^n)$ & Exponentielle & Plus de $10^{300000}$ milliards d'années & Impraticable sauf pour très petites données\\
 \hline
 \end{tabular}

 \begin{remarque}
 	Certains algorithmes peuvent être extrémement rapides pour certaines données et très lents pour d'autres. On donne souvent la complexité \textbf{dans le pire cas}, qui majorera le nombre d'opérations mais il peut être intéressant de donner également la complexité au meilleur des cas, ou même une complexité \textbf{moyenne} - mais il s'agit d'une notion plus délicate à définir.
 \end{remarque}

 \section{Instructions et opérations essentielles}

 	Détaillons les structures minimales d'un algorithme.

 	\subsection{Affectation et déclaration}

 		\subsubsection{Affectation}

 L'outil de base en algorithmique est la \textbf{variable}, une ``boite'' permettant de stocker une valeur ou un objet plus complexe. On utilisera la syntaxe :

 \begin{center}\begin{tabular}{c|c}
 pseudo-code & \textsc{python} \\\hline
 $a \leftarrow x$ & \pythoninline{a = x}
 \end{tabular}\end{center}

 \begin{remarque}
 	Dans le langage \textsc{python}, au moment où une valeur est donnée à $a$, \textsc{python} lui colle également une étiquette correspondant au type de valeur donnée : c'est la \textbf{déclaration}.
 \end{remarque}

 \begin{exemple}
 	L'instruction \pythoninline{a = 2} donne la valeur $2$ à \pythoninline{a}. Pour \textsc{python}, \pythoninline{a} sera déclarée comme une variable à valeur entière.
 \end{exemple}

 		\subsubsection{Entrée au clavier}

 Pour que la valeur affectée à \pythoninline{a} soit rentrée au clavier au moment de l'exécution du programme, on utilisera :

 \begin{center}\begin{tabular}{c|c}
 pseudo-code & \textsc{python} \\\hline
 $a \leftarrow ?$ & \pythoninline{a = input()}
 \end{tabular}\end{center}

 \begin{remarque} Attention : lorsqu'on rentre, via \pythoninline{input()}, des données, par défaut, le type de la variable sera une chaîne de caractère. Ainsi, si on entre $11$, la variable \pythoninline{a} contiendra la chaîne de caractères \pythoninline{'11'}. Pour forcer le type de la variable, on indique le type voulu. Par exemple :
 \begin{center}\pythoninline{a = int(input())}\end{center}
 	force à convertir la chaîne de caractère en entier. Ainsi, dans ce cas, la variable a contiendra bien la valeur entière $11$.
 	\end{remarque}

 	\subsection{Affichage}

 Pour la \textbf{sortie} d'un programme, on utilisera pour l'instant la commande \pythoninline{print} pour afficher les résultats souhaités :

 \begin{center}\begin{tabular}{c|c}
 pseudo-code & \textsc{python} \\\hline
 Afficher $a$ & \pythoninline{print(a)}
 \end{tabular}\end{center}

 \begin{remarque}
 	Les parenthèses autour de la variable sont obligatoires. On peut afficher du texte également, par exemple \pythoninline{'Coucou'} affichera la chaîne de caractère ``Coucou''. Pour afficher texte et variable, on convertira la variable en chaîne de caractères, avec l'instruction \pythoninline{str}; ainsi, l'instruction
 	\begin{center}\pythoninline{print('La valeur de a est ' + str(a))}\end{center}
 		affichera la chaîne de caractère ``La valeur de a est 11''.
 \end{remarque}

 	\subsection{Instruction conditionnelle}

 On peut demander à l'ordinateur de ne réaliser une séquence d'opérations que si une condition est vérifiée. On utilisera la structure :

 \begin{center}\begin{tabular}{p{5cm}|p{5cm}}
 \centering{pseudo-code} & ~~~~~~~\textsc{python} \\\hline
 Si \textit{condition} alors &
 \pythoninline{if condition:}\\Faire  &
 ~~~~\pythoninline{operations}\\ | ~~~Opérations &
 ~~~~\pythoninline{operations}
 \end{tabular}\end{center}

 Attention : il est essentiel, en \textsc{python}, d'\textbf{indenter}, c'est-à-dire de mettre une tabulation après les \textbf{:}. C'est ainsi que \textsc{python} se repère : si la condition est vérifiée, il exécutera \underline{toutes} les instructions indentées; sinon il n'en exécutera aucune.

 \begin{exemple}
 Un premier exemple :
 \begin{sanscadrepython}
 a = int(input())
 if a%2 == 0:
     print('Le nombre est pair')
 \end{sanscadrepython}
 L'instruction \texttt{a\%2} renvoie le reste de la division euclidienne de \pythoninline{a} par $2$.
 \end{exemple}

 On pourrait ajouter une deuxième condition pour traiter tous les cas :
 \begin{sanscadrepython}
 if a%2 != 0:
     print('Le nombre est impair')
 \end{sanscadrepython}

 mais on préférera la structure \pythoninline{if ... else ...} :

 \begin{center}\begin{tabular}{p{5cm}|p{5cm}}
 \centering{pseudo-code} & ~~~~~~~\textsc{python} \\\hline
 Si \textit{condition} alors &
 \pythoninline{if condition:}\\Faire  &
 ~~~~\pythoninline{operations}\\ | ~~~Opérations &
 ~~~~\pythoninline{operations}\\
 Sinon faire & \pythoninline{else:}\\
 | ~~~Opérations & ~~~~\pythoninline{operations}\\
 & ~~~~\pythoninline{operations}
 \end{tabular}\end{center}

 On peut également imbriquer des tests les uns dans les autres.

 \begin{exemple}
 Un exemple naïf :
 \begin{sanscadrepython}
 a = int(input())
 if a >= 0:
   if a == 0:
     print('a est nul')
   else:
     print('a strictement positif')
 else:
   print('a est strictement négatif')
 \end{sanscadrepython}
 \end{exemple}

 \subsection{Boucles conditionnelles}

 Plutôt que de préciser les tests à chaque itération, on peut avoir envie de répéter une séquence d'opérations tant qu'une condition est vérifiée.

 Pour cela, on utilise la structure \texttt{Tant que}.

 \begin{center}\begin{tabular}{p{5cm}|p{5cm}}
 \centering{pseudo-code} & ~~~~~~~\textsc{python} \\\hline
 Tant que \textit{condition} faire : &
 \pythoninline{while condition:}\\ | ~~~Opérations &
 ~~~~\pythoninline{operations} \\
 | ~~~Opérations & ~~~~\pythoninline{operations}
 \end{tabular}\end{center}

 On n'oubliera pas l'identation en \textsc{python}.

 \begin{exemple}
 Prenons ce programme :
 \begin{sanscadrepython}
 a = int(input())
 s = 0
 i = 1
 while s<a:
   s = s+i
   i = i+1
 print(i-1)
 \end{sanscadrepython}
 et choisissons le cas $a=7$. Détaillons les valeurs à la fin de chaque passage dans la boucle :
 \begin{enumerate}
 	\item $s=1$ et $i=2$.
 	\item $s=3$ et $i=3$.
 	\item $s=6$ et $i=4$.
 	\item $s=10$ et $i=5$.
 \end{enumerate}
 $s$ dépassant $a$, l'algorithme s'arrête. On afficher $4$, c'est-à-dire le nombre de passage dans la boucle.
 \end{exemple}

 \begin{remarque}
 	L'important, dans l'utilisation d'une telle structure, est de s'assurer que la condition de sortie sera atteinte.

 	Ici, puisque $i\geq 1$, la suite des valeurs de $s$ est strictement croissante. Il est donc certain que la condition sera vérifiée.
 \end{remarque}

 \begin{definition}
 Pour montrer qu'une méthode ou un algorithme, avec une boucle,  est correcte, on cherchera à identifier un \textbf{invariant de boucle}, c'est-à-dire une propriété sur les paramètres qui sera vraie à chaque passage dans la boucle et qui garantit, également, que la boucle s'arrêtera.
 \end{definition}

 \begin{exemple}
 	Prenons l'exemple de l'algorithme de division euclidienne vu précédemment.
 \begin{sanscadrepython}
 a = int(input())
 b = int(input())
 r = a
 q = 0
 while r>=b:
   r = r-b
   q = q+1
 print 'Quotient : ' + str(q)
 print 'Reste :' + str(r)
 \end{sanscadrepython}

 \ifprof
 Au départ, (à la $0$e boucle), on a $a=q_0\times b + r_0$ (en notant $r_0$ et $q_0$ les valeurs après la $0$e boucle) puisque $q_0=0$ et $r_0=a$.

 A chaque passage dans la boucle, on a $r_{n+1}=r_n-b$ et $q_{n+1}=q_n+1$ et donc
 \[ q_{n+1}\times b + r_{n+1} = (q_n+1)\times b + (r_n-b)= q_n\times b + r_n = a \]
 en raisonnant par récurrence. Ainsi, la propriété est vraie pour tout $n$. Et puisque la suite $(r_n)$ est strictement décroissante, la propriété de sortie sera vérifiée avec $r_N<b$ : l'algorithme est correct, et termine.
 \else
 \lignes{10}
 \fi
 \end{exemple}

 	\subsection{Boucles inconditionnelles}

 Si l'on souhaite répéter simplement un nombre fixé de fois une séquence d'opérations, on utilise la structure \texttt{Pour}.

 \begin{center}\begin{tabular}{p{5cm}|p{5cm}}
 \centering{pseudo-code} & ~~~~~~~\textsc{python} \\\hline
 Pour \textit{i allant de 1 à n} faire : &
 \pythoninline{for i in range(1,n+1):}\\ | ~~~Opérations &
 ~~~~\pythoninline{operations} \\
 | ~~~Opérations & ~~~~\pythoninline{operations}
 \end{tabular}\end{center}

 \begin{remarque}
  \begin{itemize}
 	\item A chaque exécution de la séquence, $i$ change de valeur : $1$, puis $2$, puis $3$, jusqu'à $n$.
 	\item On peut aussi ne pas aller de $1$ en $1$ avec \pythoninline{range}. Ainsi, l'instruction \pythoninline{range(n,m,p)} va de $n$ (inclus) à $m$ exclus, de $p$ en $p$.
 \end{itemize}
 \end{remarque}

\begin{attention}
En \textsc{python}, l'instruction \pythoninline{range(n,m)} parcourt toutes les valeurs de $n$ inclus à $m$ \textit{exclus} (et donc jusqu'à $m-1$).
\end{attention}

 \begin{exemple}
 Par exemple, le programme suivant affiche la somme des $1000$ premiers entiers naturels impairs.
 \begin{sanscadrepython}
 n = 1000
 s = 0
 for i in range(1,n+1):
   s = s + 2*(i-1) + 1
 print(s)
 \end{sanscadrepython}
 \end{exemple}

 \begin{exo}
 	Ecrire le même programme en utilisant un \pythoninline{range} de deux en deux.
 \end{exo}

\solution[7]{
%TODO
}

 \begin{remarque}[Sortie forcée] L'instruction \pythoninline{break} permet de sortir de la boucle en cours d'exécution même si le parcours n'est pas terminé. Par exemple, le programme
 \begin{sanscadrepython}
 while 0 < 1:
   print('passage')
   break
 \end{sanscadrepython}
 ne s'exécutera qu'une fois.
 \end{remarque}

 \section{Fonctions}

 	\subsection{Définition}
 On a vu que pour construire un algorithme, on procède par analyse descendante, décomposant la tâche à effectuer en sous-tâches plus simples.

 Il arrive souvent que plusieurs de ces sous-tâches soient de même nature. Plutôt que de réécrire toutes ces sous-tâches, on définira des \textbf{fonctions}, sous-programmes de nos programmes auxquels on pourra faire appel autant de fois que nécessaire.

 On utilisera, en \textsc{python}, la syntaxe suivante :
 \begin{sanscadrepython}
 def NomDeFonction(variable1,variable2,...):
   opérations de la fonction
 \end{sanscadrepython}

 On souhaitera souvent que notre fonction ait un résultat obtenu après son exécution, plutôt que de l'afficher. Pour cela, on aura recourt à l'instruction \pythoninline{return resultat}. Cela fait aussitôt sortir de la fonction, avec le résultat donné.

 \begin{remarque}
 	Dans la (très grande) majorité des cas, on évitera les recours à \pythoninline{input} et \pythoninline{print}, et on utilisera préférentiellement les fonctions.
 \end{remarque}

 \begin{exemple}
 	On souhaite calculer les coordonnées l'isobarycentre de quatre points données par leurs coordonnées. Pour cela, on va utiliser la fonction suivante :
 \begin{sanscadrepython}
 def milieu(abs1, ord1, abs2, ord2):
   return ( (abs1+abs2)/2, (ord1+ord2)/2 )
 \end{sanscadrepython}
 On obtient alors le programme :
 \begin{sanscadrepython}
 xA = int(input())
 xB = int(input())
 xC = int(input())
 xD = int(input())
 yA = int(input())
 yB = int(input())
 yC = int(input())
 yD = int(input())
 (absm1,ordm1) = milieu(xA,yA,xB,yB)
 (absm2,ordm2) = milieu(xC,yC,xD,yD)
 print("L'isobarycentre a pour coordonnées : " + str(milieu(absm1,ordm1,absm2,ordm2)))
 \end{sanscadrepython}
 \end{exemple}

 \begin{remarque}
 	La demande des coordonnées est fastidieuse, on verra dans le prochain chapitre que l'on peut faire plus concis. La fonction \pythoninline{milieu} renvoie ici un résultat.
 \end{remarque}

 	\subsection{Variables locales, variables globales}

 \begin{definition}
 	On appelle \textbf{variable locale} une variable manipulée dans une fonction. On appelle \textbf{variable globale} une variable ayant une valeur et un sens dans tout le programme (y compris les fonctions).
 \end{definition}

 \begin{attention}
	  Les variables locales n'ont d'existence que dans la fonction où elles apparaissent.
\end{attention}

 Pour préciser qu'une variable est globale, on utilise le mot-clé \pythoninline{global}.

 \begin{exemple}
 Voici trois fois le même exemple avec des nuances, pour comprendre les différences :

 \begin{sanscadrepython}
	 ''' Cas 1 '''
 a = 2
 def Aug():
   b=1
   a=a+b
 Aug()
 print(a)
\end{sanscadrepython}

 Dans ce cas, le programme renvoie une erreur. En effet, dans la fonction \pythoninline{Aug()}, $a$ n'a pas de valeur initiale (car variable local). L'exécution de \pythoninline{Aug()} donnera une erreur.

 \begin{sanscadrepython}
''' Cas 2 '''
 a = 2
 def Aug():
   global a
   b=1
   a=a+b
 Aug()
 print(a)
 \end{sanscadrepython}

 Ce progrmame fonctionne, et affichera $3$. En effet, l'instruction \pythoninline{global a} permet d'indiquer qu'on prend la variable globale $a$, et donc on récupère sa valeur et on peut la manipuler.

 \begin{sanscadrepython}
''' Cas 3 '''
 a = 2
 def Aug():
   global a
   b=1
   a=a+b
 Aug()
 print(b)
 \end{sanscadrepython}

 Ici, cela envoie une erreur à \pythoninline{print(b)} car la variable $b$ n'a d'existence que dans la fonction \pythoninline{Aug()}.
 \end{exemple}

 \begin{remarque}
 	On peut faire appel à une fonction à l'intérieur d'une autre, comme dans :
 \begin{sanscadrepython}
 def f(x):
   return x**2+1

 def g(x):
   return f(x)**2
 \end{sanscadrepython}

 Une remarque dans ce cas : si plus loin dans le programme, on change la fonction \pythoninline{f}, alors le changment sera également effectif pour \pythoninline{g}
 \end{remarque}

 \begin{remarque}
 	Une fonction peut être un argument d'une autre fonction. Par exemple :
 \begin{sanscadrepython}
 def f(x):
   return x**2

 def g(x):
   return x**3

 def somme(f,n):
   s=0
   for i in range(0,n):
     s=s+f(i)
   return
 \end{sanscadrepython}

 L'instruction \pythoninline{somme(f,100)} renvoie la somme des $100$ premiers carrés, alors que l'instruction \pythoninline{somme(g,100)} renverra la somme des $100$ premiers cubes.
 \end{remarque}
